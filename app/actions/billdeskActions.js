import {baseUrl, auth} from '../services/index';

export function getStrData(data) {
  let formBody = new FormData();
  formBody.append('vpc_OrderInfo', data.vpc_OrderInfo);
  formBody.append('Amount', data.Amount);
  formBody.append('Convcharge', data.Convcharge);
  formBody.append('vpc_Amount', data.vpc_Amount);
  console.warn(formBody);
  return dispatch => {
    fetch(baseUrl + '/static/dirpay2/paynowbyccdir', {
      method: 'POST',
      headers: {
        Authentication: auth,
      },
      body: formBody,
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn(JSON.stringify(responseData, undefined, 2));
        dispatch({type: 'GET_STR', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        console.warn(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function emptyPaymentData() {
  return dispatch => {
    dispatch({type: 'GET_STR', responseData: {}});
  };
}
