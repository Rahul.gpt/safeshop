import {baseUrl, auth} from '../services/index';

export function getQueryData() {
  return dispatch => {
    fetch(baseUrl + '/static/queries', {
      method: 'POST',
      headers: {
        Authentication: auth,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn(responseData);
        dispatch({type: 'GET_QUERY_DATA', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function submitQuery(data) {
  let formBody = new FormData();
  formBody.append('V_name', data.V_name);
  formBody.append('V_emailid', data.V_emailid);
  formBody.append('V_cellno', data.V_cellno);
  formBody.append('V_msg', data.V_msg);
  return dispatch => {
    fetch(baseUrl + '/static/grievances', {
      method: 'POST',
      headers: {
        Authentication: auth,
      },
      body: formBody,
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'SUBMIT_QUERY', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function emptySubmitQuery() {
  return dispatch => {
    dispatch({type: 'SUBMIT_QUERY', responseData: {}});
  };
}
