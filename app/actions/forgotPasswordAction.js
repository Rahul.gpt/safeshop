import {baseUrl, auth} from '../services/index';

export function forgotPassword(data) {
  console.warn('action', data);
  let formBody = new FormData();
  formBody.append('T_logid', data.email);
  return dispatch => {
    fetch(baseUrl + '/forgotpwd', {
      method: 'POST',
      headers: {
        Authentication: auth,
      },
      body: formBody,
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn(responseData);
        dispatch({type: 'FORGOT_PASSWORD', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function resetPassword(data, token) {
  let formBody = new FormData();
  formBody.append('logid', data.logid);
  formBody.append('V_newpwd', data.newPassword);
  formBody.append('V_newpwd2', data.confirmPassword);
  if (data.oldPassword) {
    formBody.append('V_oldpwd', data.oldPassword);
  }
  return dispatch => {
    fetch(baseUrl + '/savepwd', {
      method: 'POST',
      headers: {
        Authentication: auth,
      },
      body: formBody,
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn(responseData);
        dispatch({type: 'RESET_PASSWORD', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function resetPasswordNew(data, token) {
  let formBody = new FormData();
  formBody.append('V_otp', data.otp);
  formBody.append('V_oldpwd', data.oldPassword);
  formBody.append('V_newpwd', data.newPassword);
  formBody.append('V_newpwd2', data.confirmPassword);
  return dispatch => {
    fetch(baseUrl + '/static/decl', {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
      body: formBody,
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn(responseData);
        dispatch({type: 'RESET_PASSWORD_NEW', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function emptyresetPasswordNew() {
  return dispatch => {
    dispatch({type: 'RESET_PASSWORD_NEW', responseData: {}});
  };
}

export function emptyForgot() {
  return dispatch => {
    dispatch({type: 'EMPTY_FORGOT', value: {}});
  };
}

export function emptyresetPassword() {
  return dispatch => {
    dispatch({type: 'EMPTY_RESET_PASSWORD', value: {}});
  };
}

export function changePassword(data) {
  let formBody = new FormData();
  formBody.append('T_logid', data.loginId);
  return dispatch => {
    fetch(baseUrl + '/forgotpwd', {
      method: 'POST',
      headers: {
        Authentication: auth,
      },
      body: formBody,
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn(responseData);
        dispatch({type: 'CHANGE_PASSWORD', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function getAgreement(token) {
  return dispatch => {
    fetch(baseUrl + '/static/agree', {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.text())
      .then(responseData => {
        console.warn('responseData', responseData);
        dispatch({type: 'GET_AGREEMENT', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        console.warn('err', err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function getDeclaration(token) {
  console.warn(token);
  return dispatch => {
    fetch(baseUrl + '/static/declaration', {
      method: 'GET',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.text())
      .then(responseData => {
        console.warn('responseData', responseData);
        dispatch({type: 'GET_DECLARATION', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        console.warn('err', err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function getTnC(token) {
  return dispatch => {
    fetch(baseUrl + '/static/affid', {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.text())
      .then(responseData => {
        console.warn('responseData', responseData);
        dispatch({type: 'GET_TNC', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        console.warn('err', err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function submitFinalUser(token) {
  return dispatch => {
    fetch(baseUrl + '/static/statusD', {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.text())
      .then(responseData => {
        console.warn('responseData', responseData);
        dispatch({type: 'FINAL_SUBMIT', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        console.warn('err', err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function emptyFinal() {
  return dispatch => {
    dispatch({type: 'FINAL_SUBMIT', responseData: {}});
  };
}
