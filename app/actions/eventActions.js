import {baseUrl, auth} from '../services/index';

export function getEventsList() {
  return dispatch => {
    fetch(baseUrl + '/static/events', {
      method: 'GET',
      headers: {
        Authentication: auth,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'GET_ALL_EVENTS', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}
