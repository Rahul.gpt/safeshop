import {baseUrl, auth} from '../services/index';
import {showMessage, hideMessage} from 'react-native-flash-message';

export function getGenealogy(token) {
  return dispatch => {
    fetch(baseUrl + '/genealogy', {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        //console.warn(JSON.stringify(responseData, undefined, 2));
        dispatch({type: 'GENEALOGY_DATA', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function addMember(data, token) {
  // console.warn(data);
  return dispatch => {
    fetch(baseUrl + '/add/select/' + data.parentId + '/' + data.position, {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn(
          'responseData',
          JSON.stringify(responseData, undefined, 2),
        );
        dispatch({type: 'ADD_AT_NODE', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function emptyGetGenealogy() {
  return dispatch => {
    dispatch({type: 'ADD_AT_NODE', responseData: {}});
  };
}

export function addTeamMemberData(data, token) {
  let formBody = new FormData();
  formBody.append('T_parentid', data.T_parentid);
  formBody.append('T_referid', data.T_referid);
  formBody.append('T_placement', data.T_placement);
  formBody.append('T_logid', data.T_logid);
  formBody.append('T_password', data.T_password);
  formBody.append('T_confirm', data.T_confirm);
  formBody.append('T_name1', data.T_name1);
  formBody.append('T_name2', data.T_name2);
  formBody.append('T_name3', data.T_name3);
  formBody.append('T_father', data.T_father);
  formBody.append('T_email', data.T_email);
  formBody.append('T_addr1', data.T_addr1);
  formBody.append('T_nearby', data.T_nearby);
  formBody.append('T_district', data.T_district);
  formBody.append('T_city', data.T_city);
  formBody.append('T_state', data.T_state);
  formBody.append('T_pincode', data.T_pincode);
  formBody.append('T_telno', data.T_telno);
  formBody.append('T_cellno', data.T_cellno);
  formBody.append('T_dobdd', data.T_dobdd);
  formBody.append('T_dobmm', data.T_dobmm);
  formBody.append('T_dobyy', data.T_dobyy);
  // formBody.append('T_bankname', data.T_bankname);
  // formBody.append('T_bankcity', data.T_bankcity);
  // formBody.append('T_advname', data.T_advname);
  // formBody.append('T_advno', data.T_advno);
  if (data.T_pangir) {
    formBody.append('T_pangir', data.T_pangir);
  }
  if (data.T_acctype) {
    formBody.append('T_acctype', data.T_acctype);
  }
  if (data.T_title) {
    formBody.append('T_title', data.T_title);
  }
  // if (data.T_coappname) {
  //   formBody.append('T_coappname', data.T_coappname);
  // }
  if (data.T_addr2) {
    formBody.append('T_addr2', data.T_addr2);
  }
  // if (data.T_wardno) {
  //   formBody.append('T_wardno', data.T_wardno);
  // }
  // if (data.T_bankcode) {
  //   formBody.append('T_bankcode', data.T_bankcode);
  // }
  // if (data.T_gstno) {
  //   formBody.append('T_gstno', data.T_gstno);
  // }
  // if (data.T_adhaarno) {
  //   formBody.append('T_adhaarno', data.T_adhaarno);
  // }

  return dispatch => {
    fetch(baseUrl + '/add/savemember', {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
      body: formBody,
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn(responseData);
        dispatch({type: 'TEAM_MEMBER_DATA', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function makeChildAsParent(childName, token) {
  return dispatch => {
    fetch(baseUrl + '/genemore/' + childName, {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        //console.warn(responseData);
        dispatch({type: 'GENEALOGY_DATA', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function emptyAddConfirmation() {
  return dispatch => {
    dispatch({type: 'EMPTY_ADD_CONFIRM', value: {}});
  };
}

export function placeMember(data, token) {
  return dispatch => {
    fetch(
      baseUrl +
        '/acct/preplace?V_parentid=' +
        data.parentId +
        '&V_placement=' +
        data.position +
        '&V_prelogid=' +
        data.prelogId +
        '&V_referid=' +
        data.referId,
      {
        method: 'GET',
        headers: {
          Authentication: auth,
          Authorization: 'Bearer ' + token,
        },
      },
    )
      .then(response => response.json())
      .then(responseData => {
        // console.warn('res', responseData);
        if (responseData.response && responseData.response.status == 200) {
          let formBody = new FormData();
          formBody.append('V_prelogid', responseData.data.V_prelogid);
          formBody.append('V_parentid', responseData.data.V_parentid);
          formBody.append('V_referid', responseData.data.V_referid);
          formBody.append('V_placement', data.position);
          formBody.append('V_levelno', responseData.data.V_levelno);
          console.warn('dat1  ->', formBody);
          fetch(baseUrl + '/acct/confpre', {
            method: 'POST',
            headers: {
              Authentication: auth,
              Authorization: 'Bearer ' + token,
            },
            body: formBody,
          })
            .then(response => response.json())
            .then(getData => {
              //console.warn('get Data', getData);
              dispatch({type: 'AFTER_APPROVE', responseData: getData});
              dispatch({type: 'COMMON_LOADER', value: false});
            })
            .catch(err => {
              alert(err);
              dispatch({type: 'COMMON_LOADER', value: false});
            });
        } else {
          showMessage({
            type: 'danger',
            message: responseData.response.message,
          });
          alert(responseData.response.message);
        }
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}
