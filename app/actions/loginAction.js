import {baseUrl, auth} from '../services/index';

export function loginUser(data) {
  let formBody = new FormData();
  formBody.append('logid', data.email);
  formBody.append('password', data.password);
  formBody.append('Authentication', data.Authentication);
  console.warn(formBody);
  return dispatch => {
    fetch(baseUrl + '/login', {
      method: 'POST',
      headers: {
        Authentication: auth,
      },
      body: formBody,
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'LOGIN_DATA', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function signup(data) {
  let formBody = new FormData();
  formBody.append('T_title', data.T_title);
  formBody.append('T_name1', data.T_name1);
  formBody.append('T_name2', data.T_name2);
  formBody.append('T_name3', data.T_name3);
  formBody.append('T_email', data.T_email);
  formBody.append('T_addr1', data.T_addr1);
  formBody.append('T_addr2', data.T_addr2);
  formBody.append('T_city', data.T_city);
  formBody.append('T_state', data.T_state);
  formBody.append('T_pincode', data.T_pincode);
  formBody.append('T_telno', data.T_telno);
  formBody.append('T_pangir', data.T_pangir);
  formBody.append('T_adhaarno', data.T_adhaarno);
  if (data.T_pkgtypeF) {
    formBody.append('T_pkgtypeF', data.T_pkgtypeF);
  }
  formBody.append('typ', data.type);
  console.warn(formBody);
  return dispatch => {
    fetch(baseUrl + '/static/buydirectenquiry', {
      method: 'POST',
      headers: {
        Authentication: auth,
      },
      body: formBody,
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn(responseData);
        dispatch({type: 'SIGNUP_DATA', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        console.warn('eer', err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function emptySignup() {
  return dispatch => {
    dispatch({type: 'SIGNUP_DATA', responseData: {}});
  };
}

export function emptyLogin(value) {
  return dispatch => {
    dispatch({type: 'EMPTY_LOGIN', value: {}});
  };
}

export function userData(data) {
  return dispatch => {
    dispatch({type: 'USER_DATA', value: data});
  };
}

export function emptyLoginData() {
  return dispatch => {
    dispatch({type: 'USER_DATA', value: {}});
  };
}
