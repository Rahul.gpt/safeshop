import {auth, baseUrl} from '../services';

export function sendOtp(data) {
  return dispatch => {
    fetch(baseUrl + '/genotp/' + data.loginId, {
      method: 'POST',
      headers: {
        Authentication: auth,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn(responseData);
        dispatch({type: 'SET_OTP', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
      });
  };
}

export function sentOtpNumber(data) {
  return dispatch => {
    fetch(baseUrl + '/genotp/' + data.loginId + '/' + data.newNumber, {
      method: 'POST',
      headers: {
        Authentication: auth,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn(responseData);
        dispatch({type: 'SET_OTP_NUMBER', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
      });
  };
}

export function emptyOtpData() {
  return dispatch => {
    dispatch({type: 'EMPTY_OTP', value: {}});
  };
}
