import {baseUrl, auth} from '../services/index';

export function getCategory() {
  return dispatch => {
    fetch(baseUrl + '/homepage', {
      method: 'GET',
      headers: {
        Authentication: auth,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'GET_ALL_CATEGORY', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}
