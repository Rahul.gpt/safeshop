import {auth, baseUrl} from '../services';
import {exp} from 'react-native-reanimated';

export function getRetailOrders(token) {
  return dispatch => {
    fetch(baseUrl + '/acct/retlist', {
      method: 'GET',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn(responseData);
        dispatch({type: 'GET_RETAIL_ORDER_LIST', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
      });
  };
}

export function getPaymentId(data, token) {
  console.warn('data', data)
  let formBody = new FormData();
  formBody.append('amount', data.amount);
  formBody.append('vnum', data.vnum);
  return dispatch => {
    fetch(baseUrl + '/acct/pgwaypart1', {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
      body: formBody,
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn(responseData);
        dispatch({type: 'GET_PAYMENT_ID', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
      });
  };
}

export function generateInvoiceData(id, token) {
  return dispatch => {
    fetch(baseUrl + '/acct/retinv/' + id, {
      method: 'GET',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn(JSON.stringify(responseData, undefined, 2));
        dispatch({type: 'GET_INVOICE', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
      });
  };
}

export function emptyPaymentId() {
  return dispatch => {
    dispatch({type: 'GET_PAYMENT_ID', responseData: {}});
  };
}

export function emptyRetailOrder() {
  return dispatch => {
    dispatch({type: 'GET_RETAIL_ORDER_LIST', responseData: {}});
  };
}

export function emptyInvoiceData() {
  return dispatch => {
    dispatch({type: 'GET_INVOICE', responseData: {}});
  };
}
