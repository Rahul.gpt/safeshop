import {baseUrl, auth} from '../services/index';

export function getCourierData(token) {
  return dispatch => {
    fetch(baseUrl + '/acct/courier', {
      method: 'GET',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'GET_COURIER_DATA', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function getThisWeek(token) {
  return dispatch => {
    fetch(baseUrl + '/acct/thisweek', {
      method: 'GET',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'GET_THIS_WEEK', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function getTaxInvoice(key, token) {
  return dispatch => {
    fetch(baseUrl + '/acct/retlist/' + key, {
      method: 'GET',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'GET_DS_TAX_INVOICE', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function getPromotionData(token) {
  return dispatch => {
    fetch(baseUrl + '/acct/promostatus', {
      method: 'GET',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'GET_PROMOTION_DATA', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function getWeeklySale(token) {
  return dispatch => {
    fetch(baseUrl + '/acct/jletter', {
      method: 'GET',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'GET_WEEKLY_SALE', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function getBussinessVolume(token) {
  return dispatch => {
    fetch(baseUrl + '/acct/retcounts', {
      method: 'GET',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'GET_BUSINESS_VOLUME', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function getBankDetails(token) {
  return dispatch => {
    fetch(baseUrl + '/acct/ifsccode', {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'GET_BANK_DATA', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function getDSAList(token) {
  return dispatch => {
    fetch(baseUrl + '/acct/approvlist', {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'GET_DSA_LIST', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function DSAStatus(data, token) {
  let formBody = new FormData();
  formBody.append('logid', data.logid);
  formBody.append('referid', data.referid);
  formBody.append(data.type, data.type);
  console.warn(formBody);
  return dispatch => {
    fetch(baseUrl + '/acct/approvlist2', {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
      body: formBody,
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'STATUS_DSA_LIST', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function getDSInfo(id, token) {
  return dispatch => {
    fetch(baseUrl + '/add/mdetail/' + id, {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'GET_DS_INFO', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function getGst(id, token) {
  return dispatch => {
    fetch(baseUrl + '/acct/gstinv/' + id, {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn(JSON.stringify(responseData, undefined, 2));
        dispatch({type: 'GET_GST', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}
export function emptyGstInvoice() {
  return dispatch => {
    dispatch({type: 'GET_GST', responseData: {}});
  };
}
export function emptyDSInfo() {
  return dispatch => {
    dispatch({type: 'GET_DS_INFO', responseData: {}});
  };
}
