import {baseUrl, auth} from '../services/index';

export function viewCart(token) {
  return dispatch => {
    fetch(baseUrl + '/retail/viewcart', {
      method: 'GET',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'GET_CART_LIST', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function addCart(data, token) {
  let formBody = new FormData();
  formBody.append('package', data.package);
  formBody.append('T_qty', data.T_qty);
  return dispatch => {
    fetch(baseUrl + '/retail/addtocart', {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
      body: formBody,
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'ADD_TO_CART', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function deleteCart(id, token) {
  return dispatch => {
    fetch(baseUrl + '/retail/delcart/D/' + id, {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'DELETE_CART', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function updateCartData(linenum, quantity, token) {
  return dispatch => {
    fetch(baseUrl + '/retail/updateqty/' + linenum + '/' + quantity, {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'UPDATE_CART', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function checkoutCart(data, token) {
  let form = new FormData();
  form.append('addr1', data.addr1);
  form.append('nearby', data.nearby);
  form.append('city', data.city);
  form.append('astate', data.astate);
  form.append('name', data.name);
  form.append('cellno', data.cellno);
  form.append('pincode', data.pincode);
  console.warn('action', data);
  return dispatch => {
    fetch(baseUrl + '/retail/placeorder', {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
      body: form,
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn(responseData);
        dispatch({type: 'CHECKOUT_CART', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function emptyCheckout() {
  return dispatch => {
    dispatch({type: 'CHECKOUT_CART', responseData: {}});
  };
}

export function emptyCartData() {
  return dispatch => {
    dispatch({type: 'EMPTY_CART'});
  };
}

export function emptyCartDelete() {
  return dispatch => {
    dispatch({type: 'EMPTY_CART_DELETE'});
  };
}
