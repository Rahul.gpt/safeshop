import {baseUrl, auth} from '../services/index';
import {Platform} from 'react-native';

export function getProfile(token) {
  return dispatch => {
    fetch(baseUrl + '/acct/profile', {
      method: 'GET',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn(responseData);
        dispatch({type: 'GET_PROFILE', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function getProfileFirstLogin(token) {
  return dispatch => {
    fetch(baseUrl + '/acct/profile', {
      method: 'GET',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn(responseData);
        dispatch({type: 'GET_PROFILE_FIRST', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function updateProfile(data, token) {
  let formBody = new FormData();
  formBody.append('V_oldpwd', data.V_oldpwd);
  formBody.append('V_email', data.V_email);
  formBody.append('V_addr1', data.V_addr1);
  formBody.append('V_addr2', data.V_addr2);
  formBody.append('V_city', data.V_city);
  formBody.append('V_district', data.V_district);
  formBody.append('V_state', data.V_state);
  formBody.append('V_pincode', data.V_pincode);
  formBody.append('V_telno', data.V_telno);
  formBody.append('V_cellno', data.V_cellno);
  formBody.append('V_country', data.V_country);
  formBody.append('V_bankname', data.V_bankname);
  formBody.append('V_bankcity', data.V_bankcity);
  formBody.append('V_bankcode', data.V_bankcode);
  formBody.append('V_pangir', data.V_pangir);
  formBody.append('V_wardno', data.V_wardno);
  formBody.append('V_gstno', data.V_gstno);
  formBody.append('V_adhaarno', data.V_adhaarno);

  if (Object.keys(data.filename).length) {
    let parts = data.filename.path.split('/');
    let uri = data.filename.path;

    let name = parts[parts.length - 1];
    let type = data.filename.mime;

    var file = {
      uri,
      name,
      type,
    };
    console.warn('file', file);
    formBody.append('file_attachment', {
      name: file.name,
      uri: file.uri,
      type: file.type,
    });
    // });
  }

  console.warn('body', JSON.stringify(formBody, undefined, 2));

  return dispatch => {
    fetch(baseUrl + '/acct/saveprofile', {
      method: 'POST',
      headers: {
        Authentication: auth,
        Authorization: 'Bearer ' + token,
        // 'Content-Type': 'multipart/form-data',
        Accept: 'application/json',
      },
      body: formBody,
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn('action', responseData);
        // alert(JSON.stringify(responseData));
        dispatch({type: 'UPDATE_PROFILE', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        // console.warn('err', err);
        dispatch({type: 'COMMON_LOADER', value: false});
        // dispatch({type: 'ERROR_LOGIN', err});
      });
  };
}

export function emptyProfileData() {
  return dispatch => {
    dispatch({type: 'GET_PROFILE', responseData: {}});
  };
}
