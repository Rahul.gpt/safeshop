import {baseUrl, auth} from '../services/index';

export function getProducts(key) {
  return dispatch => {
    fetch(baseUrl + '/retail/categ/' + key, {
      method: 'GET',
      headers: {
        Authentication: auth,
      },
    })
      .then(response => {
        return response.json();
      })
      .then(responseData => {
        dispatch({type: 'GET_PRODUCTS', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
      });
  };
}

export function viewProduct(packageName) {
  return dispatch => {
    fetch(baseUrl + '/retail/product/' + packageName, {
      method: 'GET',
      headers: {
        Authentication: auth,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'PRODUCT_DETAILS', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        dispatch({type: 'COMMON_LOADER', value: false});
      });
  };
}
