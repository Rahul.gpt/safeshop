import {auth, baseUrl} from '../services';

export function searchProducts(key) {
  let formBody = new FormData();
  formBody.append('search', key);
  return dispatch => {
    fetch(baseUrl + '/retail/search', {
      method: 'POST',
      headers: {
        Authentication: auth,
      },
      body: formBody,
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'SEARCH_PRODUCT', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
      });
  };
}

export function allProducts(key) {
  let formBody = new FormData();
  formBody.append('search', key);
  return dispatch => {
    fetch(baseUrl + '/retail/search', {
      method: 'POST',
      headers: {
        Authentication: auth,
      },
      body: formBody,
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'ALL_PRODUCT', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
      });
  };
}

export function emptySearchData() {
  return dispatch => {
    dispatch({type: 'SEARCH_PRODUCT', responseData: {}});
  };
}

export function serachkeyData(value) {
  return dispatch => {
    dispatch({type: 'SEARCH_KEY', value});
  };
}

export function filterData(data) {
  console.warn('action', data);
  let formBody = new FormData();
  return dispatch => {
    fetch(
      baseUrl +
        '/retail/filter/' +
        data.categ +
        '/' +
        data.subCateg +
        '/' +
        data.price1 +
        '/' +
        data.price2 +
        '/' +
        data.fromBv +
        '/' +
        data.tobv,
      {
        method: 'POST',
        headers: {
          Authentication: auth,
        },
      },
    )
      .then(response => response.json())
      .then(responseData => {
        console.warn(JSON.stringify(responseData, undefined, 2));
        dispatch({type: 'FILTER_DATA', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
      });
  };
}

export function guestProductsList() {
  return dispatch => {
    fetch(baseUrl + '/static/buydirect', {
      method: 'POST',
      headers: {
        Authentication: auth,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        dispatch({type: 'GUEST_PRODUCTS', responseData});
        dispatch({type: 'COMMON_LOADER', value: false});
      })
      .catch(err => {
        alert(err);
        dispatch({type: 'COMMON_LOADER', value: false});
      });
  };
}
