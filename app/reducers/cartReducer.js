const initialState = {
  cartList: {},
  addCartResponse: {},
  deleteCartResponse: {},
  updateCartResponse: {},
  checkoutData: {},
};

const CartReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_CART_LIST': {
      return {
        ...state,
        cartList: action.responseData,
      };
    }
    case 'ADD_TO_CART': {
      return {
        ...state,
        addCartResponse: action.responseData,
      };
    }
    case 'DELETE_CART': {
      return {
        ...state,
        deleteCartResponse: action.responseData,
      };
    }
    case 'EMPTY_CART_DELETE': {
      return {
        ...state,
        deleteCartResponse: {},
      };
    }
    case 'UPDATE_CART': {
      return {
        ...state,
        updateCartResponse: action.responseData,
      };
    }
    case 'EMPTY_CART': {
      return {
        ...state,
        cartList: {},
      };
    }
    case 'CHECKOUT_CART': {
      return {
        ...state,
        checkoutData: action.responseData,
      };
    }
    default:
      return state;
  }
};

export default CartReducer;
