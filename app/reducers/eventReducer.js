const initialState = {
    eventList: {},
  };
  
  const EventReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'GET_ALL_EVENTS': {
        return {
          ...state,
          eventList: action.responseData,
        };
      }
      default:
        return state;
    }
  };
  
  export default EventReducer;
  