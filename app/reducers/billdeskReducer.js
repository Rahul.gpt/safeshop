const initialState = {
    strData: {},
  };
  
  const BilldeskReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'GET_STR': {
        return {
          ...state,
          strData: action.responseData,
        };
      }
      default:
        return state;
    }
  };
  
  export default BilldeskReducer;
  