const initialState = {
  getRetailOrdersData: {},
  paymentId : {},
  getInvoice : {}
};

const RetailOrderReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_RETAIL_ORDER_LIST': {
      return {
        ...state,
        getRetailOrdersData: action.responseData,
      };
    }
    case 'GET_PAYMENT_ID': {
      return {
        ...state,
        paymentId: action.responseData,
      };
    }
    case 'GET_INVOICE': {
      return {
        ...state,
        getInvoice: action.responseData,
      };
    }
    default:
      return state;
  }
};

export default RetailOrderReducer;
