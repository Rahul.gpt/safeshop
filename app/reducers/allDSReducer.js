const initialState = {
  courierData: {},
  viewDStaxInvoice: {},
  allThisWeekData: {},
  getpromotion: {},
  getWeeklyData: {},
  getBussinessVolumeData: {},
  getBankData: {},
  getDsaList: {},
  getDSAStatus: {},
  getDSData: {},
  getGstData: {},
};

const AllDSReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_COURIER_DATA': {
      return {
        ...state,
        courierData: action.responseData,
      };
    }
    case 'GET_THIS_WEEK': {
      return {
        ...state,
        allThisWeekData: action.responseData,
      };
    }
    case 'GET_DS_TAX_INVOICE': {
      return {
        ...state,
        viewDStaxInvoice: action.responseData,
      };
    }

    case 'GET_PROMOTION_DATA': {
      return {
        ...state,
        getpromotion: action.responseData,
      };
    }

    case 'GET_WEEKLY_SALE': {
      return {
        ...state,
        getWeeklyData: action.responseData,
      };
    }
    case 'GET_BUSINESS_VOLUME': {
      return {
        ...state,
        getBussinessVolumeData: action.responseData,
      };
    }
    case 'GET_BANK_DATA': {
      return {
        ...state,
        getBankData: action.responseData,
      };
    }
    case 'GET_DSA_LIST': {
      return {
        ...state,
        getDsaList: action.responseData,
      };
    }
    case 'STATUS_DSA_LIST': {
      return {
        ...state,
        getDSAStatus: action.responseData,
      };
    }
    case 'GET_DS_INFO': {
      return {
        ...state,
        getDSData: action.responseData,
      };
    }
    case 'GET_GST': {
      return {
        ...state,
        getGstData: action.responseData,
      };
    }
    default:
      return state;
  }
};

export default AllDSReducer;
