const initialState = {
  getQuery: {},
  submitQueryResponse: {},
};

const GrievenceReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_QUERY_DATA': {
      return {
        ...state,
        getQuery: action.responseData,
      };
    }
    case 'SUBMIT_QUERY': {
      return {
        ...state,
        submitQueryResponse: action.responseData,
      };
    }

    default:
      return state;
  }
};

export default GrievenceReducer;
