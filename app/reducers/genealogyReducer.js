import {act} from 'react-test-renderer';

const initialState = {
  getGenealogyData: {},
  addConfirmation: {},
  addMemberResponse: {},
  approveDS : {}
};

const GenealogyReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GENEALOGY_DATA': {
      return {
        ...state,
        getGenealogyData: action.responseData,
      };
    }

    case 'ADD_AT_NODE': {
      return {
        ...state,
        addConfirmation: action.responseData,
      };
    }

    case 'EMPTY_ADD_CONFIRM': {
      return {
        ...state,
        addConfirmation: action.value,
      };
    }

    case 'TEAM_MEMBER_DATA': {
      return {
        ...state,
        addMemberResponse: action.responseData,
      };
    }
    case 'AFTER_APPROVE': {
      return {
        ...state,
        approveDS: action.responseData,
      };
    }

    default:
      return state;
  }
};

export default GenealogyReducer;
