const initialState = {
  loginData: {},
  loginUserData: {},
  signupData: {},
};

const LoginReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN_DATA': {
      return {
        ...state,
        loginData: action.responseData,
      };
    }
    case 'EMPTY_LOGIN': {
      return {
        ...state,
        loginData: action.value,
      };
    }
    case 'USER_DATA': {
      return {
        ...state,
        loginUserData: action.value,
      };
    }
    case 'SIGNUP_DATA': {
      return {
        ...state,
        signupData: action.responseData,
      };
    }
    default:
      return state;
  }
};

export default LoginReducer;
