const initialState = {
  allCategory: {},
};

const CategoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_ALL_CATEGORY': {
      return {
        ...state,
        allCategory: action.responseData,
      };
    }
    default:
      return state;
  }
};

export default CategoryReducer;
