const initialState = {
  productDataList: {},
  productDetailsData : {}
};

const ProductListingReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_PRODUCTS': {
      return {
        ...state,
        productDataList: action.responseData,
      };
    }
    case 'PRODUCT_DETAILS': {
      return {
        ...state,
        productDetailsData: action.responseData,
      };
    }
    default:
      return state;
  }
};

export default ProductListingReducer;
