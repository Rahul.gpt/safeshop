const initialState = {
  searchData: {},
  filterProduct: {},
  allGuestProducts: {},
  getSerachKey: '',
  allProductData: {},
};

const SearchReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SEARCH_PRODUCT': {
      return {
        ...state,
        searchData: action.responseData,
      };
    }
    case 'EMPTY_SEARCH': {
      return {
        ...state,
        searchData: action.value,
      };
    }
    case 'FILTER_DATA': {
      return {
        ...state,
        filterProduct: action.responseData,
      };
    }
    case 'GUEST_PRODUCTS': {
      return {
        ...state,
        allGuestProducts: action.responseData,
      };
    }
    case 'SEARCH_KEY': {
      console.warn('redux', action.value);
      return {
        ...state,
        getSerachKey: action.value,
      };
    }
    case 'ALL_PRODUCT': {
      return {
        ...state,
        allProductData: action.responseData,
      };
    }
    default:
      return state;
  }
};

export default SearchReducer;
