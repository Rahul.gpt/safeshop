const initialState = {
    getProfileData: {},
    saveProfileData : {},
    getProfileDataFirst : {}
  };
  
  const ProfileReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'GET_PROFILE': {
        return {
          ...state,
          getProfileData: action.responseData,
        };
      }
      case 'UPDATE_PROFILE': {
        return {
          ...state,
          saveProfileData: action.responseData,
        };
      }
      case 'GET_PROFILE_FIRST': {
        return {
          ...state,
          getProfileDataFirst: action.responseData,
        };
      }
      default:
        return state;
    }
  };
  
  export default ProfileReducer;
  