const initialState = {
  forgotResponse: {},
  resetPassResponse: {},
  changePasswordResponse: {},
  getAgreeData: {},
  tncData: {},
  declData: {},
  resetPassResponseNew: {},
  submitUser: {},
};

const ForgotPasswordReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'FORGOT_PASSWORD': {
      return {
        ...state,
        forgotResponse: action.responseData,
      };
    }

    case 'CHANGE_PASSWORD': {
      return {
        ...state,
        changePasswordResponse: action.responseData,
      };
    }

    case 'EMPTY_FORGOT': {
      return {
        ...state,
        forgotResponse: action.value,
      };
    }
    case 'RESET_PASSWORD': {
      return {
        ...state,
        resetPassResponse: action.responseData,
      };
    }
    case 'EMPTY_RESET_PASSWORD': {
      return {
        ...state,
        resetPassResponse: action.value,
      };
    }
    case 'RESET_PASSWORD_NEW': {
      return {
        ...state,
        resetPassResponseNew: action.responseData,
      };
    }
    case 'GET_AGREEMENT': {
      return {
        ...state,
        getAgreeData: action.responseData,
      };
    }
    case 'GET_TNC': {
      return {
        ...state,
        tncData: action.responseData,
      };
    }
    case 'GET_DECLARATION': {
      return {
        ...state,
        declData: action.responseData,
      };
    }

    case 'FINAL_SUBMIT': {
      return {
        ...state,
        submitUser: action.responseData,
      };
    }
    default:
      return state;
  }
};

export default ForgotPasswordReducer;
