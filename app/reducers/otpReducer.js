const initialState = {
  otpData: {},
  otpDataNumber: {},
};

const OtpReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_OTP': {
      return {
        ...state,
        otpData: action.responseData,
      };
    }
    case 'EMPTY_OTP': {
      return {
        ...state,
        otpData: action.value,
        otpDataNumber: action.value,
      };
    }
    case 'SET_OTP_NUMBER': {
      return {
        ...state,
        otpDataNumber: action.responseData,
      };
    }
    default:
      return state;
  }
};

export default OtpReducer;
