import React from 'react';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import {commonColor, commonButton, commonButtonText} from '../commonStyle';
import Icon1 from 'react-native-vector-icons/Entypo';
import Modal from 'react-native-modal';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const MessageModal = props => {
  return (
    <View>
      <Modal
        deviceWidth={wp('100%')}
        deviceHeight={hp('100%')}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: hp('0%'),
        }}
        onBackButtonPress={() => props.closeModal(props.previousScreen)}
        isVisible={props.showModal}>
        <View
          style={{
            width: wp('90'),
            height: hp('40%'),
            alignItems: 'center',
            borderRadius: 20,
            backgroundColor: 'white',
          }}>
          <ScrollView>
            <View style={{width: '100%', alignItems: 'flex-end'}}>
              <TouchableOpacity
                style={{padding: 10}}
                onPress={() => props.closeModal(props.previousScreen)}>
                <Icon1 name="cross" color="black" size={30} />
              </TouchableOpacity>
            </View>
            <View style={{paddingVertical: '5%', paddingHorizontal: '2%'}}>
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: hp('2.2%'),
                  textAlign: 'center',
                  lineHeight: 30,
                }}>
                {props.showMessage}
              </Text>
            </View>
          </ScrollView>
        </View>
      </Modal>
    </View>
  );
};

export default MessageModal;
