import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  StatusBar,
  Alert,
} from 'react-native';
import BankIcon from '../../assets/images/ic_bank_account.png/';
import CartIcon from '../../assets/images/ic_cart.png/';
import ProductIcon from '../../assets/images/ic_product.png/';
import EventIcon from '../../assets/images/ic_events.png/';
import PhotoGalleryIcon from '../../assets/images/ic_gallery.png/';
import DSIcon from '../../assets/images/ic_ds_seller.png/';
import NotificationIcon from '../../assets/images/ic_notification.png/';
import LogoutIcon from '../../assets/images/ic_logout.png/';
import ProfileIcon from '../../assets/images/ic_profile.png/';
import AsyncStorage from '@react-native-community/async-storage';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import * as cartActions from '../../actions/cartActions';
import {useDispatch, useSelector} from 'react-redux';
import ProfileImage from '../../assets/images/profilePlaceholder.png';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import {commonColor} from '../commonStyle';
import {showMessage, hideMessage} from 'react-native-flash-message';
import * as loginActions from '../../actions/loginAction';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import * as profileActions from '../../actions/profileAction';
import {profileImageUrl} from '../../services/index';
import Icon1 from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/Entypo';

var abc = 0;

export default function DrawerComponent(props) {
  const [userdata, setUserData] = useState({});
  const [imageLoading, setImageLoading] = useState(true);

  const dispatch = useDispatch();
  const getProfileData = useSelector(
    state => state.ProfileReducer.getProfileData,
  );
  const loginUserData = useSelector(state => state.LoginReducer.loginUserData);

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        dispatch(profileActions.getProfile(token));
      }
    });
  }, []);

  useEffect(() => {
    if (Object.keys(getProfileData).length) {
      if (getProfileData.response && getProfileData.response.status == 200) {
        setUserData(getProfileData.data.query[0]);
      }
    }
  }, [getProfileData]);

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        dispatch(profileActions.getProfile(token));
      } else {
        setUserData({});
        // dispatch(profileActions.emptyProfileData());
      }
    });
  }, [loginUserData]);

  const _logout = () => {
    props.navigation.closeDrawer();
    Alert.alert(
      'SafeShop',
      'Are you sure you want to logout',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            AsyncStorage.removeItem('token');
            dispatch(cartActions.emptyCartData());
            dispatch(loginActions.emptyLoginData());
            props.navigation.navigate('Category');
          },
        },
      ],
      {cancelable: false},
    );
  };

  const home = () => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        props.navigation.closeDrawer();
        props.navigation.navigate('DSHome');
      } else {
        props.navigation.closeDrawer();
        props.navigation.navigate('Category');
      }
    });
  };

  const handleNavigation = key => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        props.navigation.closeDrawer();
        if (key == 'cart') {
          props.navigation.navigate('Cart');
        } else if (key == 'profile') {
          props.navigation.navigate('UpdateProfile');
        } else if (key == 'bank') {
          props.navigation.navigate('BankDetails');
        }
      } else {
        showMessage({
          message: 'You need to login',
          type: 'info',
        });
        props.navigation.closeDrawer();
        props.navigation.navigate('Login');
      }
    });
  };

  return (
    <View style={{flex: 1}}>
      {/* <StatusBar translucent backgroundColor="rgba(0, 0, 0, 0.20)" animated/> */}
      <View style={styles.profileDataContainer}>
        <View style={styles.profileImageContainer}>
          {Object.keys(userdata).length ? (
            <Image
              source={
                imageLoading
                  ? {uri: profileImageUrl + userdata.photo + `?${abc}`}
                  : ProfileImage
              }
              style={{
                width: 80,
                height: 80,
                borderRadius: 40,
              }}
              onError={() => setImageLoading(false)}

              // resizeMode="contain"
            />
          ) : (
            <Image
              source={ProfileImage}
              style={{width: '90%', height: '90%'}}
              resizeMode="contain"
            />
          )}
        </View>
        {Object.keys(userdata).length ? (
          <View style={styles.profileNameContainer}>
            <Text
              style={{
                color: 'white',
                fontWeight: 'bold',
                fontSize: 18,
                textAlign: 'center',
              }}>
              {userdata.title + ' ' + userdata.name}
            </Text>
            <Text
              style={{
                color: '#d1d2d6',
                fontWeight: 'bold',
                fontSize: 14,
                textAlign: 'center',
              }}>
              DS ID: {userdata.logid}
            </Text>
          </View>
        ) : null}
      </View>
      <ScrollView>
        <View>
          <TouchableOpacity style={styles.menuItemContainer} onPress={home}>
            <View style={styles.menuIconContainer}>
              <Icon name="home" color={commonColor.color} size={25} />
            </View>
            <View>
              <Text style={styles.menuItext} numberOfLines={1}>
                Home
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuItemContainer}
            onPress={() => handleNavigation('profile')}>
            <View style={styles.menuIconContainer}>
              <Image
                source={ProfileIcon}
                style={styles.icon}
                resizeMode="contain"
              />
            </View>
            <View>
              <Text style={styles.menuItext} numberOfLines={1}>
                Update Profile
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuItemContainer}
            onPress={() => handleNavigation('bank')}>
            <View style={styles.menuIconContainer}>
              <Image
                source={BankIcon}
                style={styles.icon}
                resizeMode="contain"
              />
            </View>
            <View>
              <Text style={styles.menuItext} numberOfLines={1}>
                Bank Details
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuItemContainer}
            onPress={() => handleNavigation('cart')}>
            <View style={styles.menuIconContainer}>
              <Image
                source={CartIcon}
                style={styles.icon}
                resizeMode="contain"
              />
            </View>
            <View>
              <Text style={styles.menuItext} numberOfLines={1}>
                My Cart
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuItemContainer}
            onPress={() => {
              props.navigation.closeDrawer();
              props.navigation.navigate('Products');
            }}>
            <View style={styles.menuIconContainer}>
              <Image
                source={ProductIcon}
                style={styles.icon}
                resizeMode="contain"
              />
            </View>
            <View>
              <Text style={styles.menuItext} numberOfLines={1}>
                Products
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuItemContainer}
            onPress={() => {
              props.navigation.closeDrawer();
              props.navigation.navigate('Events');
            }}>
            <View style={styles.menuIconContainer}>
              <Image
                source={EventIcon}
                style={styles.icon}
                resizeMode="contain"
              />
            </View>
            <View>
              <Text style={styles.menuItext} numberOfLines={1}>
                Events
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuItemContainer}
            onPress={() => {
              props.navigation.closeDrawer();
              props.navigation.navigate('Legal');
            }}>
            <View style={styles.menuIconContainer}>
              <Image
                source={EventIcon}
                style={styles.icon}
                resizeMode="contain"
              />
            </View>
            <View>
              <Text style={styles.menuItext} numberOfLines={1}>
                Legal
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuItemContainer}
            onPress={() => {
              props.navigation.closeDrawer();
              props.navigation.navigate('GrievenceCell');
            }}>
            <View style={styles.menuIconContainer}>
              <Image
                source={PhotoGalleryIcon}
                style={styles.icon}
                resizeMode="contain"
              />
            </View>
            <View>
              <Text style={styles.menuItext} numberOfLines={1}>
                Grievance Cell
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuItemContainer}
            onPress={() => {
              props.navigation.closeDrawer();
              props.navigation.navigate('Contactus');
            }}>
            <View style={styles.menuIconContainer}>
              <Icon1 name="contacts" color={commonColor.color} size={25} />
            </View>
            <View>
              <Text style={styles.menuItext} numberOfLines={1}>
                Contact Us
              </Text>
            </View>
          </TouchableOpacity>
          {Object.keys(userdata).length ? (
            <TouchableOpacity
              style={styles.menuItemContainer}
              onPress={() => _logout()}>
              <View style={styles.menuIconContainer}>
                <Image
                  source={LogoutIcon}
                  style={styles.icon}
                  resizeMode="contain"
                />
              </View>
              <View>
                <Text style={styles.menuItext} numberOfLines={1}>
                  Logout
                </Text>
              </View>
            </TouchableOpacity>
          ) : null}
        </View>
      </ScrollView>
    </View>
  );
}
const styles = StyleSheet.create({
  profileDataContainer: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#e2e2e220',
    alignItems: 'center',
    paddingVertical: '10%',
    justifyContent: 'center',
  },
  profileImageContainer: {
    width: 80,
    height: 80,
    borderRadius: 40,
    alignItems: 'center',
  },
  profileNameContainer: {
    alignItems: 'center',
    width: '50%',
  },
  menuItemContainer: {
    borderBottomWidth: 1,
    borderBottomColor: '#e2e2e220',
    paddingVertical: '5%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  menuIconContainer: {
    width: '20%',
    alignItems: 'center',
  },
  icon: {
    width: 25,
    height: 25,
  },
  menuItext: {
    color: '#d1d2d6',
    fontSize: hp('2.3%'),
  },
});
