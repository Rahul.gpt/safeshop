import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export const InputTextLabel = {
  color: '#9d9c9c',
  textTransform: 'uppercase',
  fontWeight: 'bold',
  fontSize: 16,
};

export const commonColor = {
  color: '#41caf1',
  lightColor : "#9d9c9c",
  darkColor : "#262626"
};

export const commonButton = {
  backgroundColor: '#41caf1',
  alignItems: 'center',
  paddingVertical: '4%',
  borderRadius: 6,
};

export const commonButtonText = {
  color: 'white',
  fontSize: 16,
  textTransform: 'uppercase',
  fontWeight: 'bold',
};

export const logoContainer = {
  paddingVertical: '15%',
  justifyContent: 'center',
  alignItems: 'center',
};

export const commonScreenWidth = {width: '95%'};

export const mainContainer = {alignItems: 'center'};

export const logoStyle = {width: 200, height: 100};

export const allColors = {};
