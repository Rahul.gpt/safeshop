import React, {useEffect, useState, Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
} from 'react-native';
import Modal from 'react-native-modal';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import Icon1 from 'react-native-vector-icons/Entypo';
import CheckBox from 'react-native-check-box';
import {connect} from 'react-redux';
import * as loadingAction from '../../actions/commonLoaderAction';

class FilterModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sortingData: [
        {bool: false, typeData: 'Sort by Name ( A-Z )', sortType: 'nameAsc'},
        {bool: false, typeData: 'Sort by Name ( Z-A )', sortType: 'nameDesc'},
        {
          bool: false,
          typeData: 'Sort by Price ( High to Low )',
          sortType: 'priceDesc',
        },
        {
          bool: false,
          typeData: 'Sort by Price ( Low to High )',
          sortType: 'PriceAsc',
        },
      ],
      priceRange: [
        {
          range: '0-500',
          priceBool: false,
        },
        {
          range: '500-1000',
          priceBool: false,
        },
        {
          range: '1000-2000',
          priceBool: false,
        },
        {
          range: '2000-10000',
          priceBool: false,
        },
        {
          range: '10000-50000',
          priceBool: false,
        },
      ],
      bvRangeData: [
        {bvRange: '0-10', bvBool: false},
        {bvRange: '10-25', bvBool: false},
        {bvRange: '25-50', bvBool: false},
        {bvRange: '50-100', bvBool: false},
        {bvRange: '100-200', bvBool: false},
      ],
      sortingIndex: '',
      avaialbleCategories: this.props.avaialbleCategories,
      allSubCategoryData: this.props.allSubCategoryData,
      categIndex: undefined,
      subCategIndex: undefined,
      priceIndex: undefined,
      bvIndex: undefined,
    };
  }

  componentDidMount() {
    if (this.props.sortingTypeData) {
      let sortingIndex = '';
      let sortingData = this.state.sortingData.map((item, i) => {
        if (item.sortType == this.props.sortingTypeData) {
          sortingIndex = i;
          return {...item, bool: true};
        } else {
          return {...item, bool: false};
        }
      });
      this.setState({sortingData, sortingIndex});
    }
    if (Object.keys(this.props.filterData).length) {
      let categIndex = undefined;
      let bvIndex = undefined;
      let priceIndex = undefined;
      let subCategIndex = undefined;
      let avaialbleCategories =
        typeof this.state.avaialbleCategories == 'object'
          ? this.state.avaialbleCategories.map((item, i) => {
              if (
                item.category.toLowerCase() ==
                this.props.filterData.categFilterData.toLowerCase()
              ) {
                categIndex = i;
                return {...item, catBool: true};
              } else {
                return {...item, catBool: false};
              }
            })
          : this.state.avaialbleCategories;
      let allSubCategoryData =
        typeof this.state.allSubCategoryData == 'object'
          ? this.state.allSubCategoryData.map((item, i) => {
              if (
                item.subcateg.toLowerCase() ==
                this.props.filterData.subCategFilterData.toLowerCase()
              ) {
                subCategIndex = i;
                return {...item, subCatBool: true};
              } else {
                return {...item, subCatBool: false};
              }
            })
          : this.state.allSubCategoryData;
      let bvRangeData =
        typeof this.state.bvRangeData == 'object'
          ? this.state.bvRangeData.map((item, i) => {
              if (item.bvRange == this.props.filterData.bvFilterData) {
                bvIndex = i;
                return {...item, bvBool: true};
              } else {
                return {...item, bvBool: false};
              }
            })
          : this.state.bvRangeData;
      let priceRange =
        typeof this.state.priceRange == 'object'
          ? this.state.priceRange.map((item, i) => {
              if (item.range == this.props.filterData.priceFilterdata) {
                priceIndex = i;
                return {...item, priceBool: true};
              } else {
                return {...item, priceBool: false};
              }
            })
          : this.state.priceRange;
      this.setState({
        categIndex,
        avaialbleCategories,
        allSubCategoryData,
        bvRangeData,
        bvIndex,
        subCategIndex,
        priceRange,
        priceIndex,
      });
    }
  }

  handleSortCheckox = (value, index) => {
    const newValue = this.state.sortingData.map((checkbox, i) => {
      if (i !== index)
        return {
          ...checkbox,
          bool: false,
        };
      if (i === index) {
        const item = {
          ...checkbox,
          bool: true,
        };
        return item;
      }
      return checkbox;
    });
    this.setState({sortingData: newValue, sortingIndex: index});
  };

  handleCategCheckox = (value, index) => {
    const newValue = this.state.avaialbleCategories.map((checkbox, i) => {
      if (i !== index)
        return {
          ...checkbox,
          catBool: false,
        };
      if (i === index) {
        const item = {
          ...checkbox,
          catBool: true,
        };
        return item;
      }
      return checkbox;
    });
    this.setState({avaialbleCategories: newValue, categIndex: index});
  };

  handleSubCategCheckox = (value, index) => {
    const newValue = this.state.allSubCategoryData.map((checkbox, i) => {
      if (i !== index)
        return {
          ...checkbox,
          subCatBool: false,
        };
      if (i === index) {
        const item = {
          ...checkbox,
          subCatBool: true,
        };
        return item;
      }
      return checkbox;
    });
    this.setState({allSubCategoryData: newValue, subCategIndex: index});
  };

  handlePriceCheckox = (item, index) => {
    const newValue = this.state.priceRange.map((checkbox, i) => {
      if (i !== index)
        return {
          ...checkbox,
          priceBool: false,
        };
      if (i === index) {
        const item = {
          ...checkbox,
          priceBool: true,
        };
        return item;
      }
      return checkbox;
    });
    this.setState({priceRange: newValue, priceIndex: index});
  };

  handlebvRangeCheckox = (item, index) => {
    const newValue = this.state.bvRangeData.map((checkbox, i) => {
      if (i !== index)
        return {
          ...checkbox,
          bvBool: false,
        };
      if (i === index) {
        const item = {
          ...checkbox,
          bvBool: true,
        };
        return item;
      }
      return checkbox;
    });
    this.setState({bvRangeData: newValue, bvIndex: index});
  };

  submitFilter = () => {
    let {
      avaialbleCategories,
      allSubCategoryData,
      priceRange,
      categIndex,
      priceIndex,
      subCategIndex,
      bvRangeData,
      bvIndex,
    } = this.state;
    let data = {};
    if (this.isIndexPresent(categIndex)) {
      data = {
        ...data,
        categFilterData: avaialbleCategories[categIndex].category,
      };
    } else {
      data = {
        ...data,
        categFilterData: 'ALL',
      };
    }
    if (this.isIndexPresent(priceIndex)) {
      data = {...data, priceFilterdata: priceRange[priceIndex].range};
    } else {
      data = {...data, priceFilterdata: 'ALL'};
    }
    if (this.isIndexPresent(subCategIndex)) {
      data = {
        ...data,
        subCategFilterData: allSubCategoryData[subCategIndex].subcateg,
      };
    } else {
      data = {
        ...data,
        subCategFilterData: 'ALL',
      };
    }
    if (this.isIndexPresent(bvIndex)) {
      data = {
        ...data,
        bvFilterData: bvRangeData[bvIndex].bvRange,
      };
    } else {
      data = {
        ...data,
        bvFilterData: 'ALL',
      };
    }

    this.props.submitFilterModal(data);
  };

  isIndexPresent(index) {
    return typeof index !== 'undefined';
  }

  render() {
    console.warn(
      'state index',
      this.state.priceIndex,
      this.state.categIndex,
      this.state.subCategIndex,
      this.state.bvIndex,
    );
    return (
      <View>
        <Modal
          deviceWidth={wp('100%')}
          deviceHeight={hp('100%')}
          style={styles.modal}
          isVisible={this.props.visibleModal}>
          <View style={styles.mainContainer1}>
            <View style={{width: '100%', height: '100%'}}>
              <View style={{width: '100%', alignItems: 'flex-end'}}>
                <TouchableOpacity
                  style={{padding: 10}}
                  onPress={this.props.closedModal}>
                  <Icon1 name="cross" color="black" size={30} />
                </TouchableOpacity>
              </View>
              <View>
                <Text style={styles.heading}>
                  {this.props.modalType + ' by'}
                </Text>
              </View>
              <ScrollView showsVerticalScrollIndicator={false} bounces={false}>
                {this.props.modalType == 'filter' ? (
                  <View style={{width: '100%'}}>
                    <View style={{paddingVertical: '5%'}}>
                      <Text style={{fontWeight: 'bold'}}>
                        1. Choose Sub Categories
                      </Text>
                    </View>
                    {this.state.allSubCategoryData.map((item, index) => {
                      // console.warn('item.subcateg', item.subcateg);
                      return (
                        <View
                          style={{paddingVertical: '3%', paddingLeft: '15%'}}>
                          <CheckBox
                            style={{padding: 10}}
                            onClick={() =>
                              this.handleSubCategCheckox(item, index)
                            }
                            isChecked={item.subCatBool}
                            rightText={item.subcateg}
                            checkBoxColor={commonColor.color}
                            uncheckedCheckBoxColor={'black'}
                          />
                        </View>
                      );
                    })}
                    <View style={{paddingVertical: '5%'}}>
                      <Text style={{fontWeight: 'bold'}}>
                        2. Choose Categories
                      </Text>
                    </View>
                    {this.state.avaialbleCategories.map((item, index) => {
                      // console.warn('item.subcateg', item.categories);
                      return (
                        <View
                          style={{paddingVertical: '3%', paddingLeft: '15%'}}>
                          <CheckBox
                            style={{padding: 10}}
                            onClick={() => this.handleCategCheckox(item, index)}
                            isChecked={item.catBool}
                            rightText={item.category}
                            checkBoxColor={commonColor.color}
                            uncheckedCheckBoxColor={'black'}
                          />
                        </View>
                      );
                    })}
                    <View style={{paddingVertical: '5%'}}>
                      <Text style={{fontWeight: 'bold'}}>
                        3. Choose Price Range
                      </Text>
                    </View>
                    {this.state.priceRange.map((item, index) => {
                      // console.warn('item.subcateg', item.categories);
                      return (
                        <View
                          style={{paddingVertical: '3%', paddingLeft: '15%'}}>
                          <CheckBox
                            style={{padding: 10}}
                            onClick={() => this.handlePriceCheckox(item, index)}
                            isChecked={item.priceBool}
                            rightText={item.range}
                            checkBoxColor={commonColor.color}
                            uncheckedCheckBoxColor={'black'}
                          />
                        </View>
                      );
                    })}

                    <View style={{paddingVertical: '5%'}}>
                      <Text style={{fontWeight: 'bold'}}>
                        4. Choose Bussiness Volume Range
                      </Text>
                    </View>
                    {this.state.bvRangeData.map((item, index) => {
                      // console.warn('item.subcateg', item.categories);
                      return (
                        <View
                          style={{paddingVertical: '3%', paddingLeft: '15%'}}>
                          <CheckBox
                            style={{padding: 10}}
                            onClick={() =>
                              this.handlebvRangeCheckox(item, index)
                            }
                            isChecked={item.bvBool}
                            rightText={item.bvRange}
                            checkBoxColor={commonColor.color}
                            uncheckedCheckBoxColor={'black'}
                          />
                        </View>
                      );
                    })}

                    <View
                      style={{
                        alignItems: 'center',
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                      }}>
                      <TouchableOpacity
                        style={styles.confirmButtonContainer}
                        onPress={() => this.props.resetFilter()}>
                        <View>
                          <Text style={{color: 'white'}}>Reset</Text>
                        </View>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={styles.confirmButtonContainer}
                        onPress={() => this.submitFilter()}>
                        <View>
                          <Text style={{color: 'white'}}>Submit</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                ) : (
                  <View style={{width: '100%'}}>
                    {this.state.sortingData.map((item, index) => {
                      return (
                        <View
                          style={{paddingVertical: '3%', paddingLeft: '15%'}}>
                          <CheckBox
                            style={{padding: 10}}
                            onClick={() => this.handleSortCheckox(item, index)}
                            isChecked={item.bool}
                            rightText={item.typeData}
                            checkBoxColor={commonColor.color}
                            uncheckedCheckBoxColor={'black'}
                          />
                        </View>
                      );
                    })}
                    <View
                      style={{
                        alignItems: 'center',
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                      }}>
                      <TouchableOpacity
                        style={styles.confirmButtonContainer}
                        onPress={() => this.props.submitModal(null)}>
                        <View>
                          <Text style={{color: 'white'}}>Reset</Text>
                        </View>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={styles.confirmButtonContainer}
                        onPress={() =>
                          this.props.submitModal(
                            this.state.sortingData[this.state.sortingIndex],
                          )
                        }>
                        <View>
                          <Text style={{color: 'white'}}>Submit</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                )}
              </ScrollView>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

export default connect(
  state => ({
    isLoading: state.CommonLoaderReducer.isLoading,
  }),
  {...loadingAction},
)(FilterModal);

const styles = StyleSheet.create({
  modal: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: hp('0%'),
  },
  mainContainer1: {
    height: hp('80%'),
    width: wp('100%'),
    paddingHorizontal: wp('3%'),
    paddingTop: hp('3%'),
    backgroundColor: '#ffffff',
    borderTopLeftRadius: hp('5%'),
    borderTopRightRadius: hp('5%'),
    alignItems: 'center',
    borderWidth: 1,
  },
  heading: {
    fontWeight: 'bold',
    fontSize: hp('3%'),
    textTransform: 'capitalize',
    textAlign: 'center',
  },
  confirmButtonContainer: {
    width: '30%',
    backgroundColor: commonColor.color,
    paddingVertical: '4%',
    borderRadius: 30,
    alignItems: 'center',
    marginVertical: '5%',
  },
});
