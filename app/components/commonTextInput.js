import * as React from 'react';
import {StyleSheet, Text, TextInput} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default function CustomTextInput(props) {
  return (
    <TextInput
      style={props.customStyle ? props.customStyleData : styles.defaultStyle}
      onChangeText={props.changeText}
      value={props.value}
      secureTextEntry={props.secureText}
      keyboardType={props.keyboardType && props.keyboardType}
      placeholder={props.placeholder && props.placeholder}
      editable={props.editable === false ? false : true}
      placeholderTextColor={
        props.placeholderTextColor && props.placeholderTextColor
      }
      maxLength={props.maxLength}
      onEndEditing={props.onSubmit}
    />
  );
}

const styles = StyleSheet.create({
  defaultStyle: {
    flex: 1,
    borderBottomWidth: 0.6,
    borderColor: '#e2e2e2',
    height: 50,
  },
});
