import * as React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import RNPickerSelect from 'react-native-picker-select';
import Icon from 'react-native-vector-icons/Ionicons';

const CommonDropdown = props => {
  return (
    <View>
      <View>
        <RNPickerSelect
          onValueChange={props.onValueChange}
          items={props.itemData}
          style={{borderWidth: 1}}
          value={props.value}
          useNativeAndroidPickerStyle={false}
          style={pickerStyleing}
          placeholder={{
            label: props.placeholderText
              ? props.placeholderText
              : 'Select an item',
            value: null,
          }}
        />
      </View>
      <View
        style={{
          right: wp('2%'),
          position: 'absolute',
          justifyContent: 'center',
          height: 50,
        }}>
        <Icon name="md-arrow-dropdown" color="gray" size={20} />
      </View>
    </View>
  );
};

export default CommonDropdown;

const pickerStyleing = StyleSheet.create({
  placeholder: {
    // color: '#05564d',
    fontSize: hp('2%'),
    paddingVertical: hp('0.9%'),
    paddingLeft: 8,
    paddingRight: 20,
  },
  inputIOS: {
    color: '#000',
    fontSize: hp('2%'),
    paddingVertical: hp('0.9%'),
    paddingLeft: 8,
    borderWidth: 1,
    borderColor: '#e2e2e2',
    borderRadius: 6,
    paddingLeft: '5%',
    height: 50,
  },
  inputAndroid: {
    color: '#000',
    fontSize: hp('2%'),
    paddingVertical: hp('0.9%'),
    paddingLeft: 8,
    borderWidth: 1,
    borderColor: '#e2e2e2',
    borderRadius: 6,
    paddingLeft: '5%',
    height: 50,
  },
});
