import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import Modal from 'react-native-modal';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import OTPTextView from 'react-native-otp-textinput';
import {commonColor, commonButton, commonButtonText} from '../commonStyle';
import Icon1 from 'react-native-vector-icons/Entypo';
import {showMessage, hideMessage} from 'react-native-flash-message';
import * as otpActions from '../../actions/otpActions';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';

const OtpModal = props => {
  const [otpData, setOtp] = useState('');
  const [otpFromInupt, setOtpInput] = useState('');
  const [otpLength, setOtpLength] = useState(2);

  const dispatch = useDispatch();
  const otpFromApi = useSelector(state => state.OtpReducer.otpData);
  const isLoading = useSelector(state => state.CommonLoaderReducer.isLoading);

  useEffect(() => {
    // console.warn('logid', props.loginID);
    // let data = {
    //   loginId: props.loginID,
    // };
    // dispatch(otpActions.sendOtp(data));
    setOtpLength(props.otpValue.length);
    setOtp(props.otpValue);
  }, []);

  useEffect(() => {
    if (Object.keys(otpFromApi).length) {
      if (otpFromApi.response.status == 200) {
        setOtp(otpFromApi.data.otp);
        setOtpLength(otpFromApi.data.otp.length);
        showMessage({
          type: 'success',
          message: 'Otp is successfully send to your registered mobile numer',
        });
        dispatch(otpActions.emptyOtpData());
      } else {
        console.warn('else');
        // dispatch(forgotPasswordAction.emptyForgot());
        showMessage({
          message: 'Something went wrong',
          type: 'danger',
        });
      }
    }
  }, [otpFromApi]);

  const verifyOtp = () => {
    // props.verifiedStatus(true);
    console.warn('verify', otpFromInupt, otpData);
    if (otpFromInupt == otpData) {
      props.verifiedStatus(true);
      showMessage({
        type: 'success',
        message: 'Verified',
      });
    } else {
      props.verifiedStatus(false);
      showMessage({
        type: 'danger',
        message: 'Otp does not matched',
      });
    }
  };

  const handleOtp = otp => {
    setOtpInput(otp);
  };

  useEffect(() => {
    console.warn('input', otpFromInupt);
    console.warn('api', otpData);
  });

  const resendOtp = () => {
    let data = {
      loginId: props.loginID,
    };
    console.warn(data);
    dispatch(otpActions.sendOtp(data));
  };

  return (
    <View>
      <Modal
        deviceWidth={wp('100%')}
        deviceHeight={hp('100%')}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: hp('0%'),
        }}
        onBackButtonPress={props.handleModal}
        isVisible={props.showOtpModal}>
        <View
          style={{
            width: wp('90'),
            height: hp('40%'),
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 20,
          }}>
          <View style={styles.container}>
            <View style={{width: '100%', alignItems: 'flex-end'}}>
              <TouchableOpacity
                style={{padding: 10}}
                onPress={props.crossModal}>
                <Icon1 name="cross" color="black" size={30} />
              </TouchableOpacity>
            </View>
            <View style={{marginBottom: hp('2%')}}>
              <Text style={{fontWeight: 'bold', fontSize: hp('2.5%')}}>
                Enter otp to verify
              </Text>
            </View>
            <OTPTextView
              handleTextChange={e => handleOtp(e)}
              textInputStyle={styles.roundedTextInput}
              tintColor={commonColor.color}
              inputCount={otpLength}
            />
            <View style={{marginVertical: '6%', width: '30%'}}>
              <View>
                <TouchableOpacity
                  style={[
                    commonButton,
                    {paddingVertical: '10%', borderRadius: 30},
                  ]}
                  onPress={verifyOtp}>
                  <Text style={commonButtonText}>Verify</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{marginBottom: hp('2%')}}>
              <TouchableOpacity onPress={resendOtp}>
                <Text
                  style={{
                    color: commonColor.color,
                    textDecorationLine: 'underline',
                  }}>
                  Click here to Resend OTP
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default OtpModal;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    padding: 5,
    borderRadius: 20,
  },
  roundedTextInput: {
    borderRadius: 10,
    borderWidth: 4,
  },
});
