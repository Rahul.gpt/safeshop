import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
} from 'react-native';
import Header from '../header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';
import * as retailsActions from '../../actions/retailOrderActions';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';

const Invoice = props => {
  const [cartListData, setCartListData] = useState([]);
  const [totalCart, setTotalCart] = useState(0);
  const [data1, setData1] = useState({});
  const [data2, setData2] = useState({});
  const [loading, setLoading] = useState(true);
  const [shippingAmt, setShipping] = useState(0);
  const [invoiceType, setInvoiceType] = useState('');
  const [totalBv, setTotalBv] = useState(0);
  const [referData, setReferData] = useState({});

  const dispatch = useDispatch();
  const isLoading = useSelector(state => state.CommonLoaderReducer.isLoading);
  const getInvoice = useSelector(state => state.RetailOrderReducer.getInvoice);

  useEffect(() => {
    if (props.route && props.route.params && props.route.params.id) {
      AsyncStorage.getItem('token').then(token => {
        if (token !== null) {
          dispatch(
            retailsActions.generateInvoiceData(props.route.params.id, token),
          );
        }
      });
    }
  }, []);

  useEffect(() => {
    if (Object.keys(getInvoice).length > 0) {
      console.warn(JSON.stringify(getInvoice, undefined, 2));
      if (getInvoice.response && getInvoice.response.status == 200) {
        setCartListData(getInvoice.data.cart);
        setData1(getInvoice.data.query2[0]);
        setData2(getInvoice.data.query1[0]);
        setInvoiceType(getInvoice.data.typ);
        setReferData(getInvoice.data.referid[0]);
        dispatch(retailsActions.emptyInvoiceData());
        let total = 0;
        let totalBv = 0;
        getInvoice.data.cart.map(cartData => {
          total = total + Number(cartData.amount);
          totalBv = totalBv + cartData.nqty * cartData.BV;
        });
        setTotalCart(total);
        setLoading(false);
        setTotalBv(totalBv);
      }
    }
  }, [getInvoice]);

  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          title="Invoice Preview"
          showCart={false}
          showNotification={false}
          headeHeight={60}
          back={true}
        />
      </View>
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowRadius: 5,
          shadowOpacity: 0.2,
          margin: '5%',
          elevation: 3,
        }}>
        <ScrollView>
          {loading ? (
            <View style={{height: 500, justifyContent: 'center'}}>
              <ActivityIndicator size="large" />
            </View>
          ) : (
            <View style={{alignItems: 'center'}}>
              <View
                style={{
                  width: '95%',
                  paddingVertical: hp('3%'),
                  alignItems: 'center',
                }}>
                {invoiceType == 'R' ? (
                  <View>
                    <Text style={styles.mainHeadingText}>
                      Proforma Invoice Repurchase RETAIL
                    </Text>
                  </View>
                ) : (
                  <View>
                    <Text style={styles.mainHeadingText}>{data1.LOGID}</Text>
                    <Text style={styles.mainHeadingText}>
                      Proforma Invoice RETAIL
                    </Text>
                  </View>
                )}
                {invoiceType == 'C' ? (
                  <View style={{paddingVertical: hp('2%')}}>
                    <Text style={styles.mainHeadingText}>
                      {referData.logid}
                    </Text>
                    <Text
                      style={{
                        fontSize: hp('1.7%'),
                        textAlign: 'center',
                      }}>
                      {referData.addr1 +
                        ' ' +
                        referData.district +
                        ' ' +
                        referData.city +
                        ' ' +
                        referData.state +
                        ' ' +
                        referData.pincode +
                        ' ' +
                        referData.country}
                    </Text>
                  </View>
                ) : (
                  <View style={{paddingVertical: hp('2%')}}>
                    <Text style={styles.mainHeadingText}>
                      Safe and Secure Online Marketing Pvt. Ltd.
                    </Text>
                    <Text
                      style={{
                        fontSize: hp('1.7%'),
                        textAlign: 'center',
                      }}>
                      HO: A-3/24, Janak Puri, New Delhi 110058, India
                    </Text>
                  </View>
                )}
                {invoiceType == 'R' ? (
                  <View style={styles.rowData}>
                    <View style={{width: '50%'}}>
                      <Text
                        style={{
                          fontSize: hp('2%'),
                          fontWeight: 'bold',
                        }}>
                        Invoice #
                      </Text>
                    </View>
                    <View style={{width: '50%', alignItems: 'flex-end'}}>
                      <Text
                        style={{
                          fontSize: hp('2%'),
                          fontWeight: 'bold',
                        }}>
                        {data1.VNO}
                      </Text>
                    </View>
                  </View>
                ) : null}
                <View style={styles.rowData}>
                  <View style={{width: '50%'}}>
                    <Text
                      style={{
                        fontSize: hp('2%'),
                        fontWeight: 'bold',
                      }}>
                      Order Date
                    </Text>
                  </View>
                  <View style={{width: '50%', alignItems: 'flex-end'}}>
                    <Text
                      style={{
                        fontSize: hp('2%'),
                        fontWeight: 'bold',
                      }}>
                      {moment(data1.VDATE).format('DD-MM-YYYY')}
                    </Text>
                  </View>
                </View>
                <View style={styles.rowData}>
                  <View style={{width: '50%'}}>
                    <Text
                      style={{
                        fontSize: hp('2%'),
                        fontWeight: 'bold',
                      }}>
                      Billing Person
                    </Text>
                  </View>
                  <View style={{width: '50%', alignItems: 'flex-end'}}>
                    <Text
                      style={{
                        fontSize: hp('2%'),
                        fontWeight: 'bold',
                      }}>
                      {data2.name}
                    </Text>
                  </View>
                </View>
                <View style={{width: '100%'}}>
                  <View style={styles.rowData}>
                    <Text
                      style={{
                        fontSize: hp('2%'),
                        fontWeight: 'bold',
                      }}>
                      Shipping Address
                    </Text>
                  </View>
                  <View style={[styles.rowData, {flexDirection: 'column'}]}>
                    <Text
                      style={{
                        textTransform: 'capitalize',
                      }}>
                      {data1.addr1}
                    </Text>
                    <Text>
                      {data1.nearby + ' ' + data1.district + ' ' + data1.city}
                    </Text>
                    <Text>{data1.astate + ' ' + data1.pincode}</Text>
                    <Text>{'Contact No. ' + data1.cellno}</Text>
                  </View>
                </View>
                <View style={{width: '100%'}}>
                  <View style={styles.rowData}>
                    <Text
                      style={{
                        fontSize: hp('2%'),
                        fontWeight: 'bold',
                      }}>
                      Billing Address
                    </Text>
                  </View>
                  <View style={[styles.rowData, {flexDirection: 'column'}]}>
                    <Text
                      style={{
                        textTransform: 'capitalize',
                      }}>
                      {data2.addr1}
                    </Text>
                    <Text>
                      {data2.nearby
                        ? data2.nearby
                        : '' + ' ' + data2.district + ' ' + data2.city}
                    </Text>
                    <Text>{data2.state + ' ' + data2.pincode}</Text>
                  </View>
                </View>
                <View style={styles.rowData}>
                  <View style={{width: '50%'}}>
                    <Text style={{fontWeight: 'bold', fontSize: hp('2%')}}>
                      Refer Id
                    </Text>
                  </View>
                  <View style={{width: '50%', alignItems: 'flex-end'}}>
                    <Text style={{fontWeight: 'bold', fontSize: hp('2%')}}>
                      {data2.referid}
                    </Text>
                  </View>
                </View>
                <View style={styles.rowData}>
                  <View style={{width: '50%'}}>
                    <Text style={{fontWeight: 'bold', fontSize: hp('2%')}}>
                      Buyer Id
                    </Text>
                  </View>
                  <View style={{width: '50%', alignItems: 'flex-end'}}>
                    <Text style={{fontWeight: 'bold', fontSize: hp('2%')}}>
                      {data2.logid}
                    </Text>
                  </View>
                </View>
                <View style={styles.rowData}>
                  <View style={{width: '50%'}}>
                    <Text style={{fontWeight: 'bold', fontSize: hp('2%')}}>
                      Tel. No.
                    </Text>
                  </View>
                  <View style={{width: '50%', alignItems: 'flex-end'}}>
                    <Text style={{fontWeight: 'bold', fontSize: hp('2%')}}>
                      {data2.telno}
                    </Text>
                  </View>
                </View>
                {cartListData.length ? (
                  <View style={{width: '100%'}}>
                    <View style={styles.rowData}>
                      <Text
                        style={{
                          fontSize: hp('2%'),
                          fontWeight: 'bold',
                        }}>
                        Item Details
                      </Text>
                    </View>
                    <View style={{alignItems: 'center'}}>
                      <View style={[styles.rowData, {width: '90%'}]}>
                        {cartListData.map(cartList => {
                          return (
                            <View
                              style={{
                                flexDirection: 'row',
                                justifyContent: 'space-around',
                                paddingVertical: hp('2%'),
                              }}>
                              <View
                                style={{
                                  width: '60%',
                                  alignItems: 'flex-start',
                                }}>
                                <Text numberOfLines={1}>
                                  {cartList.item_name}
                                </Text>
                                <Text style={{fontSize: hp('1.7%')}}>
                                  {parseInt(cartList.nqty) +
                                    ' x ' +
                                    cartList.rate}
                                </Text>
                                <Text style={{fontSize: hp('1.7%')}}>
                                  {'BV : ' + cartList.BV + ' ( EACH ) '}
                                </Text>
                              </View>
                              <View
                                style={{width: '40%', alignItems: 'flex-end'}}>
                                <Text>{'₹ ' + cartList.amount}</Text>
                              </View>
                            </View>
                          );
                        })}
                      </View>
                    </View>
                    <View style={{alignItems: 'center'}}>
                      <View style={[styles.rowData, {width: '90%'}]}>
                        <View style={{width: '50%', alignItems: 'flex-start'}}>
                          <Text style={{textTransform: 'capitalize'}}>
                            {'TOTAL BV POINTS ( ' + totalBv + ' )'}
                          </Text>
                        </View>
                      </View>
                      <View style={[styles.rowData, {width: '90%'}]}>
                        <View style={{width: '50%', alignItems: 'flex-start'}}>
                          <Text>Subtotal</Text>
                        </View>
                        <View style={{width: '50%', alignItems: 'flex-end'}}>
                          <Text>{'₹ ' + totalCart}</Text>
                        </View>
                      </View>
                      <View style={[styles.rowData, {width: '90%'}]}>
                        <View style={{width: '50%', alignItems: 'flex-start'}}>
                          <Text>Shipping Amount</Text>
                        </View>
                        <View style={{width: '50%', alignItems: 'flex-end'}}>
                          <Text>{'₹ ' + data1.delvch}</Text>
                        </View>
                      </View>
                    </View>
                    <View style={[styles.rowData, {width: '95%'}]}>
                      <View style={{width: '50%', alignItems: 'flex-start'}}>
                        <Text
                          style={{
                            fontSize: hp('2%'),
                            fontWeight: 'bold',
                          }}>
                          Total Amount
                        </Text>
                      </View>
                      <View style={{width: '50%', alignItems: 'flex-end'}}>
                        <Text
                          style={{
                            fontSize: hp('2%'),
                            fontWeight: 'bold',
                          }}>
                          ₹ {Number(totalCart) + Number(data1.delvch)}
                        </Text>
                      </View>
                    </View>
                  </View>
                ) : null}
              </View>
            </View>
          )}
        </ScrollView>
      </View>
    </View>
  );
};

export default Invoice;

const styles = StyleSheet.create({
  mainHeadingText: {
    fontSize: hp('2.2%'),
    fontWeight: 'bold',
    textAlign: 'center',
  },
  rowData: {
    borderBottomWidth: 1,
    paddingVertical: hp('2%'),
    borderBottomColor: '#e2e2e2',
    flexDirection: 'row',
  },
});
