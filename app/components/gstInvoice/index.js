import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
} from 'react-native';
import Header from '../../components/header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import * as allDSActions from '../../actions/allDSActions';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';

const GstInvoice = props => {
  const [gstData, setGstData] = useState([]);
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();
  const getGstData = useSelector(state => state.AllDSReducer.getGstData);

  useEffect(() => {
    if (props.route && props.route.params && props.route.params.data) {
      AsyncStorage.getItem('token').then(token => {
        if (token !== null) {
          dispatch(allDSActions.getGst(props.route.params.data, token));
        }
      });
    }
  }, []);

  useEffect(() => {
    console.warn(JSON.stringify(getGstData, undefined, 2));
    if (Object.keys(getGstData).length) {
      if (getGstData.response && getGstData.response.status == 200) {
        setGstData(getGstData.data.query[0]);
        setLoading(false);
        dispatch(allDSActions.emptyGstInvoice());
      } else {
        setLoading(false);
      }
    }
  }, [getGstData]);

  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          back={true}
          showCart={true}
          showNotification={false}
          title="Gst Invoice"
          showNotification={true}
          showCart={false}
        />
      </View>
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowRadius: 5,
          shadowOpacity: 0.2,
          margin: '5%',
          elevation: 3,
        }}>
        <ScrollView>
          {loading ? (
            <View style={{height: 500, justifyContent: 'center'}}>
              <ActivityIndicator size="large" />
            </View>
          ) : (
            <View style={{alignItems: 'center'}}>
              <View
                style={{
                  width: '95%',
                  paddingVertical: hp('3%'),
                  alignItems: 'center',
                }}>
                <View style={{alignItems: 'center'}}>
                  <Text style={styles.heading}>Tax Invoice</Text>
                  <Text style={styles.heading}>{gstData.name}</Text>
                  <Text style={styles.heading}>{gstData.logid}</Text>
                  <Text>{gstData.addr1}</Text>
                  <Text>
                    {gstData.city +
                      ' , ' +
                      gstData.state +
                      ' ' +
                      gstData.pincode}
                  </Text>
                  <Text>{'GSTIN : ' + gstData.gstno}</Text>
                </View>
                <View style={styles.rowContainer}>
                  <Text style={styles.heading}>To</Text>
                </View>
                <View style={styles.rowContainer}>
                  <Text>Safe & Secure Online Marketing Pvt. Ltd.</Text>
                  <Text>A-3/24, Janakpuri,</Text>
                  <Text>New Delhi 110058</Text>
                  <Text>GSTIN: 07AAACE9961D2ZL</Text>
                </View>
                <View style={[styles.rowContainer, {flexDirection: 'row'}]}>
                  <View style={{width: '50%'}}>
                    <Text style={styles.heading}>Date</Text>
                  </View>
                  <View style={{width: '50%', alignItems: 'flex-end'}}>
                    <Text style={{fontWeight: 'bold', fontSize: hp('2%')}}>
                      {gstData.dt}
                    </Text>
                  </View>
                </View>
                <View style={[styles.rowContainer, {flexDirection: 'row'}]}>
                  <View style={{width: '50%'}}>
                    <Text style={styles.heading}>Place of Supply</Text>
                  </View>
                  <View style={styles.rowLeft}>
                    <Text style={{fontWeight: 'bold', fontSize: hp('2%')}}>
                      {gstData.state}
                    </Text>
                  </View>
                </View>
                <View style={[styles.rowContainer, {flexDirection: 'row'}]}>
                  <View style={{width: '50%'}}>
                    <Text style={styles.heading}>State Code</Text>
                  </View>
                  <View style={styles.rowRight}>
                    <Text style={{fontWeight: 'bold', fontSize: hp('2%')}}>
                      {gstData.gstno.substring(0, 2)}
                    </Text>
                  </View>
                </View>
                <View style={styles.rowContainer}>
                  <Text style={styles.heading}>
                    {'Commission for the week ' + gstData.dt}
                  </Text>
                  <Text>(SAC Code: 9961)</Text>
                  <View
                    style={[
                      styles.rowContainer,
                      {
                        alignItems: 'center',
                      },
                    ]}>
                    <View style={{width: '90%', flexDirection: 'row'}}>
                      <View style={styles.rowLeft}>
                        <Text>Amount</Text>
                      </View>
                      <View style={styles.rowRight}>
                        <Text>{'₹ ' + gstData.topayrupees}</Text>
                      </View>
                    </View>
                    <View style={{width: '90%', flexDirection: 'row'}}>
                      <View style={styles.rowLeft}>
                        <Text>SGST Rate</Text>
                      </View>
                      <View style={styles.rowRight}>
                        {gstData.state == 'Delhi' ? (
                          <Text>9%</Text>
                        ) : (
                          <Text>-</Text>
                        )}
                      </View>
                    </View>
                    <View style={{width: '90%', flexDirection: 'row'}}>
                      <View style={styles.rowLeft}>
                        <Text>SGST Amount</Text>
                      </View>
                      <View style={styles.rowRight}>
                        {gstData.state == 'Delhi' ? (
                          <Text>{'₹ ' + (gstData.topayrupees * 9) / 100}</Text>
                        ) : (
                          <Text>-</Text>
                        )}
                      </View>
                    </View>

                    <View style={{width: '90%', flexDirection: 'row'}}>
                      <View style={styles.rowLeft}>
                        <Text>CGST Rate</Text>
                      </View>
                      <View style={styles.rowRight}>
                        {gstData.state == 'Delhi' ? (
                          <Text>9%</Text>
                        ) : (
                          <Text>-</Text>
                        )}
                      </View>
                    </View>
                    <View style={{width: '90%', flexDirection: 'row'}}>
                      <View style={styles.rowLeft}>
                        <Text>CGST Amount</Text>
                      </View>
                      <View style={styles.rowRight}>
                        {gstData.state == 'Delhi' ? (
                          <Text>{'₹ ' + (gstData.topayrupees * 9) / 100}</Text>
                        ) : (
                          <Text>-</Text>
                        )}
                      </View>
                    </View>

                    <View style={{width: '90%', flexDirection: 'row'}}>
                      <View style={styles.rowLeft}>
                        <Text>IGST Rate</Text>
                      </View>
                      <View style={styles.rowRight}>
                        {gstData.state != 'Delhi' ? (
                          <Text>9%</Text>
                        ) : (
                          <Text>-</Text>
                        )}
                      </View>
                    </View>
                    <View style={{width: '90%', flexDirection: 'row'}}>
                      <View style={styles.rowLeft}>
                        <Text>IGST Amount</Text>
                      </View>
                      <View style={styles.rowRight}>
                        {gstData.state != 'Delhi' ? (
                          <Text>{'₹ ' + (gstData.topayrupees * 18) / 100}</Text>
                        ) : (
                          <Text>-</Text>
                        )}
                      </View>
                    </View>
                    <View style={[styles.rowContainer, {flexDirection: 'row'}]}>
                      <View style={{width: '50%'}}>
                        <Text style={styles.heading}>Total Amount</Text>
                      </View>
                      <View style={styles.rowRight}>
                        <Text style={{fontWeight: 'bold', fontSize: hp('2%')}}>
                          {gstData.state == 'Delhi' ? (
                            <Text>
                              ₹
                              {Number((gstData.topayrupees * 9) / 100) +
                                Number((gstData.topayrupees * 9) / 100) +
                                Number(gstData.topayrupees)}
                            </Text>
                          ) : (
                            <Text>
                              ₹
                               {Number((gstData.topayrupees * 18) / 100) +
                                Number(gstData.topayrupees)}
                            </Text>
                          )}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          )}
        </ScrollView>
      </View>
    </View>
  );
};

export default GstInvoice;

const styles = StyleSheet.create({
  heading: {
    fontWeight: 'bold',
    fontSize: hp('2.2%'),
  },
  rowContainer: {
    borderBottomWidth: 1,
    paddingVertical: hp('2%'),
    borderBottomColor: '#e2e2e2',
    width: '100%',
  },
  rowLeft: {
    width: '50%',
    alignItems: 'flex-start',
  },
  rowRight: {
    width: '50%',
    alignItems: 'flex-end',
  },
});
