import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import {commonColor, commonButton, commonButtonText} from '../commonStyle';
import Header from '../header';
import OTPTextView from 'react-native-otp-textinput';
import Icon from 'react-native-vector-icons/Ionicons';
import Loader from '../loader';
import * as loadingActions from '../../actions/commonLoaderAction';
import {connect} from 'react-redux';
import * as otpActions from '../../actions/otpActions';
import {showMessage, hideMessage} from 'react-native-flash-message';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    padding: 5,
  },
  roundedTextInput: {
    borderRadius: 10,
    borderWidth: 4,
  },
});

class OtpScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otpValue: '',
      otpFromApi: '',
      previousScreen: '',
    };
  }

  componentDidMount() {
    // console.warn('props', this.props);
    if (
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.loginId
    ) {
      this.setState({
        loginId: this.props.route.params.loginId,
        otpFromApi: this.props.route.params.otpValue,
        previousScreen: this.props.route.params.previousScreen,
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.otpData !== this.props.otpData) {
      if (
        this.props.otpData.response &&
        this.props.otpData.response.status &&
        this.props.otpData.response.status == 200
      ) {
        this.setState({otpFromApi: this.props.otpData.data.otp});
        showMessage({
          type: 'success',
          message: 'Otp is successfully send to your registered mobile numer',
        });
      } else {
        // this.props.emptyForgot();
        showMessage({
          message: this.props.otpData.data,
          type: 'danger',
        });
      }
    }
  }

  handleOtp = otp => {
    this.setState({otpValue: otp});
    if (otp.length == 5) {
      if (this.state.otpFromApi == otp) {
        this.props.commaonLoader(false);
        showMessage({
          type: 'success',
          message: 'Verified',
        });
        if (
          this.state.previousScreen == 'profilePassword' ||
          this.state.previousScreen == 'forgotPassword'
        ) {
          console.warn('navigate');
          this.props.navigation.navigate('ResetPassword', {
            previousScreen: this.state.previousScreen,
          });
        } else if (this.state.previousScreen == 'login') {
          this.props.navigation.navigate('Login');
        } else {
          alert('error');
        }
      } else {
        this.props.commaonLoader(false);
        showMessage({
          type: 'danger',
          message: 'Otp Doen not match',
        });
      }
    }
  };

  resendOtp = () => {
    let data = {
      loginId: this.state.loginId,
    };
    this.props.sendOtp(data);
    this.props.commaonLoader(true);
  };

  verifyOtp = () => {
    this.props.commaonLoader(true);
    if (this.state.otpFromApi == this.state.otpValue) {
      this.props.commaonLoader(false);
      showMessage({
        type: 'success',
        message: 'Verified',
      });
      if (
        this.state.previousScreen == 'profilePassword' ||
        this.state.previousScreen == 'forgotPassword'
      ) {
        console.warn('navigate');
        this.props.navigation.navigate('ResetPassword');
      } else if (this.state.previousScreen == 'login') {
        this.props.navigation.navigate('login');
      } else {
        alert('error');
      }
    } else {
      this.props.commaonLoader(false);
      showMessage({
        type: 'danger',
        message: 'Otp Doen not match',
      });
    }
  };

  render() {
    // console.warn(this.state);
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <StatusBar backgroundColor="#fff" barStyle="dark-content" />
        <SafeAreaView style={{flex: 0, backgroundColor: 'white'}} />
        <SafeAreaView style={{flex: 1}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity
              style={{width: '20%', alignItems: 'center'}}
              onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-round-back" size={50} color="#000" />
            </TouchableOpacity>
            <View style={{width: '80%', alignItems: 'flex-start'}}>
              <Text
                style={{fontWeight: 'bold', fontSize: 20}}
                numberOfLines={1}>
                Enter OTP to verify
              </Text>
            </View>
          </View>
          <View style={styles.container}>
            <OTPTextView
              handleTextChange={e => this.handleOtp(e)}
              textInputStyle={styles.roundedTextInput}
              tintColor={commonColor.color}
              inputCount={5}
            />
            <View style={{marginVertical: '6%', width: '30%'}}>
              <View>
                <TouchableOpacity
                  style={[
                    commonButton,
                    {paddingVertical: '10%', borderRadius: 30},
                  ]}
                  onPress={this.verifyOtp}>
                  <Text style={commonButtonText}>Verify</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View>
              <TouchableOpacity onPress={this.resendOtp}>
                <Text
                  style={{
                    color: commonColor.color,
                    textDecorationLine: 'underline',
                  }}>
                  Click here to Resend OTP
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </SafeAreaView>
        {this.props.isLoading ? <Loader /> : null}
      </View>
    );
  }
}

export default connect(
  state => ({
    isLoading: state.CommonLoaderReducer.isLoading,
    otpData: state.OtpReducer.otpData,
  }),
  {
    ...loadingActions,
    ...otpActions,
  },
)(OtpScreen);
