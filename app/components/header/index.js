import React, {useState, useEffect} from 'react';
import {
  Button,
  View,
  Text,
  StatusBar,
  Image,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  TextInput,
} from 'react-native';
import MenuIcon from '../../assets/images/menu.png';
import Notification from '../../assets/images/notification.png';
import CartIcon from '../../assets/images/cartIcon.png';
import Logo from '../../assets/images/white_logo.png';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';
import {showMessage, hideMessage} from 'react-native-flash-message';
import UserFooter from '../..//assets/images/user_footer.png';
import * as cartActions from '../../actions/cartActions';
import {useDispatch, useSelector} from 'react-redux';

const Header = props => {
  const [cartLength, setCartLength] = useState(0);
  const dispatch = useDispatch();
  const cartList = useSelector(state => state.CartReducer.cartList);

  useEffect(() => {
    if (Object.keys(cartList).length) {
      if (
        cartList.response &&
        cartList.response.status == 200 &&
        cartList.data.cart
      ) {
        setCartLength(cartList.data.cart.length);
      } else {
        setCartLength(0);
      }
    } else {
      setCartLength(0);
    }
  }, [cartList]);

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        dispatch(cartActions.viewCart(token));
      }
    });
  }, []);

  async function addToCart() {
    let token = await AsyncStorage.getItem('token');
    if (token !== null) {
      props.navigation.navigate('Cart');
    } else {
      showMessage({
        message: 'You need to login before you want to see product in cart',
        type: 'info',
      });
      props.navigation.navigate('Login');
    }
  }
  return (
    <View>
      <StatusBar backgroundColor="#41caf1" barStyle="light-content" />
      <SafeAreaView style={styles.safeAreaContainer} />
      <View
        style={[styles.mainContainer, {height: props.headeHeight}]}
        elevation={5}>
        <View style={styles.menuIconContainer}>
          {props.back ? (
            <TouchableOpacity
              onPress={() => props.navigation.goBack()}
              style={{width: '100%', alignItems: 'center'}}>
              <Icon name="ios-arrow-round-back" size={50} color="#fff" />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => props.navigation.toggleDrawer()}
              style={styles.menuIconTouchable}>
              <Image
                source={MenuIcon}
                resizeMode="contain"
                style={styles.icons}
              />
            </TouchableOpacity>
          )}
        </View>
        <View style={styles.titleContainer}>
          {props.title == '' ? (
            <View style={styles.inputContainer}>
              <TextInput
                placeholder="Search Product"
                style={styles.input}
                onFocus={() => props.headerTextData()}
              />
            </View>
          ) : props.title ? (
            <Text
              style={
                props.comefrom !== 'dsa'
                  ? styles.title
                  : {
                      color: 'white',
                      fontSize: 20,
                    }
              }
              numberOfLines={1}>
              {props.title}
            </Text>
          ) : (
            <View style={styles.logoContainer}>
              <Image
                source={Logo}
                resizeMode="contain"
                style={{width: '80%', height: '80%'}}
              />
            </View>
          )}
        </View>
        <View style={styles.rightIconContainer}>
          {props.showCart ? (
            <TouchableOpacity
              style={
                props.showUser
                  ? styles.cartContainer
                  : {
                      width: '100%',
                      alignItems: 'center',
                      height: props.headeHeight,
                      justifyContent: 'center',
                    }
              }
              onPress={addToCart}>
              <Image
                source={CartIcon}
                resizeMode="contain"
                style={styles.icons}
              />
              {cartLength > 0 ? (
                <View
                  style={
                    props.showUser
                      ? styles.cartnumberContainer1
                      : styles.cartnumberContainer2
                  }>
                  <Text style={{color: 'white'}}>{cartLength}</Text>
                </View>
              ) : null}
            </TouchableOpacity>
          ) : (
            <View style={styles.cartContainer} />
          )}
          {/* {props.showNotification ? (
            <TouchableOpacity
              style={styles.notificationContainer}
              onPress={() => props.navigation.navigate('Notifications')}>
              <Image
                source={Notification}
                resizeMode="contain"
                style={styles.icons}
                tintColor="white"
              />
            </TouchableOpacity>
          ) : null} */}
          {props.showUser ? (
            <TouchableOpacity
              style={styles.notificationContainer}
              onPress={() => props.navigation.navigate('Login')}>
              <Image
                source={UserFooter}
                resizeMode="contain"
                style={styles.icons}
                tintColor="white"
              />
            </TouchableOpacity>
          ) : null}
        </View>
        <View style={{width: '3%'}} />
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  safeAreaContainer: {
    flex: 0,
    backgroundColor: '#41caf1',
  },
  mainContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    // height: 60,
    backgroundColor: '#41caf1',
    shadowColor: '#e2e2e2',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 0.5,
  },
  menuIconTouchable: {
    width: '100%',
    alignItems: 'center',
    height: '100%',
    justifyContent: 'center',
  },
  menuIconContainer: {
    width: '20%',
    alignItems: 'center',
  },
  icons: {
    width: 25,
    height: 25,
    tintColor: 'white',
  },
  notificationContainer: {
    width: '50%',
    alignItems: 'center',
    // height: 60,
    justifyContent: 'center',
  },
  cartContainer: {
    width: '50%',
    alignItems: 'center',
    // height: 60,
    justifyContent: 'center',
  },
  rightIconContainer: {
    width: '20%',
    alignItems: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    color: 'white',
    fontSize: 20,
    textTransform: 'capitalize',
  },
  titleContainer: {
    width: '57%',
    alignItems: 'center',
  },
  inputContainer: {
    width: '100%',
    marginHorizontal: '10%',
    justifyContent: 'center',
  },
  input: {
    paddingLeft: '5%',
    height: '80%',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
  },
  logoContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cartnumberContainer1: {
    position: 'absolute',
    borderWidth: 1,
    width: 20,
    height: 20,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    left: 25,
    bottom: 10,
    backgroundColor: '#000a23',
  },
  cartnumberContainer2: {
    position: 'absolute',
    borderWidth: 1,
    width: 20,
    height: 20,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    left: 45,
    bottom: 27,
    backgroundColor: '#000a23',
  },
});
