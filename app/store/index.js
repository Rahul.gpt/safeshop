import {combineReducers, createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import LoginReducer from '../reducers/loginReducer';
import CommonLoaderReducer from '../reducers/commonLoaderReducer';
import ForgotPasswordReducer from '../reducers/forgotPasswordReducer';
import CategoryReducer from '../reducers/categoryReducer';
import SearchReducer from '../reducers/searchProductReducer';
import ProductListingReducer from '../reducers/productListingReducer';
import GenealogyReducer from '../reducers/genealogyReducer';
import CartReducer from '../reducers/cartReducer';
import ProfileReducer from '../reducers/profileReducer';
import OtpReducer from '../reducers/otpReducer';
import RetailOrderReducer from '../reducers/retailOrderReducer';
import AllDSReducer from '../reducers/allDSReducer';
import GrievenceReducer from '../reducers/grievenceReducer';
import BilldeskReducer from '../reducers/billdeskReducer';
import EventReducer from '../reducers/eventReducer';

const rootReducer = combineReducers({
  LoginReducer,
  CommonLoaderReducer,
  ForgotPasswordReducer,
  CategoryReducer,
  SearchReducer,
  ProductListingReducer,
  GenealogyReducer,
  CartReducer,
  ProfileReducer,
  OtpReducer,
  RetailOrderReducer,
  AllDSReducer,
  GrievenceReducer,
  BilldeskReducer,
  EventReducer,
});

let store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
