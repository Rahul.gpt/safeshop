// In App.js in a new project

import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  StatusBar,
  ActivityIndicator,
} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import TabNav from './tabNavigation/index';
import Login from '../screens/login';
import ForgotPassword from '../screens/forgotPassword';
import Signup from '../screens/singup';
import ResetPassword from '../screens/resetPassword';
import OtpScreen from '../components/otpScreen';
import FirstLogin from '../screens/firstLogin';
import Agreement from '../screens/agreement';
import Declaration from '../screens/declaration';
import TnC from '../screens/tnc';
import BuyForGuset from '../screens/buyForGuest';
import OrderGuestConfirmation from '../screens/orderGuestProduct';
import AppIntroSlider from 'react-native-app-intro-slider';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {commonColor} from '../components/commonStyle';
import AsyncStorage from '@react-native-community/async-storage';

const Stack = createStackNavigator();

const slides = [
  {
    key: 1,
    title: 'One  Shop Many Brands',
    text:
      "It is a long established fact that a reader will be distracted by the readable content of a page when looking at it's layout ",
    image: require('../assets/images/intro1.png'),
    backgroundColor: '#eaf7f7',
  },
  {
    key: 2,
    title: 'Epitome of Feminity',
    text:
      'It is a long established fact that a reader will be distracted by the readable content of a page when looking at it"s layout ',
    image: require('../assets/images/intro2.png'),
    backgroundColor: '#f3f1f8',
  },
  {
    key: 3,
    title: 'Network Marketing',
    text:
      "It is a long established fact that a reader will be distracted by the readable content of a page when looking at it's layout ",
    image: require('../assets/images/intro3.png'),
    backgroundColor: '#f2f5f8',
  },
];

function MainNav() {
  const [bool, setBool] = useState(true);
  const [showIntro, setIntro] = useState(true);

  useEffect(() => {
    AsyncStorage.getItem('showIntro').then(data => {
      setBool(false);
      if (data !== null) {
        setIntro(false);
      } else {
        setIntro(true);
      }
    });
  }, []);

  const _renderItem = ({item}) => {
    return (
      <View style={[styles.container, {backgroundColor: item.backgroundColor}]}>
        {/* <Text style={styles.bannerText}>{item.title}</Text> */}
        <Image
          source={item.image}
          resizeMode="contain"
          style={{width: wp('50%'), height: hp('50%')}}
        />
        <Text style={styles.bannerText}>{item.title}</Text>
        <View style={{paddingTop: hp('2%')}}>
          <Text style={{textAlign: 'center', fontSize: hp('2.2%')}}>
            {item.text}
          </Text>
        </View>
      </View>
    );
  };
  const _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Text style={{color: 'white'}}>Next</Text>
      </View>
    );
  };
  const _renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Text style={{color: 'white'}}>Done</Text>
      </View>
    );
  };

  const _handleIntro = () => {
    AsyncStorage.setItem('showIntro', 'false');
    setIntro(false);
  };

  const _renderSkipButton = () => {
    return (
      <View style={[styles.buttonCircle, {backgroundColor: 'transparent'}]}>
        <Text style={{color: 'black'}}>Skip</Text>
      </View>
    );
  };

  return bool == false ? (
    showIntro == false ? (
      <Stack.Navigator
        headerMode="none"
        screenOptions={{
          gestureEnabled: true,
          gestureDirection: 'horizontal',
        }}>
        <Stack.Screen name="Home" component={TabNav} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
        <Stack.Screen name="Signup" component={Signup} />
        <Stack.Screen name="ResetPassword" component={ResetPassword} />
        <Stack.Screen name="OtpScreen" component={OtpScreen} />
        <Stack.Screen name="FirstLogin" component={FirstLogin} />
        <Stack.Screen name="Agreement" component={Agreement} />
        <Stack.Screen name="Declaration" component={Declaration} />
        <Stack.Screen name="TnC" component={TnC} />
        <Stack.Screen name="BuyForGuset" component={BuyForGuset} />
        <Stack.Screen
          name="OrderGuestConfirmation"
          component={OrderGuestConfirmation}
        />
      </Stack.Navigator>
    ) : (
      <View style={{flex: 1}}>
        <StatusBar backgroundColor="transparent" />
        <AppIntroSlider
          data={slides}
          keyExtractor={item => item.image}
          renderDoneButton={_renderDoneButton}
          renderNextButton={_renderNextButton}
          renderSkipButton={_renderSkipButton}
          renderItem={_renderItem}
          bottomButton={true}
          showSkipButton
          showNextButton
          bottomButton
          activeDotStyle={{backgroundColor: commonColor.color}}
          onDone={_handleIntro}
        />
      </View>
    )
  ) : (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <ActivityIndicator size="large" color={commonColor.color} />
    </View>
  );
}

export default MainNav;

const styles = StyleSheet.create({
  buttonCircle: {
    width: '100%',
    alignItems: 'center',
    paddingVertical: '4%',
    backgroundColor: commonColor.color,
  },
  bannerText: {
    fontSize: hp('3%'),
    fontWeight: 'bold',
    textAlign: 'center',
  },
  container: {
    height: hp('100%'),
    alignItems: 'center',
    width: '100%',
    paddingHorizontal: 10,
  },
});
