import * as React from 'react';
import {Button, View} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import DrawerNav from '../drawerNavigation/index';
import ProductListing from '../../screens/productListing/index';
import Cart from '../../screens/cart/index';
import ProductDetail from '../../screens/productDetail/index';
import SearchProduct from '../../screens/searchProduct';
import Category from '../../screens/category/index';
import DSHome from '../../screens/home/index';
import NewDSSignup from '../../screens/newDSSignup';
import ProfileUpdate from '../../screens/updateProfile';
import AddTeamMemebr from '../../screens/addTeamMember';
import Checkout from '../../screens/checkout';
import Notifications from '../../screens/notifications';
import BankDetails from '../../screens/bankDetails';
import RetailOrderList from '../../screens/viewRetailOrdersList';
import CourierDetail from '../../screens/courierDetail';
import RetailProductBought from '../../screens/retailProductBought';
import ChequeWeek from '../../screens/chequeWek';
import RetailBusinessVolume from '../../screens/retailBusinessVolume';
import NewDSAList from '../../screens/newDSAApproveList';
import PromotionStatus from '../../screens/promotionStatus';
import WeeklyPayment from '../../screens/weekPayment';
import Invoice from '../../components/invoice';
import Products from '../../screens/allProducts';
import EditAddress from '../../screens/editAddress';
import Declaration from '../../screens/DSDeclaration';
import Legal from '../../screens/legal';
import GrievenceCell from '../../screens/grievenceCell';
import DSInfo from '../../screens/DSInfo';
import GstInvoice from '../../components/gstInvoice';
import NextLevelStatus from '../../screens/nextLevelStatus';
import Contactus from '../../screens/contactus';
import Events from '../../screens/events';
import ViewEvents from '../../screens/viewEvents';

const Stack = createStackNavigator();

export default function StackNav() {
  return (
    <Stack.Navigator
      initialRouteName="DrawerNav"
      headerMode="none"
      screenOptions={{
        gestureEnabled: true,
        gestureDirection: 'horizontal',
      }}>
      <Stack.Screen name="DrawerNav" component={DrawerNav} />
      <Stack.Screen name="ProductListing" component={ProductListing} />
      <Stack.Screen name="Cart" component={Cart} />
      <Stack.Screen name="ProductDetail" component={ProductDetail} />
      <Stack.Screen name="SearchProduct" component={SearchProduct} />
      <Stack.Screen name="AddTeamMemebr" component={AddTeamMemebr} />
      <Stack.Screen name="Checkout" component={Checkout} />
      <Stack.Screen name="UpdateProfile" component={ProfileUpdate} />
      <Stack.Screen name="Notifications" component={Notifications} />
      <Stack.Screen name="BankDetails" component={BankDetails} />
      <Stack.Screen name="NewDSSignup" component={NewDSSignup} />
      <Stack.Screen name="RetailOrderList" component={RetailOrderList} />
      <Stack.Screen name="CourierDetail" component={CourierDetail} />
      <Stack.Screen name="ChequeWeek" component={ChequeWeek} />
      <Stack.Screen name="NewDSAList" component={NewDSAList} />
      <Stack.Screen name="PromotionStatus" component={PromotionStatus} />
      <Stack.Screen name="WeeklyPayment" component={WeeklyPayment} />
      <Stack.Screen
        name="RetailProductBought"
        component={RetailProductBought}
      />
      <Stack.Screen
        name="RetailBusinessVolume"
        component={RetailBusinessVolume}
      />
      <Stack.Screen name="Invoice" component={Invoice} />
      <Stack.Screen name="Products" component={Products} />
      <Stack.Screen name="EditAddress" component={EditAddress} />
      <Stack.Screen name="Declaration" component={Declaration} />
      <Stack.Screen name="Legal" component={Legal} />
      <Stack.Screen name="GrievenceCell" component={GrievenceCell} />
      <Stack.Screen name="DSInfo" component={DSInfo} />
      <Stack.Screen name="GstInvoice" component={GstInvoice} />
      <Stack.Screen name="NextLevelStatus" component={NextLevelStatus} />
      <Stack.Screen name="Contactus" component={Contactus} />
      <Stack.Screen name="Events" component={Events} />
      <Stack.Screen name="ViewEvents" component={ViewEvents} />
    </Stack.Navigator>
  );
}
