import React, {useState, useEffect} from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import DrawerNav from '../drawerNavigation/index';
import Header from '../../components/header/index';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MenuFooter from '../../assets/images/menu_footer.png';
import Offer from '../../assets/images/offer_footer.png';
import Search from '../../assets/images/search_footer.png';
import CartFooter from '../../assets/images/cartFooter.png';
import UserFooter from '../..//assets/images/user_footer.png';
import StackNav from '../stackNavigation/index';
import SearchProduct from '../../screens/searchProduct';
import ProfileUpdate from '../../screens/updateProfile';
import Cart from '../../screens/cart/index';
import * as cartActions from '../../actions/cartActions';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../../screens/login';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

function SearchProductData() {
  return (
    <Stack.Navigator
      initialRouteName="SearchProduct"
      headerMode="none"
      screenOptions={{
        gestureEnabled: true,
        gestureDirection: 'horizontal',
      }}>
      <Stack.Screen name="SearchProduct" component={SearchProduct} />
    </Stack.Navigator>
  );
}

export default function TabNav() {
  const [cartLength, setCartLength] = useState(0);
  const [tokenData, setToken] = useState(false);

  const dispatch = useDispatch();

  const cartList = useSelector(state => state.CartReducer.cartList);
  const loginUserData = useSelector(state => state.LoginReducer.loginUserData);

  // const token = await AsyncStorage.getItem('token');

  useEffect(() => {
    // console.warn("cartList", cartList)
    if (Object.keys(cartList).length) {
      if (
        cartList.response &&
        cartList.response.status == 200 &&
        cartList.data.cart &&
        cartList.data.cart.length > 0
      ) {
        setCartLength(cartList.data.cart.length);
      } else {
        setCartLength(0);
      }
    } else {
      setCartLength(0);
    }
  }, [cartList]);

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        dispatch(cartActions.viewCart(token));
        setToken(token);
      }
    });
  }, []);

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        setToken(token);
      }
    });
  }, [loginUserData]);

  useEffect(() => {
    if (Object.keys(loginUserData).length > 0) {
      setToken(true);
    } else {
      setToken(false);
    }
  }, [loginUserData]);

  return (
    <Tab.Navigator
      tabBarOptions={{
        showLabel: false,
        activeTintColor: '#41caf1',
        inactiveTintColor: 'gray',
        style: {
          borderTopWidth: 1,
          width: '100%',
          alignItems: 'center',
          justifyContent: 'center',
        },
      }}
      backBehavior="initialRoute">
      <Tab.Screen
        name="Home"
        component={StackNav}
        options={{
          tabBarIcon: ({color, size}) => (
            <Image
              source={MenuFooter}
              resizeMode="contain"
              style={[styles.iconStyle, {tintColor: color}]}
              tintColor={color}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Search"
        component={SearchProductData}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: ({color, size}) => (
            <Image
              source={Search}
              resizeMode="contain"
              style={[styles.iconStyle, {tintColor: color}]}
              tintColor={color}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Cart"
        component={tokenData ? Cart : Login}
        options={{
          tabBarLabel: 'Cart',
          tabBarIcon: ({color, size}) => (
            <View>
              <Image
                source={CartFooter}
                resizeMode="contain"
                style={[styles.iconStyle, {tintColor: color}]}
                tintColor={color}
              />
              {cartLength != 0 ? (
                <View
                  style={{
                    position: 'absolute',
                    borderWidth: 1,
                    width: 20,
                    height: 20,
                    borderRadius: 10,
                    justifyContent: 'center',
                    alignItems: 'center',
                    left: 10,
                    bottom: 10,
                    backgroundColor: '#000a23',
                  }}>
                  <Text style={{color: 'white'}}>{cartLength}</Text>
                </View>
              ) : null}
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="User"
        component={tokenData ? ProfileUpdate : Login}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({color, size}) => (
            <Image
              source={UserFooter}
              resizeMode="contain"
              style={[styles.iconStyle, {tintColor: color}]}
              tintColor={color}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  iconStyle: {
    width: 20,
    height: 20,
  },
});
