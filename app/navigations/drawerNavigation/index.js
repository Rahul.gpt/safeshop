import React, {useState, useEffect} from 'react';
import {Button, View, ActivityIndicator, BackHandler} from 'react-native';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import Header from '../../components/header/index';
import Category from '../../screens/category/index';
import DSHome from '../../screens/home/index';
import DrawerComponent from '../../components/customDrawer';
import NewDSSignup from '../../screens/newDSSignup';
import ProfileUpdate from '../../screens/updateProfile';
import TabNav from '../tabNavigation';
import AsyncStorage from '@react-native-community/async-storage';
import * as categoryAction from '../../actions/categoryAction';
import {useDispatch, useSelector} from 'react-redux';

const Drawer = createDrawerNavigator();

export default function DrawerNav(props) {
  const [initialRoutes, setInitialRoute] = useState('');
  const [navBool, setNavBool] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    props.navigation.addListener('focus', () => setNavigationRoute());
   dispatch(categoryAction.getCategory());
    async function setNavigationRoute() {
      console.warn('event');
      let token = await AsyncStorage.getItem('token');
      if (token !== null) {
        // console.warn('if', token);
        setInitialRoute('DSHome');
        setNavBool(true);
      } else {
        // console.warn('else');
        setInitialRoute('Category');
        setNavBool(true);
      }
    }
  }, []);

  return navBool ? (
    <Drawer.Navigator
      initialRouteName={initialRoutes}
      drawerStyle={{
        backgroundColor: '#000a23',
        width: '70%',
      }}
      drawerContentOptions={{
        activeTintColor: 'white',
        inactiveTintColor: 'white',
      }}
      drawerType="slide"
      drawerContent={props => <DrawerComponent {...props} />}
      backBehavior="order">
      <Drawer.Screen name="Category" component={Category} />
      <Drawer.Screen name="DSHome" component={DSHome} />
    </Drawer.Navigator>
  ) : (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <ActivityIndicator size="large" color={'#41caf1'} />
    </View>
  );
}
