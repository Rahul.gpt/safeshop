import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  TextInput,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import Logo from '../../assets/images/logo.png';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from '../../components/commonTextInput';
import {
  InputTextLabel,
  commonColor,
  commonButton,
  commonButtonText,
  logoContainer,
  commonScreenWidth,
  mainContainer,
  logoStyle,
} from '../../components/commonStyle';
import * as loginActions from '../../actions/loginAction';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';
import AsyncStorage from '@react-native-community/async-storage';
import * as cartActions from '../../actions/cartActions';
import * as forgotPasswordAction from '../../actions/forgotPasswordAction';
import OtpModal from '../../components/otpModal';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

function Login(props) {
  const [state, setState] = useState({
    email: '',
    password: '',
    showOtpModal: false,
    otpValue: '12345',
    userToken: '',
  });

  const dispatch = useDispatch();
  const loginData = useSelector(state => state.LoginReducer.loginData);
  const isLoading = useSelector(state => state.CommonLoaderReducer.isLoading);

  useEffect(() => {
    props.navigation.addListener('focus', () => setUserCredentials());
    async function setUserCredentials() {
      let userCredentials = await AsyncStorage.getItem('userCredentials');
      let userCredentialsData = JSON.parse(userCredentials);
      if (userCredentialsData !== null) {
        setState({
          ...state,
          email: userCredentialsData.logid,
          password: userCredentialsData.password,
        });
      } else {
        setState({
          ...state,
          email: '',
          password: '',
        });
      }
    }
    // setUserCredentials();
  }, []);

  useEffect(() => {
    if (Object.keys(loginData).length) {
      if (loginData.response && loginData.response.status == 200) {
        if (loginData.logindata[0].disableflag == '1') {
          showMessage({
            type: 'danger',
            message: 'Your logid is disabled',
          });
        }
        if (loginData.logindata[0].statusflag == 'N') {
          let token = loginData.data.jwt;
          let loginId = loginData.logindata[0].logid;
          let cellno = loginData.logindata[0].cellno;
          console.warn('cell', cellno);
          props.navigation.navigate('ResetPassword', {
            previousScreen: 'login',
            oldPasswordField: true,
            userToken: token,
            loginId: loginId,
            cellno: cellno,
          });
          showMessage({
            type: 'warning',
            message: 'You need to change your password',
          });
          dispatch(loginActions.emptyLogin());
        } else {
          let token = loginData.data.jwt;
          AsyncStorage.setItem('token', token);
          let initialData = loginData.initdata;
          AsyncStorage.setItem('status', JSON.stringify(initialData));
          AsyncStorage.setItem(
            'userCredentials',
            JSON.stringify(loginData.logindata[0]),
          );
          showMessage({
            message: 'Login Successfully',
            type: 'success',
          });
          dispatch(loginActions.emptyLogin());
          dispatch(loginActions.userData(loginData.logindata[0]));
          dispatch(cartActions.viewCart(token));
          props.navigation.navigate('DSHome');
        }
      } else if (loginData.response && loginData.response.status == 204) {
        dispatch(loginActions.emptyLogin());
        showMessage({
          message: 'Invalid Logid or Password... Please try again.',
          type: 'danger',
        });
      } else {
        console.warn('els');
        showMessage({
          message: loginData.response.message,
          type: 'danger',
        });
      }
    }
  }, [loginData]);

  async function loginDataApi() {
    let fcmToken = await AsyncStorage.getItem('fcmtoken');
    console.warn('fcmtoken', fcmToken);
    if (!state.email || !state.password) {
      showMessage({
        message: 'All fields are mandatory',
        type: 'danger',
      });
    } else {
      let data = {
        email: state.email,
        password: state.password,
        Authentication: fcmToken,
      };
      dispatch(loginActions.loginUser(data));
      dispatch(loadingAction.commaonLoader(true));
    }
  }

  const handleModal = () => {
    setState({...state, showOtpModal: false});
  };

  const crossModal = () => {
    setState({...state, showOtpModal: false});
  };

  const verifiedStatus = value => {
    console.warn('value from child', value);
    setState({...state, showOtpModal: false});
    if (value == true) {
      props.navigation.navigate('ResetPassword', {
        previousScreen: 'login',
        oldPasswordField: true,
        userToken: state.userToken,
      });
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      {/* <SafeAreaView style={{flex : 0}}/> */}
      <SafeAreaView>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity style={{width: '20%', alignItems: 'center'}}>
            <Icon
              name="ios-arrow-round-back"
              size={50}
              color="#000"
              onPress={() => props.navigation.goBack()}
            />
          </TouchableOpacity>
          <View style={{width: '80%'}}>
            <Text style={{fontSize: hp('2.2%')}} numberOfLines={1}>
              Login
            </Text>
          </View>
        </View>
        <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
          <View style={[mainContainer, {paddingBottom: '50%'}]}>
            <View style={commonScreenWidth}>
              <View style={logoContainer}>
                <Image source={Logo} style={logoStyle} resizeMode="contain" />
              </View>
              <View>
                <View>
                  <Text style={InputTextLabel}>logid</Text>
                </View>
                <View>
                  <CustomTextInput
                    value={state.email}
                    changeText={e => setState({...state, email: e})}
                    placeholder="Enter Your logid"
                  />
                </View>
              </View>
              <View style={{marginTop: '6%'}}>
                <View>
                  <Text style={InputTextLabel}>password</Text>
                </View>
                <View>
                  <CustomTextInput
                    value={state.password}
                    changeText={e => setState({...state, password: e})}
                    secureText={true}
                    placeholder="Enter Your Password"
                  />
                </View>
              </View>
              <View style={{alignItems: 'flex-end'}}>
                <TouchableOpacity
                  onPress={() => props.navigation.navigate('ForgotPassword')}>
                  <Text style={commonColor}>Forgot password ?</Text>
                </TouchableOpacity>
              </View>
              <View style={{marginVertical: '6%'}}>
                <View>
                  <TouchableOpacity style={commonButton} onPress={loginDataApi}>
                    <Text style={commonButtonText}>login</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
        {isLoading ? <Loader /> : null}
        {state.showOtpModal ? (
          <OtpModal
            showOtpModal={state.showOtpModal}
            handleModal={handleModal}
            crossModal={crossModal}
            otpValue={state.otpValue}
            verifiedStatus={verifiedStatus}
            loginID={state.email}
          />
        ) : null}
      </SafeAreaView>
    </View>
  );
}

export default Login;
