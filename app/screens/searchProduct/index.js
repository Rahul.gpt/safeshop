import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
  StyleSheet,
  TextInput,
  Image,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import Header from '../../components/header';
import ProductCard from '../productCard.js/index';
import {commonColor} from '../../components/commonStyle';
import Icon from 'react-native-vector-icons/Ionicons';
import CartIcon from '../../assets/images/cartIcon.png';
import * as searchActions from '../../actions/searchAction';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';
import AsyncStorage from '@react-native-community/async-storage';

const SearchProduct = props => {
  const [showCart, setCart] = useState(false);
  const [searchKey, setSearchKey] = useState('');
  const [searchResult, setSearchResult] = useState([]);

  const dispatch = useDispatch();
  const searchData = useSelector(state => state.SearchReducer.searchData);
  const isLoading = useSelector(state => state.CommonLoaderReducer.isLoading);
  const getSerachKey = useSelector(state => state.SearchReducer.getSerachKey);

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      console.warn('getSearchKey', getSerachKey);

      if (getSerachKey.trim().length) {
        setCart(true);
        setSearchKey(getSerachKey);
        dispatch(searchActions.searchProducts(getSerachKey));
      } else {
        setSearchKey('');
        setSearchResult([]);
      }
    });

    return unsubscribe;
  }, [props.navigation]);

  useEffect(() => {
    if (Object.keys(searchData).length > 0) {
      if (searchData.response.status == 200) {
        setSearchResult(searchData.data.products);
        dispatch(searchActions.emptySearchData());
      }
    }
  }, [searchData]);

  useEffect(() => {
    console.warn('searchKey', searchKey);
  });

  const SearchProductData = () => {
    if (searchKey.trim().length == 0) {
      showMessage({
        message: 'Type something to search',
        type: 'danger',
      });
    } else {
      setSearchResult([]);
      dispatch(searchActions.searchProducts(searchKey));
      dispatch(loadingAction.commaonLoader(true));
      setCart(true);
    }
  };

  const _renderList = (item, index) => {
    return (
      <TouchableOpacity
        style={styles.listItem}
        onPress={() =>
          props.navigation.navigate('ProductDetail', {
            packgeName: item.package,
            name: item.detl,
          })
        }>
        <ProductCard data={item} {...props} />
      </TouchableOpacity>
    );
  };

  async function addToCart() {
    let token = await AsyncStorage.getItem('token');
    if (token !== null) {
      props.navigation.navigate('Cart');
    } else {
      showMessage({
        message: 'You need to login before you want to add product in cart',
        type: 'info',
      });
      props.navigation.navigate('Login');
    }
  }

  return (
    <View style={{flex: 1}}>
      <StatusBar backgroundColor={commonColor.color} />
      <SafeAreaView style={{backgroundColor: commonColor.color, flex: 0}} />
      <SafeAreaView style={{flex: 1}}>
        <View
          style={{
            flexDirection: 'row',
            height: 60,
            backgroundColor: showCart ? commonColor.color : 'white',
            alignItems: 'center',
          }}
          elevation={5}>
          <View style={{width: '20%', alignItems: 'center'}}>
            <Icon
              name="ios-arrow-round-back"
              size={50}
              color={showCart ? 'white' : 'black'}
              onPress={() => props.navigation.goBack()}
            />
          </View>
          <View style={{width: '60%', alignItems: 'flex-start'}}>
            {showCart ? (
              <TouchableOpacity
                style={{
                  width: '100%',
                  height: '100%',
                  alignItems: 'center',
                  flexDirection: 'row',
                }}
                onPress={() => setCart(false)}>
                <View style={{width: '50%', alignItems: 'flex-start'}}>
                  <Text style={{color: 'white'}}>{searchKey}</Text>
                </View>
                <View style={{width: '50%', alignItems: 'flex-end'}}>
                  <Icon name="ios-search" size={25} color="white" />
                </View>
              </TouchableOpacity>
            ) : (
              <TextInput
                style={{width: '100%', height: '100%'}}
                autoFocus
                returnKeyType="search"
                placeholder="Search Product"
                onSubmitEditing={() => SearchProductData()}
                value={searchKey}
                onChangeText={e => {
                  setSearchKey(e);
                  dispatch(searchActions.serachkeyData(e));
                }}
              />
            )}
          </View>
          {showCart ? (
            <TouchableOpacity
              style={{width: '20%', alignItems: 'center'}}
              onPress={addToCart}>
              <Image
                source={CartIcon}
                resizeMode="contain"
                style={{
                  width: 25,
                  height: 25,
                }}
              />
            </TouchableOpacity>
          ) : null}
        </View>
        <View style={{flex: 1}}>
          {searchResult.length > 0 ? (
            <FlatList
              data={searchResult}
              renderItem={({item, index}) => _renderList(item, index)}
              numColumns={2}
              style={{marginHorizontal: 10}}
            />
          ) : (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                height: 500,
              }}>
              <Text>No data found</Text>
            </View>
          )}
        </View>
        {isLoading ? <Loader /> : null}
      </SafeAreaView>
    </View>
  );
};

export default SearchProduct;

const styles = StyleSheet.create({
  subCategoryContainer: {
    height: 50,
    width: '100%',
    backgroundColor: 'white',
  },
  subCategoryInnerContainer: {
    height: 50,
    width: 90,
    justifyContent: 'center',
    alignItems: 'center',
  },
  listItem: {
    width: '45%',
    marginVertical: '4%',
    marginHorizontal: 10,
    borderRadius: 20,
  },
});
