import React, {useState, useEffect, Component} from 'react';
import Header from '../../components/header';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Image,
  ActivityIndicator,
} from 'react-native';
import BannerBox from '../../assets/images/box.png';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
// import UserFooter from '../..//assets/images/user_footer.png';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/Feather';
import Icon1 from 'react-native-vector-icons/Entypo';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import * as genealogyActions from '../../actions/genealogyActions';
import Loader from '../../components/loader';
import * as loadingActions from '../../actions/commonLoaderAction';
import {showMessage, hideMessage} from 'react-native-flash-message';
import Modal from 'react-native-modal';
import UserFooter from '../../assets/images/profilePlaceholder.png';
import * as profileActions from '../../actions/profileAction';

var abc = 0;

class NewDSSignup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      parentId: '',
      scrollOffset: null,
      child11: {
        id: '',
        paymentStatus: 0,
        profilePhoto: null,
        imageLoading: true,
      },
      child12: {
        id: '',
        paymentStatus: 0,
        profilePhoto: null,
        imageLoading: true,
      },
      child21: {
        id: '',
        paymentStatus: 0,
        profilePhoto: null,
        imageLoading: true,
      },
      child22: {
        id: '',
        paymentStatus: 0,
        profilePhoto: null,
        imageLoading: true,
      },
      child31: {
        id: '',
        paymentStatus: 0,
        profilePhoto: null,
        imageLoading: true,
      },
      child32: {
        id: '',
        paymentStatus: 0,
        profilePhoto: null,
        imageLoading: true,
      },
      visibleModal: false,
      token: '',
      parentName: '',
      referId: '',
      referName: '',
      addPosition: '',
      parentProfile: '',
      profileImageUrl: '',
      imageLoading: true,
      parentPaymentStatus: 0,
    };
    this.scrollViewRef = React.createRef();
  }

  async componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', async () => {
      if (
        (this.props.route &&
          this.props.route.params &&
          this.props.route.params.logid) ||
        this.props.route.params.previousScreen
      ) {
        //console.warn('previousScreen', this.props.route.params.previousScreen);
        let token = await AsyncStorage.getItem('token');
        let userData = await AsyncStorage.getItem('userCredentials');
        if (token !== null) {
          this.props.commaonLoader(true);
          this.props.makeChildAsParent(this.props.route.params.logid, token);
          this.setState({token: token});
        }
      }
    });

    // this._unsubscribe = this.props.navigation.addListener('focus', async () => {
    //   // do something
    //   let token = await AsyncStorage.getItem('token');
    //   let userData = await AsyncStorage.getItem('userCredentials');
    //   if (token !== null) {
    //     this.setState({token: token});
    //     this.props.commaonLoader(true);
    //     this.props.getGenealogy(token);
    //   }
    //   // userData = JSON.parse(userData);
    //   // if (userData !== null) {
    //   // }
    // });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.getGenealogyData !== this.props.getGenealogyData) {
      let {
        child11,
        child12,
        child21,
        child22,
        child31,
        child32,
        parentId,
      } = this.state;
      if (this.props.getGenealogyData.response.status == 200) {
        let members = this.props.getGenealogyData.data.members;
        var profileImageUrl = this.state.profileImageUrl;
        var parentPaymentStatus = this.props.getGenealogyData.data.members
          .$parentvst;
        profileImageUrl = this.props.getGenealogyData.data.photopath;
        child11.id = members.$vmem11;
        child12.id = members.$vmem12;
        child21.id = members.$vvmem21;
        child22.id = members.$vvmem22;
        child31.id = members.$vvmem31;
        child32.id = members.$vvmem32;
        parentId = members.$lid;
        var parentProfile = '';
        if (this.props.getGenealogyData.data.member[0].photo == null) {
          parentProfile = this.props.getGenealogyData.data.member[0].photo;
        } else {
          parentProfile =
            this.props.getGenealogyData.data.member[0].photo + `?${abc}`;
        }

        this.props.getGenealogyData.data.members.$child.map(childData => {
          if (childData.logid == child11.id) {
            child11.paymentStatus = childData.vst;
            if (childData.photo == null) {
              child11.profilePhoto = childData.photo;
            } else {
              child11.profilePhoto = childData.photo + `?${abc}`;
            }
          } else if (childData.logid == child12.id) {
            child12.paymentStatus = childData.vst;
            if (childData.photo == null) {
              child12.profilePhoto = childData.photo;
            } else {
              child12.profilePhoto = childData.photo + `?${abc}`;
            }
          } else if (childData.logid == child21.id) {
            child21.paymentStatus = childData.vst;
            if (childData.photo == null) {
              child21.profilePhoto = childData.photo;
            } else {
              child21.profilePhoto = childData.photo + `?${abc}`;
            }
          } else if (childData.logid == child22.id) {
            child22.paymentStatus = childData.vst;
            if (childData.photo == null) {
              child22.profilePhoto = childData.photo;
            } else {
              child22.profilePhoto = childData.photo + `?${abc}`;
            }
          } else if (childData.logid == child31.id) {
            child31.paymentStatus = childData.vst;
            if (childData.photo == null) {
              child31.profilePhoto = childData.photo;
            } else {
              child31.profilePhoto = childData.photo + `?${abc}`;
            }
          } else if (childData.logid == child32.id) {
            child32.paymentStatus = childData.vst;
            if (childData.photo == null) {
              child32.profilePhoto = childData.photo;
            } else {
              child32.profilePhoto = childData.photo + `?${abc}`;
            }
          }
        });
      }
      this.setState({
        child11,
        child12,
        child21,
        child22,
        child31,
        child32,
        parentId,
        profileImageUrl,
        parentProfile,
        parentPaymentStatus,
      });
      abc++;
    }

    if (prevProps.approveDS !== this.props.approveDS) {
      if (
        this.props.approveDS.response &&
        this.props.approveDS.response.status == 200
      ) {
        this.props.navigation.navigate('NewDSAList');
        showMessage({
          type: 'success',
          message: 'DSA approved successfully and placed in genealogy tree',
        });
      }
    }

    // if (prevProps.getProfileData !== this.props.getProfileData) {
    //   if (this.props.getProfileData.response.status == 200) {
    //     let data = this.props.getProfileData.data.query[0];
    //     this.setState({
    //       parentProfile: this.props.getProfileData.data.photopath + data.photo,
    //     });
    //   }
    // }
  }

  componentWillUnmount() {
    // this._unsubscribe();
  }

  AddTeamMember = position => {
    let {child11, child12, child21, child22, child31, child32} = this.state;
    if (position == '11' && child11.id === 'Empty') {
      let data = {
        parentId: this.state.parentId,
        position: 'L',
      };
      this.props.addMember(data, this.state.token);
      // this.props.commaonLoader(true);
      this.setState({visibleModal: true, addPosition: 'L'});
    }
    if (position == '12') {
      if (child11.id == 'Empty') {
        showMessage({
          message: 'Please add previous member',
          type: 'danger',
        });
      } else {
        let data = {
          parentId: this.state.parentId,
          position: 'R',
        };
        this.props.addMember(data, this.state.token);
        // this.props.commaonLoader(true);
        this.setState({visibleModal: true, addPosition: 'R'});
      }
    }
    if (position == '21') {
      if (child12.id == 'Empty') {
        showMessage({
          message: 'Please add previous member',
          type: 'danger',
        });
      } else {
        let data = {
          parentId: this.state.parentId,
          position: 'L2',
        };
        this.props.addMember(data, this.state.token);
        // this.props.commaonLoader(true);
        this.setState({visibleModal: true, addPosition: 'L2'});
      }
    }
    if (position == '22') {
      if (child21.id == 'Empty') {
        showMessage({
          message: 'Please add previous member',
          type: 'danger',
        });
      } else {
        let data = {
          parentId: this.state.parentId,
          position: 'R2',
        };
        this.props.addMember(data, this.state.token);
        // this.props.commaonLoader(true);
        this.setState({visibleModal: true, addPosition: 'R2'});
      }
    }
    if (position == '31') {
      if (child22.id == 'Empty') {
        showMessage({
          message: 'Please add previous member',
          type: 'danger',
        });
      } else {
        let data = {
          parentId: this.state.parentId,
          position: 'L3',
        };
        this.props.addMember(data, this.state.token);
        // this.props.commaonLoader(true);
        this.setState({visibleModal: true, addPosition: 'L3'});
      }
    }
    if (position == '32') {
      if (child31.id == 'Empty') {
        showMessage({
          message: 'Please add previous member',
          type: 'danger',
        });
      } else {
        let data = {
          parentId: this.state.parentId,
          position: 'R3',
        };
        this.props.addMember(data, this.state.token);
        // this.props.commaonLoader(true);
        this.setState({visibleModal: true, addPosition: 'R3'});
      }
    }
  };

  handleOnScroll = event => {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y,
    });
  };
  handleScrollTo = p => {
    if (this.scrollViewRef.current) {
      this.scrollViewRef.current.scrollTo(p);
    }
  };

  close = () => {
    this.setState({visibleModal: false});
  };

  confirmToAdd = () => {
    this.setState({visibleModal: false}, () => {
      if (this.props.route.params.previousScreen == 'home') {
        this.props.navigation.navigate('AddTeamMemebr', {
          parentId: this.props.addConfirmation.data.V_parentid,
          referId: this.props.addConfirmation.data.V_referid,
          position: this.state.addPosition,
        });
      } else {
        let data = {
          parentId: this.props.addConfirmation.data.V_parentid,
          referId: this.props.addConfirmation.data.V_referid,
          position: this.state.addPosition,
          prelogId: this.props.route.params.prelogId,
        };
        AsyncStorage.getItem('token').then(token => {
          if (token !== null) {
            this.props.commaonLoader(true);
            this.props.placeMember(data, token);
          }
        });
        //console.warn('data', data);
      }
    });
  };

  makeParent = child => {
    let {child11, child12, child21, child22, child31, child32} = this.state;
    if (child === '11') {
      if (child11.id !== 'Empty') {
        this.props.commaonLoader(true);
        this.props.navigation.push('NewDSSignup', {
          logid: child11.id,
          previousScreen: this.props.route.params.previousScreen,
          prelogId: this.props.route.params.prelogId,
        });
        // this.props.makeChildAsParent(child11.id, this.state.token);
      } else {
        showMessage({
          message: 'Please add member',
          type: 'danger',
        });
      }
    }
    if (child === '12') {
      if (child12.id !== 'Empty') {
        this.props.commaonLoader(true);
        this.props.navigation.push('NewDSSignup', {
          logid: child12.id,
          previousScreen: this.props.route.params.previousScreen,
          prelogId: this.props.route.params.prelogId,
        });
        // this.props.makeChildAsParent(child12.id, this.state.token);
      } else {
        showMessage({
          message: 'Please add member',
          type: 'danger',
        });
      }
    }
    if (child === '21') {
      if (child21.id !== 'Empty') {
        this.props.commaonLoader(true);
        this.props.navigation.push('NewDSSignup', {
          logid: child21.id,
          previousScreen: this.props.route.params.previousScreen,
          prelogId: this.props.route.params.prelogId,
        });
        // this.props.makeChildAsParent(child21.id, this.state.token);
      } else {
        showMessage({
          message: 'Please add member',
          type: 'danger',
        });
      }
    }
    if (child === '22') {
      if (child22.id !== 'Empty') {
        this.props.commaonLoader(true);
        this.props.navigation.push('NewDSSignup', {
          logid: child22.id,
          previousScreen: this.props.route.params.previousScreen,
          prelogId: this.props.route.params.prelogId,
        });
        // this.props.makeChildAsParent(child22.id, this.state.token);
      } else {
        showMessage({
          message: 'Please add member',
          type: 'danger',
        });
      }
    }
    if (child === '31') {
      if (child31.id !== 'Empty') {
        this.props.commaonLoader(true);
        this.props.navigation.push('NewDSSignup', {
          logid: child31.id,
          previousScreen: this.props.route.params.previousScreen,
          prelogId: this.props.route.params.prelogId,
        });
        // this.props.makeChildAsParent(child31.id, this.state.token);
      } else {
        showMessage({
          message: 'Please add member',
          type: 'danger',
        });
      }
    }
    if (child === '32') {
      if (child32.id !== 'Empty') {
        this.props.commaonLoader(true);
        this.props.navigation.push('NewDSSignup', {
          logid: child32.id,
          previousScreen: this.props.route.params.previousScreen,
          prelogId: this.props.route.params.prelogId,
        });
        // this.props.makeChildAsParent(child32.id, this.state.token);
      } else {
        showMessage({
          message: 'Please add member',
          type: 'danger',
        });
      }
    }
  };

  viewDSInfo = id => {
    if (id !== 'Empty') {
      this.props.navigation.navigate('DSInfo', {logid: id});
    }
  };

  handleImageError = type => {
    let {child11, child12, child21, child22, child31, child32} = this.state;
    if (type == '11') {
      child11.imageLoading = false;
    } else if (type == '12') {
      child12.imageLoading = false;
    } else if (type == '21') {
      child21.imageLoading = false;
    } else if (type == '22') {
      child22.imageLoading = false;
    } else if (type == '31') {
      child31.imageLoading = false;
    } else if (type == '32') {
      child32.imageLoading = false;
    }
    this.setState({child11, child12, child21, child22, child31, child32});
  };

  render() {
    let {
      child11,
      child12,
      child21,
      child22,
      child31,
      child32,
      imageLoading,
      parentPaymentStatus,
    } = this.state;
    return (
      <View style={{flex: 1}}>
        <Header
          {...this.props}
          title="Direct Seller - Select Position"
          showCart={false}
          showNotification={true}
          headeHeight={60}
          back={true}
        />
        <View style={styles.pyamenInfoContainer} elevation={5}>
          <View style={{width: '15%', alignItems: 'center'}}>
            <Text>Legend</Text>
          </View>
          <View style={styles.innerHeadingContainer}>
            <View style={[styles.circleStyle, {backgroundColor: '#58b626'}]} />
            <View>
              <Text style={styles.headingText}>Payment Reciceved</Text>
            </View>
          </View>
          <View style={styles.innerHeadingContainer}>
            <View style={[styles.circleStyle, {backgroundColor: '#d81322'}]} />
            <View>
              <Text style={styles.headingText}>Payment Pending</Text>
            </View>
          </View>
          <View style={styles.innerHeadingContainer}>
            <View style={[styles.circleStyle, {backgroundColor: '#2a74bc'}]} />
            <View>
              <Text style={styles.headingText}>Status Qualified</Text>
            </View>
          </View>
        </View>
        {/* <View style={{flex: 1}}> */}
        <KeyboardAwareScrollView>
          <View style={[mainContainer, {paddingBottom: '10%'}]}>
            <View style={{width: '100%'}}>
              <View style={styles.parentContainer}>
                <View style={{paddingVertical: '3%'}}>
                  <Text
                    style={{fontSize: hp('3.5%'), textTransform: 'uppercase'}}>
                    {this.state.parentId}
                  </Text>
                </View>
                <View style={styles.parentImageContainer}>
                  <TouchableOpacity
                    onPress={() => this.viewDSInfo(this.state.parentId)}>
                    {this.state.parentProfile ? (
                      <Image
                        source={
                          imageLoading
                            ? {
                                uri:
                                  this.state.profileImageUrl +
                                  this.state.parentProfile,
                              }
                            : UserFooter
                        }
                        style={{
                          width: '100%',
                          height: '100%',
                          borderRadius: 90,
                        }}
                        resizeMode="cover"
                        onError={() => this.setState({imageLoading: false})}
                      />
                    ) : (
                      <Image
                        source={UserFooter}
                        style={{width: '100%', height: '100%'}}
                        resizeMode="cover"
                      />
                    )}
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    height: 15,
                    width: 15,
                    borderRadius: 10,
                    backgroundColor:
                      parentPaymentStatus == 0 ? 'red' : '#58b626',
                    marginVertical: 5,
                  }}
                />
              </View>
              <View style={{alignItems: 'center'}}>
                <View style={styles.initialBranch} />
                {/* <View style={styles.initialJunction} /> */}
              </View>
              {/* <View style={styles.firstJunctionContainer}>
                <View style={styles.leftChildBranch}>
                  <View style={styles.leftChildJunction} />
                </View>
                <View style={styles.midleChildBranch}>
                  <View style={styles.midleChildJunction} />
                </View>
                <View style={styles.rightChildBranch}>
                  <View style={styles.rightChildJunction} />
                </View>
              </View> */}
              <View
                style={{
                  flexDirection: 'row',
                  marginHorizontal: '15%',
                  paddingBottom: '5%',
                }}>
                <View
                  style={{
                    width: '50%',
                    height: 50,
                    borderWidth: 2,
                    borderTopColor: '#2a74bc',
                    borderLeftColor: '#2a74bc',
                    borderBottomColor: 'transparent',
                    borderRightColor: '#df7728',
                    left: 1,
                  }}>
                  <View
                    style={{
                      height: 10,
                      width: 10,
                      borderRadius: 5,
                      backgroundColor: '#2a74bc',
                      marginVertical: 5,
                      bottom: -10,
                      position: 'absolute',
                      left: -5,
                    }}
                  />
                  <View
                    style={{
                      height: 10,
                      width: 10,
                      borderRadius: 5,
                      backgroundColor: '#df7728',
                      marginVertical: 5,
                      bottom: -10,
                      position: 'absolute',
                      right: -6,
                    }}
                  />
                </View>
                <View
                  style={{
                    width: '50%',
                    height: 50,
                    borderWidth: 2,
                    borderBottomColor: 'transparent',
                    borderLeftColor: 'transparent',
                    borderTopColor: '#a732ce',
                    borderRightColor: '#a732ce',
                  }}>
                  <View
                    style={{
                      height: 10,
                      width: 10,
                      borderRadius: 5,
                      backgroundColor: '#a732ce',
                      marginVertical: 5,
                      bottom: -10,
                      position: 'absolute',
                      right: -5,
                    }}
                  />
                </View>
              </View>
              <View style={styles.allChildContainer}>
                <View style={{width: '25%'}}>
                  <View style={styles.blueGroupContainer}>
                    <Text style={{color: 'white'}} numberOfLines={1}>
                      Blue Group
                    </Text>
                  </View>
                  <View style={styles.firsSubChildBranch}>
                    <View
                      style={[
                        styles.subChildJunction,
                        {borderColor: '#2a74bc50'},
                      ]}>
                      <View
                        style={[
                          styles.commonJunction,
                          {backgroundColor: '#2a74bc'},
                        ]}
                      />
                    </View>
                    <View
                      style={[
                        styles.subChildJunction1,
                        {borderColor: '#2a74bc50'},
                      ]}>
                      <View
                        style={[
                          styles.commonJunction,
                          {backgroundColor: '#2a74bc'},
                        ]}
                      />
                    </View>
                  </View>
                  <View style={styles.subChilderContainer}>
                    <View style={styles.leftChildContainer}>
                      <TouchableOpacity
                        onPress={() => this.viewDSInfo(child11.id)}>
                        {child11.profilePhoto ? (
                          <Image
                            source={
                              child11.imageLoading
                                ? {
                                    uri:
                                      this.state.profileImageUrl +
                                      child11.profilePhoto,
                                  }
                                : UserFooter
                            }
                            style={{width: 30, height: 30, borderRadius:90}}
                            resizeMode="contain"
                            onError={() => this.handleImageError('11')}
                          />
                        ) : (
                          <Image
                            source={UserFooter}
                            style={{width: 30, height: 30}}
                            resizeMode="contain"
                          />
                        )}
                      </TouchableOpacity>
                      <View style={styles.childIdContainer}>
                        <TouchableOpacity
                          onPress={
                            child11.id == 'Empty'
                              ? () => this.AddTeamMember('11')
                              : null
                          }
                          style={styles.addTeamMemberButton}>
                          <Text
                            style={[
                              styles.childIdStyle,
                              {color: child11.id == 'Empty' ? 'blue' : 'black'},
                            ]}>
                            {child11.id}
                          </Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.paymentStatusContainer}>
                        <View
                          style={
                            child11.paymentStatus == '1'
                              ? styles.paymentStatusRecieved
                              : styles.paymentStatusPending
                          }
                        />
                      </View>
                      <TouchableOpacity
                        style={[
                          styles.childSelector,
                          {backgroundColor: '#2a74bc'},
                        ]}
                        onPress={() => this.makeParent('11')}>
                        <Icon name="download" size={20} color="white" />
                      </TouchableOpacity>
                    </View>
                    <View style={styles.rightChildContainer}>
                      <TouchableOpacity
                        onPress={() => this.viewDSInfo(child12.id)}>
                        {child12.profilePhoto ? (
                          <Image
                            source={
                              child12.imageLoading
                                ? {
                                    uri:
                                      this.state.profileImageUrl +
                                      child12.profilePhoto,
                                  }
                                : UserFooter
                            }
                            style={{width: 30, height: 30, borderRadius:90}}
                            resizeMode="contain"
                            onError={() => this.handleImageError('12')}
                          />
                        ) : (
                          <Image
                            source={UserFooter}
                            style={{width: 30, height: 30}}
                            resizeMode="contain"
                          />
                        )}
                      </TouchableOpacity>
                      <View style={styles.childIdContainer}>
                        <TouchableOpacity
                          onPress={
                            child12.id == 'Empty'
                              ? () => this.AddTeamMember('12')
                              : null
                          }
                          style={styles.addTeamMemberButton}>
                          <Text
                            style={[
                              styles.childIdStyle,
                              {color: child12.id == 'Empty' ? 'blue' : 'black'},
                            ]}>
                            {child12.id}
                          </Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.paymentStatusContainer}>
                        <View
                          style={
                            child12.paymentStatus == '1'
                              ? styles.paymentStatusRecieved
                              : styles.paymentStatusPending
                          }
                        />
                      </View>
                      <TouchableOpacity
                        style={[
                          styles.childSelector,
                          {backgroundColor: '#2a74bc'},
                        ]}
                        onPress={() => this.makeParent('12')}>
                        <Icon name="download" size={20} color="white" />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
                <View style={{width: '25%'}}>
                  <View style={styles.amberGroupContainer}>
                    <Text style={{color: 'white'}} numberOfLines={1}>
                      Amber Group
                    </Text>
                  </View>
                  <View style={styles.firsSubChildBranch}>
                    <View
                      style={[
                        styles.subChildJunction,
                        {
                          borderColor: '#df772850',
                        },
                      ]}>
                      <View
                        style={[
                          styles.commonJunction,
                          {backgroundColor: '#df7728'},
                        ]}
                      />
                    </View>
                    <View
                      style={[
                        styles.subChildJunction1,
                        {
                          borderColor: '#df772850',
                        },
                      ]}>
                      <View
                        style={[
                          styles.commonJunction,
                          {backgroundColor: '#df7728'},
                        ]}
                      />
                    </View>
                  </View>
                  <View style={styles.subChilderContainer}>
                    <View style={styles.leftChildContainer}>
                      <TouchableOpacity
                        onPress={() => this.viewDSInfo(child21.id)}>
                        {child21.profilePhoto ? (
                          <Image
                            source={
                              child21.imageLoading
                                ? {
                                    uri:
                                      this.state.profileImageUrl +
                                      child21.profilePhoto,
                                  }
                                : UserFooter
                            }
                            style={{width: 30, height: 30, borderRadius:90}}
                            resizeMode="contain"
                            onError={() => this.handleImageError('21')}
                          />
                        ) : (
                          <Image
                            source={UserFooter}
                            style={{width: 30, height: 30}}
                            resizeMode="contain"
                          />
                        )}
                      </TouchableOpacity>
                      <View style={[styles.childIdContainer, {marginRight: 7}]}>
                        <TouchableOpacity
                          onPress={
                            child21.id == 'Empty'
                              ? () => this.AddTeamMember('21')
                              : null
                          }
                          style={styles.addTeamMemberButton}>
                          <Text
                            style={[
                              styles.childIdStyle,
                              {color: child21.id == 'Empty' ? 'blue' : 'black'},
                            ]}>
                            {child21.id}
                          </Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.paymentStatusContainer}>
                        <View
                          style={
                            child21.paymentStatus == '1'
                              ? styles.paymentStatusRecieved
                              : styles.paymentStatusPending
                          }
                        />
                      </View>
                      <TouchableOpacity
                        style={[
                          styles.childSelector,
                          {backgroundColor: '#df7728'},
                        ]}
                        onPress={() => this.makeParent('21')}>
                        <Icon name="download" size={20} color="white" />
                      </TouchableOpacity>
                    </View>
                    <View style={styles.rightChildContainer}>
                      <TouchableOpacity
                        onPress={() => this.viewDSInfo(child22.id)}>
                        {child22.profilePhoto ? (
                          <Image
                            source={
                              child22.imageLoading
                                ? {
                                    uri:
                                      this.state.profileImageUrl +
                                      child22.profilePhoto,
                                  }
                                : UserFooter
                            }
                            style={{width: 30, height: 30, borderRadius:90}}
                            resizeMode="contain"
                            onError={() => this.handleImageError('22')}
                          />
                        ) : (
                          <Image
                            source={UserFooter}
                            style={{width: 30, height: 30}}
                            resizeMode="contain"
                          />
                        )}
                      </TouchableOpacity>
                      <View style={styles.childIdContainer}>
                        <TouchableOpacity
                          onPress={
                            child22.id == 'Empty'
                              ? () => this.AddTeamMember('22')
                              : null
                          }
                          style={styles.addTeamMemberButton}>
                          <Text
                            style={[
                              styles.childIdStyle,
                              {color: child22.id == 'Empty' ? 'blue' : 'black'},
                            ]}>
                            {child22.id}
                          </Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.paymentStatusContainer}>
                        <View
                          style={
                            child22.paymentStatus == '1'
                              ? styles.paymentStatusRecieved
                              : styles.paymentStatusPending
                          }
                        />
                      </View>
                      <TouchableOpacity
                        style={[
                          styles.childSelector,
                          {backgroundColor: '#df7728'},
                        ]}
                        onPress={() => this.makeParent('22')}>
                        <Icon name="download" size={20} color="white" />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
                <View style={{width: '25%'}}>
                  <View style={styles.purpleGroupContainer}>
                    <Text style={{color: 'white'}} numberOfLines={1}>
                      Purple Group
                    </Text>
                  </View>
                  <View style={styles.firsSubChildBranch}>
                    <View
                      style={[
                        styles.subChildJunction,
                        {borderColor: '#a732ce50'},
                      ]}>
                      <View
                        style={[
                          styles.commonJunction,
                          {backgroundColor: '#a732ce'},
                        ]}
                      />
                    </View>
                    <View
                      style={[
                        styles.subChildJunction1,
                        {borderColor: '#a732ce50'},
                      ]}>
                      <View
                        style={[
                          styles.commonJunction,
                          {backgroundColor: '#a732ce'},
                        ]}
                      />
                    </View>
                  </View>
                  <View style={styles.subChilderContainer}>
                    <View style={styles.leftChildContainer}>
                      <TouchableOpacity
                        onPress={() => this.viewDSInfo(child31.id)}>
                        {child31.profilePhoto ? (
                          <Image
                            source={
                              child31.imageLoading
                                ? {
                                    uri:
                                      this.state.profileImageUrl +
                                      child31.profilePhoto,
                                  }
                                : UserFooter
                            }
                            style={{width: 30, height: 30, borderRadius:90}}
                            resizeMode="contain"
                            onError={() => this.handleImageError('31')}
                          />
                        ) : (
                          <Image
                            source={UserFooter}
                            style={{width: 30, height: 30}}
                            resizeMode="contain"
                          />
                        )}
                      </TouchableOpacity>
                      <View style={[styles.childIdContainer, {marginRight: 7}]}>
                        <TouchableOpacity
                          onPress={
                            child31.id == 'Empty'
                              ? () => this.AddTeamMember('31')
                              : null
                          }
                          style={styles.addTeamMemberButton}>
                          <Text
                            style={[
                              styles.childIdStyle,
                              {color: child31.id == 'Empty' ? 'blue' : 'black'},
                            ]}>
                            {child31.id}
                          </Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.paymentStatusContainer}>
                        <View
                          style={
                            child31.paymentStatus == '1'
                              ? styles.paymentStatusRecieved
                              : styles.paymentStatusPending
                          }
                        />
                      </View>
                      <TouchableOpacity
                        style={[
                          styles.childSelector,
                          {backgroundColor: '#a732ce'},
                        ]}
                        onPress={() => this.makeParent('31')}>
                        <Icon name="download" size={20} color="white" />
                      </TouchableOpacity>
                    </View>
                    <View style={styles.rightChildContainer}>
                      <TouchableOpacity
                        onPress={() => this.viewDSInfo(child32.id)}>
                        {child32.profilePhoto ? (
                          <Image
                            source={
                              child32.imageLoading
                                ? {
                                    uri:
                                      this.state.profileImageUrl +
                                      child32.profilePhoto,
                                  }
                                : UserFooter
                            }
                            style={{width: 30, height: 30, borderRadius:90}}
                            resizeMode="contain"
                            onError={() => this.handleImageError('32')}
                          />
                        ) : (
                          <Image
                            source={UserFooter}
                            style={{width: 30, height: 30}}
                            resizeMode="contain"
                          />
                        )}
                      </TouchableOpacity>
                      <View style={styles.childIdContainer}>
                        <TouchableOpacity
                          onPress={
                            child32.id == 'Empty'
                              ? () => this.AddTeamMember('32')
                              : null
                          }
                          style={styles.addTeamMemberButton}>
                          <Text
                            style={[
                              styles.childIdStyle,
                              {color: child32.id == 'Empty' ? 'blue' : 'black'},
                            ]}>
                            {child32.id}
                          </Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.paymentStatusContainer}>
                        <View
                          style={
                            child32.paymentStatus == '1'
                              ? styles.paymentStatusRecieved
                              : styles.paymentStatusPending
                          }
                        />
                      </View>
                      <TouchableOpacity
                        style={[
                          styles.childSelector,
                          {backgroundColor: '#a732ce'},
                        ]}
                        onPress={() => this.makeParent('32')}>
                        <Icon name="download" size={20} color="white" />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
        {this.props.isLoading ? <Loader /> : null}
        <View>
          <Modal
            deviceWidth={wp('100%')}
            deviceHeight={hp('100%')}
            style={{
              justifyContent: 'flex-end',
              alignItems: 'center',
              marginBottom: hp('0%'),
            }}
            isVisible={this.state.visibleModal}
            // onSwipeComplete={this.close}
            // swipeDirection={'down'}
            // onBackdropPress={() => this.setState({visibleModal: false})}
            // scrollTo={this.handleScrollTo}
            // scrollOffset={this.state.scrollOffset}
            // scrollOffsetMax={400 - 300}
          >
            {this.props.addConfirmation && this.props.addConfirmation.data ? (
              <View
                style={{
                  height: hp('80%'),
                  width: wp('100%'),
                  paddingHorizontal: wp('3%'),
                  paddingTop: hp('3%'),
                  backgroundColor: '#ffffff',
                  borderTopLeftRadius: hp('5%'),
                  borderTopRightRadius: hp('5%'),
                  alignItems: 'center',
                }}>
                <View style={{width: '100%', alignItems: 'flex-end'}}>
                  <TouchableOpacity
                    style={{padding: 10}}
                    onPress={() => this.setState({visibleModal: false})}>
                    <Icon1 name="cross" color="black" size={30} />
                  </TouchableOpacity>
                </View>
                <View>
                  <Text style={{fontWeight: 'bold', fontSize: hp('3%')}}>
                    Confirm Parent ID and Refer ID
                  </Text>
                </View>
                <View
                  style={{
                    backgroundColor: 'white',
                    width: '90%',
                    marginVertical: '5%',
                    paddingVertical: '5%',
                    paddingHorizontal: '4%',
                    alignItems: 'center',
                  }}
                  elevation={5}>
                  <View>
                    <View
                      style={{
                        flexDirection: 'row',
                        paddingVertical: '4%',
                        borderBottomWidth: 1,
                        borderBottomColor: '#e2e2e250',
                      }}>
                      <View style={{width: '50%', alignItems: 'flex-start'}}>
                        <Text style={styles.modalTextLeft}>Parent ID</Text>
                      </View>
                      <View style={{width: '50%', alignItems: 'flex-start'}}>
                        <Text style={styles.modalTextRight}>
                          {this.props.addConfirmation.data.V_parentid}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        paddingVertical: '4%',
                        borderBottomWidth: 1,
                        borderBottomColor: '#e2e2e250',
                      }}>
                      <View style={{width: '50%', alignItems: 'flex-start'}}>
                        <Text style={styles.modalTextLeft}>Parent ID Name</Text>
                      </View>
                      <View style={{width: '50%', alignItems: 'flex-start'}}>
                        <Text style={styles.modalTextRight}>
                          {this.props.addConfirmation.data.V_parentname}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        paddingVertical: '4%',
                        borderBottomWidth: 1,
                        borderBottomColor: '#e2e2e250',
                      }}>
                      <View style={{width: '50%', alignItems: 'flex-start'}}>
                        <Text style={styles.modalTextLeft}>Refer ID</Text>
                      </View>
                      <View style={{width: '50%', alignItems: 'flex-start'}}>
                        <Text style={styles.modalTextRight}>
                          {this.props.addConfirmation.data.V_referid}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        paddingVertical: '4%',
                        borderBottomWidth: 1,
                        borderBottomColor: '#e2e2e250',
                      }}>
                      <View style={{width: '50%', alignItems: 'flex-start'}}>
                        <Text style={styles.modalTextLeft}>Refer ID Name</Text>
                      </View>
                      <View style={{width: '50%', alignItems: 'flex-start'}}>
                        <Text style={styles.modalTextRight}>
                          {this.props.addConfirmation.data.V_refername}
                        </Text>
                      </View>
                    </View>

                    <View style={{alignItems: 'center'}}>
                      <TouchableOpacity
                        style={styles.confirmButtonContainer}
                        onPress={() => this.confirmToAdd()}>
                        <View>
                          <Text style={{color: 'white'}}>Confirm</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            ) : (
              <ActivityIndicator />
            )}
          </Modal>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    getGenealogyData: state.GenealogyReducer.getGenealogyData,
    isLoading: state.CommonLoaderReducer.isLoading,
    addConfirmation: state.GenealogyReducer.addConfirmation,
    getProfileData: state.ProfileReducer.getProfileData,
    approveDS: state.GenealogyReducer.approveDS,
  }),
  {...genealogyActions, ...loadingActions, ...profileActions},
)(NewDSSignup);

const styles = StyleSheet.create({
  pyamenInfoContainer: {
    flexDirection: 'row',
    paddingVertical: '2%',
    justifyContent: 'space-around',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  circleStyle: {
    borderWidth: 1,
    width: 15,
    height: 15,
    borderRadius: 10,
    marginRight: 2,
  },
  headingText: {
    fontSize: hp('1.1%'),
  },
  innerHeadingContainer: {
    flexDirection: 'row',
    width: '25%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  parentContainer: {
    alignItems: 'center',
    marginTop: hp('5%'),
  },
  parentImageContainer: {
    width: wp('20%'),
    height: wp('20%'),
  },
  initialBranch: {
    borderWidth: 1,
    height: hp('5%'),
    borderColor: '#53ceaa',
  },
  initialJunction: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: 'black',
  },
  firstJunctionContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    height: 100,
  },
  leftChildBranch: {
    borderWidth: 1,
    width: 150,
    borderColor: '#2a74bc50',
    transform: [{rotate: '155deg'}],
    top: hp('4%'),
    left: wp('1%'),
    height: 1,
  },
  leftChildJunction: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: '#2a74bc',
    top: -5,
    left: 140,
  },
  midleChildBranch: {
    borderWidth: 1,
    borderColor: '#df772850',
    height: 60,
    width: 1,
  },
  midleChildJunction: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: '#df7728',
    top: 58,
    left: -5,
  },
  rightChildBranch: {
    borderWidth: 1,
    width: 150,
    borderColor: '#a732ce50',
    transform: [{rotate: '205deg'}],
    top: hp('4%'),
    left: -wp('1%'),
    height: 1,
  },
  rightChildJunction: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: '#a732ce',
    top: -5,
  },
  allChildContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  blueGroupContainer: {
    width: '100%',
    backgroundColor: '#2a74bc',
    paddingVertical: '10%',
    borderRadius: 10,
    alignItems: 'center',
  },
  amberGroupContainer: {
    width: '100%',
    backgroundColor: '#df7728',
    paddingVertical: '10%',
    borderRadius: 10,
    alignItems: 'center',
  },
  purpleGroupContainer: {
    width: '100%',
    backgroundColor: '#a732ce',
    paddingVertical: '10%',
    borderRadius: 10,
    alignItems: 'center',
  },
  firsSubChildBranch: {
    flexDirection: 'row',
    height: 80,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  subChildJunction: {
    height: 80,
    width: 1,
    borderWidth: 1,
    justifyContent: 'flex-end',
    left: 24,
    bottom: 2,
    transform: [{rotate: '18deg'}],
  },
  subChildJunction1: {
    height: 80,
    width: 1,
    borderWidth: 1,
    borderColor: '#2a74bc50',
    justifyContent: 'flex-end',
    transform: [{rotate: '-18deg'}],
    right: 24,
    bottom: 2,
  },
  commonJunction: {
    width: 10,
    height: 10,
    borderRadius: 5,
    right: 5,
  },
  subChilderContainer: {
    flexDirection: 'row',
    width: '100%',
    paddingVertical: '10%',
  },
  childSelector: {
    marginTop: '50%',
    padding: 5,
    borderRadius: 6,
  },
  leftChildContainer: {
    width: '50%',
    alignItems: 'flex-start',
  },
  rightChildContainer: {
    width: '50%',
    alignItems: 'flex-end',
  },
  paymentStatusContainer: {
    width: '60%',
    alignItems: 'center',
    marginTop: '50%',
  },
  paymentStatusPending: {
    width: 15,
    height: 15,
    borderRadius: 10,
    backgroundColor: 'red',
  },
  paymentStatusRecieved: {
    width: 15,
    height: 15,
    borderRadius: 10,
    backgroundColor: '#58b626',
  },
  childIdStyle: {
    fontSize: 9,
    fontWeight: 'bold',
  },
  childIdContainer: {
    height: 50,
    justifyContent: 'center',
  },
  addTeamMemberButton: {
    height: '100%',
    width: '100%',
    justifyContent: 'center',
  },
  modalTextLeft: {
    fontSize: hp('2%'),
  },
  modalTextRight: {
    fontSize: hp('2%'),
    fontWeight: 'bold',
  },
  confirmButtonContainer: {
    width: '60%',
    backgroundColor: commonColor.color,
    paddingVertical: '4%',
    borderRadius: 30,
    alignItems: 'center',
    marginVertical: '5%',
  },
});
