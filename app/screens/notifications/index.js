import React, {useState, useEffect} from 'react';
import {View, Text, FlatList, TouchableOpacity} from 'react-native';
import Header from '../../components/header';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Notifications = props => {
  const [refreshStatus, setRefreshStatus] = useState(false);
  const [notificationList, setNotification] = useState([
    {
      title: 'Product add in cart',
      discription: 'You have added product in your cart',
      time: '18 min ago',
    },
    {
      title: 'Product add in cart',
      discription: 'You have added product in your cart',
      time: '18 min ago',
    },
    {
      title: 'Product add in cart',
      discription: 'You have added product in your cart',
      time: '18 min ago',
    },
    {
      title: 'Product add in cart',
      discription: 'You have added product in your cart',
      time: '18 min ago',
    },
    {
      title: 'Product add in cart',
      discription: 'You have added product in your cart',
      time: '18 min ago',
    },
    {
      title: 'Product add in cart',
      discription: 'You have added product in your cart',
      time: '18 min ago',
    },
    {
      title: 'Product add in cart',
      discription: 'You have added product in your cart',
      time: '18 min ago',
    },
    {
      title: 'Product add in cart',
      discription: 'You have added product in your cart',
      time: '18 min ago',
    },
    {
      title: 'Product add in cart',
      discription: 'You have added product in your cart',
      time: '18 min ago',
    },
    {
      title: 'Product add in cart',
      discription: 'You have added product in your cart',
      time: '18 min ago',
    },
    {
      title: 'Product add in cart',
      discription: 'You have added product in your cart',
      time: '18 min ago',
    },
    {
      title: 'Product add in cart',
      discription: 'You have added product in your cart',
      time: '18 min ago',
    },
    {
      title: 'Product add in cart',
      discription: 'You have added product in your cart',
      time: '18 min ago',
    },
  ]);

  const renderNotifications = item => {
    return (
      <TouchableOpacity
        style={{
          borderBottomColor: '#e2e2e2',
          borderBottomWidth: 1,
          padding: '5%',
          backgroundColor: 'white',
          elevation: 1,
          marginVertical: '1%',
          marginHorizontal: '1%',
        }}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <View
            style={{
              width: 10,
              height: 10,
              backgroundColor: commonColor.color,
              borderRadius: 7.5,
            }}
          />
          <View style={{marginLeft: '2%'}}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: hp('2.2'),
                textTransform: 'capitalize',
              }}>
              {item.title}
            </Text>
          </View>
        </View>
        <View style={{paddingVertical: '2%'}}>
          <Text style={{color: 'gray', fontSize: hp('1.7')}}>
            {item.discription}
          </Text>
        </View>
        <View>
          <Text style={{color: commonColor.color, fontSize: hp('1.7')}}>
            {item.time}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const handleRefresh = () => {
    setRefreshStatus(true);
  };

  useEffect(() => {
    if (refreshStatus == true) {
      alert('refresh');
    }

    setRefreshStatus(false);
  }, [refreshStatus]);

  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          title="Notifications"
          showCart={false}
          showNotification={false}
          headeHeight={60}
          back={true}
        />
      </View>
      <View style={{flex: 1}}>
        <FlatList
          data={notificationList}
          renderItem={({item}) => renderNotifications(item)}
          refreshing={refreshStatus}
          onRefresh={() => handleRefresh()}
          scrollToIndex={0.5}
        />
      </View>
    </View>
  );
};

export default Notifications;
