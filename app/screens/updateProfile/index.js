import React, {useState, useEffect, Component} from 'react';
import Header from '../../components/header';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import BannerBox from '../../assets/images/box.png';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
  commonButtonText,
  commonButton,
} from '../../components/commonStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import DSData from '../DSdata';
import CustomTextInput from '../../components/commonTextInput';
import ProfilePlaceholder from '../../assets/images/profilePlaceholder.png';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import * as profileActions from '../../actions/profileAction';
import * as loadingActions from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import CommonDropdown from '../../components/commonDropdown';
import {stateData} from '../../components/data';
import DatePicker from 'react-native-datepicker';
import * as forgotPasswordAction from '../../actions/forgotPasswordAction';
import {showMessage, hideMessage} from 'react-native-flash-message';
import * as otpActions from '../../actions/otpActions';
import Modal from 'react-native-modal';
import Icon1 from 'react-native-vector-icons/Entypo';
import {TextInput} from 'react-native-gesture-handler';
import OtpModal from '../../components/otpModal';
import {profileImageUrl} from '../../services/index';
import OTPTextView from 'react-native-otp-textinput';
import {useDispatch, useSelector} from 'react-redux';

const options = ['Open Camera', 'Open Gallery', 'cancel'];
const title = 'Select Image From';
var abc = 0;

class ProfileUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isImageViewVisible: false,
      imageData: [],
      selected: '',
      stateData: stateData.allStates,
      profileImage: '',
      email: '',
      address1: '',
      address2: '',
      city: '',
      state: '',
      country: 'India',
      pincode: '',
      telephone: '',
      mobileNo: '',
      wardNo: '',
      bankName: '',
      bankCity: '',
      bankCode: '',
      gstNo: '',
      adharno: '',
      stateData: stateData.allStates,
      district: '',
      dob: '',
      panNo: '',
      fatherName: '',
      co_applicant: '',
      loginId: '',
      imageResponse: {},
      ifsc: '',
      visibleModal: false,
      showOtpModal: false,
      otpValue: '',
      imageLoading: true,
      panEdit: true,
    };
  }

  componentDidMount() {
    // console.warn('CDM');
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      // focus listner
      AsyncStorage.getItem('token').then(token => {
        if (token != null) {
          this.props.getProfile(token);
          this.props.commaonLoader(true);
          this.setState({password: ''});
        } else {
          this.props.navigation.navigate('Category');
        }
      });
      // AsyncStorage.getItem('userCredentials').then(userCredentials => {
      //   let userCredentialsData = JSON.parse(userCredentials);
      //   if (userCredentialsData !== null) {
      //     this.setState({password: userCredentialsData.password});
      //   }
      // });
    });
  }

  updateState = input => {
    if (input) {
      var words = input.split(' ');
      var CapitalizedWords = [];
      words.forEach(element => {
        CapitalizedWords.push(
          element[0].toUpperCase() +
            element.slice(1, element.length).toLowerCase(),
        );
      });
      return CapitalizedWords.join(' ');
    }
    return '';
  };

  componentDidUpdate(prevProps) {
    if (prevProps.getProfileData !== this.props.getProfileData) {
      if (this.props.getProfileData.response.status == 200) {
        let panEdit = this.state.panEdit;
        let data = this.props.getProfileData.data.query[0];
        let imagePath = this.props.getProfileData.data.photopath;
        let states = this.updateState(data.state);
        console.warn('tgtrgrtgtg', states);
        let profileImage = '';
        if (data.photo == null) {
          profileImage = imagePath + data.photo;
        } else {
          profileImage = imagePath + data.photo + `?${abc}`;
        }
        let adharno = data.adhaarno;
        if (adharno == '0') {
          adharno = '';
        }
        let panNo = data.pangir;
        if (panNo.length == 10) {
          panEdit = false;
        }
        this.setState({
          email: data.email,
          address1: data.addr1,
          address2: data.addr2,
          states: states,
          pincode: data.pincode,
          city: data.city,
          telephone: data.telno,
          mobileNo: data.cellno,
          wardNo: data.wardno,
          bankName: data.bankname,
          bankCity: data.bankcity,
          bankCode: data.bankcode,
          gstNo: data.gstno,
          panNo: data.pangir,
          dob: data.dob,
          adharno: adharno,
          fatherName: data.father,
          co_applicant: data.coappname,
          loginId: data.logid,
          district: data.district,
          ifsc: data.issccode,
          profileImage: profileImage,
          imageResponse: '',
          panEdit,
        });
        abc++;
      }
    }

    if (prevProps.otpData !== this.props.otpData) {
      if (
        this.props.otpData.response &&
        this.props.otpData.response.status &&
        this.props.otpData.response.status == 200
      ) {
        this.setState({
          showOtpModal: true,
          otpValue: this.props.otpData.data.otp,
        });
        showMessage({
          type: 'success',
          message: 'Otp is successfully send to your registered mobile numer',
        });
        this.props.emptyOtpData();
      }
    }

    if (prevProps.saveProfileData !== this.props.saveProfileData) {
      if (
        this.props.saveProfileData &&
        this.props.saveProfileData.response &&
        this.props.saveProfileData.response.status == 200
      ) {
        AsyncStorage.getItem('token').then(token => {
          if (token !== null) {
            this.props.getProfile(token);
            this.props.commaonLoader(true);
            showMessage({
              message: 'Profile updated successfully',
              type: 'success',
            });
          }
        });
      } else {
        showMessage({
          message: this.props.saveProfileData.response.message,
          type: 'danger',
        });
      }
    }
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  showActionSheet = () => {
    this.ActionSheet.show();
  };

  handlePress(i) {
    this.setState({
      selected: i,
    });
    if (i == 0) {
      ImagePicker.openCamera({
        width: 300,
        height: 300,
        compressImageQuality: 0.5,
        cropping: true,
        multiple: false,
        includeBase64: false,
        useFrontCamera: true,
      })
        .then(image => {
          this.setState({profileImage: image.path, imageResponse: image});
        })
        .catch(err => {
          console.warn(err);
        });
    } else if (i == 1) {
      ImagePicker.openPicker({
        width: 300,
        height: 300,
        compressImageQuality: 0.5,
        cropping: false,
        multiple: false,
        includeBase64: false,
        mediaType: 'photo',
      })
        .then(image => {
          console.warn(JSON.stringify(image, undefined, 2));
          this.setState({profileImage: image.path, imageResponse: image});
        })
        .catch(err => {
          // alert(err.message);
        });
    }
  }

  viewImage = () => {
    let imageData = [...this.state.imageData];
    imageData.push({
      source: {
        uri: this.state.profileImage,
      },
    });
    this.setState({isImageViewVisible: true, imageData});
  };

  onChangeText = (value, type) => {
    let {
      password,
      email,
      address1,
      address2,
      pincode,
      city,
      telephone,
      mobileNo,
      panNo,
      wardNo,
      bankCity,
      bankCode,
      bankName,
      gstNo,
      adharno,
      district,
      fatherName,
      co_applicant,
    } = this.state;
    if (type === 'password') {
      password = value;
    } else if (type === 'email') {
      let pattern = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff])[\ufe0e\ufe0f]?(?:[\u0300-\u036f\ufe20-\ufe23\u20d0-\u20f0]|\ud83c[\udffb-\udfff])?(?:\u200d(?:[^\ud800-\udfff]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff])[\ufe0e\ufe0f]?(?:[\u0300-\u036f\ufe20-\ufe23\u20d0-\u20f0]|\ud83c[\udffb-\udfff])?)*/;
      if (!pattern.test(value)) {
        email = value.replace(/[^A-Za-z0-9@_.]/g, '');
      }
    } else if (type === 'fName') {
      fatherName = value.replace(/[^A-Za-z]/g, '');
    } else if (type === 'co_applicant') {
      co_applicant = value.replace(/[^A-Za-z]/g, '');
    } else if (type === 'address1') {
      address1 = value;
    } else if (type === 'address2') {
      address2 = value;
    } else if (type === 'district') {
      district = value;
    } else if (type === 'city') {
      city = value;
    } else if (type === 'pinCode') {
      pincode = value.replace(/[^0-9]/g, '');
    } else if (type === 'telephone') {
      telephone = value.replace(/[^0-9]/g, '');
    } else if (type === 'mobile') {
      mobileNo = value.replace(/[^0-9]/g, '');
    } else if (type === 'pan') {
      panNo = value.replace(/[^0-9A-Za-z]/g, '');
    } else if (type === 'gst') {
      gstNo = value.replace(/[^0-9A-Za-z]/g, '');
    } else if (type === 'adhar') {
      adharno = value.replace(/[^0-9]/g, '');
    } else if (type === 'wardNo') {
      wardNo = value.replace(/[^0-9A-Za-z]/g, '');
    } else if (type === 'bankName') {
      bankName = value.replace(/[^A-Za-z]/g, '');
    } else if (type === 'bankCity') {
      bankCity = value.replace(/[^A-Za-z]/g, '');
    } else if (type === 'bankCode') {
      bankCode = value.replace(/[^0-9A-Za-z]/g, '');
    }

    this.setState({
      password,
      email,
      address1,
      address2,
      pincode,
      city,
      telephone,
      mobileNo,
      panNo,
      wardNo,
      bankCity,
      bankCode,
      bankName,
      gstNo,
      adharno,
      district,
      fatherName,
      co_applicant,
    });
  };

  validateEmail = () => {
    let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!pattern.test(this.state.email)) {
      showMessage({
        type: 'danger',
        message: 'Email is not valid',
        position: 'top',
      });
      this.setState({email: ''});
    }
  };

  updatePassword = () => {
    let data = {
      loginId: this.state.loginId,
    };
    this.props.sendOtp(data);
    this.props.commaonLoader(true);
  };

  updateMobile = () => {
    this.setState({visibleModal: false});
  };

  updateProfile = () => {
    let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    console.warn('1');
    let {
      email,
      address1,
      address2,
      pincode,
      city,
      telephone,
      mobileNo,
      panNo,
      wardNo,
      bankCity,
      bankCode,
      bankName,
      gstNo,
      adharno,
      district,
      states,
      imageResponse,
      password,
      country,
    } = this.state;

    if (adharno !== '' && adharno.length < 12) {
      console.warn('2');
      showMessage({
        type: 'danger',
        message: 'Invalid Adhar Number',
      });
    } else if (panNo !== '' && panNo.length < 10) {
      console.warn('3');
      showMessage({
        type: 'danger',
        message: 'Invalid Pan Number',
      });
    } else if (this.state.password == '') {
      console.warn('4');
      showMessage({
        type: 'danger',
        message: 'Please enter password',
      });
    } else if (email !== '' && !pattern.test(email)) {
      showMessage({
        type: 'danger',
        message: 'Email is not valid',
      });
    } else {
      console.warn('5', states);
      if (
        password &&
        email &&
        address1 &&
        city &&
        district &&
        states &&
        pincode &&
        telephone &&
        mobileNo &&
        country &&
        bankName &&
        bankCity
      ) {
        let data = {
          V_oldpwd: password,
          V_email: email,
          V_addr1: address1,
          V_addr2: address2,
          V_city: city,
          V_district: district,
          V_state: states,
          V_pincode: pincode,
          V_telno: telephone,
          V_cellno: mobileNo,
          V_country: country,
          V_bankname: bankName,
          V_bankcity: bankCity,
          V_bankcode: bankCode,
          V_pangir: panNo,
          V_wardno: wardNo,
          V_gstno: gstNo,
          V_adhaarno: adharno,
          filename: imageResponse,
        };
        AsyncStorage.getItem('token').then(token => {
          if (token !== null) {
            this.props.updateProfile(data, token);
            this.props.commaonLoader(true);
          }
        });
      } else {
        showMessage({
          type: 'danger',
          message: 'Fields mark with * can not be empty',
        });
      }
    }
  };

  handleModal = () => {
    this.setState({showOtpModal: false});
  };

  crossModal = () => {
    this.setState({showOtpModal: false});
  };

  verifiedStatus = value => {
    // console.warn('value from child', value);
    this.setState({showOtpModal: false}, () => {
      if (value == true) {
        this.props.navigation.navigate('ResetPassword', {
          previousScreen: 'profilePassword',
          oldPasswordField: true,
          loginId: this.state.loginId,
          cellno: '',
        });
      }
    });
  };

  verifiedStatusNumber = value => {
    // console.warn('value from child', value);
    this.setState({visibleModal: false, mobileNo: value}, () => {
      if (value) {
        setTimeout(() => {
          this.updateProfile();
        }, 1000);
      }
    });
  };

  closeMobileModal = () => {
    this.setState({visibleModal: false});
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <View>
          <Header
            {...this.props}
            title="Update Profile"
            showCart={false}
            showNotification={true}
            headeHeight={60}
            back={true}
          />
        </View>
        <View style={{flex: 1}}>
          <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
            <View style={mainContainer}>
              <View style={commonScreenWidth}>
                <DSData />
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: '#e2e2e2',
                    paddingVertical: '10%',
                    paddingHorizontal: '2%',
                  }}>
                  <View
                    style={{
                      alignItems: 'center',
                      paddingBottom: '5%',
                    }}>
                    <TouchableOpacity>
                      {this.state.imageResponse ? (
                        <Image
                          source={{uri: this.state.profileImage}}
                          style={{width: 150, height: 150, borderRadius: 75}}
                        />
                      ) : this.state.profileImage ? (
                        <Image
                          source={
                            this.state.imageLoading
                              ? {uri: this.state.profileImage}
                              : ProfilePlaceholder
                          }
                          style={{width: 150, height: 150, borderRadius: 75}}
                          onError={() => this.setState({imageLoading: false})}
                        />
                      ) : (
                        <Image
                          source={ProfilePlaceholder}
                          style={{width: 150, height: 150}}
                        />
                      )}
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        position: 'absolute',
                        left: '60%',
                        top: '55%',
                      }}
                      onPress={this.showActionSheet}>
                      <Icon name="edit" size={50} />
                    </TouchableOpacity>
                    {/* <ImageView
                      images={this.state.imageData}
                      imageIndex={0}
                      controls={{next: true, prev: true}}
                      isSwipeCloseEnabled={true}
                      isPinchZoomEnabled={true}
                      isTapZoomEnabled={true}
                      isVisible={this.state.isImageViewVisible}
                      onClose={() => this.setState({isImageViewVisible: false})}
                    /> */}
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>E-mail * </Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Email"
                          changeText={e => this.onChangeText(e, 'email')}
                          value={this.state.email}
                          onSubmit={() => this.validateEmail()}
                        />
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Password *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="**********"
                          secureText={true}
                          changeText={e => this.onChangeText(e, 'password')}
                          value={this.state.password}
                        />
                      </View>
                      <TouchableOpacity
                        style={{alignItems: 'flex-end'}}
                        onPress={this.updatePassword}>
                        <View>
                          <Text
                            style={{
                              textDecorationLine: 'underline',
                              color: commonColor.color,
                              fontSize: hp('1.5%'),
                              fontWeight: 'bold',
                            }}>
                            Change Password
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View>
                    <View>
                      <Text style={styles.inputLabel}>Address * </Text>
                    </View>
                    <View style={styles.commonMargin}>
                      <CustomTextInput
                        customStyle={true}
                        customStyleData={styles.textInputStyle}
                        placeholder="Address 1"
                        changeText={e => this.onChangeText(e, 'address1')}
                        value={this.state.address1}
                      />
                    </View>
                    <View style={styles.commonMargin}>
                      <CustomTextInput
                        customStyle={true}
                        customStyleData={styles.textInputStyle}
                        placeholder="Address 2"
                        changeText={e => this.onChangeText(e, 'address2')}
                        value={this.state.address2}
                      />
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>State *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CommonDropdown
                          itemData={this.state.stateData}
                          onValueChange={value =>
                            this.setState({states: value})
                          }
                          value={this.state.states}
                          placeholderText="Select Your State"
                        />
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Pin Code *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Pin Code"
                          changeText={e => this.onChangeText(e, 'pinCode')}
                          value={this.state.pincode}
                          keyboardType="numeric"
                        />
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>City *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="New Delhi"
                          changeText={e => this.onChangeText(e, 'city')}
                          value={this.state.city}
                        />
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Country</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="India"
                          value={this.state.country}
                        />
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Telephone *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="telephone"
                          changeText={e => this.onChangeText(e, 'telephone')}
                          value={this.state.telephone}
                          keyboardType="numeric"
                        />
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>
                          Mobile no for OTP *
                        </Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="mobile no"
                          changeText={e => this.onChangeText(e, 'mobile')}
                          value={this.state.mobileNo}
                          keyboardType="numeric"
                          maxLength={10}
                          editable={false}
                        />
                      </View>
                      <TouchableOpacity
                        style={{alignItems: 'flex-end'}}
                        onPress={() => this.setState({visibleModal: true})}>
                        <View>
                          <Text
                            style={{
                              textDecorationLine: 'underline',
                              color: commonColor.color,
                              fontSize: hp('1.5%'),
                              fontWeight: 'bold',
                            }}>
                            Update Mobile No.
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                      marginBottom: '2%',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>District *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="District"
                          changeText={e => this.onChangeText(e, 'district')}
                          value={this.state.district}
                        />
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>PAN No.</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Pan No."
                          changeText={e => this.onChangeText(e, 'pan')}
                          value={this.state.panNo}
                          maxLength={10}
                          editable={this.state.panEdit}
                        />
                      </View>
                      {this.state.panNo ? (
                        <View>
                          <Text
                            style={{
                              fontSize: 12,
                              color: commonColor.color,
                              fontWeight: 'bold',
                            }}>
                            * Please call customer support to change
                          </Text>
                        </View>
                      ) : null}
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Ward / Circle No.</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Ward No"
                          changeText={e => this.onChangeText(e, 'wardNo')}
                          value={this.state.wardNo}
                        />
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Bank Name * </Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Bank Name"
                          changeText={e => this.onChangeText(e, 'bankName')}
                          value={this.state.bankName}
                        />
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Bank City/Town *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Bank City"
                          changeText={e => this.onChangeText(e, 'bankCity')}
                          value={this.state.bankCity}
                        />
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Bank Code</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Bank Code"
                          changeText={e => this.onChangeText(e, 'bankCode')}
                          value={this.state.bankCode}
                        />
                      </View>
                    </View>
                  </View>
                  <View style={{marginBottom: '2%'}}>
                    <View>
                      <Text style={styles.inputLabel}>IFSC Code</Text>
                    </View>
                    <View style={styles.commonMargin}>
                      <CustomTextInput
                        customStyle={true}
                        customStyleData={styles.textInputStyle}
                        placeholder="IFSC Code"
                        value={this.state.ifsc}
                        editable={false}
                      />
                    </View>
                    {this.state.ifsc && this.state.ifsc.length > 2 ? (
                      <View>
                        <Text
                          style={{
                            fontSize: 12,
                            color: commonColor.color,
                            fontWeight: 'bold',
                          }}>
                          * Your IFSC code has been verified
                        </Text>
                      </View>
                    ) : (
                      <View>
                        <Text
                          style={{
                            fontSize: 12,
                            color: commonColor.color,
                            fontWeight: 'bold',
                          }}>
                          * Your IFSC code not verified
                        </Text>
                      </View>
                    )}
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>GST No.</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="GST"
                          changeText={e => this.onChangeText(e, 'gst')}
                          value={this.state.gstNo}
                        />
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Adhar No.</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Adhar No."
                          changeText={e => this.onChangeText(e, 'adhar')}
                          value={this.state.adharno}
                          keyboardType="numeric"
                          maxLength={12}
                        />
                      </View>
                    </View>
                  </View>
                  <View style={styles.buttonContainer}>
                    <TouchableOpacity
                      style={styles.button}
                      onPress={this.updateProfile}>
                      <Text style={{color: 'white'}}>Submit</Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity
                      style={styles.button}
                      onPress={() => this.props.navigation.navigate('DSHome')}>
                      <Text style={{color: 'white'}}>Reset</Text>
                    </TouchableOpacity> */}
                  </View>
                </View>
              </View>
            </View>
          </KeyboardAwareScrollView>
          <ActionSheet
            ref={o => (this.ActionSheet = o)}
            title={title}
            options={options}
            cancelButtonIndex={2}
            destructiveButtonIndex={1}
            onPress={index => this.handlePress(index)}
          />
          {this.props.isLoading ? <Loader /> : null}
        </View>
        {this.state.showOtpModal == true ? (
          <OtpModal
            showOtpModal={this.state.showOtpModal}
            handleModal={this.handleModal}
            crossModal={this.crossModal}
            otpValue={this.state.otpValue}
            verifiedStatus={this.verifiedStatus}
            loginID={this.state.loginId}
          />
        ) : null}
        {this.state.visibleModal ? (
          <MobileModal
            closeMobileModal={this.closeMobileModal}
            visibleModal={this.state.visibleModal}
            loginID={this.state.loginId}
            verifiedStatusNumber={this.verifiedStatusNumber}
          />
        ) : null}
      </View>
    );
  }
}

export default connect(
  state => ({
    isLoading: state.CommonLoaderReducer.isLoading,
    getProfileData: state.ProfileReducer.getProfileData,
    changePasswordResponse: state.ForgotPasswordReducer.changePasswordResponse,
    otpData: state.OtpReducer.otpData,
    saveProfileData: state.ProfileReducer.saveProfileData,
  }),
  {
    ...profileActions,
    ...loadingActions,
    ...forgotPasswordAction,
    ...otpActions,
  },
)(ProfileUpdate);

const MobileModal = props => {
  const [updatedMobile, setMobile] = useState('');
  const [bool, setBool] = useState(false);
  const [otpData, setOtp] = useState('');
  const [otpApi, setOtpApi] = useState('');
  const [errMsg, setErrMsg] = useState('');

  const dispatch = useDispatch();
  const otpDataNumber = useSelector(state => state.OtpReducer.otpDataNumber);

  useEffect(() => {
    console.warn('otpDataNumber', otpDataNumber);
    if (Object.keys(otpDataNumber).length) {
      if (otpDataNumber.response.status == 200) {
        setOtpApi(otpDataNumber.data.otp);
        showMessage({
          type: 'success',
          message: 'Otp is successfully send to your registered mobile numer',
        });
        dispatch(otpActions.emptyOtpData());
      } else {
        showMessage({
          message: 'Something went wrong',
          type: 'danger',
        });
      }
    }
  }, [otpDataNumber]);

  const verifyOtp = () => {
    if (otpApi == otpData) {
      props.verifiedStatusNumber(updatedMobile);
      showMessage({
        type: 'success',
        message: 'Verified',
      });
    } else {
      setErrMsg('Otp does not matched');
      // props.verifiedStatusNumber(updatedMobile);
      // showMessage({
      //   type: 'danger',
      //   message: 'Otp does not matched',
      // });
    }
  };

  const updateMobileNumber = () => {
    if (updatedMobile.length < 10) {
      setErrMsg('Invalid mobile number');
    } else {
      let data = {
        loginId: props.loginID,
        newNumber: updatedMobile,
      };
      console.warn(data);
      dispatch(otpActions.sentOtpNumber(data));
      setBool(true);
    }
  };

  const mobileNumber = e => {
    let getMobileNumber = e.replace(/[^0-9]/g, '');
    setMobile(getMobileNumber);
  };

  const handleOtp = otp => {
    setOtp(otp);
  };

  const sendOtp = () => {
    let data = {
      loginId: props.loginID,
      newNumber: updatedMobile,
    };

    dispatch(otpActions.sentOtpNumber(data));
  };

  return (
    <View>
      <Modal
        deviceWidth={wp('100%')}
        deviceHeight={hp('100%')}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: hp('0%'),
        }}
        avoidKeyboard={false}
        isVisible={props.visibleModal}>
        <View
          style={{
            height: hp('40%'),
            width: wp('90%'),
            paddingHorizontal: wp('3%'),
            paddingTop: hp('3%'),
            backgroundColor: '#ffffff',
            borderRadius: 20,
            // borderTopLeftRadius: hp('5%'),
            // borderTopRightRadius: hp('5%'),
            alignItems: 'center',
          }}>
          <View style={{width: '100%', alignItems: 'flex-end'}}>
            <TouchableOpacity
              style={{padding: 3}}
              onPress={props.closeMobileModal}>
              <Icon1 name="cross" color="black" size={30} />
            </TouchableOpacity>
          </View>
          {bool === false ? (
            <View style={{width: '90%'}}>
              <View>
                <Text
                  style={{
                    fontWeight: 'bold',
                    textAlign: 'center',
                    fontSize: hp('3%'),
                  }}>
                  Enter New Mobile Number
                </Text>
              </View>
              <View
                style={{
                  alignItems: 'center',
                  width: '100%',
                  height: 50,
                  marginVertical: '5%',
                }}>
                <TextInput
                  style={{
                    borderBottomWidth: 1,
                    width: '80%',
                    height: 50,
                    borderColor: 'gray',
                    paddingLeft: '3%',
                  }}
                  placeholder="Enter New Mobile Number"
                  keyboardType="numeric"
                  maxLength={10}
                  value={updatedMobile}
                  onChangeText={e => {
                    mobileNumber(e);
                    setErrMsg('');
                  }}
                />
                {errMsg !== '' ? (
                  <Text style={{color: 'red'}}>{'* ' + errMsg}</Text>
                ) : null}
              </View>
              <View style={{alignItems: 'center'}}>
                <TouchableOpacity
                  style={styles.confirmButtonContainer}
                  onPress={updateMobileNumber}>
                  <View>
                    <Text style={{color: 'white'}}>Confirm</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          ) : (
            <View style={{width: '90%', alignItems: 'center'}}>
              <View>
                <Text
                  style={{
                    fontWeight: 'bold',
                    textAlign: 'center',
                    fontSize: hp('3%'),
                  }}>
                  Verify New Mobile Number
                </Text>
              </View>
              <OTPTextView
                handleTextChange={e => handleOtp(e)}
                textInputStyle={styles.roundedTextInput}
                tintColor={commonColor.color}
                inputCount={5}
              />
              {errMsg !== '' ? (
                <Text style={{color: 'red'}}>{'* ' + errMsg}</Text>
              ) : null}
              <View style={{marginVertical: '6%', width: '30%'}}>
                <View>
                  <TouchableOpacity
                    style={[
                      commonButton,
                      {paddingVertical: '10%', borderRadius: 30},
                    ]}
                    onPress={verifyOtp}>
                    <Text style={commonButtonText}>Verify</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{marginBottom: hp('2%')}}>
                <TouchableOpacity onPress={sendOtp}>
                  <Text
                    style={{
                      color: commonColor.color,
                      textDecorationLine: 'underline',
                    }}>
                    Click here to Resend OTP
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  textInputStyle: {
    borderWidth: 1,
    borderColor: '#e2e2e2',
    borderRadius: 6,
    paddingLeft: '5%',
    height: 50,
  },
  commonMargin: {
    marginVertical: '5%',
  },
  inputLabel: {
    fontWeight: 'bold',
  },
  button: {
    width: '40%',
    alignItems: 'center',
    paddingVertical: '3%',
    borderRadius: 30,
    backgroundColor: commonColor.color,
  },
  buttonContainer: {
    flexDirection: 'row',
    paddingVertical: '5%',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  confirmButtonContainer: {
    width: '60%',
    backgroundColor: commonColor.color,
    paddingVertical: '4%',
    borderRadius: 30,
    alignItems: 'center',
    marginVertical: '5%',
  },
  roundedTextInput: {
    borderRadius: 10,
    borderWidth: 4,
  },
});
