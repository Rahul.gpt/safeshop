import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  TextInput,
  Image,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import Logo from '../../assets/images/logo.png';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from '../../components/commonTextInput';
import {
  InputTextLabel,
  commonColor,
  commonButton,
  commonButtonText,
  logoContainer,
  commonScreenWidth,
  mainContainer,
  logoStyle,
} from '../../components/commonStyle';
import {onChange} from 'react-native-reanimated';
import {stateData} from '../../components/data';
import CommonDropdown from '../../components/commonDropdown';
import {showMessage, hideMessage} from 'react-native-flash-message';
import * as loginActions from '../../actions/loginAction';
import {useDispatch, useSelector} from 'react-redux';
import * as loadingAction from '../../actions/commonLoaderAction';
import MessageModal from '../../components/messageModal';
import CheckBox from 'react-native-check-box';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/Ionicons';


function Signup(props) {
  const [state, setState] = useState({
    title: '',
    firstName: '',
    lastName: '',
    middleName: '',
    email: '',
    address: '',
    city: '',
    stateData: stateData.allStates,
    states: '',
    pincode: '',
    panNo: '',
    adharNo: '',
    mobile: '',
    titleType: [
      {label: 'Mr', value: 'Mr'},
      {label: 'Ms', value: 'Ms'},
      {label: 'Mrs', value: 'Mrs'},
    ],
    errorText: '* Required',
    firstNameBool: false,
    lastNameBool: false,
    middleNameBool: false,
    emailBool: false,
    addressBool: false,
    cityBool: false,
    stateBool: false,
    adharBool: false,
    mobileBool: false,
    panBool: false,
    titleBool: false,
    pincodeBool: false,
    showModal: false,
    signupResponse: '',
    ageCheck: false,
    tcCheck: false,
  });

  const dispatch = useDispatch();
  const signupData = useSelector(state => state.LoginReducer.signupData);

  useEffect(() => {
    if (Object.keys(signupData).length) {
      if (signupData.response && signupData.response.status == 200) {
        dispatch(loginActions.emptySignup());
        setState({
          ...state,
          showModal: true,
          signupResponse: signupData.data.message,
          title: '',
          firstName: '',
          middleName: '',
          lastName: '',
          email: '',
          address: '',
          city: '',
          states: '',
          pincode: '',
          mobile: '',
          adharNo: '',
          panNo: '',
        });
      }
    }
  }, [signupData]);

  const onChange = (value, type) => {
    let {
      title,
      firstName,
      middleName,
      lastName,
      email,
      address,
      city,
      states,
      pincode,
      panNo,
      adharNo,
      mobile,
      firstNameBool,
      titleBool,
      middleNameBool,
      lastNameBool,
      emailBool,
      addressBool,
      panBool,
      cityBool,
      adharBool,
      stateBool,
      pincodeBool,
      mobileBool,
    } = state;

    if (type === 'firstname') {
      firstName = value.replace(/[^A-Za-z]/g, '');
      firstNameBool = false;
    } else if (type === 'lastname') {
      lastName = value.replace(/[^A-Za-z]/g, '');
      lastNameBool = false;
    } else if (type === 'middlename') {
      middleName = value.replace(/[^A-Za-z]/g, '');
      middleNameBool = false;
    } else if (type === 'add1') {
      address = value;
      addressBool = false;
    } else if (type === 'city') {
      city = value.replace(/[^A-Za-z]/g, '');
      cityBool = false;
    } else if (type === 'pan') {
      panNo = value.replace(/[^A-Za-z0-9]/g, '');
      panBool = false;
    } else if (type === 'adhar') {
      adharNo = value.replace(/[^0-9]/g, '');
      adharBool = false;
    } else if (type === 'mobile') {
      mobile = value.replace(/[^0-9]/g, '');
      mobileBool = false;
    } else if (type === 'pincode') {
      pincode = value.replace(/[^0-9]/g, '');
      pincodeBool = false;
    } else if (type === 'email') {
      email = value;
      emailBool = false;
    }

    setState({
      ...state,
      firstName,
      middleName,
      lastName,
      email,
      city,
      pincode,
      adharNo,
      panNo,
      mobile,
      address,
      firstNameBool,
      titleBool,
      middleNameBool,
      lastNameBool,
      emailBool,
      addressBool,
      panBool,
      cityBool,
      adharBool,
      stateBool,
      pincodeBool,
      mobileBool,
    });
  };

  const validateEmail = () => {
    let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!pattern.test(state.email)) {
      showMessage({
        type: 'danger',
        message: 'Email is not valid',
        position: 'top',
      });
      setState({...state, email: ''});
    }
  };

  const submitForm = () => {
    let {
      title,
      firstName,
      middleName,
      lastName,
      email,
      address,
      city,
      states,
      pincode,
      panNo,
      adharNo,
      mobile,
      firstNameBool,
      titleBool,
      middleNameBool,
      lastNameBool,
      emailBool,
      addressBool,
      panBool,
      cityBool,
      adharBool,
      stateBool,
      pincodeBool,
      mobileBool,
      tcCheck,
      ageCheck,
    } = state;

    if (
      title &&
      firstName &&
      middleName &&
      lastName &&
      email &&
      address &&
      city &&
      states &&
      pincode &&
      panNo &&
      adharNo &&
      mobile
    ) {
      if (panNo.length < 10) {
        showMessage({
          type: 'danger',
          message: 'Invalid Pan Number',
        });
      } else if (adharNo.length < 12) {
        showMessage({
          type: 'danger',
          message: 'Invalid Adhar Number',
        });
      } else if (ageCheck == false) {
        showMessage({
          type: 'danger',
          message: 'Please confirm age criteria',
        });
      } else if (mobile.length < 10) {
        showMessage({
          type: 'danger',
          message: 'Invalid Mobile No.',
        });
      } else if (tcCheck == false) {
        showMessage({
          type: 'danger',
          message: 'Please agree for term and conditions',
        });
      } else {
        let data = {
          T_title: title,
          T_name1: firstName,
          T_name2: middleName,
          T_name3: lastName,
          T_email: email,
          T_addr1: address,
          T_addr2: '',
          T_city: city,
          T_state: states,
          T_pincode: pincode,
          T_telno: mobile,
          T_pangir: panNo,
          T_adhaarno: adharNo,
          type: 'wwu',
        };
        dispatch(loadingAction.commaonLoader(true));
        dispatch(loginActions.signup(data));
      }
    }
    if (title == '') {
      titleBool = true;
    }
    if (firstName == '') {
      firstNameBool = true;
    }
    if (lastName == '') {
      lastNameBool = true;
    }
    if (middleName == '') {
      middleNameBool = true;
    }
    if (email == '') {
      emailBool = true;
    }
    if (address == '') {
      adharBool = true;
    }
    if (city == '') {
      cityBool = true;
    }
    if (states == '') {
      stateBool = true;
    }
    if (pincode == '') {
      pincodeBool = true;
    }
    if (adharNo == '') {
      adharBool = true;
    }
    if (panNo == '') {
      panBool = true;
    }
    if (mobile == '') {
      mobileBool = true;
    }
    setState({
      ...state,
      firstNameBool,
      titleBool,
      middleNameBool,
      lastNameBool,
      emailBool,
      addressBool,
      panBool,
      cityBool,
      adharBool,
      stateBool,
      pincodeBool,
      mobileBool,
    });
  };

  const closeModal = data => {
    if (data == 'signup') {
      setState({...state, showModal: false});
      setTimeout(() => {
        props.navigation.navigate('Category');
      }, 400);
    }
  };

  const handleAgeCheck = () => {
    let ageCheck = state.ageCheck;
    ageCheck = !ageCheck;
    setState({...state, ageCheck});
  };

  const handleTCcheck = () => {
    let tcCheck = state.tcCheck;
    tcCheck = !tcCheck;
    setState({...state, tcCheck});
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <SafeAreaView style={{flex:1}}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity style={{width: '20%', alignItems: 'center'}}>
            <Icon
              name="ios-arrow-round-back"
              size={50}
              color="#000"
              onPress={() => props.navigation.goBack()}
            />
          </TouchableOpacity>
          <View style={{width: '80%'}}>
            <Text style={{fontSize: hp('2.2%')}} numberOfLines={1}>
              Become DSA Directly
            </Text>
          </View>
        </View>
        <KeyboardAwareScrollView>
          <View style={mainContainer}>
            <View style={commonScreenWidth}>
              <View style={logoContainer}>
                <Image source={Logo} style={logoStyle} resizeMode="contain" />
              </View>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View style={{width: '48%'}}>
                  <View>
                    <Text style={InputTextLabel}>Title</Text>
                  </View>
                  <View style={styles.commonMargin}>
                    <CommonDropdown
                      itemData={state.titleType}
                      onValueChange={value =>
                        setState({...state, title: value, titleBool: false})
                      }
                      value={state.title}
                      placeholderText="Select Gender"
                    />
                  </View>
                  {state.titleBool ? (
                    <Text style={{color: 'red'}}>{state.errorText}</Text>
                  ) : null}
                </View>
                <View style={{width: '48%'}}>
                  <View>
                    <Text style={InputTextLabel}>First Name</Text>
                  </View>
                  <View style={styles.commonMargin}>
                    <CustomTextInput
                      value={state.firstName}
                      changeText={e => onChange(e, 'firstname')}
                      placeholder="Enter Your First Name"
                      customStyleData={styles.textInputStyle}
                      customStyle={true}
                    />
                  </View>
                  {state.firstNameBool ? (
                    <Text style={{color: 'red'}}>{state.errorText}</Text>
                  ) : null}
                </View>
              </View>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View style={{width: '48%'}}>
                  <View>
                    <Text style={InputTextLabel}>Middle Name</Text>
                  </View>
                  <View style={styles.commonMargin}>
                    <CustomTextInput
                      value={state.middleName}
                      changeText={e => onChange(e, 'middlename')}
                      placeholder="Enter Your Middle Name"
                      customStyleData={styles.textInputStyle}
                      customStyle={true}
                    />
                  </View>
                  {state.middleNameBool ? (
                    <Text style={{color: 'red'}}>{state.errorText}</Text>
                  ) : null}
                </View>
                <View style={{width: '48%'}}>
                  <View>
                    <Text style={InputTextLabel}>Last Name</Text>
                  </View>
                  <View style={styles.commonMargin}>
                    <CustomTextInput
                      value={state.lastName}
                      changeText={e => onChange(e, 'lastname')}
                      placeholder="Enter Your Last Name"
                      customStyleData={styles.textInputStyle}
                      customStyle={true}
                    />
                  </View>
                  {state.lastNameBool ? (
                    <Text style={{color: 'red'}}>{state.errorText}</Text>
                  ) : null}
                </View>
              </View>
              <View style={{marginVertical: '6%'}}>
                <View>
                  <Text style={InputTextLabel}>email id</Text>
                </View>
                <View style={styles.commonMargin}>
                  <CustomTextInput
                    value={state.email}
                    changeText={e => onChange(e, 'email')}
                    placeholder="Enter Your Email"
                    customStyleData={styles.textInputStyle}
                    customStyle={true}
                    onSubmit={() => validateEmail()}
                  />
                </View>
                {state.emailBool ? (
                  <Text style={{color: 'red'}}>{state.errorText}</Text>
                ) : null}
              </View>
              <View>
                <View>
                  <Text style={InputTextLabel}>Address</Text>
                </View>
                <View style={styles.commonMargin}>
                  <CustomTextInput
                    value={state.address}
                    changeText={e => onChange(e, 'add1')}
                    placeholder="Enter Address"
                    customStyleData={styles.textInputStyle}
                    customStyle={true}
                  />
                </View>
                {state.addressBool ? (
                  <Text style={{color: 'red'}}>{state.errorText}</Text>
                ) : null}
              </View>
              <View style={{marginVertical: '6%'}}>
                <View>
                  <Text style={InputTextLabel}>city</Text>
                </View>
                <View style={styles.commonMargin}>
                  <CustomTextInput
                    value={state.city}
                    placeholder="Enter City"
                    changeText={e => onChange(e, 'city')}
                    customStyleData={styles.textInputStyle}
                    customStyle={true}
                  />
                </View>
                {state.cityBool ? (
                  <Text style={{color: 'red'}}>{state.errorText}</Text>
                ) : null}
              </View>
              <View>
                <View>
                  <Text style={InputTextLabel}>state</Text>
                </View>
                <View style={styles.commonMargin}>
                  <CommonDropdown
                    itemData={state.stateData}
                    onValueChange={value =>
                      setState({...state, states: value, stateBool: false})
                    }
                    value={state.states}
                    placeholderText="Select Your State"
                  />
                </View>
                {state.stateBool ? (
                  <Text style={{color: 'red'}}>{state.errorText}</Text>
                ) : null}
              </View>
              <View style={{marginVertical: '6%'}}>
                <View>
                  <Text style={InputTextLabel}>pincode</Text>
                </View>
                <View style={styles.commonMargin}>
                  <CustomTextInput
                    customStyleData={styles.textInputStyle}
                    customStyle={true}
                    value={state.pincode}
                    changeText={e => onChange(e, 'pincode')}
                    placeholder="Enter pincode"
                    keyboardType="numeric"
                  />
                </View>
                {state.pincodeBool ? (
                  <Text style={{color: 'red'}}>{state.errorText}</Text>
                ) : null}
              </View>
              <View>
                <View>
                  <Text style={InputTextLabel}>pan no</Text>
                </View>
                <View style={{marginVertical: '6%'}}>
                  <CustomTextInput
                    customStyleData={styles.textInputStyle}
                    customStyle={true}
                    value={state.panNo}
                    changeText={e => onChange(e, 'pan')}
                    placeholder="Enter Pan No."
                    maxLength={10}
                  />
                </View>
                {state.panBool ? (
                  <Text style={{color: 'red'}}>{state.errorText}</Text>
                ) : null}
              </View>
              <View style={{marginVertical: '6%'}}>
                <View>
                  <Text style={InputTextLabel}>adhar card no</Text>
                </View>
                <View style={styles.commonMargin}>
                  <CustomTextInput
                    customStyleData={styles.textInputStyle}
                    customStyle={true}
                    value={state.adharNo}
                    changeText={e => onChange(e, 'adhar')}
                    placeholder="Enter Adhar No."
                    maxLength={12}
                    keyboardType="numeric"
                  />
                </View>
                {state.adharBool ? (
                  <Text style={{color: 'red'}}>{state.errorText}</Text>
                ) : null}
              </View>
              <View>
                <View>
                  <Text style={InputTextLabel}>mobile no</Text>
                </View>
                <View style={styles.commonMargin}>
                  <CustomTextInput
                    customStyleData={styles.textInputStyle}
                    customStyle={true}
                    value={state.mobile}
                    changeText={e => onChange(e, 'mobile')}
                    placeholder="Enter Mobile No."
                    maxLength={10}
                    keyboardType="numeric"
                  />
                </View>
                {state.mobileBool ? (
                  <Text style={{color: 'red'}}>{state.errorText}</Text>
                ) : null}
              </View>
              <View style={{paddingBottom: '5%'}}>
                <CheckBox
                  style={{flex: 1, padding: 10}}
                  onClick={() => handleAgeCheck()}
                  isChecked={state.ageCheck}
                  rightText={'I agree that I am over 18 years of age'}
                  checkBoxColor={commonColor.color}
                  uncheckedCheckBoxColor={'black'}
                />
              </View>
              <View style={{paddingVertical: '5%'}}>
                <CheckBox
                  style={{flex: 1, padding: 10}}
                  onClick={() => handleTCcheck()}
                  isChecked={state.tcCheck}
                  rightText={
                    'I have read and I agree to the terms and conditions mentioned in the legalities section of this site. I further agree to pay the product price within 15 days.'
                  }
                  checkBoxColor={commonColor.color}
                  uncheckedCheckBoxColor={'black'}
                />
              </View>
              <View style={{marginVertical: '6%'}}>
                <View>
                  <TouchableOpacity style={commonButton} onPress={submitForm}>
                    <Text style={commonButtonText}>submit</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
        {state.showModal ? (
          <MessageModal
            showModal={state.showModal}
            closeModal={closeModal}
            previousScreen="signup"
            showMessage={state.signupResponse}
          />
        ) : null}
      </SafeAreaView>
    </View>
  );
}

export default Signup;

const styles = StyleSheet.create({
  textInputStyle: {
    borderWidth: 1,
    borderColor: '#e2e2e2',
    borderRadius: 6,
    paddingLeft: '5%',
    height: 50,
  },
  commonMargin: {
    marginVertical: '5%',
  },
});
