import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Header from '../../components/header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import DSData from '../DSdata';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import Icon from 'react-native-vector-icons/FontAwesome5';
import * as allDSActions from '../../actions/allDSActions';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';
import AsyncStorage from '@react-native-community/async-storage';

const WeeklyPayment = props => {
  const [chequeData, setChequeData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [thisWeekData, setThisWeekData] = useState({});

  const dispatch = useDispatch();
  const allThisWeekData = useSelector(
    state => state.AllDSReducer.allThisWeekData,
  );

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        dispatch(allDSActions.getThisWeek(token));
      }
    });
  }, []);

  useEffect(() => {
    console.warn(JSON.stringify(allThisWeekData, undefined, 2));
    if (Object.keys(allThisWeekData).length) {
      if (
        allThisWeekData.response &&
        allThisWeekData.response.status == 200 &&
        allThisWeekData.data.query2 &&
        allThisWeekData.data.query[0]
      ) {
        setChequeData(allThisWeekData.data.query2);
        setThisWeekData(allThisWeekData.data.query[0]);
        setLoading(false);
      } else {
        setLoading(false);
      }
    }
  }, [allThisWeekData]);

  const generateInovice = item => {
    let data = item.dt;
    data = data.split('/').join('A');
    props.navigation.navigate('GstInvoice', {data});
  };

  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          back={true}
          showCart={true}
          showNotification={false}
          title="Cheque This Week"
          showNotification={true}
          showCart={false}
        />
      </View>
      {loading ? (
        <View style={{flex: 1, justifyContent: 'center'}}>
          <ActivityIndicator size="large" />
        </View>
      ) : (
        <View style={mainContainer}>
          <View style={commonScreenWidth}>
            <DSData allDetails={false} />
          </View>
          <ScrollView
            contentContainerStyle={{
              paddingBottom: hp('25%'),
              alignItems: 'center',
            }}>
            {Object.keys(thisWeekData).length > 0 ? (
              <View style={styles.boxContainer}>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: '#e2e2e2',
                    borderBottomWidth: 1,
                    paddingBottom: '4%',
                  }}>
                  <View style={{width: '50%', alignItems: 'center'}}>
                    <Text style={{fontSize: 12}}>Total Remuneration</Text>
                  </View>
                  <View style={{width: '50%', alignItems: 'center'}}>
                    <Text style={{fontWeight: 'bold', fontSize: 12}}>
                      {parseInt(thisWeekData.topayrupees)}
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingVertical: '4%',
                    borderBottomColor: '#e2e2e2',
                    borderBottomWidth: 1,
                  }}>
                  <View style={{width: '25%'}} />
                  <View style={{width: '25%', alignItems: 'center'}}>
                    <Text style={{fontWeight: 'bold', fontSize: 12}}>
                      Blue Group
                    </Text>
                  </View>
                  <View style={{width: '25%', alignItems: 'center'}}>
                    <Text style={{fontWeight: 'bold', fontSize: 12}}>
                      Amber Group
                    </Text>
                  </View>
                  <View style={{width: '25%', alignItems: 'center'}}>
                    <Text style={{fontWeight: 'bold', fontSize: 12}}>
                      Purple Group
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingVertical: '4%',
                    borderBottomColor: '#e2e2e2',
                    borderBottomWidth: 1,
                  }}>
                  <View style={{width: '25%'}}>
                    <Text style={styles.commFontSize}>
                      Team Business Income (1st Purchase)
                    </Text>
                  </View>
                  <View style={{width: '25%', alignItems: 'center'}}>
                    <Text style={styles.commFontSize}>
                      {parseInt(thisWeekData.topayrs1)}
                    </Text>
                  </View>
                  <View style={{width: '25%', alignItems: 'center'}}>
                    <Text style={styles.commFontSize}>
                      {thisWeekData.topayrs2}
                    </Text>
                  </View>
                  <View style={{width: '25%', alignItems: 'center'}}>
                    <Text style={styles.commFontSize}>
                      {parseInt(thisWeekData.topayrs3)}
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingVertical: '4%',
                    borderBottomColor: '#e2e2e2',
                    borderBottomWidth: 1,
                  }}>
                  <View style={{width: '25%'}}>
                    <Text style={styles.commFontSize}>
                      Team Re-Purchase Business Income
                    </Text>
                  </View>
                  <View style={{width: '25%', alignItems: 'center'}}>
                    <Text style={styles.commFontSize}>
                      {parseInt(thisWeekData.topayrs4)}
                    </Text>
                  </View>
                  <View style={{width: '25%', alignItems: 'center'}}>
                    <Text style={styles.commFontSize}>
                      {parseInt(thisWeekData.topayrs5)}
                    </Text>
                  </View>
                  <View style={{width: '25%', alignItems: 'center'}}>
                    <Text style={styles.commFontSize}>
                      {parseInt(thisWeekData.topayrs6)}
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: '#e2e2e2',
                    borderBottomWidth: 1,
                    paddingVertical: '4%',
                  }}>
                  <View style={{width: '50%', alignItems: 'center'}}>
                    <Text style={{fontSize: 12}}>
                      Preferred customer remuneration
                    </Text>
                  </View>
                  <View style={{width: '50%', alignItems: 'center'}}>
                    <Text style={{fontWeight: 'bold', fontSize: 12}}>
                      {parseInt(thisWeekData.topayrs7)}
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: '#e2e2e2',
                    borderBottomWidth: 1,
                    paddingVertical: '4%',
                  }}>
                  <View style={{width: '50%', alignItems: 'center'}}>
                    <Text style={{fontSize: 12}}>Booster Income</Text>
                  </View>
                  <View style={{width: '50%', alignItems: 'center'}}>
                    <Text style={{fontWeight: 'bold', fontSize: 12}}>
                      {parseInt(thisWeekData.amtrefho)}
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    borderBottomColor: '#e2e2e2',
                    borderBottomWidth: 1,
                    paddingVertical: '4%',
                  }}>
                  <View style={{width: '50%', alignItems: 'center'}}>
                    <Text style={{fontSize: 12}}>Pending Amount</Text>
                  </View>
                  <View style={{width: '50%', alignItems: 'center'}}>
                    <Text style={{fontWeight: 'bold', fontSize: 12}}>
                      {parseInt(thisWeekData.topayrslast)}
                    </Text>
                  </View>
                </View>
              </View>
            ) : null}
            <View
              style={{
                alignItems: 'center',
                marginHorizontal: wp('2%'),
                paddingVertical: '5%',
              }}>
              <Text style={{fontWeight: 'bold', fontSize: hp('3%')}}>
                Cheque History
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                backgroundColor: 'white',
                elevation: 2,
                marginHorizontal: wp('2%'),
              }}>
              {chequeData &&
                chequeData.map(data => {
                  return (
                    <View
                      style={{
                        width: '50%',
                        alignItems: 'center',
                        paddingVertical: '3%',
                        flexDirection: 'row',
                        justifyContent: 'center',
                      }}>
                      <TouchableOpacity
                        onPress={() => generateInovice(data)}
                        style={{padding: 10}}>
                        <Icon name="file-invoice" size={25} />
                      </TouchableOpacity>
                      <View style={{marginLeft: '2%'}}>
                        <Text style={{fontSize: 12}}>{data.dt + ' : '}</Text>
                      </View>
                      <View style={{marginLeft: '2%'}}>
                        <Text style={{fontSize: 12}}>
                          {parseInt(data.topayrupees)}
                        </Text>
                      </View>
                    </View>
                  );
                })}
            </View>
            <View style={{paddingVertical: '5%', marginHorizontal: wp('2%')}}>
              <Text style={{fontSize: 12}}>
                सभी जी एस टी धारक अपना साप्ताहिक बिल cheque this week से डाउनलोड
                करके बिल अपने हस्ताक्षर सहित कम्पनी के आफिस में email
                (gstinvoice@safeshopindia.com) या कोरिएर के द्वारा बिल की तारिख
                (Invoice) से तीन दिन के अन्दर भेंजे। बिल प्राप्ति के बाद ही चेक़
                जारी किया जायेगा ।
              </Text>
            </View>
          </ScrollView>
        </View>
      )}
    </View>
  );
};

export default WeeklyPayment;

const styles = StyleSheet.create({
  boxContainer: {
    backgroundColor: 'white',
    elevation: 2,
    width: wp('95%'),
    paddingHorizontal: '2%',
    paddingVertical: '5%',
  },
  commFontSize: {
    fontSize: 12,
    textAlign: 'center',
  },
  columnHeadingContainer: {
    paddingTop: '5%',
    paddingBottom: hp('2%'),
  },
});
