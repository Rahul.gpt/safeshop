import React, {useState, useEffect} from 'react';
import Header from '../../components/header';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  ScrollView,
} from 'react-native';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Fashion1 from '../../assets/images/image_3.png';
import ImageView from 'react-native-image-viewing';

const ViewEvents = props => {
  const [eventName, setEventName] = useState([]);
  const [loading, setLoading] = useState(true);
  const [gallaryData, setGallaryData] = useState([]);
  const [imageData, setImageData] = useState([]);
  const [isImageViewVisible, setImageVisible] = useState(false);
  const [imageIndex, setImageIndex] = useState(0);
  const [imageUrl, setUrl] = useState('');

  useEffect(() => {
    if (props.route && props.route.params && props.route.params.photos) {
      setGallaryData(props.route.params.photos);
      setUrl(props.route.params.imageUrl);
      setEventName(props.route.params.eventName);
      let imageDatas = imageData;
      props.route.params.photos.map(data => {
        imageDatas.push({uri: props.route.params.imageUrl + data});
      });
      setImageData(imageDatas);
    }
  }, []);

  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          showCart={false}
          showNotification={true}
          headeHeight={60}
          title="Event Details"
          back={true}
        />
      </View>
      <ScrollView>
        <View style={[mainContainer, {paddingBottom: '5%'}]}>
          <View style={{width: '90%'}}>
            <View style={{paddingVertical: '5%', alignItems: 'center'}}>
              <Text style={styles.titleText}>
                {eventName[0] + ' ' + eventName[1]}
              </Text>
            </View>
            <View style={{alignItems: 'center'}}>
              <Text style={styles.titleText}> Event Photos</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-between',
                flexWrap: 'wrap',
                paddingHorizontal: '5%',
              }}>
              {gallaryData.map((item, index) => {
                return (
                  <TouchableOpacity
                    style={{
                      height: wp('25%'),
                      width: wp('25%'),
                      marginVertical: '3%',
                      backgroundColor: '#e2e2e2',
                    }}
                    onPress={() => {
                      setImageVisible(true);
                      setImageIndex(index);
                    }}>
                    <Image
                      source={{uri: imageUrl + item}}
                      style={{width: '100%', height: '100%'}}
                      resizeMode="stretch"
                    />
                  </TouchableOpacity>
                );
              })}
            </View>
            <ImageView
              images={imageData}
              imageIndex={imageIndex}
              visible={isImageViewVisible}
              onRequestClose={() => setImageVisible(false)}
              swipeToCloseEnabled={true}
              FooterComponent={imageIndex => {
                return (
                  <View style={{paddingVertical: '5%'}}>
                    <Text style={{color: 'white', textAlign: 'center'}}>
                      {imageIndex.imageIndex + 1 + ' / ' + gallaryData.length}
                    </Text>
                  </View>
                );
              }}
              onLongPress={() => alert('frferf')}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default ViewEvents;

const styles = StyleSheet.create({
  imageContainer: {
    width: '100%',
    height: hp('30%'),
    marginVertical: '5%',
  },
  titleText: {
    fontWeight: 'bold',
    fontSize: hp('2.5%'),
  },
  button: {
    backgroundColor: commonColor.color,
    paddingHorizontal: '5%',
    paddingVertical: '3%',
    borderRadius: 30,
  },
});
