import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  TextInput,
  Image,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Alert,
} from 'react-native';
import Logo from '../../assets/images/logo.png';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  InputTextLabel,
  commonColor,
  commonButton,
  commonButtonText,
  logoContainer,
  commonScreenWidth,
  mainContainer,
  logoStyle,
} from '../../components/commonStyle';
import {showMessage, hideMessage} from 'react-native-flash-message';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon1 from 'react-native-vector-icons/Entypo';
import Modal from 'react-native-modal';
import * as paymentActions from '../../actions/billdeskActions';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';
import {WebView} from 'react-native-webview';
import RazorpayCheckout from 'react-native-razorpay';
import * as retailsActions from '../../actions/retailOrderActions';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';

const OrderGuestConfirmation = props => {
  const [userData, setUserData] = useState({});
  const [msg, setMsg] = useState('');
  const [showModal, setModal] = useState(false);
  const [str, setStr] = useState('');
  const [paymentUrl, setPaymentUrl] = useState('');
  const [webBool, setWebBool] = useState(false);
  const [webData, setWebData] = useState('');

  const dispatch = useDispatch();
  const strData = useSelector(state => state.BilldeskReducer.strData);
  const paymentId = useSelector(state => state.RetailOrderReducer.paymentId);

  useEffect(() => {
    if (props.route && props.route.params && props.route.params.data) {
      setUserData(props.route.params.data);
      setMsg(props.route.params.msg);
      dispatch(retailsActions.emptyPaymentId());
    }
  }, []);

  useEffect(() => {
    if (Object.keys(strData).length) {
      if (strData.response && strData.response.status == 200) {
        dispatch(paymentActions.emptyPaymentData());
        console.warn(
          'strData.data.message.str1',
          strData.data.message.str1,
          strData.data.message.pgurl,
        );
        // setStr(strData.data.message.str1);
        // setPaymentUrl(strData.data.message.pgurl);
        payment(strData.data.message.pgurl, strData.data.message.str1);
      }
    }
  }, [strData]);

  useEffect(() => {
    console.warn('paymentId', paymentId);
    if (Object.keys(paymentId).length) {
      if (paymentId.response && paymentId.response.status == 200) {
        confirmPayment();
        // dispatch(retailsActions.emptyPaymentId());
      }
    }
  }, [paymentId]);

  const closeModal = () => {
    setModal(false);
  };

  const payment = (url, strrr) => {
    console.warn('paymentUrl', url, strrr);
    fetch(url, {
      method: 'POST',
      body: JSON.stringify({msg: strrr}),
    })
      .then(response => response.text())
      .then(responseData => {
        console.warn(JSON.stringify(responseData, undefined, 2));
        setWebData(responseData);
        setWebBool(true);
      })
      .catch(err => {
        console.warn(err);
      });
  };

  // const payNow = () => {
  //   let data = {
  //     vpc_OrderInfo: msg.orderno,
  //     Amount: msg.T_memamt,
  //     Convcharge: 0,
  //     vpc_Amount: msg.T_memamt,
  //   };
  //   dispatch(paymentActions.getStrData(data));
  // };

  const getPaymentOrderId = () => {
    let data = {
      amount: parseInt(msg.T_memamt),
      vnum: msg.orderno,
    };
    console.warn(data);
    let token =
      'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.ImV5SnBZWFFpT2pFMU9UZ3pOVGM0TURZc0ltcDBhU0k2SW5OclZWWkdVbTh5ZG5ZM2NHTmxSV2RDZFZNek1FMUVPRUl3UVRSWVRVUlNjMjk0V1VOTFJETXlWSEV4Y1RSa05IcE9WM3ByTmpoamJteDNPSFo1WTBkRU5reFRjM05qV25sa2FFVjZjRGhvWldKTlpscDNQVDBpTENKcGMzTWlPaUpvZEhSd09sd3ZYQzkzZDNjdWMzTnZiWEJzTG1OdmJWd3ZJaXdpYm1KbUlqb3hOVGs0TXpVM09EQTJMQ0psZUhBaU9qRTFPVGd6TmpFME1EWXNJbVJoZEdFaU9uc2liRzluYVdRaU9pSklRVkpUU0VKSlRFeEJJbjE5Ig.6sCdk947NGQO8hTMY7XEkWJY4grdXDSn8D1FSq-DiRY';

    dispatch(retailsActions.getPaymentId(data, token));
    // dispatch(retailsActions.emptyPaymentId());
    //  showModal(true)
  };

  const confirmPayment = () => {
    let {id, receipt} = paymentId.data.response_array;
    let {amount} = paymentId.data.msg;
    console.warn('amount', amount);
    var options = {
      description: 'Credits towards consultation',
      image: 'https://www.safeshopindia.com/dm/assets/images/logo.png',
      currency: 'INR',
      key: paymentId.data.msg.key_id,
      amount: amount,
      name: 'SafeShop India',
      order_id: id, //Replace this with an order_id created using Orders API. Learn more at https://razorpay.com/docs/api/orders.
      prefill: {
        email: '',
        contact: '',
        name: '',
      },
      theme: {color: commonColor.color},
    };
    console.warn(options);
    RazorpayCheckout.open(options)
      .then(data => {
        dispatch(retailsActions.emptyPaymentId());
        Alert.alert(
          'SafeShop India',
          'Payment Received ₹ ' +
            amount +
            ' against Invoice No. ' +
            receipt +
            ' Please note the Transaction No. ' +
            data.razorpay_payment_id,
          [
            {
              text: 'OK',
              onPress: () => props.navigation.navigate('Category'),
            },
          ],
          {cancelable: false},
        );
      })
      .catch(error => {
        dispatch(retailsActions.emptyPaymentId());
        alert(`Error: ${error.code} | ${error.description}`);
      });
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <SafeAreaView style={{flex: 1}}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity style={{width: '20%', alignItems: 'center'}}>
            <Icon
              name="ios-arrow-round-back"
              size={50}
              color="#000"
              onPress={() => props.navigation.goBack()}
            />
          </TouchableOpacity>
          <View style={{width: '80%'}}>
            <Text style={{fontSize: hp('2.2%')}} numberOfLines={1}>
              Review Order
            </Text>
          </View>
        </View>
        <KeyboardAwareScrollView>
          <View style={styles.container}>
            <View
              style={{
                paddingVertical: '10%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image source={Logo} style={logoStyle} resizeMode="contain" />
            </View>
            <Text
              style={{
                fontSize: hp('2.5%'),
                fontWeight: 'bold',
                color: commonColor.color,
              }}>
              {msg && msg.ms ? msg.ms : null}
            </Text>
            <View style={{paddingVertical: '5%'}}>
              <Text
                style={{fontSize: hp('2.5%'), fontWeight: 'bold'}}
                onPress={() => setModal(true)}>
                Click here to generate invoice
              </Text>
            </View>
            <View style={{paddingVertical: '5%'}}>
              <Text
                style={{fontSize: hp('2.5%'), fontWeight: 'bold'}}
                onPress={() => getPaymentOrderId()}>
                Click here to pay
              </Text>
            </View>
          </View>
        </KeyboardAwareScrollView>
        {showModal ? (
          <InvoiceModal
            userData={userData}
            closeModal={closeModal}
            showModal={showModal}
            msg={msg}
          />
        ) : null}
        {/* {webBool ? (
          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <View
              style={{
                width: wp('90%'),
                height: hp('50%'),
                borderWidth: 1,
                position: 'absolute',
              }}>
              <WebView source={{html: webData}} />
            </View>
          </View>
        ) : null} */}
      </SafeAreaView>
    </View>
  );
};

export default OrderGuestConfirmation;

const InvoiceModal = props => {
  return (
    <View>
      <Modal
        deviceWidth={wp('100%')}
        deviceHeight={hp('100%')}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: hp('0%'),
        }}
        onBackButtonPress={() => props.closeModal()}
        isVisible={props.showModal}>
        <View
          style={{
            width: wp('90'),
            height: hp('90%'),
            alignItems: 'center',
            borderRadius: 20,
            backgroundColor: 'white',
          }}>
          <ScrollView contentContainerStyle={{paddingHorizontal: '2%'}}>
            <View style={{width: '100%', alignItems: 'flex-end'}}>
              <TouchableOpacity
                style={{padding: 10}}
                onPress={() => props.closeModal()}>
                <Icon1 name="cross" color="black" size={30} />
              </TouchableOpacity>
            </View>
            <View style={{paddingVertical: hp('2%')}}>
              <Text style={styles.mainHeadingText}>
                Safe and Secure Online Marketing Pvt. Ltd.
              </Text>
              <Text
                style={{
                  fontSize: hp('1.7%'),
                  textAlign: 'center',
                }}>
                HO: A-3/24, Janak Puri, New Delhi 110058, India
              </Text>
            </View>
            <View style={{width: '100%'}}>
              <View style={styles.row}>
                <Text
                  style={{
                    fontSize: hp('2%'),
                    fontWeight: 'bold',
                  }}>
                  Shipping Address
                </Text>
              </View>
              <View
                style={{
                  paddingVertical: hp('2%'),
                  borderBottomWidth: 1,
                  borderBottomColor: '#e2e2e2',
                  width: '100%',
                }}>
                <Text
                  style={{
                    textTransform: 'capitalize',
                  }}>
                  {props.userData.T_addr1}
                </Text>
                <Text>
                  {props.userData.T_city +
                    ' ' +
                    props.userData.T_state +
                    ' ' +
                    props.userData.T_pincode}
                </Text>
              </View>
              <View style={styles.row}>
                <View style={{width: '50%'}}>
                  <Text
                    style={{
                      fontSize: hp('2%'),
                      fontWeight: 'bold',
                    }}>
                    Order No.
                  </Text>
                </View>
                <View style={{width: '50%', alignItems: 'flex-end'}}>
                  <Text
                    style={{
                      fontSize: hp('2%'),
                      fontWeight: 'bold',
                    }}>
                    {props.msg && props.msg.orderno ? props.msg.orderno : null}
                  </Text>
                </View>
              </View>
              <View style={styles.row}>
                <View style={{width: '50%'}}>
                  <Text
                    style={{
                      fontSize: hp('2%'),
                      fontWeight: 'bold',
                    }}>
                    Order Date
                  </Text>
                </View>
                <View style={{width: '50%', alignItems: 'flex-end'}}>
                  <Text
                    style={{
                      fontSize: hp('2%'),
                      fontWeight: 'bold',
                    }}>
                    {props.userData.orderDate}
                  </Text>
                </View>
              </View>
              <View style={styles.row}>
                <View style={{width: '50%'}}>
                  <Text
                    style={{
                      fontSize: hp('2%'),
                      fontWeight: 'bold',
                    }}>
                    Tel. No.
                  </Text>
                </View>
                <View style={{width: '50%', alignItems: 'flex-end'}}>
                  <Text
                    style={{
                      fontSize: hp('2%'),
                      fontWeight: 'bold',
                    }}>
                    {props.userData.T_telno}
                  </Text>
                </View>
              </View>
              <View style={styles.row}>
                <View style={{width: '50%'}}>
                  <Text
                    style={{
                      fontSize: hp('2%'),
                      fontWeight: 'bold',
                    }}>
                    E-mail
                  </Text>
                </View>
                <View style={{width: '50%', alignItems: 'flex-end'}}>
                  <Text
                    style={{
                      fontSize: hp('2%'),
                      fontWeight: 'bold',
                    }}>
                    {props.userData.T_email}
                  </Text>
                </View>
              </View>
              <View style={styles.row}>
                <View style={{width: '50%'}}>
                  <Text
                    style={{
                      fontSize: hp('2%'),
                      fontWeight: 'bold',
                    }}>
                    For sale of PRODUCT CODE :
                  </Text>
                </View>
                <View style={{width: '50%', alignItems: 'flex-end'}}>
                  <Text
                    style={{
                      fontSize: hp('2%'),
                      fontWeight: 'bold',
                    }}>
                    {props.userData.T_pkgtypeF}
                  </Text>
                </View>
              </View>
              <View style={styles.row}>
                <View style={{width: '50%'}}>
                  <Text
                    style={{
                      fontSize: hp('2%'),
                      fontWeight: 'bold',
                    }}>
                    Total Amount
                  </Text>
                </View>
                <View style={{width: '50%', alignItems: 'flex-end'}}>
                  <Text
                    style={{
                      fontSize: hp('2%'),
                      fontWeight: 'bold',
                    }}>
                    {props.msg && props.msg.T_memamt
                      ? '₹ ' + props.msg.T_memamt
                      : null}
                  </Text>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingVertical: '5%',
    justifyContent: 'center',
    height: 500,
    paddingHorizontal: '5%',
    position: 'relative',
  },
  mainHeadingText: {
    fontSize: hp('2.2%'),
    fontWeight: 'bold',
    textAlign: 'center',
  },
  row: {
    borderBottomWidth: 1,
    paddingVertical: hp('2%'),
    borderBottomColor: '#e2e2e2',
    flexDirection: 'row',
  },
});
