import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Header from '../../components/header';
import {
  InputTextLabel,
  commonColor,
  commonButton,
  commonButtonText,
  logoContainer,
  commonScreenWidth,
  mainContainer,
  logoStyle,
} from '../../components/commonStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {showMessage, hideMessage} from 'react-native-flash-message';
import AsyncStorage from '@react-native-community/async-storage';
import CustomTextInput from '../../components/commonTextInput';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {stateData} from '../../components/data';
import CommonDropdown from '../../components/commonDropdown';
import {add} from 'react-native-reanimated';

class EditAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      city: '',
      district: '',
      address: '',
      pincode: '',
      states: '',
      nearBy: '',
      mobile: '',
      stateData: stateData.allStates,
    };
  }

  updateState = input => {
    if (input) {
      var words = input.split(' ');
      var CapitalizedWords = [];
      words.forEach(element => {
        CapitalizedWords.push(
          element[0].toUpperCase() +
            element.slice(1, element.length).toLowerCase(),
        );
      });
      return CapitalizedWords.join(' ');
    }
    return '';
  };

  componentDidMount() {
    console.warn(this.props);
    if (
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.data
    ) {
      let newData = this.props.route.params.data;
      if (newData.sh_addr1 !== null) {
        let states = this.updateState(newData.sh_state);
        this.setState({
          name: newData.sh_name,
          address: newData.sh_addr1,
          nearBy: newData.sh_nearby,
          city: newData.sh_city,
          states: states,
          pincode: newData.sh_pincode,
          mobile: newData.cellno,
          district: newData.sh_district,
        });
      } else {
        let states = this.updateState(newData.state);
        this.setState({
          name: newData.name,
          address: newData.addr1,
          nearBy: newData.nearby,
          city: newData.city,
          states: states,
          pincode: newData.pincode,
          mobile: newData.cellno,
          district: newData.district,
        });
      }
    }
  }

  onChangeText = (value, type) => {
    let name = '';
    let address = '';
    let nearBy = '';
    let city = '';
    let states = '';
    let pincode = '';
    let mobile = '';
    let district = '';
    if (type === 'name') {
      name = value.replace(/[^A-Za-z_ ]/g, '');
      this.setState({name});
    } else if (type === 'address') {
      address = value;
      this.setState({address});
    } else if (type === 'nearBy') {
      nearBy = value;
      this.setState({nearBy});
    } else if (type === 'city') {
      city = value.replace(/[^A-Za-z]/g, '');
      this.setState({city});
    } else if (type === 'state') {
      states = value.replace(/[^A-Za-z]/g, '');
      this.setState({states});
    } else if (type === 'pincode') {
      pincode = value.replace(/[^0-9]/g, '');
      this.setState({pincode});
    } else if (type === 'mobile') {
      mobile = value.replace(/[^0-9]/g, '');
      this.setState({mobile});
    } else if (type === 'district') {
      district = value.replace(/[^A-Za-z]/g, '');
      this.setState({district});
    }
  };

  submitAddress = () => {
    let {
      name,
      city,
      address,
      nearBy,
      mobile,
      states,
      district,
      pincode,
    } = this.state;
    if (
      name &&
      city &&
      address &&
      nearBy &&
      mobile &&
      district &&
      states &&
      pincode
    ) {
      this.props.navigation.navigate('Checkout', {
        data: this.state,
      });
    } else {
      showMessage({
        type: 'danger',
        message: 'Please fill all details',
      });
    }
  };

  render() {
    console.warn(this.state);
    return (
      <View style={{flex: 1}}>
        <View>
          <Header
            {...this.props}
            back={true}
            showCart={true}
            showNotification={false}
            title="Edit Address"
            showNotification={false}
            showCart={false}
          />
        </View>
        <View
          style={{
            flex: 1,
            margin: hp('4%'),
            // justifyContent: 'center',
            // alignItems: 'center',
          }}>
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.modalContainer}>
              <View style={{marginBottom: hp('2%')}}>
                <Text style={{fontWeight: 'bold'}}>Receiver's Name</Text>
              </View>
              <View>
                <CustomTextInput
                  customStyle={true}
                  customStyleData={styles.textInputStyle}
                  placeholder="Name"
                  value={this.state.name}
                  changeText={e => this.onChangeText(e, 'name')}
                />
              </View>
              <View style={{marginVertical: hp('2%')}}>
                <Text style={{fontWeight: 'bold'}}>Address</Text>
              </View>
              <View>
                <CustomTextInput
                  customStyle={true}
                  customStyleData={styles.textInputStyle}
                  placeholder="Address"
                  value={this.state.address}
                  changeText={e => this.onChangeText(e, 'address')}
                />
                <View style={{marginVertical: '2%'}}>
                  <CustomTextInput
                    customStyle={true}
                    customStyleData={styles.textInputStyle}
                    placeholder="Near By"
                    value={this.state.nearBy}
                    changeText={e => this.onChangeText(e, 'nearBy')}
                  />
                </View>
                <CustomTextInput
                  customStyle={true}
                  customStyleData={styles.textInputStyle}
                  placeholder="City"
                  value={this.state.city}
                  changeText={e => this.onChangeText(e, 'city')}
                />
                <View style={{marginVertical: '2%'}}>
                  <CustomTextInput
                    customStyle={true}
                    customStyleData={styles.textInputStyle}
                    placeholder="District"
                    value={this.state.district}
                    changeText={e => this.onChangeText(e, 'district')}
                  />
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  marginVertical: hp('2%'),
                }}>
                <View style={{width: '48%'}}>
                  <View style={{marginBottom: hp('2%')}}>
                    <Text style={{fontWeight: 'bold'}}>State</Text>
                  </View>
                  <View>
                    <CommonDropdown
                      itemData={this.state.stateData}
                      onValueChange={value => this.setState({states: value})}
                      value={this.state.states}
                    />
                  </View>
                </View>
                <View style={{width: '48%'}}>
                  <View style={{marginBottom: hp('2%')}}>
                    <Text style={{fontWeight: 'bold'}}>Pin Code</Text>
                  </View>
                  <View>
                    <CustomTextInput
                      customStyle={true}
                      customStyleData={styles.textInputStyle}
                      placeholder="Pin Code"
                      value={this.state.pincode}
                      keyboardType="numeric"
                      changeText={e => this.onChangeText(e, 'pincode')}
                    />
                  </View>
                </View>
              </View>
              <View style={{marginBottom: hp('2%')}}>
                <Text style={{fontWeight: 'bold'}}>
                  Receiver's Phone Number
                </Text>
              </View>
              <View>
                <CustomTextInput
                  customStyle={true}
                  customStyleData={styles.textInputStyle}
                  placeholder="Contact Number"
                  keyboardType="numeric"
                  value={this.state.mobile}
                  changeText={e => this.onChangeText(e, 'mobile')}
                  maxLength={10}
                />
              </View>
              <View style={styles.buttonContainer}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => this.props.navigation.goBack()}>
                  <Text style={{color: 'white'}}>Back</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => this.submitAddress()}>
                  <Text style={{color: 'white'}}>Submit</Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </View>
      </View>
    );
  }
}

export default EditAddress;

const styles = StyleSheet.create({
  textInputStyle: {
    borderWidth: 1,
    borderColor: '#e2e2e2',
    borderRadius: 6,
    paddingLeft: '5%',
    height: 50,
  },
  button: {
    width: '40%',
    alignItems: 'center',
    paddingVertical: '3%',
    borderRadius: 30,
    backgroundColor: commonColor.color,
  },
  buttonContainer: {
    flexDirection: 'row',
    paddingVertical: '5%',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
});
