import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Header from '../../components/header';
import DSData from '../DSdata';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import * as AllDSActions from '../../actions/allDSActions';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import * as loadingActions from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {showMessage} from 'react-native-flash-message';

class NewDSAList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DSAList: [],
      loading: false,
      referid: '',
      actionType: '',
    };
  }

  async componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', async () => {
      let token = await AsyncStorage.getItem('token');
      if (token !== null) {
        this.props.getDSAList(token);
        this.props.commaonLoader(true);
      }
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.getDsaList !== this.props.getDsaList) {
      if (
        this.props.getDsaList.response &&
        this.props.getDsaList.response.status == 200
      ) {
        this.setState({
          DSAList: this.props.getDsaList.data.query1,
          referid: this.props.getDsaList.data.lid,
        });
      }
    }

    if (prevProps.getDSAStatus !== this.props.getDSAStatus) {
      if (this.props.getDSAStatus.response.status == 200) {
        if (this.state.actionType == 'Approve') {
          showMessage({
            type: 'success',
            message: 'Select position to add in genealogy',
          });
          this.props.navigation.navigate('NewDSSignup', {
            logid: this.props.getDSAStatus.data.members.$lid,
            previousScreen: 'dsalist',
            prelogId: this.props.getDSAStatus.data.prelogid,
          });
        } else {
          showMessage({
            type: 'success',
            message: 'Delete Successfully',
          });
          AsyncStorage.getItem('token').then(token => {
            if (token !== null) {
              this.props.getDSAList(token);
              this.props.commaonLoader(true);
            }
          });
        }
      }
    }
  }

  _renderList = data => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          paddingVertical: hp('2%'),
          borderBottomColor: '#e2e2e2',
          borderBottomWidth: 1,
        }}>
        <View style={[styles.container2, {paddingLeft: '1%'}]}>
          <Text style={styles.tableData}>{data.logid}</Text>
        </View>
        <View style={styles.container1}>
          <Text style={styles.tableData}>{data.name}</Text>
        </View>
        <View style={styles.container2}>
          <Text style={styles.tableData}>{data.cellno}</Text>
        </View>
        <View style={styles.container2}>
          <Text style={styles.tableData}>
            {data.addr1 + ' ' + data.district}
          </Text>
        </View>
        <View style={styles.container1}>
          <Text style={styles.tableData}>
            {data.state + ' ' + data.pincode}
          </Text>
        </View>
        <View style={styles.container1}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.handleStatus(data, 'Approve')}>
            <Text style={{color: 'white'}} numberOfLines={1}>
              Approve
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.container1}>
          <TouchableOpacity
            style={[styles.button, {backgroundColor: 'red'}]}
            onPress={() => this.handleStatus(data, 'Delete')}>
            <Text style={{color: 'white'}} numberOfLines={1}>
              Delete
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  handleStatus = (item, type) => {
    let data = {
      logid: item.logid,
      referid: this.state.referid,
      type: type,
    };
    this.setState({actionType: type});
    console.warn(data);
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        this.props.DSAStatus(data, token);
        this.props.commaonLoader(true);
      }
    });
  };

  render() {
    console.warn(JSON.stringify(this.props.getDSAStatus, undefined, 2));
    return (
      <View style={{flex: 1}}>
        <View>
          <Header
            {...this.props}
            back={true}
            title="New DSA's List"
            showCart={false}
            showNotification={true}
            headeHeight={60}
            comefrom="dsa"
          />
        </View>
        <View style={{flex: 1, alignItems: 'center'}}>
          {/* <View style={{width: '90%'}}> */}
          <View style={{width: '95%'}}>
            <DSData allDetails={false} />
          </View>
          <View style={{alignItems: 'center', paddingVertical: '5%'}}>
            <Text style={{fontWeight: 'bold', fontSize: hp('3%')}}>
              List of DSA's for approval
            </Text>
          </View>

          <ScrollView horizontal={true}>
            {this.state.loading ? (
              <ActivityIndicator size="large" />
            ) : (
              <View style={styles.tableContainer}>
                <View style={styles.tableHeadingContainer}>
                  <View style={[styles.container2, {paddingLeft: '1%'}]}>
                    <Text style={styles.tableHadding}>Logid</Text>
                  </View>
                  <View style={styles.container1}>
                    <Text style={styles.tableHadding}>Name</Text>
                  </View>
                  <View style={styles.container2}>
                    <Text style={styles.tableHadding}>Cell No.</Text>
                  </View>
                  <View style={styles.container2}>
                    <Text style={styles.tableHadding}>Address</Text>
                  </View>
                  <View style={styles.container1}>
                    <Text style={styles.tableHadding}>Distric/City</Text>
                  </View>
                  <View style={styles.container1}>
                    <Text style={styles.tableHadding}>Approve</Text>
                  </View>
                  <View style={styles.container1}>
                    <Text style={styles.tableHadding}>Delete</Text>
                  </View>
                </View>
                <View>
                  <FlatList
                    data={this.state.DSAList}
                    renderItem={({item}) => this._renderList(item)}
                    keyExtractor={item => item.vno}
                  />
                </View>
              </View>
            )}
          </ScrollView>
          {/* </View> */}
          {this.props.isLoading ? <Loader /> : null}
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    isLoading: state.CommonLoaderReducer.isLoading,
    getDsaList: state.AllDSReducer.getDsaList,
    getDSAStatus: state.AllDSReducer.getDSAStatus,
  }),
  {...AllDSActions, ...loadingActions},
)(NewDSAList);

const styles = StyleSheet.create({
  tableHadding: {
    fontWeight: 'bold',
    textAlign: 'center',
  },
  button: {
    width: '80%',
    alignItems: 'center',
    paddingVertical: hp('1%'),
    borderRadius: 30,
    backgroundColor: commonColor.color,
  },
  container1: {
    width: wp('25%'),
    alignItems: 'center',
  },
  container2: {
    width: wp('40%'),
    alignItems: 'center',
  },
  tableHeadingContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: '#e2e2e290',
    paddingVertical: hp('2%'),
  },
  tableContainer: {
    borderWidth: 1,
    borderColor: '#e2e2e2',
    paddingBottom: hp('10%'),
    marginHorizontal: wp('3%'),
  },
  button: {
    width: '80%',
    alignItems: 'center',
    paddingVertical: hp('1%'),
    borderRadius: 30,
    backgroundColor: commonColor.color,
  },
  tableData: {
    textAlign: 'center',
  },
});
