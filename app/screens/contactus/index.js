import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  TextInput,
  Image,
  TouchableOpacity,
  SafeAreaView,
  ActivityIndicator,
  StyleSheet,
  Linking,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Header from '../../components/header';
import {commonColor} from '../../components/commonStyle';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import * as grievenceActions from '../../actions/grievenceActions';
import {useDispatch, useSelector} from 'react-redux';

const Contactus = props => {
  const [grievenceData, setGrievenceData] = useState({});
  const [loading, setLoading] = useState(true);

  const dispatch = useDispatch();
  const getQuery = useSelector(state => state.GrievenceReducer.getQuery);

  useEffect(() => {
    dispatch(grievenceActions.getQueryData());
  }, []);

  useEffect(() => {
    if (Object.keys(getQuery).length) {
      if (getQuery.response && getQuery.response.status == 200) {
        setGrievenceData(getQuery.data.message);
        // console.warn(getQuery.data.message.e-mail)
        setLoading(false);
      }
    }
  }, [getQuery]);

  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          back={true}
          showCart={true}
          showNotification={false}
          title="Contact Us"
          showNotification={false}
          showCart={false}
        />
      </View>

      {loading ? (
        <View style={styles.container}>
          <ActivityIndicator size="large" color={commonColor.color} />
        </View>
      ) : (
        <View style={styles.container}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              flexWrap: 'wrap',
              paddingHorizontal: '5%',
            }}>
            <Text style={styles.text}>Address : </Text>
            <Text style={styles.text}>{grievenceData.addr}</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.text}>Phone : </Text>
            <Text style={styles.text}>{grievenceData.phone}</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.text}>Website : </Text>
            <Text
              style={[
                styles.text,
                {textDecorationLine: 'underline', color: 'blue'},
              ]}
              onPress={() => Linking.openURL(grievenceData.website)}>
              {grievenceData.website}
            </Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.text}>E-mail : </Text>
            <Text
              style={[
                styles.text,
                {textDecorationLine: 'underline', color: 'blue'},
              ]}
              onPress={() =>
                Linking.openURL('mailto:support@safeshopindia.com')
              }>
              support@safeshopindia.com
            </Text>
          </View>
          <View style={{paddingTop: hp('5%'), alignItems: 'center'}}>
            <Text
              style={{fontWeight: 'bold', fontSize: hp('2.5%')}}
              onPress={() => props.navigation.navigate('BuyForGuset')}>
              Buy Direct for Customers
            </Text>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: hp('2.5%'),
                paddingVertical: '3%',
              }}
              onPress={() => props.navigation.navigate('Signup')}>
              Become DSA Directly
            </Text>
          </View>
        </View>
      )}
    </View>
  );
};

export default Contactus;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    elevation: 3,
    margin: '5%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 0.2,
  },
  text: {
    fontSize: hp('2.3%'),
    // fontWeight: 'bold',
    textAlign: 'center',
  },
});
