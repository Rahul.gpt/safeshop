import React, {useEffect, useState} from 'react';
import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {commonColor} from '../../components/commonStyle';
import CartIcon from '../../assets/images/cartIcon.png';
import {imageUrl, baseUrl} from '../../services';
import ImageLoad from 'react-native-image-placeholder';
import AsyncStorage from '@react-native-community/async-storage';
import {showMessage, hideMessage} from 'react-native-flash-message';

const ProductCard = props => {
  const [productData, setProductData] = useState([]);
  useEffect(() => {
    setProductData(props.data);
  }, []);

  async function addToCart() {
    let token = await AsyncStorage.getItem('token');
    if (token !== null) {
      props.navigation.navigate('Cart');
    } else {
      showMessage({
        message: 'You need to login before you want to add product in cart',
        type: 'info',
      });
      props.navigation.navigate('Login');
    }
  }

  return (
    <View style={styles.cardContainer}>
      <View style={styles.cardImageContainer}>
        <ImageLoad
          style={styles.cartImage}
          loadingStyle={{size: 'small', color: commonColor.color}}
          source={{uri: imageUrl + '/uploads/retail/' + productData.photos}}
          resizeMode="contain"
        />
      </View>
      <View style={[styles.cardName, {paddingTop: '2%'}]}>
        <Text
          style={{fontSize: hp('1.6%'), textTransform: 'uppercase'}}
          numberOfLines={2}>
          {productData.detl}
        </Text>
      </View>
      {/* <View
        style={[
          styles.cardName,
          {flexDirection: 'row', paddingVertical: '2%'},
        ]}>
        <View style={{width: '15%'}}>
          <Text style={{fontSize: hp('1.5%'), color: commonColor.lightColor}}>
            sku :{' '}
          </Text>
        </View>
        <View style={{width: '85%'}}>
          <Text
            style={{fontSize: hp('1.5%'), color: commonColor.lightColor}}
            numberOfLines={1}>
            {productData.package}
          </Text>
        </View>
      </View> */}
      {/* <View style={[styles.cardName, {flexDirection: 'row'}]}> */}
      <View
        style={{width: '100%', alignItems: 'flex-start', paddingLeft: '5%'}}>
        <Text
          style={{
            color: commonColor.color,
            fontSize: hp('2%'),
            fontWeight: 'bold',
          }}>
          {'₹' + ' ' + parseInt(productData.memamt)}
        </Text>
      </View>
      {/* <View style={{width: '50%', alignItems: 'flex-end'}}>
          <TouchableOpacity
            style={{
              padding: 5,
              borderRadius: 20,
              backgroundColor: commonColor.color,
            }}
            onPress={addToCart}>
            <View>
              <Image
                source={CartIcon}
                resizeMode="contain"
                style={{width: 20, height: 20}}
              />
            </View>
          </TouchableOpacity>
        </View> */}
    </View>
    // </View>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    // height: hp('30%'),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    width: '100%',
    paddingVertical: '3%',
  },
  cardImageContainer: {
    width: wp('30%'),
    height: wp('30%'),
  },
  cartImage: {
    width: '100%',
    height: '100%',
  },
  cardName: {
    width: '100%',
    paddingLeft: '5%',
  },
  cartContainer: {
    position: 'absolute',
    bottom: '2%',
    right: '2%',
    borderRadius: 20,
    backgroundColor: commonColor.color,
  },
});

export default ProductCard;
