import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, ActivityIndicator} from 'react-native';
import Header from '../../components/header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import DSData from '../DSdata';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import * as allDSActions from '../../actions/allDSActions';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';
import AsyncStorage from '@react-native-community/async-storage';

const CourierDetail = props => {
  const [courierAllData, setCourierData] = useState([]);
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();
  const courierData = useSelector(state => state.AllDSReducer.courierData);

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        dispatch(allDSActions.getCourierData(token));
        dispatch(loadingAction.commaonLoader(true));
      }
    });
  }, []);

  useEffect(() => {
    if (Object.keys(courierData).length) {
      if (courierData.response && courierData.response.status == 200) {
        setCourierData(courierData.data.query);
        setLoading(false);
      } else {
        setLoading(false);
      }
    }
  }, [courierData]);

  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          back={true}
          showCart={true}
          showNotification={false}
          title="Product Courier Detail"
          showNotification={true}
          showCart={false}
        />
      </View>
      <View style={mainContainer}>
        <View style={commonScreenWidth}>
          <DSData allDetails={false} />
        </View>
        {loading ? (
          <View style={{height: 200, justifyContent: 'center'}}>
            <ActivityIndicator size="large" />
          </View>
        ) : courierAllData.length ? (
          <View style={styles.boxContainer}>
            <View style={styles.rowContainer}>
              <View style={styles.rowInnerContainer}>
                <Text>Courier</Text>
              </View>
              <View style={styles.rowInnerContainer}>
                <Text style={styles.rowRightText}>
                  {courierAllData[0].cour_name}
                </Text>
              </View>
            </View>
            <View style={[styles.rowContainer, {marginVertical: '8%'}]}>
              <View style={styles.rowInnerContainer}>
                <Text>Send Date</Text>
              </View>
              <View style={styles.rowInnerContainer}>
                <Text style={styles.rowRightText}>
                  {courierAllData[0].cour_date}
                </Text>
              </View>
            </View>
            <View style={styles.rowContainer}>
              <View style={styles.rowInnerContainer}>
                <Text>Courier No.</Text>
              </View>
              <View style={styles.rowInnerContainer}>
                <Text style={styles.rowRightText}>
                  {courierAllData[0].cour_details}
                </Text>
              </View>
            </View>
          </View>
        ) : (
          <View style={{height: 200, justifyContent: 'center'}}>
            <Text>No Data Found</Text>
          </View>
        )}
      </View>
    </View>
  );
};

export default CourierDetail;

const styles = StyleSheet.create({
  boxContainer: {
    width: '90%',
    backgroundColor: 'white',
    elevation: 2,
    marginVertical: '20%',
    paddingVertical: '10%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  rowInnerContainer: {
    width: '50%',
    alignItems: 'center',
  },
  rowRightText: {
    fontWeight: 'bold',
  },
});
