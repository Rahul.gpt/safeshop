import React, {Component, useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
} from 'react-native';
import Header from '../../components/header';
import DSData from '../DSdata';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import * as retailsActions from '../../actions/retailOrderActions';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import * as loadingActions from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import RazorpayCheckout from 'react-native-razorpay';
import Modal from 'react-native-modal';
import Icon1 from 'react-native-vector-icons/Entypo';
import {showMessage, hideMessage} from 'react-native-flash-message';
import {useDispatch, useSelector} from 'react-redux';

RetailOrderList = props => {
  const [orderList, setOrderList] = useState([]);
  const [loading, setLoading] = useState(true);

  const dispatch = useDispatch();
  const getRetailOrdersData = useSelector(
    state => state.RetailOrderReducer.getRetailOrdersData,
  );

  const paymentId = useSelector(state => state.RetailOrderReducer.paymentId);

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        dispatch(retailsActions.getRetailOrders(token));
      }
    });
  }, []);

  useEffect(() => {
    if (Object.keys(getRetailOrdersData).length) {
      if (
        getRetailOrdersData.response &&
        getRetailOrdersData.response.status == 200
      ) {
        setOrderList(getRetailOrdersData.data.list);
        setLoading(false);
      }
    }
  }, [getRetailOrdersData]);

  useEffect(() => {
    if (Object.keys(paymentId).length) {
      if (paymentId.response && paymentId.response.status == 200) {
        confirmPayment();
        dispatch(retailsActions.emptyPaymentId());
      }
    }
  }, [paymentId]);

  const _renderList = order => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          paddingVertical: hp('2%'),
          borderBottomColor: '#e2e2e2',
          borderBottomWidth: 1,
        }}>
        <TouchableOpacity
          style={[styles.container1, {alignItems: 'center'}]}
          onPress={() => generateInvoice(order)}>
          <Icon name="file-invoice" size={25} />
        </TouchableOpacity>
        <View style={styles.container2}>
          <Text style={styles.tableData}>{order.vno}</Text>
        </View>
        <View style={styles.container1}>
          <Text style={styles.tableData}>
            {moment(order.vdate).format('YYYY-MM-DD')}
          </Text>
        </View>
        <View style={styles.container1}>
          <Text style={styles.tableData}>{order.amtdr}</Text>
        </View>
        <View style={styles.container1}>
          <Text style={styles.tableData}>{order.delvch}</Text>
        </View>
        <View style={styles.container1}>
          <Text style={styles.tableData}>{order.amtcr}</Text>
        </View>
        <View style={styles.container1}>
          <Text style={styles.tableData}>{order.bv}</Text>
        </View>
        <View style={styles.container1}>
          {order.vostatus == 'NEW' ? (
            <Text style={styles.tableData}>Not Paid</Text>
          ) : (
            <Text style={styles.tableData}>Paid</Text>
          )}
        </View>
        <View style={styles.container1}>
          {order.vostatus == 'NEW' ? (
            <TouchableOpacity
              style={styles.button}
              onPress={() => getPaymentOrderId(order)}>
              <Text style={{color: 'white'}} numberOfLines={1}>
                Pay Now
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>
      </View>
    );
  };

  const generateInvoice = item => {
    props.navigation.navigate('Invoice', {id: item.vno});
  };

  const getPaymentOrderId = order => {
    let data = {
      amount: parseInt(order.amtdr),
      vnum: order.vno,
    };
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        dispatch(retailsActions.getPaymentId(data, token));
        dispatch(retailsActions.emptyPaymentId());
      }
    });
  };

  const confirmPayment = () => {
    let {id, amount, receipt} = paymentId.data.response_array;
    var options = {
      description: 'Credits towards consultation',
      image: 'https://www.safeshopindia.com/dm/assets/images/logo.png',
      currency: 'INR',
      key: paymentId.data.msg.key_id,
      amount: paymentId.data.msg.amount,
      name: 'SafeShop India',
      order_id: id, //Replace this with an order_id created using Orders API. Learn more at https://razorpay.com/docs/api/orders.
      prefill: {
        email: '',
        contact: '',
        name: '',
      },
      theme: {color: commonColor.color},
    };
    console.warn(options);
    RazorpayCheckout.open(options)
      .then(data => {
        Alert.alert(
          'SafeShop India',
          'Payment Received ₹ ' +
            paymentId.data.msg.amount +
            ' against Invoice No. ' +
            receipt +
            ' Please note the Transaction No. ' +
            data.razorpay_payment_id,
          [
            {
              text: 'OK',
            },
          ],
          {cancelable: false},
        );
        //this.props.emptyPaymentId();
        // handle success
        // showMessage({
        //   type: 'success',
        //   message: `${data.razorpay_payment_id}`,
        // });
        //alert(JSON.stringify(data));
      })
      .catch(error => {
        // // handle failure
        // showMessage({
        //   type: 'danger',
        //   message: `${error.code} | ${error.description}`,
        // });
        alert(JSON.stringify(error));
        // alert(`Error: ${error.code} | ${error.description}`);
        //this.props.emptyPaymentId();
      });
  };

  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          back={true}
          title="Retail Order List"
          showCart={false}
          showNotification={true}
          headeHeight={60}
        />
      </View>
      <View style={{flex: 1, alignItems: 'center'}}>
        {/* <View style={{width: '90%'}}> */}
        <View style={{width: '95%'}}>
          <DSData allDetails={false} />
        </View>
        <View style={{alignItems: 'center', paddingVertical: '5%'}}>
          <Text style={{fontWeight: 'bold', fontSize: hp('3%')}}>
            Order List
          </Text>
        </View>
        <ScrollView horizontal={true}>
          {loading ? (
            <ActivityIndicator size="large" />
          ) : (
            <View style={styles.tableContainer}>
              <View style={styles.tableHeadingContainer}>
                <View style={[styles.container1, {alignItems: 'center'}]}>
                  <Text style={styles.tableHadding}>Print</Text>
                </View>
                <View style={styles.container2}>
                  <Text style={styles.tableHadding}>Order No.</Text>
                </View>
                <View style={styles.container1}>
                  <Text style={styles.tableHadding}>Date</Text>
                </View>
                <View style={styles.container1}>
                  <Text style={styles.tableHadding}>Inv. Amount</Text>
                </View>
                <View style={styles.container1}>
                  <Text style={styles.tableHadding}>Shipping</Text>
                </View>
                <View style={styles.container1}>
                  <Text style={styles.tableHadding}>Discount</Text>
                </View>
                <View style={styles.container1}>
                  <Text style={styles.tableHadding}>RBV</Text>
                </View>
                <View style={styles.container1}>
                  <Text style={styles.tableHadding}>Status</Text>
                </View>
                <View style={styles.container1}>
                  <Text style={styles.tableHadding}>Pay Now</Text>
                </View>
              </View>
              <View>
                {orderList.length ? (
                  <FlatList
                    data={orderList}
                    renderItem={({item}) => _renderList(item)}
                    keyExtractor={item => item.vno}
                  />
                ) : (
                  <View style={{paddingLeft: 10}}>
                    <Text>No Data</Text>
                  </View>
                )}
              </View>
            </View>
          )}
        </ScrollView>
        {/* </View> */}
        {/* <View>
            <Modal
              deviceWidth={wp('100%')}
              deviceHeight={hp('100%')}
              style={{
                justifyContent: 'flex-end',
                alignItems: 'center',
                marginBottom: hp('0%'),
              }}
              isVisible={this.state.visibleModal}>
              {this.props.paymentId &&
              this.props.paymentId.response &&
              this.props.paymentId.response.status == 200 &&
              this.props.paymentId.data ? (
                <View
                  style={{
                    height: hp('80%'),
                    width: wp('100%'),
                    paddingHorizontal: wp('3%'),
                    paddingTop: hp('3%'),
                    backgroundColor: '#ffffff',
                    borderTopLeftRadius: hp('5%'),
                    borderTopRightRadius: hp('5%'),
                    alignItems: 'center',
                  }}>
                  <View style={{width: '100%', alignItems: 'flex-end'}}>
                    <TouchableOpacity
                      style={{padding: 10}}
                      onPress={() => this.setState({visibleModal: false})}>
                      <Icon1 name="cross" color="black" size={30} />
                    </TouchableOpacity>
                  </View>
                  <View>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: hp('3%'),
                        textAlign: 'center',
                      }}>
                      Confirm Payment Amount and Retail Order Id
                    </Text>
                  </View>
                  <View
                    style={{
                      backgroundColor: 'white',
                      width: '90%',
                      marginVertical: '5%',
                      paddingVertical: '5%',
                      paddingHorizontal: '4%',
                      alignItems: 'center',
                    }}
                    elevation={5}>
                    <View>
                      <View
                        style={{
                          flexDirection: 'row',
                          paddingVertical: '4%',
                          borderBottomWidth: 1,
                          borderBottomColor: '#e2e2e250',
                        }}>
                        <View style={{width: '45%', alignItems: 'flex-start'}}>
                          <Text style={styles.modalTextLeft}>
                            Amount To Pay :
                          </Text>
                        </View>
                        <View style={{width: '55%', alignItems: 'flex-start'}}>
                          <Text style={styles.modalTextRight}>
                            {this.props.paymentId.data.msg.amount}
                          </Text>
                        </View>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          paddingVertical: '4%',
                          borderBottomWidth: 1,
                          borderBottomColor: '#e2e2e250',
                        }}>
                        <View style={{width: '45%', alignItems: 'flex-start'}}>
                          <Text style={styles.modalTextLeft}>
                            Retail Order Id :
                          </Text>
                        </View>
                        <View style={{width: '55%', alignItems: 'flex-start'}}>
                          <Text style={styles.modalTextRight}>
                            {this.props.paymentId.data.msg.receipt}
                          </Text>
                        </View>
                      </View>
                      <View style={{alignItems: 'center'}}>
                        <TouchableOpacity
                          style={styles.confirmButtonContainer}
                          onPress={() => this.confirmPayment()}>
                          <View>
                            <Text style={{color: 'white'}}>Confirm</Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </View>
              ) : (
                <Loader />
              )}
            </Modal>
          </View> */}
      </View>
    </View>
  );
};

export default RetailOrderList;

const styles = StyleSheet.create({
  tableHadding: {
    fontWeight: 'bold',
  },
  button: {
    width: '80%',
    alignItems: 'center',
    paddingVertical: hp('1%'),
    borderRadius: 30,
    backgroundColor: commonColor.color,
  },
  container1: {
    width: wp('25%'),
    alignItems: 'center',
  },
  container2: {
    width: wp('40%'),
    alignItems: 'center',
  },
  tableHeadingContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: '#e2e2e2',
    paddingVertical: hp('2%'),
  },
  tableContainer: {
    borderWidth: 1,
    borderColor: '#e2e2e2',
    paddingBottom: hp('10%'),
    marginHorizontal: wp('3%'),
  },
  modalTextRight: {
    fontWeight: 'bold',
  },
  confirmButtonContainer: {
    width: '60%',
    backgroundColor: commonColor.color,
    paddingVertical: '4%',
    borderRadius: 30,
    alignItems: 'center',
    marginVertical: '5%',
  },
});
