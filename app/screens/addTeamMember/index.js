import React, {useState, useEffect, Component} from 'react';
import Header from '../../components/header';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import BannerBox from '../../assets/images/box.png';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import DSData from '../DSdata';
import CustomTextInput from '../../components/commonTextInput';
import CommonDropdown from '../../components/commonDropdown';
import AsyncStorage from '@react-native-community/async-storage';
import {showMessage, hideMessage} from 'react-native-flash-message';
import {connect} from 'react-redux';
import * as geanealogyActions from '../../actions/genealogyActions';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import * as loadingActions from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {stateData} from '../../components/data';
import MessageModal from '../../components/messageModal';

class AddTeamMemebr extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accountTypeItems: [
        {
          label: 'Individual/Proprietorship',
          value: 'Individual/Proprietorship',
        },
        {label: 'Partnership', value: 'Partnership'},
        {label: 'HUF', value: 'HUF'},
        {label: 'Company', value: 'Company'},
      ],
      loginID: '',
      password: '',
      cPassword: '',
      accountType: '',
      fatherName: '',
      co_applicant: '',
      email: '',
      address1: '',
      address2: '',
      city: '',
      state: '',
      country: '',
      pincode: '',
      telephone: '',
      mobileNo: '',
      nearBy: '',
      wardNo: '',
      bankName: '',
      bankCity: '',
      bankCode: '',
      srAdvisoryName: '',
      srCellNo: '',
      gstNo: '',
      adharno: '',
      genderType: [
        {label: 'Mr', value: 'Mr'},
        {label: 'Ms', value: 'Ms'},
        {label: 'Mrs', value: 'Mrs'},
      ],
      stateData: stateData.allStates,
      firstName: '',
      lastName: '',
      landmark: '',
      district: '',
      dob: '',
      gender: 'Mr',
      panNo: '',
      showModal: false,
      middleName: '',
      logidBool: false,
      titleBool: false,
      firstBool: false,
      lastBool: false,
      fatherBool: false,
      emailBool: false,
      address1Bool: false,
      landmarkBool: false,
      districtBool: false,
      stateBool: false,
      pincodeBool: false,
      cityBool: false,
      mobileBool: false,
      dobBool: false,
      passwordBool: false,
      cPasswordBool: false,
      middleBool: false,
      errorText: '* Required',
      states: '',
    };
  }

  componentDidMount() {
    // console.warn(this.props);
    showMessage({
      message: 'Fields which are marked with * mandatory to filled',
      type: 'danger',
      position: 'top',
    });
  }

  onChangeText = (value, type) => {
    let {
      loginID,
      password,
      cPassword,
      firstName,
      lastName,
      fatherName,
      co_applicant,
      email,
      address1,
      address2,
      landmark,
      pincode,
      city,
      telephone,
      mobileNo,
      panNo,
      wardNo,
      bankCity,
      bankCode,
      bankName,
      srAdvisoryName,
      srCellNo,
      gstNo,
      adharno,
      district,
      middleName,
      logidBool,
      passwordBool,
      cPasswordBool,
      titleBool,
      firstBool,
      lastBool,
      middleBool,
      fatherBool,
      emailBool,
      address1Bool,
      landmarkBool,
      districtBool,
      stateBool,
      pincodeBool,
      cityBool,
      mobileBool,
    } = this.state;
    if (type === 'loginId') {
      loginID = value.replace(/[^0-9A-Za-z]/g, '');
      logidBool = false;
    } else if (type === 'password') {
      password = value;
      passwordBool = false;
    } else if (type === 'cPassword') {
      cPassword = value;
      cPasswordBool = false;
    } else if (type === 'firstName') {
      firstName = value.replace(/[^A-Za-z]/g, '');
      firstBool = false;
    } else if (type === 'middlename') {
      middleName = value.replace(/[^A-Za-z]/g, '');
      middleBool = false;
    } else if (type === 'lastName') {
      lastName = value.replace(/[^A-Za-z]/g, '');
      lastBool = false;
    } else if (type === 'fName') {
      fatherName = value.replace(/[^A-Za-z]/g, '');
      fatherBool = false;
    } else if (type === 'co_applicant') {
      co_applicant = value.replace(/[^A-Za-z]/g, '');
    } else if (type === 'email') {
      email = value;
      emailBool = false;
    } else if (type === 'address1') {
      address1 = value;
      address1Bool = false;
    } else if (type === 'address2') {
      address2 = value;
    } else if (type === 'landmark') {
      landmark = value;
      landmarkBool = false;
    } else if (type === 'district') {
      district = value;
      districtBool = false;
    } else if (type === 'city') {
      city = value;
      cityBool = false;
    } else if (type === 'pinCode') {
      pincode = value.replace(/[^0-9]/g, '');
      pincodeBool = false;
    } else if (type === 'telephone') {
      telephone = value.replace(/[^0-9]/g, '');
    } else if (type === 'mobile') {
      mobileNo = value.replace(/[^0-9]/g, '');
      mobileBool = false;
    } else if (type === 'pan') {
      panNo = value.replace(/[^0-9A-Za-z]/g, '');
    } else if (type === 'gst') {
      gstNo = value.replace(/[^0-9A-Za-z]/g, '');
    } else if (type === 'adhar') {
      adharno = value.replace(/[^0-9]/g, '');
    } else if (type === 'advCell') {
      srCellNo = value.replace(/[^0-9]/g, '');
    } else if (type === 'advName') {
      srAdvisoryName = value.replace(/[^A-Za-z]/g, '');
    } else if (type === 'wardNo') {
      wardNo = value.replace(/[^0-9A-Za-z]/g, '');
    } else if (type === 'bankName') {
      bankName = value.replace(/[^A-Za-z]/g, '');
    } else if (type === 'bankCity') {
      bankCity = value.replace(/[^A-Za-z]/g, '');
    } else if (type === 'bankCode') {
      bankCode = value.replace(/[^0-9A-Za-z]/g, '');
    }

    this.setState({
      loginID,
      password,
      cPassword,
      firstName,
      lastName,
      fatherName,
      co_applicant,
      email,
      address1,
      address2,
      landmark,
      pincode,
      city,
      telephone,
      mobileNo,
      panNo,
      wardNo,
      bankCity,
      bankCode,
      bankName,
      srAdvisoryName,
      srCellNo,
      gstNo,
      adharno,
      district,
      middleName,
      logidBool,
      passwordBool,
      cPasswordBool,
      titleBool,
      firstBool,
      lastBool,
      middleBool,
      fatherBool,
      emailBool,
      address1Bool,
      landmarkBool,
      districtBool,
      stateBool,
      pincodeBool,
      cityBool,
      mobileBool,
    });
  };

  validateEmail = () => {
    let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!pattern.test(this.state.email)) {
      showMessage({
        type: 'danger',
        message: 'Email is not valid',
        position: 'top',
      });
      this.setState({email: ''});
    }
  };

  submit = async () => {
    let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let {
      loginID,
      password,
      cPassword,
      firstName,
      lastName,
      fatherName,
      co_applicant,
      email,
      address1,
      address2,
      landmark,
      pincode,
      city,
      telephone,
      mobileNo,
      panNo,
      wardNo,
      bankCity,
      bankCode,
      bankName,
      srAdvisoryName,
      srCellNo,
      gstNo,
      adharno,
      district,
      dob,
      gender,
      states,
      accountType,
      middleName,
      logidBool,
      passwordBool,
      cPasswordBool,
      titleBool,
      firstBool,
      lastBool,
      middleBool,
      fatherBool,
      emailBool,
      address1Bool,
      landmarkBool,
      districtBool,
      stateBool,
      pincodeBool,
      cityBool,
      mobileBool,
      dobBool,
    } = this.state;

    console.warn('dob', dob);

    let {parentId, referId, position} = this.props.route.params;
    if (
      loginID &&
      password &&
      cPassword &&
      firstName &&
      middleName &&
      lastName &&
      fatherName &&
      email &&
      address1 &&
      landmark &&
      district &&
      states &&
      pincode &&
      city &&
      mobileNo &&
      dob &&
      // bankName &&
      // bankCity &&
      // srAdvisoryName &&
      // srCellNo &&
      middleName
    ) {
      if (adharno !== '' && adharno.length < 12) {
        showMessage({
          type: 'danger',
          message: 'Invalid Adhar Number',
        });
      } else if (panNo !== '' && panNo.length < 10) {
        showMessage({
          type: 'danger',
          message: 'Invalid Pan Number',
        });
      } else if (
        loginID !== '' &&
        loginID.match(/^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$/) == null
      ) {
        showMessage({
          type: 'danger',
          message: 'Login Id must be alphanumeric',
        });
      } else if (address1 !== '' && address1.length < 3) {
        showMessage({
          type: 'danger',
          message: 'Invalid address',
        });
      } else if (mobileNo.length < 10) {
        showMessage({
          type: 'danger',
          message: 'Invalid Mobile No.',
        });
      } else if (email !== '' && !pattern.test(email)) {
        showMessage({
          type: 'danger',
          message: 'Email is not valid',
        });
      } else {
        let data = {
          T_parentid: parentId,
          T_referid: referId,
          T_website: 'xyz',
          T_placement: position,
          T_logid: loginID,
          T_password: password,
          T_confirm: cPassword,
          T_acctype: accountType,
          T_title: gender,
          T_name1: firstName,
          T_name2: middleName,
          T_name3: lastName,
          T_father: fatherName,
          // T_coappname: co_applicant,
          T_email: email,
          T_addr1: address1,
          T_addr2: address2,
          T_nearby: landmark,
          T_district: district,
          T_city: city,
          T_state: states,
          T_country: 'India',
          T_pincode: pincode,
          T_telno: telephone,
          T_cellno: mobileNo,
          T_dobdd: moment(dob).format('DD'),
          T_dobmm: moment(dob).format('MM'),
          T_dobyy: moment(dob).format('YYYY'),
          T_pangir: panNo,
          // T_wardno: wardNo,
          // T_bankname: bankName,
          // T_bankcity: bankCity,
          // T_bankcode: bankCode,
          // T_advname: srAdvisoryName,
          // T_advno: srCellNo,
          // T_gstno: gstNo,
          // T_adhaarno: adharno,
        };
        // console.warn('data', JSON.stringify(data, undefined, 2));
        let token = await AsyncStorage.getItem('token');
        if (token !== null) {
          this.props.addTeamMemberData(data, token);
          this.props.commaonLoader(true);
        }
      }
    } else {
      showMessage({
        message: 'Please fill all fields mark with *',
        type: 'danger',
      });
    }
    if (loginID == '') {
      logidBool = true;
    }
    if (password == '') {
      passwordBool = true;
    }
    if (cPassword == '') {
      cPasswordBool = true;
    }
    if (gender == '') {
      titleBool = true;
    }
    if (firstName == '') {
      firstBool = true;
    }
    if (middleName == '') {
      middleBool = true;
    }
    if (lastName == '') {
      lastBool = true;
    }
    if (fatherName == '') {
      fatherBool = true;
    }
    if (email == '') {
      emailBool = true;
    }
    if (address1 == '') {
      address1Bool = true;
    }
    if (landmark == '') {
      landmarkBool = true;
    }
    if (district == '') {
      districtBool = true;
    }
    if (states == '') {
      stateBool = true;
    }
    if (pincode == '') {
      pincodeBool = true;
    }
    if (mobileNo == '') {
      mobileBool = true;
    }
    if (dob == '') {
      dobBool = true;
    }
    if (city == '') {
      cityBool = true;
    }
    this.setState({
      logidBool,
      passwordBool,
      cPasswordBool,
      titleBool,
      firstBool,
      lastBool,
      middleBool,
      fatherBool,
      emailBool,
      address1Bool,
      landmarkBool,
      districtBool,
      stateBool,
      pincodeBool,
      cityBool,
      mobileBool,
      dobBool,
    });
  };

  closeModal = data => {
    if (data == 'addMember') {
      this.setState({showModal: false}, () => {
        setTimeout(() => {
          // this.props.navigation.navigate('NewDSSignup', {
          //   logid: this.props.route.params.parentId,
          // });
          this.props.navigation.navigate('DSHome');
        }, 400);
      });
    }
  };

  componentDidUpdate(prevProps) {
    if (prevProps.addMemberResponse !== this.props.addMemberResponse) {
      if (
        this.props.addMemberResponse.response &&
        this.props.addMemberResponse.response.status == 200
      ) {
        // this.props.navigation.navigate('NewDSSignup', {
        //   logid: this.props.route.params.parentId,
        // });
        this.setState({
          showModal: true,
          showMessage:
            'Member is added successfully, Please provide the Direct Seller ID (Login ID) and the Password to the Direct Seller.OTP has been sent to the registered mobile number, it will be required for signing in. ',
        });
      } else {
        showMessage({
          type: 'danger',
          message: this.props.addMemberResponse.response.message,
        });
      }
    }
  }

  reset = () => {
    this.setState({
      loginID: '',
      password: '',
      cPassword: '',
      firstName: '',
      lastName: '',
      fatherName: '',
      co_applicant: '',
      email: '',
      address1: '',
      address2: '',
      landmark: '',
      pincode: '',
      city: '',
      telephone: '',
      mobileNo: '',
      panNo: '',
      wardNo: '',
      bankCity: '',
      bankCode: '',
      bankName: '',
      srAdvisoryName: '',
      srCellNo: '',
      gstNo: '',
      adharno: '',
      district: '',
      dob: '',
      gender: '',
      states: '',
      accountType: '',
      logidBool: false,
      passwordBool: false,
      cPasswordBool: false,
      titleBool: false,
      firstBool: false,
      lastBool: false,
      middleBool: false,
      fatherBool: false,
      emailBool: false,
      address1Bool: false,
      landmarkBool: false,
      districtBool: false,
      stateBool: false,
      pincodeBool: false,
      cityBool: false,
      mobileBool: false,
      dobBool: false,
    });
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <View>
          <Header
            {...this.props}
            back={true}
            title="Direct Seller Details"
            showCart={false}
            showNotification={true}
            headeHeight={60}
          />
        </View>
        <View style={{flex: 1}}>
          <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
            <View style={mainContainer}>
              <View style={commonScreenWidth}>
                <DSData />
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: '#e2e2e2',
                    paddingVertical: '10%',
                    paddingHorizontal: '2%',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Logid *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Logid"
                          value={this.state.loginID}
                          changeText={e => this.onChangeText(e, 'loginId')}
                        />
                        {this.state.logidBool ? (
                          <Text style={styles.errorText}>
                            {this.state.errorText}
                          </Text>
                        ) : null}
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Password *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="**********"
                          secureText={true}
                          changeText={e => this.onChangeText(e, 'password')}
                          value={this.state.password}
                        />
                        {this.state.passwordBool ? (
                          <Text style={styles.errorText}>
                            {this.state.errorText}
                          </Text>
                        ) : null}
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>
                          Confirm Password *
                        </Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="**********"
                          secureText={true}
                          changeText={e => this.onChangeText(e, 'cPassword')}
                          value={this.state.cPassword}
                        />
                        {this.state.cPasswordBool ? (
                          <Text style={styles.errorText}>
                            {this.state.errorText}
                          </Text>
                        ) : null}
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Account Type</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CommonDropdown
                          itemData={this.state.accountTypeItems}
                          onValueChange={value =>
                            this.setState({accountType: value})
                          }
                          value={this.state.accountType}
                          placeholderText="Select Account Type"
                        />
                      </View>
                    </View>
                  </View>
                  <View style={{marginVertical: '2%', paddingHorizontal: '2%'}}>
                    <Text style={styles.inputLabel}>Name *</Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                      alignItems: 'center',
                    }}>
                    <View style={{width: '48%'}}>
                      <CommonDropdown
                        itemData={this.state.genderType}
                        onValueChange={value =>
                          this.setState({gender: value, titleBool: false})
                        }
                        value={this.state.gender}
                        placeholderText="Select gender"
                      />
                      {this.state.titleBool ? (
                        <Text style={styles.errorText}>
                          {this.state.errorText}
                        </Text>
                      ) : null}
                    </View>
                    <View style={{width: '48%'}}>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="First Name"
                          changeText={e => this.onChangeText(e, 'firstName')}
                          value={this.state.firstName}
                        />
                        {this.state.firstBool ? (
                          <Text style={styles.errorText}>
                            {this.state.errorText}
                          </Text>
                        ) : null}
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                      alignItems: 'center',
                    }}>
                    <View style={{width: '48%'}}>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Middle Name"
                          changeText={e => this.onChangeText(e, 'middlename')}
                          value={this.state.middleName}
                        />
                        {this.state.middleBool ? (
                          <Text style={styles.errorText}>
                            {this.state.errorText}
                          </Text>
                        ) : null}
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Last Name"
                          changeText={e => this.onChangeText(e, 'lastName')}
                          value={this.state.lastName}
                        />
                        {this.state.lastBool ? (
                          <Text style={styles.errorText}>
                            {this.state.errorText}
                          </Text>
                        ) : null}
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      // justifyContent: 'space-around',
                    }}>
                    <View style={{width: '100%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Father Name *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Father Name"
                          changeText={e => this.onChangeText(e, 'fName')}
                          value={this.state.fatherName}
                        />
                        {this.state.fatherBool ? (
                          <Text style={styles.errorText}>
                            {this.state.errorText}
                          </Text>
                        ) : null}
                      </View>
                    </View>
                    {/* <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Co-Applicant</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Co-Applicant"
                          changeText={e => this.onChangeText(e, 'co_applicant')}
                          value={this.state.co_applicant}
                        />
                      </View>
                    </View> */}
                  </View>
                  <View>
                    <View>
                      <Text style={styles.inputLabel}>E-mail *</Text>
                    </View>
                    <View style={styles.commonMargin}>
                      <CustomTextInput
                        customStyle={true}
                        customStyleData={styles.textInputStyle}
                        placeholder="Email"
                        changeText={e => this.onChangeText(e, 'email')}
                        value={this.state.email}
                        onSubmit={() => this.validateEmail()}
                      />
                      {this.state.emailBool ? (
                        <Text style={styles.errorText}>
                          {this.state.errorText}
                        </Text>
                      ) : null}
                    </View>
                  </View>
                  <View>
                    <View>
                      <Text style={styles.inputLabel}>Address *</Text>
                    </View>
                    <View style={styles.commonMargin}>
                      <CustomTextInput
                        customStyle={true}
                        customStyleData={styles.textInputStyle}
                        placeholder="Address 1"
                        changeText={e => this.onChangeText(e, 'address1')}
                        value={this.state.address1}
                      />
                    </View>
                    {this.state.address1Bool ? (
                      <Text style={styles.errorText}>
                        {this.state.errorText}
                      </Text>
                    ) : null}
                    <View style={styles.commonMargin}>
                      <CustomTextInput
                        customStyle={true}
                        customStyleData={styles.textInputStyle}
                        placeholder="Address 2"
                        changeText={e => this.onChangeText(e, 'address2')}
                        value={this.state.address2}
                      />
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Landmark *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Landmark"
                          changeText={e => this.onChangeText(e, 'landmark')}
                          value={this.state.landmark}
                        />
                        {this.state.landmarkBool ? (
                          <Text style={styles.errorText}>
                            {this.state.errorText}
                          </Text>
                        ) : null}
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>District *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="District"
                          changeText={e => this.onChangeText(e, 'district')}
                          value={this.state.district}
                        />
                        {this.state.districtBool ? (
                          <Text style={styles.errorText}>
                            {this.state.errorText}
                          </Text>
                        ) : null}
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>State *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CommonDropdown
                          itemData={this.state.stateData}
                          onValueChange={value =>
                            this.setState({states: value, stateBool: false})
                          }
                          value={this.state.states}
                          placeholderText="Select Your State"
                        />
                        {this.state.stateBool ? (
                          <Text style={styles.errorText}>
                            {this.state.errorText}
                          </Text>
                        ) : null}
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Pin Code *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Pin Code"
                          changeText={e => this.onChangeText(e, 'pinCode')}
                          value={this.state.pincode}
                          keyboardType="numeric"
                        />
                        {this.state.pincodeBool ? (
                          <Text style={styles.errorText}>
                            {this.state.errorText}
                          </Text>
                        ) : null}
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>City *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="City"
                          changeText={e => this.onChangeText(e, 'city')}
                          value={this.state.city}
                        />
                        {this.state.cityBool ? (
                          <Text style={styles.errorText}>
                            {this.state.errorText}
                          </Text>
                        ) : null}
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Country</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="India"
                          value="India"
                        />
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Telephone</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="telephone"
                          changeText={e => this.onChangeText(e, 'telephone')}
                          value={this.state.telephone}
                          keyboardType="numeric"
                        />
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>
                          Mobile no for OTP *
                        </Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="mobile no"
                          changeText={e => this.onChangeText(e, 'mobile')}
                          value={this.state.mobileNo}
                          keyboardType="numeric"
                          maxLength={10}
                        />
                        {this.state.mobileBool ? (
                          <Text style={styles.errorText}>
                            {this.state.errorText}
                          </Text>
                        ) : null}
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Date of Birth *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <DatePicker
                          date={this.state.dob}
                          mode="date"
                          placeholder="Date Of Birth"
                          format="YYYY-MM-DD"
                          minDate="1900-05-01"
                          maxDate={moment().subtract('18', 'years')}
                          customStyles={{
                            dateInput: {
                              borderColor: 'transparent',
                              alignItems: 'flex-start',
                            },
                          }}
                          style={{
                            width: '100%',
                            borderColor: '#e2e2e2',
                            borderRadius: 6,
                            paddingLeft: '5%',
                            height: 50,
                            borderWidth: 1,
                            justifyContent: 'center',
                          }}
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          showIcon={false}
                          onDateChange={date => {
                            this.setState({
                              dob: date,
                              dobBool: false,
                            });
                          }}
                        />
                      </View>
                      {this.state.dobBool ? (
                        <Text style={styles.errorText}>
                          {this.state.errorText}
                        </Text>
                      ) : null}
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>PAN No.</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Pan No."
                          changeText={e => this.onChangeText(e, 'pan')}
                          value={this.state.panNo}
                          maxLength={10}
                        />
                      </View>
                    </View>
                  </View>
                  {/* <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Ward / Circle No.</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Ward No"
                          changeText={e => this.onChangeText(e, 'wardNo')}
                          value={this.state.wardNo}
                        />
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Bank Name *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Bank Name"
                          changeText={e => this.onChangeText(e, 'bankName')}
                          value={this.state.bankName}
                        />
                      </View>
                    </View>
                  </View> */}
                  {/* <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Bank City/Town *</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Bank City"
                          changeText={e => this.onChangeText(e, 'bankCity')}
                          value={this.state.bankCity}
                        />
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Bank Code</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Bank Code"
                          changeText={e => this.onChangeText(e, 'bankCode')}
                          value={this.state.bankCode}
                        />
                      </View>
                    </View>
                  </View> */}
                  {/* <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>
                          Sr. Advisor Name *
                        </Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Sr Advisory Name"
                          changeText={e => this.onChangeText(e, 'advName')}
                          value={this.state.srAdvisoryName}
                        />
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>
                          Sr. Adv Cell No. *
                        </Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Advisory Cell No."
                          changeText={e => this.onChangeText(e, 'advCell')}
                          value={this.state.srCellNo}
                          keyboardType="numeric"
                        />
                      </View>
                    </View>
                  </View> */}
                  {/* <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>GST No.</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="GST"
                          changeText={e => this.onChangeText(e, 'gst')}
                          value={this.state.gstNo}
                        />
                      </View>
                    </View>
                    <View style={{width: '48%'}}>
                      <View>
                        <Text style={styles.inputLabel}>Adhar No.</Text>
                      </View>
                      <View style={styles.commonMargin}>
                        <CustomTextInput
                          customStyle={true}
                          customStyleData={styles.textInputStyle}
                          placeholder="Adhar No."
                          changeText={e => this.onChangeText(e, 'adhar')}
                          value={this.state.adharno}
                          keyboardType="numeric"
                          maxLength={12}
                        />
                      </View>
                    </View>
                  </View> */}
                  <View style={styles.buttonContainer}>
                    <TouchableOpacity
                      style={styles.button}
                      onPress={this.submit}>
                      <Text style={{color: 'white'}}>Submit</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.button}
                      onPress={() => this.reset()}>
                      <Text style={{color: 'white'}}>Reset</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </KeyboardAwareScrollView>
          {this.props.isLoading ? <Loader /> : null}
          {this.state.showModal ? (
            <MessageModal
              showModal={this.state.showModal}
              closeModal={this.closeModal}
              previousScreen="addMember"
              showMessage={this.state.showMessage}
            />
          ) : null}
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    isLoading: state.CommonLoaderReducer.isLoading,
    addMemberResponse: state.GenealogyReducer.addMemberResponse,
  }),
  {...geanealogyActions, ...loadingActions},
)(AddTeamMemebr);

const styles = StyleSheet.create({
  textInputStyle: {
    borderWidth: 1,
    borderColor: '#e2e2e2',
    borderRadius: 6,
    paddingLeft: '5%',
    height: 50,
  },
  commonMargin: {
    marginVertical: '5%',
  },
  inputLabel: {
    fontWeight: 'bold',
  },
  button: {
    width: '30%',
    alignItems: 'center',
    paddingVertical: '3%',
    borderRadius: 30,
    backgroundColor: commonColor.color,
  },
  buttonContainer: {
    flexDirection: 'row',
    paddingVertical: '5%',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  errorText: {
    color: 'red',
  },
});
