import React, {useState, useEffect} from 'react';
import Header from '../../components/header';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Fashion1 from '../../assets/images/image_3.png';
import * as eventActions from '../../actions/eventActions';
import {useDispatch, useSelector} from 'react-redux';

const Events = props => {
  const [eventData, setEventData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [eventPhotos, setPhotos] = useState([]);
  const [imageUrl, setUrl] = useState('');

  const dispatch = useDispatch();
  const eventList = useSelector(state => state.EventReducer.eventList);

  useEffect(() => {
    dispatch(eventActions.getEventsList());
  }, []);

  useEffect(() => {
    if (Object.keys(eventList).length) {
      if (eventList.response && eventList.response.status == 200) {
        setLoading(false);
        setEventData(eventList.data.events);
        setUrl(eventList.data.basepath);
        setPhotos(eventList.data.photos);
      }
    }
  }, [eventList]);

  const viewPhotos = item => {
    eventPhotos.map((data, index) => {
      if (item[0] == data[0]) {
        props.navigation.navigate('ViewEvents', {
          photos: data[1],
          imageUrl: imageUrl,
          eventName: item,
        });
      }
    });
  };

  function _renderEvents(item, index) {
    return (
      <TouchableOpacity
        style={{marginVertical: hp('2%')}}
        onPress={() => viewPhotos(item)}>
        <View>
          <View
            style={{
              width: '100%',
              height: hp('25%'),
              position: 'absolute',
              zIndex: 3,
              backgroundColor: '#11111190',
              borderRadius: 30,
              justifyContent: 'center',
            }}>
            <View
              style={{
                width: '100%',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: 'white',
                  fontSize: hp('2.5%'),
                  fontWeight: 'bold',
                }}
                numberOfLines={1}>
                {item[0]}
              </Text>
              <Text
                style={{
                  color: 'white',
                  fontSize: hp('2.5%'),
                  fontWeight: 'bold',
                }}
                numberOfLines={1}>
                {item[1]}
              </Text>
            </View>
          </View>
          <Image
            source={{uri: imageUrl + eventPhotos[index][1][0]}}
            style={{width: '100%', height: hp('25%'), borderRadius: 30}}
          />
        </View>
      </TouchableOpacity>
    );
  }

  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          showCart={false}
          showNotification={true}
          headeHeight={60}
          title="Events"
          back={true}
        />
      </View>
      <View style={mainContainer}>
        <View style={[commonScreenWidth, {paddingBottom: '15%'}]}>
          {loading ? (
            <View style={{height: 500, justifyContent: 'center'}}>
              <ActivityIndicator size="large" color={commonColor.color} />
            </View>
          ) : (
            <FlatList
              data={eventData}
              renderItem={({item, index}) => _renderEvents(item, index)}
              keyExtractor={item => item[1]}
            />
          )}
        </View>
      </View>
    </View>
  );
};

export default Events;
