import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
  StyleSheet,
  Image,
  ActivityIndicator,
} from 'react-native';
import Header from '../../components/header';
import {
  InputTextLabel,
  commonColor,
  commonButton,
  commonButtonText,
  logoContainer,
  commonScreenWidth,
  mainContainer,
  logoStyle,
} from '../../components/commonStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CrossIcon from '../../assets/images/ic_close.png';
import * as cartActions from '../../actions/cartActions';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';
import AsyncStorage from '@react-native-community/async-storage';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ImageLoad from 'react-native-image-placeholder';
import {imageUrl, baseUrl} from '../../services';

const Cart = props => {
  const [cartData, setCartData] = useState([]);
  const [userToken, setToken] = useState('');
  const [loading, setLoading] = useState(true);
  const [profileData, setProfileData] = useState({});
  const [totalBv, setTotalBv] = useState('');

  const dispatch = useDispatch();
  const cartList = useSelector(state => state.CartReducer.cartList);
  const deleteCartResponse = useSelector(
    state => state.CartReducer.deleteCartResponse,
  );
  const updateCartResponse = useSelector(
    state => state.CartReducer.updateCartResponse,
  );

  const isLoading = useSelector(state => state.CommonLoaderReducer.isLoading);
  const getProfileData = useSelector(
    state => state.ProfileReducer.getProfileData,
  );

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        setToken(token);
        dispatch(cartActions.viewCart(token));
        dispatch(loadingAction.commaonLoader(true));
        setProfileData(getProfileData.data.query[0]);
      }
    });
  }, []);

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      AsyncStorage.getItem('token').then(token => {
        if (token !== null) {
          setToken(token);
          dispatch(cartActions.viewCart(token));
          dispatch(loadingAction.commaonLoader(true));
        }
      });
    });

    return unsubscribe;
  }, [props.navigation]);

  useEffect(() => {
    console.warn('cartList', JSON.stringify(cartList, undefined, 2));
    if (Object.keys(cartList).length) {
      if (cartList.response && cartList.response.status == 200) {
        let bv = 0;
        cartList.data.cart.length &&
          cartList.data.cart.map(item => {
            bv = bv + Number(item.nqty * item.BV);
          });
        setTotalBv(bv);
        setCartData(cartList.data.cart);
        dispatch(cartActions.emptyCartDelete());
        setLoading(false);
      }
    }
  }, [cartList]);

  useEffect(() => {
    if (Object.keys(deleteCartResponse).length) {
      if (
        deleteCartResponse.response &&
        deleteCartResponse.response.status == 200
      ) {
        showMessage({
          type: 'success',
          message: 'Product removed successfully',
        });
        dispatch(cartActions.viewCart(userToken));
        dispatch(loadingAction.commaonLoader(true));
      }
    }
  }, [deleteCartResponse]);

  useEffect(() => {
    if (Object.keys(updateCartResponse).length) {
      if (
        updateCartResponse.response &&
        updateCartResponse.response.status == 200
      ) {
        dispatch(cartActions.viewCart(userToken));
      }
    }
  }, [updateCartResponse]);

  const deleteCartData = async id => {
    let token = await AsyncStorage.getItem('token');
    if (token !== null) {
      dispatch(cartActions.deleteCart(id, token));
      dispatch(loadingAction.commaonLoader(true));
    }
  };

  const updateQuantity = (nqty, linenum, type) => {
    let initaialQuantity = parseInt(nqty);
    if (type == 'increase') {
      initaialQuantity = initaialQuantity + 1;
      dispatch(
        cartActions.updateCartData(linenum, initaialQuantity, userToken),
      );
      dispatch(loadingAction.commaonLoader(true));
    } else {
      if (initaialQuantity <= 1) {
        showMessage({
          message: 'Minimum qunatity can not be less than 1',
          type: 'warning',
        });
      } else {
        initaialQuantity = initaialQuantity - 1;
        dispatch(
          cartActions.updateCartData(linenum, initaialQuantity, userToken),
        );
        dispatch(loadingAction.commaonLoader(true));
      }
    }
  };

  const viewProduct = item => {
    props.navigation.navigate('ProductDetail', {
      packgeName: item.item_option,
      name: item.item_name,
    });
  };

  const _renderCartData = (item, index) => {
    return (
      <View style={styles.rowContainer}>
        <TouchableOpacity
          style={styles.rowImageContainer}
          onPress={() => viewProduct(item)}>
          <ImageLoad
            style={styles.rowImage}
            loadingStyle={{size: 'small', color: commonColor.color}}
            source={{uri: imageUrl + '/uploads/retail/' + item.photos}}
            resizeMode="contain"
          />
        </TouchableOpacity>
        <View style={styles.rowDataContainer}>
          <TouchableOpacity onPress={() => viewProduct(item)}>
            <Text style={styles.productNameText} numberOfLines={2}>
              {item.item_name}
            </Text>
          </TouchableOpacity>
          <View>
            <Text style={styles.productSkuText}>{item.item_option}</Text>
          </View>
          <View>
            <Text style={{fontWeight: 'normal', fontSize: hp('2%')}}>
              {'₹ ' + item.rate}
            </Text>
          </View>
          <View>
            <Text style={{fontWeight: 'normal', fontSize: hp('2%')}}>
              {'BV ' + ': ' + item.BV}
            </Text>
          </View>
          <View style={styles.quantityContainer}>
            <TouchableOpacity
              style={styles.decQuantity}
              onPress={() =>
                updateQuantity(item.nqty, item.linenum, 'decrease')
              }>
              <Text style={{color: 'white'}}>-</Text>
            </TouchableOpacity>
            <View
              style={[styles.decQuantity, {backgroundColor: 'transparent'}]}>
              <Text>{parseInt(item.nqty)}</Text>
            </View>
            <TouchableOpacity
              style={styles.decQuantity}
              onPress={() =>
                updateQuantity(item.nqty, item.linenum, 'increase')
              }>
              <Text style={{color: 'white'}}>+</Text>
            </TouchableOpacity>
          </View>
        </View>

        <TouchableOpacity
          style={{
            width: '10%',
            height: 30,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() => deleteCartData(item.linenum)}>
          <Image
            source={CrossIcon}
            style={{width: 25, height: 25}}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    );
  };

  const checkout = () => {
    if (
      cartList &&
      cartList.data &&
      cartList.data.grandtotal &&
      Number(cartList.data.grandtotal) > 50000
    ) {
      showMessage({
        type: 'danger',
        message: 'Order amount has to be lesser than 50,000/-',
      });
    } else {
      props.navigation.navigate('Checkout');
    }
  };

  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          back={true}
          showCart={true}
          showNotification={false}
          title="My Cart"
          showNotification={true}
          showCart={false}
        />
      </View>
      <View
        style={{
          flex: 1,
        }}>
        {/* <KeyboardAwareScrollView showsVerticalScrollIndicator={false}> */}
        <View style={styles.headingContainer}>
          <View style={{width: '50%', alignItems: 'flex-start'}}>
            <Text>Products</Text>
          </View>
          <View style={{width: '50%', alignItems: 'flex-end'}}>
            {cartData.length ? (
              <Text>{cartData.length + ' ITEM'}</Text>
            ) : (
              <Text>{0 + ' ITEM'}</Text>
            )}
          </View>
        </View>
        <View style={mainContainer}>
          {loading ? (
            <ActivityIndicator />
          ) : cartData.length > 0 ? (
            <View style={[commonScreenWidth, {height: hp('70%')}]}>
              <FlatList
                data={cartData}
                renderItem={({item, index}) => _renderCartData(item, index)}
              />
              {Object.keys(cartList).length > 0 ? (
                <View>
                  <View style={styles.priceContainer}>
                    <View style={{width: '50%', alignItems: 'flex-start'}}>
                      <Text>BV</Text>
                    </View>
                    <View style={{width: '50%', alignItems: 'flex-end'}}>
                      <Text style={[commonColor, {fontWeight: 'bold'}]}>
                        {totalBv}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.priceContainer}>
                    <View style={{width: '50%', alignItems: 'flex-start'}}>
                      <Text>Subtotal</Text>
                    </View>
                    <View style={{width: '50%', alignItems: 'flex-end'}}>
                      <Text style={[commonColor, {fontWeight: 'bold'}]}>
                        {'₹ ' + cartList.data.total}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.priceContainer}>
                    <View style={{width: '50%', alignItems: 'flex-start'}}>
                      <Text>Shipping</Text>
                    </View>
                    <View style={{width: '50%', alignItems: 'flex-end'}}>
                      <Text style={[commonColor, {fontWeight: 'bold'}]}>
                        {'₹ ' + cartList.data.shipping}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.priceContainer}>
                    <View style={{width: '50%', alignItems: 'flex-start'}}>
                      <Text>Grand total</Text>
                    </View>
                    <View style={{width: '50%', alignItems: 'flex-end'}}>
                      <Text style={[commonColor, {fontWeight: 'bold'}]}>
                        {'₹ ' + cartList.data.grandtotal}
                      </Text>
                    </View>
                  </View>
                </View>
              ) : null}
              {/* <View style={{marginVertical: '8%'}}> */}
              <View style={styles.priceContainer1}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => props.navigation.goBack()}>
                  <Text style={{color: 'white'}}>Continue Shopping</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={checkout}>
                  <Text style={{color: 'white'}}>Checkout</Text>
                </TouchableOpacity>
              </View>
              {/* </View> */}
            </View>
          ) : (
            <View>
              <Text>No Item in Cart</Text>
            </View>
          )}
        </View>
        {/* </KeyboardAwareScrollView> */}
        {isLoading ? <Loader /> : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  headingContainer: {
    flexDirection: 'row',
    backgroundColor: '#ededed',
    paddingHorizontal: '4%',
    paddingVertical: '4%',
    marginVertical: '5%',
  },
  priceContainer: {
    flexDirection: 'row',
    paddingHorizontal: '4%',
    borderBottomColor: '#e2e2e2',
    borderBottomWidth: 1,
    paddingVertical: '1.5%',
  },
  rowContainer: {
    flexDirection: 'row',
    marginVertical: '5%',
    borderBottomWidth: 1,
    borderBottomColor: '#e2e2e2',
    paddingBottom: '5%',
  },
  rowImageContainer: {
    width: '30%',
    alignItems: 'center',
    backgroundColor: '#e6f5f4',
    borderRadius: 30,
    marginHorizontal: '2%',
  },
  rowImage: {
    width: '80%',
    height: 100,
  },
  rowDataContainer: {
    width: '55%',
    paddingLeft: '5%',
  },
  productNameText: {
    fontWeight: 'bold',
    fontSize: hp('2.3%'),
  },
  productSkuText: {
    fontSize: 10,
    color: 'gray',
  },
  quantityContainer: {
    flexDirection: 'row',
    paddingTop: 10,
  },
  decQuantity: {
    paddingHorizontal: '5%',
    borderRadius: 6,
    paddingVertical: '2%',
    backgroundColor: commonColor.color,
  },
  priceContainer1: {
    flexDirection: 'row',
    paddingVertical: '5%',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  button: {
    width: '40%',
    alignItems: 'center',
    paddingVertical: '3%',
    borderRadius: 30,
    backgroundColor: commonColor.color,
  },
});

export default Cart;
