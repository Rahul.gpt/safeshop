import React, {useEffect, useState, Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  Image,
} from 'react-native';
import Header from '../../components/header';
import {Crousal} from '../category/index';
import {
  InputTextLabel,
  commonColor,
  commonButton,
  commonButtonText,
  logoContainer,
  commonScreenWidth,
  mainContainer,
  logoStyle,
} from '../../components/commonStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import StarRating from 'react-native-star-rating';
import Loader from '../../components/loader';
import {connect} from 'react-redux';
import * as productListingAction from '../../actions/productListingAction';
import * as loadingAction from '../../actions/commonLoaderAction';
import {imageUrl, baseUrl} from '../../services';
import ImageLoad from 'react-native-image-placeholder';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import {showMessage, hideMessage} from 'react-native-flash-message';
import * as cartActions from '../../actions/cartActions';
import ImageView from 'react-native-image-viewing';

class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      starCount: 3.5,
      loading: false,
      isImageViewVisible: false,
      imageData: [],
      detailsOfProduct: [],
      quantity: 1,
      title: '',
      colorOptions: [],
      sizeOptions: [],
      selectSize: '',
      selectColor: '',
    };
  }

  componentDidMount() {
    if (
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.packgeName
    ) {
      this.props.viewProduct(this.props.route.params.packgeName);
      this.setState({title: this.props.route.params.name});
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.productDetailsData !== this.props.productDetailsData) {
      if (this.props.productDetailsData.response.status == 200) {
        this.setState(
          {
            detailsOfProduct: this.props.productDetailsData.data.product,
          },
          () => {
            let imageData = [];
            let colorOptions = [];
            let sizeOptions = [];
            imageData.push({
              uri:
                imageUrl +
                '/uploads/retail/' +
                this.state.detailsOfProduct[0].photob,
            });
            this.props.productDetailsData.data.options.map(data => {
              // console.warn('data', data);
              if (data.setname === 'COLOUR') {
                colorOptions = data.values;
              } else {
                sizeOptions = data.values;
              }
            });
            this.setState({imageData, sizeOptions, colorOptions});
          },
        );
      }
    }

    if (prevProps.addCartResponse !== this.props.addCartResponse) {
      if (
        this.props.addCartResponse.response &&
        this.props.addCartResponse.response.status == 200
      ) {
        this.props.navigation.navigate('Cart');
      }
    }
  }

  onLayout = e => {
    this.setState({
      width: e.nativeEvent.layout.width,
    });
  };

  addToCart = async () => {
    let colorOptions = [...this.state.colorOptions];
    let sizeOptions = [...this.state.sizeOptions];
    let detailsOfProduct = [...this.state.detailsOfProduct];
    let quantity = this.state.quantity;
    let token = await AsyncStorage.getItem('token');
    if (token !== null) {
      if (detailsOfProduct[0].active1 == '0.0') {
        showMessage({
          message: 'This product is currently not available',
          type: 'warning',
        });
      } else if (colorOptions.length > 0 && this.state.selectColor == '') {
        showMessage({
          message: 'Please select color',
          type: 'warning',
        });
      } else if (sizeOptions.length > 0 && this.state.selectSize == '') {
        showMessage({
          message: 'Please select size',
          type: 'warning',
        });
      } else {
        let data = {
          package: detailsOfProduct[0].package,
          T_qty: quantity,
        };
        this.props.addCart(data, token);
        this.props.commaonLoader(true);
      }

      // this.props.navigation.navigate('Cart');
    } else {
      showMessage({
        message: 'You need to login before you want to add product in cart',
        type: 'info',
      });
      this.props.navigation.navigate('Login');
    }
  };

  handlePress = () => {
    this.setState({isImageViewVisible: true});
  };

  changeQuantiy = type => {
    let quantity = this.state.quantity;
    if (type == 'add') {
      quantity += 1;
    } else if (this.state.quantity > 1) {
      quantity -= 1;
    }
    this.setState({quantity});
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <View>
          <Header
            {...this.props}
            back={true}
            showCart={true}
            showNotification={false}
            title={this.state.title}
            showNotification={false}
            showCart={true}
            headeHeight={60}
          />
        </View>

        <View style={{flex: 1}}>
          {this.state.detailsOfProduct.length > 0 ? (
            <View style={{flex: 1}}>
              <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
                <TouchableOpacity
                  style={styles.imageContainer}
                  onPress={this.handlePress}>
                  <ImageLoad
                    loadingStyle={{size: 'small', color: commonColor.color}}
                    source={{
                      uri:
                        imageUrl +
                        '/uploads/retail/' +
                        this.state.detailsOfProduct[0].photob,
                    }}
                    style={styles.productImage}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
                <ImageView
                  images={this.state.imageData}
                  imageIndex={0}
                  visible={this.state.isImageViewVisible}
                  onRequestClose={() =>
                    this.setState({isImageViewVisible: false})
                  }
                />

                <View style={[mainContainer, {paddingBottom: '5%'}]}>
                  <View style={commonScreenWidth}>
                    <View style={styles.priceContainer}>
                      <View style={{width: '30%', justifyContent: 'center'}}>
                        <Text style={styles.currentPrice}>
                          {'₹ ' +
                            parseInt(this.state.detailsOfProduct[0].memamt)}
                        </Text>
                      </View>
                      <View
                        style={{
                          width: '30%',
                          alignItems: 'flex-start',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.regularPrice}>
                          {'MRP : ' +
                            '₹ ' +
                            parseInt(this.state.detailsOfProduct[0].mrp)}
                        </Text>
                      </View>
                      <View style={styles.quantityContainer}>
                        <TouchableOpacity
                          style={styles.quantityButton}
                          onPress={() => this.changeQuantiy('minus')}>
                          <Text style={{color: 'white'}}>-</Text>
                        </TouchableOpacity>
                        <View
                          style={[
                            styles.quantityButton,
                            {backgroundColor: 'transparent'},
                          ]}>
                          <Text>{this.state.quantity}</Text>
                        </View>
                        <TouchableOpacity
                          style={styles.quantityButton}
                          onPress={() => this.changeQuantiy('add')}>
                          <Text style={{color: 'white'}}>+</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View style={[styles.priceContainer, {paddingVertical: 0}]}>
                      <View
                        style={{
                          flexDirection: 'row',
                          width: '50%',
                          alignItems: 'center',
                        }}>
                        <View style={{width: '25%'}}>
                          <Text>SKU : </Text>
                        </View>
                        <View style={{width: '75%', justifyContent: 'center'}}>
                          <Text style={[commonColor, {fontWeight: 'bold'}]}>
                            {this.state.detailsOfProduct[0].package}
                          </Text>
                        </View>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          width: '50%',
                          justifyContent: 'flex-end',
                        }}>
                        <View>
                          <Text>Availability : </Text>
                        </View>
                        <View>
                          {this.state.detailsOfProduct[0].active1 == '0.0' ? (
                            <Text style={{fontWeight: 'bold', color: 'red'}}>
                              Out of stock
                            </Text>
                          ) : (
                            <Text style={[commonColor, {fontWeight: 'bold'}]}>
                              In Stock
                            </Text>
                          )}
                        </View>
                      </View>
                    </View>
                    <View style={styles.starContainer}>
                      <View style={{width: '50%'}}>
                        <Text>
                          {'BV : ' + this.state.detailsOfProduct[0].bv}
                        </Text>
                      </View>
                      <View style={{width: '30%'}}>
                        <StarRating
                          disabled={false}
                          maxStars={5}
                          rating={this.state.starCount}
                          starSize={15}
                          starStyle={{color: '#fb9708'}}
                        />
                      </View>
                    </View>
                    <View style={styles.nameContainer}>
                      <View style={{paddingBottom: '3%'}}>
                        <Text style={styles.name}>
                          {this.state.detailsOfProduct[0].detl}
                        </Text>
                      </View>
                    </View>
                    {this.state.sizeOptions.length ||
                    this.state.colorOptions.length ? (
                      <View
                        style={[styles.nameContainer, {flexDirection: 'row'}]}>
                        <View style={styles.mainOptionContainer}>
                          {this.state.sizeOptions.length ? (
                            <View>
                              <View style={{paddingVertical: '5%'}}>
                                <Text>Select Size</Text>
                              </View>
                              <View style={styles.optionContainer}>
                                {this.state.sizeOptions.map((size, index) => {
                                  return (
                                    <TouchableOpacity
                                      style={[
                                        styles.sizeContainer,
                                        {
                                          borderColor:
                                            this.state.selectSize === size.value
                                              ? 'black'
                                              : 'transparent',
                                          borderWidth: 2,
                                        },
                                      ]}
                                      onPress={() =>
                                        this.setState({selectSize: size.value})
                                      }>
                                      <View>
                                        <Text style={{fontWeight: 'bold'}}>
                                          {size.value}
                                        </Text>
                                      </View>
                                    </TouchableOpacity>
                                  );
                                })}
                              </View>
                            </View>
                          ) : null}
                        </View>
                        <View style={styles.mainOptionContainer}>
                          {this.state.colorOptions.length ? (
                            <View style={{paddingVertical: '5%'}}>
                              <Text>Select Color</Text>
                              <View
                                style={[
                                  styles.optionContainer,
                                  {paddingVertical: '5%'},
                                ]}>
                                {this.state.colorOptions.map(size => {
                                  return (
                                    <TouchableOpacity
                                      style={[
                                        styles.colorContainer,
                                        {
                                          backgroundColor: size.colorcode,
                                          borderColor:
                                            this.state.selectColor ===
                                            size.value
                                              ? 'black'
                                              : 'transparent',
                                          borderWidth: 2,
                                        },
                                      ]}
                                      onPress={() =>
                                        this.setState({selectColor: size.value})
                                      }>
                                      <View />
                                    </TouchableOpacity>
                                  );
                                })}
                              </View>
                            </View>
                          ) : null}
                        </View>
                      </View>
                    ) : null}
                    <View style={styles.priceContainer}>
                      <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.props.navigation.goBack()}>
                        <Text style={{color: 'white'}}>Back</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.addToCart()}>
                        <Text style={{color: 'white'}}>Add to Cart</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </KeyboardAwareScrollView>
            </View>
          ) : (
            <View style={styles.loaderContainer}>
              <ActivityIndicator size="large" color={commonColor.color} />
            </View>
          )}
        </View>

        {this.state.loading ? <Loader /> : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imageContainer: {
    height: 300,
    width: '100%',
    backgroundColor: 'white',
  },
  productImage: {
    width: '100%',
    height: '100%',
  },
  priceContainer: {
    flexDirection: 'row',
    paddingVertical: '5%',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  currentPrice: {
    color: '#fd6824',
    fontWeight: 'bold',
    fontSize: hp('2.7%'),
  },
  regularPrice: {
    color: 'gray',
    textDecorationLine: 'line-through',
    fontSize: hp('2.4%'),
  },
  quantityContainer: {
    flexDirection: 'row',
    width: '30%',
    justifyContent: 'flex-end',
  },
  quantityButton: {
    paddingHorizontal: 10,
    borderRadius: 6,
    paddingVertical: 2,
    backgroundColor: commonColor.color,
  },
  starContainer: {
    paddingVertical: '5%',
    borderBottomColor: '#e2e2e2',
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  nameContainer: {
    paddingVertical: '5%',
    borderBottomColor: '#e2e2e2',
    borderBottomWidth: 1,
  },
  name: {
    fontSize: hp('3%'),
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  button: {
    width: '30%',
    alignItems: 'center',
    paddingVertical: '3%',
    borderRadius: 30,
    backgroundColor: commonColor.color,
  },
  optionContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    paddingRight: 10,
    width: '100%',
    // paddingTop: '5%',
  },
  mainOptionContainer: {
    width: '50%',
    alignItems: 'flex-start',
  },
  sizeContainer: {
    paddingHorizontal: '5%',
    paddingVertical: '3%',
    backgroundColor: '#e2e2e2',
    marginHorizontal: 2,
    marginBottom: 10,
  },
  colorContainer: {
    height: 30,
    width: 30,
    borderRadius: 15,
    marginHorizontal: 2,
  },
  loaderContainer: {
    height: 500,
    justifyContent: 'center',
  },
});

export default connect(
  state => ({
    isLoading: state.CommonLoaderReducer.isLoading,
    productDetailsData: state.ProductListingReducer.productDetailsData,
    addCartResponse: state.CartReducer.addCartResponse,
  }),
  {...loadingAction, ...productListingAction, ...cartActions},
)(ProductDetail);
