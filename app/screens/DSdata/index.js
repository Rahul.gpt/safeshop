import React, {useState, useEffect} from 'react';
import Header from '../../components/header';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import BannerBox from '../../assets/images/box.png';
import {commonScreenWidth, mainContainer} from '../../components/commonStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import SquareBox from '../../assets/images/square_box.png';
import NewDSSignup from '../../assets/images/new_direct_seller.png';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import {useDispatch, useSelector} from 'react-redux';

const DSData = props => {
  const [state, setState] = useState({
    DSLogId: '',
    lastPurchaseDate: '',
    DSLevel: '',
    parentDSLogId: '',
    referenceDSLogId: '',
    disqualificationdate: '',
    newData: false,
  });

  const dispatch = useDispatch();

  const loginUserData = useSelector(state => state.LoginReducer.loginUserData);

  useEffect(() => {
    if (Object.keys(loginUserData).length > 0) {
      let lastPurchaseDates;
      let initialStatus = loginUserData.initstatus;
      if (loginUserData.lastbuydate == null) {
        lastPurchaseDates = '';
      } else {
        lastPurchaseDates = moment(loginUserData.lastbuydate).format(
          'YYYY-MM-DD',
        );
      }
      if (initialStatus == 'F') {
        initialStatus = 'Fresher';
      } else if (initialStatus == 'S') {
        initialStatus = 'Silver';
      } else if (initialStatus == 'G') {
        initialStatus = 'Gold';
      } else if (initialStatus == 'P') {
        initialStatus = 'Pearl';
      } else if (initialStatus == 'T') {
        initialStatus = 'Topaz';
      } else if (initialStatus == 'E') {
        initialStatus = 'Emarald';
      } else if (initialStatus == 'R') {
        initialStatus = 'Ruby';
      } else if (initialStatus == 'D') {
        initialStatus = 'Diamond';
      } else if (initialStatus == '2') {
        initialStatus = 'Double Diamond';
      } else if (initialStatus == '3') {
        initialStatus = 'Triple Diamond';
      } else if (initialStatus == 'V') {
        initialStatus = 'Venus';
      } else if (initialStatus == 'M') {
        initialStatus = 'Mercury';
      } else if (initialStatus == 'C') {
        initialStatus = 'Crown';
      } else if (initialStatus == 'A') {
        initialStatus = 'Crown Ambassador';
      } else {
        initialStatus = 'Fresher';
      }
      setState({
        ...state,
        DSLogId: loginUserData.logid,
        lastPurchaseDate: lastPurchaseDates,
        DSLevel: initialStatus,
        parentDSLogId: loginUserData.parentid,
        referenceDSLogId: loginUserData.referid,
      });
    } else {
      setState({
        ...state,
        DSLogId: '',
        lastPurchaseDate: '',
        DSLevel: '',
        parentDSLogId: '',
        referenceDSLogId: '',
      });
    }
  }, [loginUserData]);

  return (
    <ImageBackground
      source={BannerBox}
      style={styles.bannerStyle}
      imageStyle={{borderRadius: 10, opacity: 0.7}}>
      {props.allDetails ? (
        <View style={{padding: '5%'}}>
          <View style={styles.rowContainer}>
            <View style={styles.rowLeftContainer}>
              <Text style={styles.rowLeftText}>DS Logid</Text>
            </View>
            <View style={styles.rowRightContainer}>
              <Text style={styles.rowRightText}>{state.DSLogId}</Text>
            </View>
          </View>
          <View style={styles.rowContainer}>
            <View style={styles.rowLeftContainer}>
              <Text style={styles.rowLeftText}>
                Last Retail Purchase Detail
              </Text>
            </View>
            <View style={styles.rowRightContainer}>
              <Text style={styles.rowRightText}>{state.lastPurchaseDate}</Text>
            </View>
          </View>
          <View style={styles.rowContainer}>
            <View style={styles.rowLeftContainer}>
              <Text style={styles.rowLeftText}>DS Level</Text>
            </View>
            <View style={styles.rowRightContainer}>
              <Text style={styles.rowRightText}>{state.DSLevel}</Text>
            </View>
          </View>
          <View style={styles.rowContainer}>
            <View style={styles.rowLeftContainer}>
              <Text style={styles.rowLeftText}>Parent DS LoginID</Text>
            </View>
            <View style={styles.rowRightContainer}>
              <Text style={styles.rowRightText}>{state.parentDSLogId}</Text>
            </View>
          </View>
          <View style={styles.rowContainer}>
            <View style={styles.rowLeftContainer}>
              <Text style={styles.rowLeftText}>Reference DS LoginID</Text>
            </View>
            <View style={styles.rowRightContainer}>
              <Text style={styles.rowRightText}>{state.referenceDSLogId}</Text>
            </View>
          </View>
          <View style={styles.rowContainer}>
            <View style={styles.rowLeftContainer}>
              <Text style={styles.rowLeftText}>Disqualification Date</Text>
            </View>
            <View style={styles.rowRightContainer}>
              <Text style={styles.rowRightText}>30-03-2020</Text>
            </View>
          </View>
        </View>
      ) : (
        <View style={{padding: '5%'}}>
          <View style={styles.rowContainer}>
            <View style={styles.rowLeftContainer}>
              <Text style={styles.rowLeftText}>DS Logid</Text>
            </View>
            <View style={styles.rowRightContainer}>
              <Text style={styles.rowRightText}>{state.DSLogId}</Text>
            </View>
          </View>
        </View>
      )}
    </ImageBackground>
  );
};

export default DSData;

const styles = StyleSheet.create({
  rowContainer: {
    flexDirection: 'row',
  },
  rowLeftContainer: {
    width: '50%',
    alignItems: 'flex-start',
  },
  rowRightContainer: {
    width: '50%',
    alignItems: 'flex-end',
  },
  bannerStyle: {
    width: '100%',
    marginVertical: '4%',
  },
  rowLeftText: {
    fontSize: hp('1.7%'),
  },
  rowRightText: {
    color: 'white',
    textTransform: 'uppercase',
    fontSize: hp('1.7%'),
  },
});
