import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  TextInput,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import Logo from '../../assets/images/logo.png';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from '../../components/commonTextInput';
import {
  InputTextLabel,
  commonColor,
  commonButton,
  commonButtonText,
  logoContainer,
  commonScreenWidth,
  mainContainer,
  logoStyle,
} from '../../components/commonStyle';
import Icon from 'react-native-vector-icons/Ionicons';
import * as forgotPasswordAction from '../../actions/forgotPasswordAction';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';
import OtpModal from '../../components/otpModal';
import * as otpActions from '../../actions/otpActions';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

function ForgotPassword(props) {
  const [state, setState] = useState({
    email: '',
    showOtpModal: false,
    otpValue: '',
  });

  const dispatch = useDispatch();
  const forgotResponse = useSelector(
    state => state.ForgotPasswordReducer.forgotResponse,
  );
  const isLoading = useSelector(state => state.CommonLoaderReducer.isLoading);
  const otpData = useSelector(state => state.OtpReducer.otpData);

  useEffect(() => {
    if (Object.keys(forgotResponse).length) {
      if (forgotResponse.response.status == 200) {
        console.warn('forgotResponse', forgotResponse);
        setState({
          ...state,
          showOtpModal: true,
          otpValue: forgotResponse.data.otp,
        });
        showMessage({
          type: 'success',
          message: 'Otp is successfully send to your registered mobile numer',
        });
        dispatch(forgotPasswordAction.emptyForgot());
      } else {
        // dispatch(forgotPasswordAction.emptyForgot());
        showMessage({
          message: forgotResponse.data,
          type: 'danger',
        });
      }
    }
  }, [forgotResponse]);

  // useEffect(() => {
  //   if (Object.keys(otpData).length) {
  //     setState({
  //       ...state,
  //       otpValue: forgotResponse.data.otp,
  //     });
  //     dispatch(otpActions.emptyOtpData());
  //   }
  // }, [otpData]);

  function submit() {
    if (!state.email) {
      showMessage({
        message: 'Please enter your logid',
        type: 'danger',
      });
    } else {
      let data = {
        email: state.email,
      };
      dispatch(forgotPasswordAction.forgotPassword(data));
      dispatch(loadingAction.commaonLoader(true));
    }
  }

  const handleModal = () => {
    setState({...state, showOtpModal: false});
  };

  const crossModal = () => {
    setState({...state, showOtpModal: false});
  };

  const verifiedStatus = value => {
    console.warn('value from child', value);
    setState({...state, showOtpModal: false});
    if (value == true) {
      props.navigation.navigate('ResetPassword', {
        previousScreen: 'ForgotPassword',
        oldPasswordField: false,
        userToken: '',
        loginId: state.email,
        cellno: '',
      });
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <SafeAreaView style={{flex: 0, backgroundColor: 'white'}} />
      <SafeAreaView style={{flex: 1}}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity style={{width: '20%', alignItems: 'center'}}>
            <Icon
              name="ios-arrow-round-back"
              size={50}
              color="#000"
              onPress={() => props.navigation.goBack()}
            />
          </TouchableOpacity>
          <View style={{width: '80%'}}>
            <Text style={{fontSize: hp('2.2%')}} numberOfLines={1}>
              Forgot Password
            </Text>
          </View>
        </View>
        <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
          <View style={mainContainer}>
            <View style={commonScreenWidth}>
              <View style={logoContainer}>
                <Image source={Logo} style={logoStyle} resizeMode="contain" />
              </View>
              <View>
                <View>
                  <Text style={InputTextLabel}>logid</Text>
                </View>
                <View>
                  <CustomTextInput
                    value={state.email}
                    changeText={e => setState({...state, email: e})}
                    placeholder="Enter your logid"
                  />
                </View>
              </View>
              <View style={{marginVertical: '6%'}}>
                <View>
                  <TouchableOpacity style={commonButton} onPress={submit}>
                    <Text style={commonButtonText}>submit</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
        {state.showOtpModal ? (
          <OtpModal
            showOtpModal={state.showOtpModal}
            handleModal={handleModal}
            crossModal={crossModal}
            otpValue={state.otpValue}
            verifiedStatus={verifiedStatus}
            loginID={state.email}
          />
        ) : null}
      </SafeAreaView>
    </View>
  );
}

export default ForgotPassword;
