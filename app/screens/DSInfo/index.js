import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
  StyleSheet,
  Image,
  ActivityIndicator,
} from 'react-native';
import Header from '../../components/header';
import {
  InputTextLabel,
  commonColor,
  commonButton,
  commonButtonText,
  logoContainer,
  commonScreenWidth,
  mainContainer,
  logoStyle,
} from '../../components/commonStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import * as allDSActions from '../../actions/allDSActions';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';
import AsyncStorage from '@react-native-community/async-storage';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import moment from 'moment';

const DSInfo = props => {
  const [info, setInfo] = useState([]);
  const [loading, setLoading] = useState(true);
  const [referids, setReferids] = useState([]);

  const dispatch = useDispatch();
  const getDSData = useSelector(state => state.AllDSReducer.getDSData);

  useEffect(() => {
    if (props.route && props.route.params && props.route.params.logid) {
      AsyncStorage.getItem('token').then(token => {
        if (token !== null) {
          dispatch(allDSActions.getDSInfo(props.route.params.logid, token));
        }
      });
    }
  }, []);

  useEffect(() => {
    console.warn('getDSData', JSON.stringify(getDSData, undefined, 2));
    if (Object.keys(getDSData).length) {
      if (getDSData.response && getDSData.response.status == 200) {
        if (getDSData.data && getDSData.data.query.length) {
          setInfo(getDSData.data.query[0]);
          setReferids(getDSData.data.referids);
          // dispatch(allDSActions.emptyDSInfo());
        } else {
          setInfo([]);
        }

        setLoading(false);
      }
    }
  }, [getDSData]);

  useEffect(() => {
    console.warn('info', info);
  });

  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          back={true}
          showCart={true}
          showNotification={false}
          title="DS Info"
          showNotification={true}
          showCart={false}
        />
      </View>
      <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
        {loading ? (
          <View
            style={{
              height: 500,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ActivityIndicator color={commonColor.color} size="large" />
          </View>
        ) : Object.keys(info).length ? (
          <View style={mainContainer}>
            <View
              style={[
                commonScreenWidth,
                {backgroundColor: 'white', elevation: 5, marginVertical: '5%'},
              ]}>
              <View style={styles.rowContainer}>
                <View style={styles.row}>
                  <Text style={styles.rowText}>Login ID</Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.rowText}>{info.logid}</Text>
                </View>
              </View>
              <View style={styles.rowContainer}>
                <View style={styles.row}>
                  <Text style={styles.rowText}>Name</Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.rowText}>
                    {info.title + ' ' + info.name}
                  </Text>
                </View>
              </View>
              <View style={styles.rowContainer}>
                <View style={styles.row}>
                  <Text style={styles.rowText}>Pay Status</Text>
                </View>
                <View style={styles.row}>
                  {info.memberflag == '1' ? (
                    <Text style={styles.rowText}>Paid</Text>
                  ) : (
                    <Text style={styles.rowText}>Not Paid</Text>
                  )}
                </View>
              </View>
              <View style={styles.rowContainer}>
                <View style={styles.row}>
                  <Text style={styles.rowText}>Parent ID</Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.rowText}>{info.parentid}</Text>
                </View>
              </View>
              <View style={styles.rowContainer}>
                <View style={styles.row}>
                  <Text style={styles.rowText}>Reference ID</Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.rowText}>{info.referid}</Text>
                </View>
              </View>
              <View style={styles.rowContainer}>
                <View style={styles.row}>
                  <Text style={styles.rowText}>
                    Eligible for Team Royalty Remuneration?
                  </Text>
                </View>
                <View style={styles.row}>
                  {info.memberflag == '1' &&
                  info.activeflag == '1' &&
                  (info.expiryflag == '0' || info.expiryflag == null) &&
                  (info.disableflag == '0' || info.disableflag == null) &&
                  (info.termflag == '0' || info.termflag == null) ? (
                    <Text style={styles.rowText}>Yes - Eligible</Text>
                  ) : null}
                  {info.memberflag == '0' ? (
                    <Text style={styles.rowText}>
                      NO Your DSA ID is not Active.{' '}
                    </Text>
                  ) : null}
                  {info.memberflag == '1' && info.activeflag == '0' ? (
                    info.id1 == null || info.id1 == '' ? (
                      <Text style={styles.rowText}>
                        NO - You need 2 direct referrals of Direct Sellers.
                      </Text>
                    ) : info.id2 == null || info.id2 == '' ? (
                      <Text style={styles.rowText}>
                        NO - You need 1 more direct referral of Direct Seller.
                      </Text>
                    ) : null
                  ) : null}
                  {info.expiryflag == '1' ? (
                    <Text style={styles.rowText}>NO - Period is expired.</Text>
                  ) : null}
                  {info.disableflag == '1' ? (
                    <Text style={styles.rowText}>NO - Logid is Disabled.</Text>
                  ) : null}
                  {info.termflag == '1' ? (
                    <Text style={styles.rowText}>
                      {' '}
                      NO - Logid is Terminated.
                    </Text>
                  ) : null}
                </View>
              </View>
              <View style={styles.rowContainer}>
                <View style={styles.row}>
                  <Text style={styles.rowText}>
                    Eligible for Team Retail Shopping Remuneration?
                  </Text>
                </View>
                <View style={styles.row}>
                  {info.retailflag == '0' || info.retailflag == null ? (
                    <Text style={styles.rowText}>
                      Retail purchase is NOT sufficient.
                    </Text>
                  ) : null}
                  {moment(info.lastincomedate).format('YYYY-MM-DD') ||
                  info.lastincomedate ==
                    null <
                      moment()
                        .subtract('3', 'months')
                        .format('YYYY-MM-DD') ? (
                    <Text style={styles.rowText}>
                      NO: Team Royalty Remuneration is not sufficient.
                    </Text>
                  ) : info.retailflag == '1' ? (
                    <Text style={styles.rowText}>Yes - Eligible.</Text>
                  ) : null}
                </View>
              </View>
              <View style={styles.rowContainer}>
                <View style={styles.row}>
                  <Text style={styles.rowText}>
                    Sales with your Direct Reference
                  </Text>
                </View>
                <View style={styles.row}>
                  {referids.length
                    ? referids.map((data, index) => {
                        let gbv = '';
                        let bulls = '';
                        let paydate = moment(data.paydate).format('DD-MM-YYYY');
                        if (data.halfflag == 0) gbv = '200 GBV';
                        if (data.halfflag == 1) gbv = '100 GBV';
                        if (data.halfflag == 2) gbv = '400 GBV';
                        if (data.monoflag == 1) bulls = '----Booster Income';
                        return (
                          <View>
                            <Text>
                              {Number(index) +
                                1 +
                                ' . ' +
                                data.logid +
                                ' --- ' +
                                gbv +
                                ' --- ' +
                                paydate +
                                ' --- ' +
                                bulls}
                            </Text>
                          </View>
                        );
                      })
                    : null}
                </View>
              </View>
            </View>
          </View>
        ) : (
          <View
            style={{
              height: 500,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text>No Data Found</Text>
          </View>
        )}
      </KeyboardAwareScrollView>
    </View>
  );
};

export default DSInfo;

const styles = StyleSheet.create({
  rowContainer: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#e2e2e250',
    paddingVertical: '5%',
  },
  row: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  rowText: {
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
