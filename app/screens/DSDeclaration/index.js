import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  InputTextLabel,
  commonColor,
  commonButton,
  commonButtonText,
  logoContainer,
  commonScreenWidth,
  mainContainer,
  logoStyle,
} from '../../components/commonStyle';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {showMessage} from 'react-native-flash-message';
import HTML from 'react-native-render-html';
import {IGNORED_TAGS} from 'react-native-render-html/src/HTMLUtils';
import * as forgotPasswordAction from '../../actions/forgotPasswordAction';
import Header from '../../components/header';

const DSDeclaration = props => {
  const [bool, setBool] = useState(true);
  const [data, setData] = useState('');
  const [userToken, setToken] = useState('');

  const dispatch = useDispatch();
  const declData = useSelector(state => state.ForgotPasswordReducer.declData);

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        dispatch(forgotPasswordAction.getDeclaration(token));
      }
    });
  }, []);

  useEffect(() => {
    console.warn('getAgreeData', declData);
    if (Object.keys(declData).length) {
      setData(declData);
      setBool(false);
    }
  }, [declData]);

  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          back={true}
          title="Declaration"
          showCart={false}
          showNotification={true}
          headeHeight={60}
        />
      </View>
      <View style={{flex: 1}}>
        <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
          {bool ? (
            <View
              style={{
                height: 500,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <ActivityIndicator color={commonColor.color} size="large" />
            </View>
          ) : (
            <View style={mainContainer}>
              <View style={[commonScreenWidth, {paddingBottom: '5%'}]}>
                <HTML
                  html={data}
                  imagesMaxWidth={Dimensions.get('window').width}
                  ignoredStyles={['line-height']}
                  // onLinkPress={url => console.warn('clicked link: ', url)}
                />
              </View>
            </View>
          )}
        </KeyboardAwareScrollView>
      </View>
    </View>
  );
};

export default DSDeclaration;
