import React, {Component, useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import Header from '../../components/header';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
  InputTextLabel,
  commonButton,
  commonButtonText,
} from '../../components/commonStyle';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';
import * as grievenceActions from '../../actions/grievenceActions';
import * as loadingAction from '../../actions/commonLoaderAction';
import CustomTextInput from '../../components/commonTextInput';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';

const GrievenceCell = props => {
  const [grievenceData, setGrievenceData] = useState({});
  const [loading, setLoading] = useState(true);
  const [state, setState] = useState({
    name: '',
    email: '',
    mobile: '',
    company: '',
    subject: '',
    message: '',
    nameBool: false,
    emailBool: false,
    mobileBool: false,
    companyBool: false,
    subjectBool: false,
    msgBool: false,
    errorText: '* Required',
  });

  const dispatch = useDispatch();
  const getQuery = useSelector(state => state.GrievenceReducer.getQuery);
  const submitQueryResponse = useSelector(
    state => state.GrievenceReducer.submitQueryResponse,
  );

  useEffect(() => {
    dispatch(grievenceActions.getQueryData());
    // dispatch(loadingAction.commaonLoader(true));
  }, []);

  useEffect(() => {
    if (Object.keys(getQuery).length) {
      if (getQuery.response && getQuery.response.status == 200) {
        setGrievenceData(getQuery.data.message);
        setLoading(false);
      }
    }
  }, [getQuery]);

  useEffect(() => {
    if (Object.keys(submitQueryResponse).length) {
      if (
        submitQueryResponse.response &&
        submitQueryResponse.response.status == 200
      ) {
        showMessage({
          type: 'success',
          message: 'Your query is submitted successfully',
        });
        dispatch(grievenceActions.emptySubmitQuery());
        AsyncStorage.getItem('token').then((token) => {
          if(token!==null){
            props.navigation.navigate('DSHome')
          } else {
            props.navigation.navigate('Category')
          }
        })
      } else {
        showMessage({
          type: 'danger',
          message: submitQueryResponse.response.message,
        });
        dispatch(grievenceActions.emptySubmitQuery());
      }
    }
  }, [submitQueryResponse]);

  const onChangeText = (value, type) => {
    let {
      name,
      mobile,
      message,
      subject,
      email,
      company,
      nameBool,
      emailBool,
      mobileBool,
      msgBool,
    } = state;
    if (type === 'name') {
      name = value.replace(/[^A-Za-z_ ]/g, '');
      nameBool = false;
    } else if (type === 'email') {
      email = value;
      emailBool = false;
    } else if (type === 'mobile') {
      mobile = value.replace(/[^0-9]/g, '');
      mobileBool = false;
    } else if (type === 'company') {
      company = value.replace(/[^A-Za-z_ ]/g, '');
    } else if (type === 'subject') {
      subject = value.replace(/[^A-Za-z_ ]/g, '');
    } else if (type === 'message') {
      message = value.replace(/[^A-Za-z_ ]/g, '');
      msgBool = false;
    }

    setState({
      ...state,
      name,
      mobile,
      company,
      message,
      subject,
      email,
      nameBool,
      emailBool,
      mobileBool,
      msgBool,
    });
  };

  const validateEmail = () => {
    let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!pattern.test(state.email)) {
      showMessage({
        type: 'danger',
        message: 'Email is not valid',
        position: 'top',
      });
      setState({...state, email: ''});
    }
  };

  const sendQuery = () => {
    let {
      name,
      mobile,
      message,
      subject,
      email,
      company,
      nameBool,
      emailBool,
      mobileBool,
      msgBool,
    } = state;
    if (!name || !mobile || !message || !email) {
      showMessage({
        type: 'danger',
        message: 'All fields are mandatory',
      });
    }
    if (email == '') {
      emailBool = true;
    }
    if (mobile == '') {
      mobileBool = true;
    }
    if (message == '') {
      msgBool = true;
    }
    if (name == '') {
      nameBool = true;
    }
    if (name && email && mobile && message) {
      let data = {
        V_name: name,
        V_emailid: email,
        V_cellno: mobile,
        V_msg: message,
      };
      dispatch(grievenceActions.submitQuery(data));
      dispatch(loadingAction.commaonLoader(true));
    }
    setState({
      ...state,
      nameBool,
      emailBool,
      mobileBool,
      msgBool,
    });
  };

  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          back={true}
          title="Grievance Cell"
          showCart={false}
          showNotification={false}
          headeHeight={60}
        />
      </View>
      <View style={{flex: 1}}>
        <KeyboardAwareScrollView>
          {loading ? (
            <View
              style={{
                height: 500,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <ActivityIndicator />
            </View>
          ) : (
            <View style={mainContainer}>
              <View style={commonScreenWidth}>
                <View style={{paddingVertical: '5%'}}>
                  <Text style={styles.headingText}>Submit Your Query</Text>
                </View>
                <View style={styles.rowContainer}>
                  <View style={styles.commonWidth}>
                    <View>
                      <Text style={styles.inputLabel}>Name</Text>
                    </View>
                    <View style={styles.commonMargin}>
                      <CustomTextInput
                        customStyle={true}
                        customStyleData={styles.textInputStyle}
                        placeholder="Enter Your Name"
                        value={state.name}
                        changeText={e => onChangeText(e, 'name')}
                      />
                      {state.nameBool ? (
                        <Text style={styles.errorText}>{state.errorText}</Text>
                      ) : null}
                    </View>
                  </View>
                  <View style={styles.commonWidth}>
                    <View>
                      <Text style={styles.inputLabel}>Email ID</Text>
                    </View>
                    <View style={styles.commonMargin}>
                      <CustomTextInput
                        customStyle={true}
                        customStyleData={styles.textInputStyle}
                        placeholder="Enter Your Email"
                        value={state.email}
                        changeText={e => onChangeText(e, 'email')}
                        onSubmit={() => validateEmail()}
                      />
                      {state.emailBool ? (
                        <Text style={styles.errorText}>{state.errorText}</Text>
                      ) : null}
                    </View>
                  </View>
                </View>
                <View style={styles.rowContainer}>
                  <View style={styles.commonWidth}>
                    <View>
                      <Text style={styles.inputLabel}>Telephone</Text>
                    </View>
                    <View style={styles.commonMargin}>
                      <CustomTextInput
                        customStyle={true}
                        customStyleData={styles.textInputStyle}
                        placeholder="Enter Your telephone no."
                        value={state.mobile}
                        changeText={e => onChangeText(e, 'mobile')}
                        maxLength={10}
                        keyboardType="numeric"
                      />
                      {state.mobileBool ? (
                        <Text style={styles.errorText}>{state.errorText}</Text>
                      ) : null}
                    </View>
                  </View>
                  <View style={styles.commonWidth}>
                    <View>
                      <Text style={styles.inputLabel}>Company</Text>
                    </View>
                    <View style={styles.commonMargin}>
                      <CustomTextInput
                        customStyle={true}
                        customStyleData={styles.textInputStyle}
                        placeholder="Enter Your Company Name"
                        value={state.company}
                        changeText={e => onChangeText(e, 'company')}
                      />
                    </View>
                  </View>
                </View>
                <View style={styles.rowContainer}>
                  <View style={styles.commonWidth}>
                    <View>
                      <Text style={styles.inputLabel}>Subject</Text>
                    </View>
                    <View style={styles.commonMargin}>
                      <CustomTextInput
                        customStyle={true}
                        customStyleData={styles.textInputStyle}
                        placeholder="Enter Subject"
                        value={state.subject}
                        changeText={e => onChangeText(e, 'subject')}
                      />
                    </View>
                  </View>
                  <View style={styles.commonWidth}>
                    <View>
                      <Text style={styles.inputLabel}>Message</Text>
                    </View>
                    <View style={styles.commonMargin}>
                      <CustomTextInput
                        customStyle={true}
                        customStyleData={styles.textInputStyle}
                        placeholder="Enter Your Message"
                        value={state.message}
                        changeText={e => onChangeText(e, 'message')}
                      />
                      {state.msgBool ? (
                        <Text style={styles.errorText}>{state.errorText}</Text>
                      ) : null}
                    </View>
                  </View>
                </View>
                <View style={{marginVertical: '6%'}}>
                  <View>
                    <TouchableOpacity style={commonButton} onPress={sendQuery}>
                      <Text style={commonButtonText}>Submit</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={{paddingVertical: '5%'}}>
                  <Text style={styles.headingText}>DS QUERIES</Text>
                </View>
                <View>
                  <View>
                    <Text>{'Contact Person : ' + grievenceData.contact}</Text>
                  </View>
                  <View>
                    <Text>{'Contact Number : ' + grievenceData.cell}</Text>
                  </View>
                  <View>
                    <Text>{'Address : ' + grievenceData.addr}</Text>
                  </View>
                  <View>
                    <Text>{'Working Hours : ' + grievenceData.hours}</Text>
                  </View>
                  <View>
                    <Text>{grievenceData.off + ' OFF'}</Text>
                  </View>
                  <View style={{paddingTop: '5%'}}>
                    <Text style={{fontWeight: 'bold'}}>
                      Retail Product Dispatch / Delivery
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View>
                      <Icon name="phone" size={15} color="gray" />
                    </View>
                    <View>
                      <Text>{' ' + grievenceData.dispatchnum}</Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View>
                      <Icon name="envelope" size={15} color="gray" />
                    </View>
                    <View>
                      <Text>{' ' + grievenceData.email}</Text>
                    </View>
                  </View>
                  <View style={{paddingTop: '5%'}}>
                    <Text style={{fontWeight: 'bold'}}>Commission Cheque</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View>
                      <Icon name="phone" size={15} color="gray" />
                    </View>
                    <View>
                      <Text>{' ' + grievenceData.commchqno}</Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View>
                      <Icon name="envelope" size={15} color="gray" />
                    </View>
                    <View>
                      <Text>{' ' + grievenceData.commchqemail}</Text>
                    </View>
                  </View>
                  <View style={{paddingTop: '5%'}}>
                    <Text style={{fontWeight: 'bold'}}>New DS activation</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View>
                      <Icon name="phone" size={15} color="gray" />
                    </View>
                    <View>
                      <Text>{' ' + grievenceData.newdsno}</Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View>
                      <Icon name="envelope" size={15} color="gray" />
                    </View>
                    <View>
                      <Text>{' ' + grievenceData.newdsemail}</Text>
                    </View>
                  </View>
                  <View style={{paddingTop: '5%'}}>
                    <Text style={{fontWeight: 'bold'}}>
                      Pending DS activation
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View>
                      <Icon name="phone" size={15} color="gray" />
                    </View>
                    <View>
                      <Text>{' ' + grievenceData.pendingnum}</Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', paddingBottom: '10%'}}>
                    <View>
                      <Icon name="envelope" size={15} color="gray" />
                    </View>
                    <View>
                      <Text>{' ' + grievenceData.pendingdsemail}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          )}
        </KeyboardAwareScrollView>
      </View>
    </View>
  );
};

export default GrievenceCell;

const styles = StyleSheet.create({
  textInputStyle: {
    borderWidth: 1,
    borderColor: '#e2e2e2',
    borderRadius: 6,
    paddingLeft: '5%',
    height: 50,
  },
  commonMargin: {
    marginVertical: '5%',
  },
  inputLabel: {
    fontWeight: 'bold',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  commonWidth: {
    width: '48%',
  },
  headingText: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: hp('3%'),
  },
  errorText: {
    color: 'red',
  },
});
