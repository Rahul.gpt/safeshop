import React, {Component, useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
} from 'react-native';
import Header from '../../components/header';
import DSData from '../DSdata';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import * as retailsActions from '../../actions/retailOrderActions';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import * as allDSActions from '../../actions/allDSActions';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';
import RazorpayCheckout from 'react-native-razorpay';

const RetailProductBought = props => {
  const [orderList, setOrderList] = useState([]);
  const [activeTab, setActiveTab] = useState('bought');
  const [loading, setLoading] = useState(true);
  const [modalBool, showModal] = useState(false);

  const dispatch = useDispatch();
  const viewDStaxInvoice = useSelector(
    state => state.AllDSReducer.viewDStaxInvoice,
  );

  const paymentId = useSelector(state => state.RetailOrderReducer.paymentId);

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        dispatch(allDSActions.getTaxInvoice(activeTab, token));
      }
    });
  }, []);

  useEffect(() => {
    if (Object.keys(viewDStaxInvoice).length) {
      if (
        viewDStaxInvoice.response &&
        viewDStaxInvoice.response.status == 200
      ) {
        setOrderList(viewDStaxInvoice.data.list);
        setLoading(false);
      }
    }
  }, [viewDStaxInvoice]);

  useEffect(() => {
    if (Object.keys(paymentId).length) {
      if (paymentId.response && paymentId.response.status == 200) {
        confirmPayment();
        dispatch(retailsActions.emptyPaymentId());
      }
    }
  }, [paymentId]);

  const getInvoice = type => {
    setLoading(true);
    if (type == 'bought') {
      setActiveTab('bought');
    } else {
      setActiveTab('sold');
    }
  };

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        dispatch(allDSActions.getTaxInvoice(activeTab, token));
      }
    });
  }, [activeTab]);

  useEffect(() => {
    console.warn(loading, orderList.length);
  });

  generateInvoice = item => {
    props.navigation.navigate('Invoice', {id: item.vno});
  };

  const _renderList = order => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          paddingVertical: hp('2%'),
          borderBottomColor: '#e2e2e2',
          borderBottomWidth: 1,
        }}>
        <TouchableOpacity
          style={[styles.container1, {alignItems: 'center'}]}
          onPress={() => this.generateInvoice(order)}>
          <Icon name="file-invoice" size={25} />
        </TouchableOpacity>
        <View style={styles.container2}>
          <Text style={styles.tableData}>{order.vno}</Text>
        </View>
        <View style={styles.container1}>
          <Text style={styles.tableData}>
            {moment(order.vdate).format('YYYY-MM-DD')}
          </Text>
        </View>
        <View style={styles.container1}>
          <Text style={styles.tableData}>{parseInt(order.amtdr)}</Text>
        </View>
        <View style={styles.container1}>
          <Text style={styles.tableData}>{parseInt(order.delvch)}</Text>
        </View>
        <View style={styles.container1}>
          <Text style={styles.tableData}>{order.amtcr}</Text>
        </View>
        <View style={styles.container1}>
          <Text style={styles.tableData}>{order.bv}</Text>
        </View>
        <View style={styles.container1}>
          {order.vostatus == 'NEW' ? (
            <Text style={styles.tableData}>Not Paid</Text>
          ) : (
            <Text style={styles.tableData}>Paid</Text>
          )}
        </View>
        {activeTab == 'bought' ? (
          <View style={styles.container1}>
            {order.vostatus == 'NEW' ? (
              <TouchableOpacity
                style={styles.button}
                onPress={() => getPaymentOrderId(order)}>
                <Text style={{color: 'white'}} numberOfLines={1}>
                  Pay Now
                </Text>
              </TouchableOpacity>
            ) : null}
          </View>
        ) : null}
      </View>
    );
  };

  const getPaymentOrderId = order => {
    let data = {
      amount: parseInt(order.amtdr),
      vnum: order.vno,
    };
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        dispatch(retailsActions.getPaymentId(data, token));
        dispatch(retailsActions.emptyPaymentId());
        //  showModal(true)
      }
    });
  };

  const confirmPayment = () => {
    let {id, amount, receipt} = paymentId.data.response_array;
    // console.warn('hit', id, amount_due);
    var options = {
      description: 'Credits towards consultation',
      image: 'https://www.safeshopindia.com/dm/assets/images/logo.png',
      currency: 'INR',
      key: paymentId.data.msg.key_id,
      amount: paymentId.data.msg.amount,
      name: 'SafeShop India',
      order_id: id, //Replace this with an order_id created using Orders API. Learn more at https://razorpay.com/docs/api/orders.
      prefill: {
        email: '',
        contact: '',
        name: '',
      },
      theme: {color: commonColor.color},
    };
    console.warn(options);
    RazorpayCheckout.open(options)
      .then(data => {
        Alert.alert(
          'SafeShop India',
          'Payment Received ₹ ' +
            paymentId.data.msg.amount +
            ' against Invoice No. ' +
            receipt +
            ' Please note the Transaction No. ' +
            data.razorpay_payment_id,
          [
            {
              text: 'OK',
            },
          ],
          {cancelable: false},
        );
        // handle success
        // showMessage({
        //   type: 'success',
        //   message: `${data.razorpay_payment_id}`,
        // });
        // alert(JSON.stringify(data));
      })
      .catch(error => {
        // // handle failure
        // showMessage({
        //   type: 'danger',
        //   message: `${error.code} | ${error.description}`,
        // });
        alert(`Error: ${error.code} | ${error.description}`);
      });
  };

  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          back={true}
          title="Retail Product Bought"
          showCart={false}
          showNotification={true}
          headeHeight={60}
        />
      </View>
      <View style={{flex: 1, alignItems: 'center'}}>
        {/* <View style={{width: '90%'}}> */}
        <View style={{width: '95%'}}>
          <DSData allDetails={false} />
        </View>
        <View style={{alignItems: 'center', paddingVertical: '5%'}}>
          <Text style={{fontWeight: 'bold', fontSize: hp('3%')}}>
            Retail Product Bought
          </Text>
        </View>
        <View
          style={{
            alignItems: 'center',
            paddingBottom: '5%',
            width: '95%',
          }}>
          <Text style={{textAlign: 'center'}}>
            Please take a print out of all the invoices, sign and maintain a
            file for your own records.
          </Text>
        </View>
        <View style={{flexDirection: 'row', marginHorizontal: wp('3%')}}>
          <TouchableOpacity
            style={{
              width: '50%',
              alignItems: 'center',
              paddingVertical: '3%',
              backgroundColor:
                activeTab == 'bought' ? commonColor.color : '#e2e2e2',
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
            }}
            onPress={() => getInvoice('bought')}>
            <Text
              style={{
                color: activeTab == 'bought' ? 'white' : 'black',
                fontWeight: 'bold',
              }}>
              Product Bought
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '50%',
              alignItems: 'center',
              paddingVertical: '3%',
              backgroundColor:
                activeTab == 'sold' ? commonColor.color : '#e3e3e3',
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
            }}
            onPress={() => getInvoice('sold')}>
            <Text
              style={{
                color: activeTab == 'sold' ? 'white' : 'black',
                fontWeight: 'bold',
              }}>
              Product Sold
            </Text>
          </TouchableOpacity>
        </View>
        <ScrollView horizontal={true}>
          {loading ? (
            <ActivityIndicator size="large" />
          ) : orderList.length ? (
            <View style={styles.tableContainer}>
              <View style={styles.tableHeadingContainer}>
                <View style={[styles.container1, {alignItems: 'center'}]}>
                  <Text style={styles.tableHadding}>Print</Text>
                </View>
                <View style={styles.container2}>
                  <Text style={styles.tableHadding}>Order No.</Text>
                </View>
                <View style={styles.container1}>
                  <Text style={styles.tableHadding}>Date</Text>
                </View>
                <View style={styles.container1}>
                  <Text style={styles.tableHadding}>Inv. Amount</Text>
                </View>
                <View style={styles.container1}>
                  <Text style={styles.tableHadding}>Shipping</Text>
                </View>
                <View style={styles.container1}>
                  <Text style={styles.tableHadding}>Discount</Text>
                </View>
                <View style={styles.container1}>
                  <Text style={styles.tableHadding}>RBV</Text>
                </View>
                <View style={styles.container1}>
                  <Text style={styles.tableHadding}>Status</Text>
                </View>
                {activeTab == 'bought' ? (
                  <View style={styles.container1}>
                    <Text style={styles.tableHadding}>Pay Now</Text>
                  </View>
                ) : null}
              </View>
              <View>
                <FlatList
                  data={orderList}
                  renderItem={({item}) => _renderList(item)}
                  keyExtractor={item => item.vno}
                />
              </View>
            </View>
          ) : (
            <View style={{height: 200, justifyContent: 'center'}}>
              <Text>No Data Found</Text>
            </View>
          )}
        </ScrollView>
        {/* </View> */}
        {/* {this.props.isLoading ? <Loader /> : null} */}
      </View>
    </View>
  );
};

export default RetailProductBought;

const styles = StyleSheet.create({
  tableHadding: {
    fontWeight: 'bold',
  },
  button: {
    width: '80%',
    alignItems: 'center',
    paddingVertical: hp('1%'),
    borderRadius: 30,
    backgroundColor: commonColor.color,
  },
  container1: {
    width: wp('25%'),
    alignItems: 'center',
  },
  container2: {
    width: wp('40%'),
    alignItems: 'center',
  },
  tableHeadingContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: '#e2e2e290',
    paddingVertical: hp('2%'),
  },
  tableContainer: {
    borderWidth: 1,
    borderColor: '#e2e2e2',
    paddingBottom: hp('10%'),
    marginHorizontal: wp('3%'),
  },
});
