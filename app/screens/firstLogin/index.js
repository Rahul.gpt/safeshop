import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  SafeAreaView,
  StyleSheet,
  ActivityIndicator,
  Image,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  InputTextLabel,
  commonColor,
  commonButton,
  commonButtonText,
  logoContainer,
  commonScreenWidth,
  mainContainer,
  logoStyle,
} from '../../components/commonStyle';
import Icon from 'react-native-vector-icons/Ionicons';
import Logo from '../../assets/images/logo.png';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import CheckBox from 'react-native-check-box';
import {showMessage} from 'react-native-flash-message';
import * as profileActions from '../../actions/profileAction';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';

const FirstLogin = props => {
  const [ageCheck, setAgeCheck] = useState(false);
  const [tc1, setTc1] = useState(false);
  const [tc2, setTc2] = useState(false);
  const [affidevit, setAff] = useState(false);
  const [userdata, setUserData] = useState({});
  const [userToken, setToken] = useState('');

  const dispatch = useDispatch();
  const getProfileData = useSelector(
    state => state.ProfileReducer.getProfileDataFirst,
  );

  useEffect(() => {
    if (props.route && props.route.params && props.route.params.userToken) {
      setToken(props.route.params.userToken);
      dispatch(
        profileActions.getProfileFirstLogin(props.route.params.userToken),
      );
    }
  }, []);

  useEffect(() => {
    console.warn('getProfileData', getProfileData);
    if (Object.keys(getProfileData).length) {
      if (getProfileData.response && getProfileData.response.status == 200) {
        setUserData(getProfileData.data.query[0]);
        dispatch(profileActions.emptyProfileData());
      }
    }
  }, [getProfileData]);

  const handleAgeCheck = () => {
    let checkAge = ageCheck;
    checkAge = !checkAge;
    setAgeCheck(checkAge);
  };

  const handleTc1 = () => {
    let TC1 = tc1;
    TC1 = !TC1;
    setTc1(TC1);
  };

  const handleTc2 = () => {
    let TC2 = tc2;
    TC2 = !TC2;
    setTc2(TC2);
  };

  const handleaff = () => {
    let aff = affidevit;
    aff = !aff;
    setAff(aff);
  };

  const handleConfirm = () => {
    if (!ageCheck || !tc1 || !tc2 || !affidevit) {
      showMessage({
        type: 'danger',
        message: 'please confirm all check boxes',
      });
    } else {
      props.navigation.navigate('Agreement', {
        userToken: userToken,
      });
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <SafeAreaView style={{flex: 0, backgroundColor: 'white'}} />
      <SafeAreaView style={{flex: 1}}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity style={{width: '20%', alignItems: 'center'}}>
            <Icon
              name="ios-arrow-round-back"
              size={50}
              color="#000"
              onPress={() => props.navigation.goBack()}
            />
          </TouchableOpacity>
          <View style={{width: '80%'}}>
            <Text style={{fontSize: hp('2.2%')}} numberOfLines={1}>
              First Time Login
            </Text>
          </View>
        </View>
        <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
          {Object.keys(userdata).length ? (
            <View style={mainContainer}>
              <View style={commonScreenWidth}>
                <View style={logoContainer}>
                  <Image source={Logo} style={logoStyle} resizeMode="contain" />
                </View>
                <View style={{paddingTop: hp('2%')}}>
                  <Text style={{fontWeight: 'bold'}}>Welcome !!</Text>
                </View>
                <View style={{paddingVertical: hp('2%')}}>
                  <Text>
                    We would like to thank you and welcome to the Safeshop s a
                    Direct Seller
                  </Text>
                </View>
                <View style={{paddingVertical: hp('2%')}}>
                  <Text>
                    To start your Direct Seller business with Safeshop, you will
                    need to digitally sign the Direct Seller Agreement on the
                    following pages
                  </Text>
                </View>
                <View style={{paddingVertical: hp('2%')}}>
                  <Text>We wish you all the best in your businees ahead</Text>
                </View>

                <View
                  style={{
                    paddingVertical: hp('2%'),
                    borderBottomWidth: 1,
                    borderBottomColor: '#e2e2e2',
                  }}>
                  <Text>
                    As a first step , Please confirm the Reference ID to proceed
                    further
                  </Text>
                </View>
                <View style={styles.dsDataContainer}>
                  <View style={{width: '50%'}}>
                    <Text>Your Login ID / Direct Seller ID</Text>
                  </View>
                  <View style={{width: '50%'}}>
                    <Text>{userdata.logid}</Text>
                  </View>
                </View>
                <View style={styles.dsDataContainer}>
                  <View style={{width: '50%'}}>
                    <Text>Name</Text>
                  </View>
                  <View style={{width: '50%'}}>
                    <Text>{userdata.name}</Text>
                  </View>
                </View>
                <View style={styles.dsDataContainer}>
                  <View style={{width: '50%'}}>
                    <Text>Parent ID</Text>
                  </View>
                  <View style={{width: '50%'}}>
                    <Text>{userdata.parentid}</Text>
                  </View>
                </View>
                <View style={styles.dsDataContainer}>
                  <View style={{width: '50%'}}>
                    <Text>Reference ID</Text>
                  </View>
                  <View style={{width: '50%'}}>
                    <Text>{userdata.referid}</Text>
                  </View>
                </View>
                <View style={styles.dsDataContainer}>
                  <View style={{width: '50%'}}>
                    <CheckBox
                      style={{flex: 1, padding: 10}}
                      onClick={handleAgeCheck}
                      isChecked={ageCheck}
                      checkBoxColor={commonColor.color}
                      uncheckedCheckBoxColor={'black'}
                    />
                  </View>
                  <View style={{width: '50%'}}>
                    <Text>I agree that i am over 18 years of age</Text>
                  </View>
                </View>
                <View style={styles.dsDataContainer}>
                  <View style={{width: '50%'}}>
                    <CheckBox
                      style={{flex: 1, padding: 10}}
                      onClick={handleTc1}
                      isChecked={tc1}
                      checkBoxColor={commonColor.color}
                      uncheckedCheckBoxColor={'black'}
                    />
                  </View>
                  <View style={{width: '50%'}}>
                    <Text>
                      I have read and I agree to the terms and conditions
                      mentioned in the legalities section of this site
                    </Text>
                  </View>
                </View>
                <View style={styles.dsDataContainer}>
                  <View style={{width: '50%'}}>
                    <CheckBox
                      style={{flex: 1, padding: 10}}
                      onClick={handleTc2}
                      isChecked={tc2}
                      checkBoxColor={commonColor.color}
                      uncheckedCheckBoxColor={'black'}
                    />
                  </View>
                  <View style={{width: '50%'}}>
                    <Text>
                      {' '}
                      I have read and I agree to the terms and conditions
                      mentioned in the 'Direct Sller Agreement' section of this
                      site
                    </Text>
                  </View>
                </View>
                <View style={styles.dsDataContainer}>
                  <View style={{width: '50%'}}>
                    <CheckBox
                      style={{flex: 1, padding: 10}}
                      onClick={handleaff}
                      isChecked={affidevit}
                      checkBoxColor={commonColor.color}
                      uncheckedCheckBoxColor={'black'}
                    />
                  </View>
                  <View style={{width: '50%'}}>
                    <Text>
                      I have read and I agree to the terms and conditions
                      mentioned in the 'Affidavit'{' '}
                    </Text>
                  </View>
                </View>
                <View style={{alignItems: 'center', marginVertical: hp('4%')}}>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={handleConfirm}>
                    <Text style={{color: 'white'}}>Confirm</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          ) : (
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <ActivityIndicator />
            </View>
          )}
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </View>
  );
};

export default FirstLogin;

const styles = StyleSheet.create({
  dsDataContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: hp('2%'),
    borderBottomWidth: 1,
    borderBottomColor: '#e2e2e2',
  },
  button: {
    width: '50%',
    borderRadius: 30,
    alignItems: 'center',
    backgroundColor: commonColor.color,
    paddingVertical: '3%',
  },
});
