import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import Header from '../../components/header';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import {connect} from 'react-redux';
import LegalImage from '../../assets/images/legal.png';
import Modal from 'react-native-modal';
import Pdf from 'react-native-pdf';
import Icon1 from 'react-native-vector-icons/Entypo';

class Legal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      legalData: [
        {
          legalText: 'Disclaimer',
          pdfUrlLink: 'http://www.ssompl.com/assets/docs/disclaimer.pdf',
        },
        {
          legalText: 'Direct Seller COde of Conduct',
          pdfUrlLink: 'http://www.ssompl.com/assets/docs/code.pdf',
        },
        {
          legalText: 'Direct Seller Agreement',
          pdfUrlLink: 'http://www.ssompl.com/assets/docs/dsa.pdf',
        },
        {
          legalText: 'Affidavit',
          pdfUrlLink: 'http://www.ssompl.com/assets/docs/affidavit.pdf',
        },
        {
          legalText: 'KYC for Direct Seller',
          pdfUrlLink: 'http://www.ssompl.com/assets/docs/kyc.pdf',
        },
        {
          legalText:
            'Model Guidelines on Direct Selling (Ministry of Consumer Affairs)',
          pdfUrlLink: 'http://www.ssompl.com/assets/docs/gazette.pdf',
        },
        {
          legalText: 'Obligations and Prohibitions of a Direct Seller',
          pdfUrlLink: 'http://www.ssompl.com/assets/docs/obli.pdf',
        },
        {
          legalText: 'Submission of Declaration (Ministry of Consumer Affairs)',
          pdfUrlLink: 'http://www.ssompl.com/assets/docs/dca.jpg',
        },
        {
          legalText: 'D-VAT Registration',
          pdfUrlLink: 'http://www.ssompl.com/assets/docs/dvat07.pdf',
        },
        {
          legalText: 'Company PAN',
          pdfUrlLink: 'http://www.ssompl.com/assets/docs/PAN.jpg',
        },
        {
          legalText: 'Trademarks',
          pdfUrlLink: 'http://www.ssompl.com/assets/docs/TM.pdf',
        },
        {
          legalText: 'ISO Certification',
          pdfUrlLink: 'http://www.ssompl.com/assets/docs/ISO.jpg',
        },
        {
          legalText: 'Memorandum of Association',
          pdfUrlLink: 'http://www.ssompl.com/assets/docs/incor.pdf',
        },
        {
          legalText: 'Articles of Association',
          pdfUrlLink: 'http://www.ssompl.com/assets/docs/aa.pdf',
        },
      ],
      modalVisible: false,
      pdfLink: '',
    };
  }

  handleModal = () => {
    this.setState({modalVisible: false});
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <View>
          <Header
            {...this.props}
            back={true}
            title="Legal"
            showCart={false}
            showNotification={false}
            headeHeight={60}
          />
        </View>
        <View style={{flex: 1}}>
          <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
            <View style={mainContainer}>
              <View style={commonScreenWidth}>
                <View
                  style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    justifyContent: 'space-between',
                    paddingVertical: '10%',
                  }}>
                  {this.state.legalData.map((data, index) => {
                    return (
                      <TouchableOpacity
                        style={{width: '45%', marginVertical: '5%'}}
                        onPress={() =>
                          this.setState({
                            modalVisible: true,
                            pdfLink: data.pdfUrlLink,
                          })
                        }>
                        <View style={{height: 100, borderRadius: 20}}>
                          <Image
                            source={LegalImage}
                            style={{
                              width: '100%',
                              height: '100%',
                              borderRadius: 20,
                            }}
                          />
                        </View>
                        <View style={{paddingTop: '2%'}}>
                          <Text style={{textAlign: 'center'}}>
                            {data.legalText}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    );
                  })}
                </View>
              </View>
            </View>
          </KeyboardAwareScrollView>
          {this.state.modalVisible ? (
            <ViewLegalData
              handleModal={this.handleModal}
              modalVisible={this.state.modalVisible}
              pdfLink={this.state.pdfLink}
            />
          ) : null}
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({}),
  {},
)(Legal);

const ViewLegalData = props => {
  let linkUrl = props.pdfLink.split('.');
  return (
    <View>
      <Modal
        deviceWidth={wp('100%')}
        deviceHeight={hp('100%')}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: hp('0%'),
        }}
        onBackButtonPress={props.handleModal}
        isVisible={props.modalVisible}>
        {linkUrl[linkUrl.length - 1] == 'pdf' ? (
          <View style={styles.pdf}>
            <View style={{width: '100%', alignItems: 'flex-end'}}>
              <TouchableOpacity
                style={{padding: 5}}
                onPress={props.handleModal}>
                <Icon1 name="cross" color="black" size={30} />
              </TouchableOpacity>
            </View>
            <View style={styles.container}>
              <Pdf
                source={{uri: props.pdfLink}}
                onLoadComplete={(numberOfPages, filePath) => {
                  console.log(`number of pages: ${numberOfPages}`);
                }}
                onPageChanged={(page, numberOfPages) => {
                  console.log(`current page: ${page}`);
                }}
                onError={error => {
                  console.log(error);
                }}
                onPressLink={uri => {
                  console.log(`Link presse: ${uri}`);
                }}
                style={styles.pdf}
                activityIndicator={<ActivityIndicator />}
              />
            </View>
          </View>
        ) : (
          <View style={styles.pdf}>
            <View style={{width: '100%', alignItems: 'flex-end'}}>
              <TouchableOpacity
                style={{padding: 5}}
                onPress={props.handleModal}>
                <Icon1 name="cross" color="black" size={30} />
              </TouchableOpacity>
            </View>
            <View style={{width: '100%', height: '90%'}}>
              <Image
                source={{uri: props.pdfLink}}
                style={{width: '100%', height: '100%'}}
                resizeMode="contain"
              />
            </View>
          </View>
        )}
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 25,
  },
  pdf: {
    width: wp('90'),
    height: hp('90%'),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
});
