import * as React from 'react';
import {
  Text,
  View,
  Image,
  ActivityIndicator,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  FlatList,
  BackHandler,
} from 'react-native';
import Header from '../../components/header/index';
import {SliderBox} from 'react-native-image-slider-box';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import * as loadingAction from '../../actions/commonLoaderAction';
import * as categoryAction from '../../actions/categoryAction';
import Loader from '../../components/loader';
import {connect} from 'react-redux';
import {imageUrl} from '../../services';
import AsyncStorage from '@react-native-community/async-storage';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ImageLoad from 'react-native-image-placeholder';

class Category extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [],
      showCrousal: false,
      fashionSubCategory: [],
      lifeStyleSubCategory: [],
      healthSubCategory: [],
      avaialbleCategories: [],
      allSubCategoryData: [],
      subCategory: [],
      pathForCateImage: '',
    };
    this.props.getCategory();
    this._unsubscribeSiFocus = this.props.navigation.addListener('focus', e => {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    });
    this._unsubscribeSiBlur = this.props.navigation.addListener('blur', e => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        this.handleBackButton,
      );
    });

    // onButtonPress = () => {
    //   BackHandler.removeEventListener(
    //     'hardwareBackPress',
    //     this.handleBackButton,
    //   );
    // };
  }

  handleBackButton = () => {
    AsyncStorage.getItem('token').then(token => {
      if (token == null) {
        BackHandler.exitApp();
      } else {
        this.props.navigation.navigate('DSHome');
      }
    });

    return true;
  };

  async componentDidMount() {
    let screen = await AsyncStorage.getItem('NotificationScreen');
    if (JSON.parse(screen) !== null) {
      this.props.navigation.navigate('Notifications');
      AsyncStorage.removeItem('NotificationScreen');
    } else {
      AsyncStorage.removeItem('NotificationScreen');
    }
    setTimeout(() => {
      this.setState({showCrousal: true});
    }, 2000);
  }

  componentWillUnmount() {
    this._unsubscribeSiFocus();
    this._unsubscribeSiBlur();
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.allCategory !== this.props.allCategory) {
      if (this.props.allCategory.response.status == 200) {
        let avaialbleCategories = [];
        let allSubCategoryData = [];
        let images = [];
        allSubCategoryData = this.props.allCategory.data.categories;
        this.props.allCategory.data.categ.map(item => {
          if (!avaialbleCategories.includes(item.category1)) {
            avaialbleCategories.push({
              category: item.category1,
              smallImage: item.photos,
              bigImage: item.photob,
            });
          }
        });
        this.props.allCategory.data.banner.map(bannerImages => {
          let urlImage = bannerImages;
          images.push(urlImage);
        });
        this.setState({
          images,
          avaialbleCategories,
          allSubCategoryData,
          pathForCateImage: this.props.allCategory.data.categurl,
        });
      }
    }
  }

  onLayout = e => {
    this.setState({
      width: e.nativeEvent.layout.width,
    });
  };

  _renderCategories = item => {
    return (
      <View style={styles.rowItemContainer}>
        <TouchableOpacity
          style={{alignItems: 'center'}}
          onPress={() => this.handleSubCategories(item.category)}>
          <View style={styles.rowImage}>
            <ImageLoad
              style={{width: 45, height: 45, borderRadius: 25}}
              loadingStyle={{size: 'small', color: commonColor.color}}
              source={{uri: this.state.pathForCateImage + item.smallImage}}
              resizeMode="cover"
            />
          </View>
          <View style={styles.rowItemText}>
            <Text style={styles.rowText}>{item.category}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  handleSubCategories = category => {
    let allSubCategoryData = [...this.state.allSubCategoryData];
    let subCategory = [];
    allSubCategoryData.map(subCategoryData => {
      // console.warn(subCategoryData);
      if (subCategoryData.category1 == category) {
        subCategory.push(subCategoryData.subcategory1);
      }
    });
    this.setState({subCategory}, () => {
      this.props.navigation.navigate('ProductListing', {
        subCategory: this.state.subCategory,
      });
    });
  };

  _renderCatgeroiesWithBanner = (item, index) => {
    return (
      <View style={{marginVertical: '5%'}}>
        <ImageBackground
          style={[
            styles.categoryWithBannerContainer,
            {
              alignItems:
                item.category !== 'Fashion' ? 'flex-end' : 'flex-start',
            },
          ]}
          source={{uri: this.state.pathForCateImage + item.bigImage}}>
          {/* <View style={{width: '50%', alignItems: 'center'}}>
            <Image
              source={{uri: this.state.pathForCateImage + item.bigImage}}
              style={{width: '90%', height: 120}}
              resizeMode="contain"
            />
          </View> */}
          <View
            style={[
              styles.categoryBannerInfo,
              {
                alignItems:
                  item.category == 'Fashion' ? 'flex-start' : 'flex-end',
              },
            ]}>
            <View>
              <Text style={{fontSize: 24}}>{item.category}</Text>
            </View>
            <View style={{paddingVertical: '2%'}}>
              <Text>Spring New Arrival</Text>
            </View>
            <TouchableOpacity
              style={styles.shopButtonStyle}
              onPress={() => this.handleSubCategories(item.category)}>
              <View>
                <Text style={{color: 'white'}}>Shop Now</Text>
              </View>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View>
    );
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <View>
          <Header
            {...this.props}
            showCart={true}
            showUser={true}
            headeHeight={60}
          />
        </View>
        <View style={{flex: 1, paddingBottom: '2%'}}>
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
            <View style={mainContainer}>
              <View style={commonScreenWidth}>
                {this.state.showCrousal ? (
                  <Crousal
                    images={this.state.images}
                    onLayout={this.onLayout}
                    width={this.state.width}
                    margin="3%"
                  />
                ) : (
                  <View
                    style={{
                      height: 200,
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginVertical: '3%',
                    }}>
                    <ActivityIndicator />
                  </View>
                )}
                <View style={styles.rowContainer}>
                  <View style={{width: '80%'}}>
                    <FlatList
                      data={this.state.avaialbleCategories}
                      horizontal={true}
                      renderItem={({item}) => this._renderCategories(item)}
                      // showsHorizontalScrollIndicator={false}
                      keyExtractor={item => item.category}
                    />
                  </View>
                </View>

                <View>
                  <FlatList
                    data={this.state.avaialbleCategories}
                    renderItem={({item, index}) =>
                      this._renderCatgeroiesWithBanner(item, index)
                    }
                    keyExtractor={item => item.category}
                  />
                </View>
              </View>
            </View>
          </KeyboardAwareScrollView>
          {this.props.isLoading ? <Loader /> : null}
        </View>
      </View>
    );
  }
}

export const Crousal = props => {
  return props.width !== '' ? (
    <View
      style={{width: '100%', marginVertical: props.margin}}
      onLayout={props.onLayout}>
      <SliderBox
        images={props.images}
        parentWidth={props.width ? props.width : ''}
        sliderBoxHeight={200}
        dotStyle={{
          width: 5,
          height: 5,
          borderRadius: 15,
          padding: 0,
          margin: 0,
        }}
        autoplay
        circleLoop
        imageLoadingColor="#41caf1"
        onCurrentImagePressed={props.handlePress}
        resizeMode="cover"
      />
    </View>
  ) : (
    <ActivityIndicator />
  );
};

export default connect(
  state => ({
    isLoading: state.CommonLoaderReducer.isLoading,
    allCategory: state.CategoryReducer.allCategory,
  }),
  {...loadingAction, ...categoryAction},
)(Category);

const styles = StyleSheet.create({
  rowContainer: {
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 0.2,
    marginVertical: '5%',
    paddingVertical: '5%',
    backgroundColor: '#fff',
    elevation: 1,
  },
  rowInnerContainer: {
    flexDirection: 'row',
    width: '70%',
    justifyContent: 'space-around',
  },
  rowItemContainer: {
    width: wp('25%'),
    alignItems: 'center',
  },
  rowImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  rowText: {
    fontWeight: 'bold',
    fontSize: 10,
    textAlign: 'center',
  },
  rowItemText: {
    paddingTop: '5%',
  },
  bannerImage: {
    width: '100%',
    height: 150,
  },
  shopButtonStyle: {
    backgroundColor: '#41caf1',
    paddingVertical: '3%',
    paddingHorizontal: '5%',
    borderRadius: 10,
  },
  categoryWithBannerContainer: {
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 1,
    // },
    // shadowRadius: 5,
    // shadowOpacity: 0.2,
    paddingVertical: '5%',
    // backgroundColor: '#fff',
    // alignItems: 'center',
    // elevation: 1,
    width: '100%',
    // height: 150,
  },
  categoryBannerInfo: {
    // height: '100%',
    justifyContent: 'center',
    paddingHorizontal: '5%',
    // width: '50%',
  },
});
