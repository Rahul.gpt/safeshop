import React, {useState, useEffect, Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import Header from '../../components/header';
import * as searchActions from '../../actions/searchAction';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';
import AsyncStorage from '@react-native-community/async-storage';
import {commonColor} from '../../components/commonStyle';
import ProductCard from '../productCard.js/index';
import FilterModal from '../../components/productFilter';
import {connect} from 'react-redux';
import * as categoryAction from '../../actions/categoryAction';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class Products extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchResult: [],
      loading: true,
      visibleModal: false,
      modalType: '',
      sortingTypeData: '',
      avaialbleCategories: [],
      allSubCategoryData: [],
      sortUpdate: false,
      filterData: {},
    };
    this.props.getCategory();
  }

  componentDidMount() {
    this.props.allProducts('');
  }

  componentDidUpdate(prevProps) {
    if (prevProps.allProductData !== this.props.allProductData) {
      if (
        this.props.allProductData.response &&
        this.props.allProductData.response.status == 200
      ) {
        this.setState({
          searchResult: this.props.allProductData.data.products,
          loading: false,
        });
      }
    }
    if (prevProps.filterProduct !== this.props.filterProduct) {
      if (
        this.props.filterProduct.response &&
        this.props.filterProduct.response.status == 200
      ) {
        this.setState({
          searchResult: this.props.filterProduct.data.products,
          loading: false,
        });
      }
    }
    if (prevProps.allCategory !== this.props.allCategory) {
      if (this.props.allCategory.response.status == 200) {
        let avaialbleCategories = [];
        let allSubCategoryData = [];
        let images = [];
        allSubCategoryData = this.props.allCategory.data.categories.map(
          itemData => {
            return {...itemData, subCatBool: false};
          },
        );
        this.props.allCategory.data.categ.map(item => {
          if (!avaialbleCategories.includes(item.category1)) {
            avaialbleCategories.push({
              category: item.category1,
              catBool: false,
            });
          }
        });
        this.setState({
          avaialbleCategories,
          allSubCategoryData,
        });
      }
    }
  }

  _renderList = (item, index) => {
    return (
      <TouchableOpacity
        style={styles.listItem}
        onPress={() =>
          this.props.navigation.navigate('ProductDetail', {
            packgeName: item.package,
            name: item.detl,
          })
        }>
        <ProductCard data={item} {...this.props} />
      </TouchableOpacity>
    );
  };

  handleModal = type => {
    this.setState({visibleModal: true, modalType: type}, () => {
      if (type == 'filter') {
        this.setState({sortingTypeData: ''});
      }
    });
  };

  closedModal = () => {
    this.setState({visibleModal: false});
  };

  submitFilterModal = value => {
    console.warn('value', value);
    if (value !== null) {
      this.setState(
        {visibleModal: false, filterData: value, loading: true},
        () => {
          let data = {
            categ: value.categFilterData,
            subCateg: value.subCategFilterData,
          };
          if (value.priceFilterdata !== 'ALL') {
            let priceFilterdata = value.priceFilterdata.split('-');
            data = {
              ...data,
              price1: priceFilterdata[0],
              price2: priceFilterdata[1],
            };
          } else {
            data = {
              ...data,
              price1: 'ALL',
              price2: 'ALL',
            };
          }
          if (value.bvFilterData !== 'ALL') {
            let bvFilterData = value.bvFilterData.split('-');
            data = {
              ...data,
              fromBv: bvFilterData[0],
              tobv: bvFilterData[1],
            };
          } else {
            data = {
              ...data,
              fromBv: 'ALL',
              tobv: 'ALL',
            };
          }
          this.props.filterData(data);
        },
      );
    }
  };

  resetFilter = () => {
    this.setState(
      {visibleModal: false, filterData: {}, loading: true},
      () => {
        let data = {
          categ: 'ALL',
          subCateg: 'ALL',
          price1: 'All',
          price2: 'ALL',
          fromBv: 'ALL',
          tobv: 'ALL',
        };
        setTimeout(() => {
          this.props.filterData(data);
          this.props.commaonLoader(true);
        });
      },
      500,
    );
  };

  submitModal = value => {
    if (value !== null) {
      this.setState(
        {visibleModal: false, sortingTypeData: value.sortType, loading: true},
        () => {
          let searchResultData = [...this.state.searchResult];
          if (value.sortType == 'priceDesc') {
            searchResultData.sort((a, b) => {
              return parseFloat(b.memamt) - parseFloat(a.memamt);
            });
          } else if (value.sortType == 'PriceAsc') {
            searchResultData.sort((a, b) => {
              return parseFloat(a.memamt) - parseFloat(b.memamt);
            });
          } else if (value.sortType == 'nameAsc') {
            searchResultData.sort((a, b) => {
              return a.detl > b.detl ? 1 : b.detl > a.detl ? -1 : 0;
            });
          } else if (value.sortType == 'nameDesc') {
            searchResultData.sort((a, b) => {
              return b.detl > a.detl ? 1 : a.detl > b.detl ? -1 : 0;
            });
          }
          this.setState({
            searchResult: searchResultData,
            sortUpdate: true,
            loading: false,
          });
        },
      );
    } else {
      this.setState({visibleModal: false, sortingTypeData: ''});
    }
  };

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <View>
          <Header
            {...this.props}
            title="All Products"
            showCart={false}
            showNotification={true}
            headeHeight={60}
            back={true}
          />
        </View>
        <View style={{flex: 1}}>
          <View style={styles.filterContainer}>
            <TouchableOpacity
              style={[
                styles.filterButton,
                {backgroundColor: commonColor.color},
              ]}
              onPress={() => this.handleModal('sort')}>
              <Text style={{color: 'white', fontWeight: 'bold'}}>Sort By</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.filterButton,
                {backgroundColor: commonColor.color},
              ]}
              onPress={() => this.handleModal('filter')}>
              <Text style={{color: 'white', fontWeight: 'bold'}}>
                Filter By
              </Text>
            </TouchableOpacity>
          </View>
          {this.state.loading ? (
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <ActivityIndicator size="large" color={commonColor.color} />
            </View>
          ) : this.state.searchResult.length > 0 ? (
            <View style={{paddingBottom: hp('10%')}}>
              <FlatList
                data={this.state.searchResult}
                renderItem={({item, index}) => this._renderList(item, index)}
                numColumns={2}
                style={{marginHorizontal: 10}}
                extraData={this.state}
                legacyImplementation={true}
                keyExtractor={item => item.package}
              />
            </View>
          ) : (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                height: 500,
              }}>
              <Text>No data found</Text>
            </View>
          )}
        </View>
        {this.state.visibleModal ? (
          <FilterModal
            visibleModal={this.state.visibleModal}
            closedModal={this.closedModal}
            modalType={this.state.modalType}
            submitModal={this.submitModal}
            sortingTypeData={this.state.sortingTypeData}
            allSubCategoryData={this.state.allSubCategoryData}
            avaialbleCategories={this.state.avaialbleCategories}
            submitFilterModal={this.submitFilterModal}
            filterData={this.state.filterData}
            resetFilter={this.resetFilter}
          />
        ) : null}
      </View>
    );
  }
}

export default connect(
  state => ({
    allProductData: state.SearchReducer.allProductData,
    allCategory: state.CategoryReducer.allCategory,
    filterProduct: state.SearchReducer.filterProduct,
  }),
  {...searchActions, ...loadingAction, ...categoryAction},
)(Products);

const styles = StyleSheet.create({
  subCategoryContainer: {
    height: 50,
    width: '100%',
    backgroundColor: 'white',
  },
  subCategoryInnerContainer: {
    height: 50,
    width: 90,
    justifyContent: 'center',
    alignItems: 'center',
  },
  listItem: {
    width: '45%',
    marginVertical: '4%',
    marginHorizontal: 10,
    borderRadius: 20,
  },
  filterButton: {
    paddingHorizontal: '10%',
    paddingVertical: '2%',
    borderRadius: 15,
  },
  filterContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: '10%',
    marginVertical: '5%',
  },
});
