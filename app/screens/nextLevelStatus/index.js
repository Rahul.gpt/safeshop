import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  TextInput,
  Image,
  TouchableOpacity,
  SafeAreaView,
  ActivityIndicator,
  StyleSheet,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Header from '../../components/header';
import {commonColor} from '../../components/commonStyle';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const NextLevelStatus = props => {
  const [data, setData] = useState('');
  const [status, setStatus] = useState('');
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    AsyncStorage.getItem('userCredentials').then(userCredentials => {
      let userCredentialsData = JSON.parse(userCredentials);
      if (userCredentialsData !== null) {
        setStatus(userCredentialsData.initstatus);
      }
    });
    AsyncStorage.getItem('status').then(item => {
      let data1 = JSON.parse(item);
      if (data1 !== null) {
        setData(data1);
      }
    });
    setLoading(false);
  }, []);

  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          back={true}
          showCart={true}
          showNotification={false}
          title="Next level status"
          showNotification={true}
          showCart={false}
        />
      </View>

      {loading ? (
        <View style={styles.container}>
          <ActivityIndicator size="large" color={commonColor.color} />
        </View>
      ) : (
        <View style={styles.container}>
          {status == 'A' ? (
            <Text style={styles.text}>
              Congratulations - You are at the Crown Ambassador level!!!
            </Text>
          ) : data.togetpaid == '0' && data.togetweak == '0' ? (
            <Text style={styles.text}>
              Congratulations, you have achieved the next level of '.$next
            </Text>
          ) : (
            <Text style={styles.text}>
              {'You need total Business Volume of ' +
                data.togetpaid * 200 +
                ' and ' +
                data.togetweak * 200 +
                ' Business Volume in your weaker leg to get the next level of ' +
                data.next}
            </Text>
          )}
        </View>
      )}
    </View>
  );
};

export default NextLevelStatus;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    elevation: 3,
    margin: '5%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 0.2,
  },
  text: {
    fontSize: hp('2.3%'),
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
