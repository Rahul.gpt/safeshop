import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
  Image,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  InputTextLabel,
  commonColor,
  commonButton,
  commonButtonText,
  logoContainer,
  commonScreenWidth,
  mainContainer,
  logoStyle,
} from '../../components/commonStyle';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {showMessage} from 'react-native-flash-message';
import HTML from 'react-native-render-html';
import {IGNORED_TAGS} from 'react-native-render-html/src/HTMLUtils';
import * as forgotPasswordAction from '../../actions/forgotPasswordAction';
import Logo from '../../assets/images/logo.png';

const Agreement = props => {
  const [bool, setBool] = useState(true);
  const [data, setData] = useState('');
  const [userToken, setToken] = useState('');

  const dispatch = useDispatch();
  const getAgreeData = useSelector(
    state => state.ForgotPasswordReducer.getAgreeData,
  );

  useEffect(() => {
    if (props.route && props.route.params && props.route.params.userToken) {
      dispatch(forgotPasswordAction.getAgreement(props.route.params.userToken));
      setToken(props.route.params.userToken);
    }
  }, []);

  useEffect(() => {
    console.warn('getAgreeData', getAgreeData);
    if (Object.keys(getAgreeData).length) {
      setData(getAgreeData);
      setBool(false);
    }
  }, [getAgreeData]);

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <SafeAreaView style={{flex: 0, backgroundColor: 'white'}} />
      <SafeAreaView style={{flex: 1}}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity style={{width: '20%', alignItems: 'center'}}>
            <Icon
              name="ios-arrow-round-back"
              size={50}
              color="#000"
              onPress={() => props.navigation.goBack()}
            />
          </TouchableOpacity>
          <View style={{width: '80%'}}>
            <Text style={{fontSize: hp('2.2%')}} numberOfLines={1}>
              Agreement
            </Text>
          </View>
        </View>
        <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
          {bool ? (
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <ActivityIndicator color={commonColor.color} />
            </View>
          ) : (
            <View style={mainContainer}>
              <View style={commonScreenWidth}>
                <View style={logoContainer}>
                  <Image source={Logo} style={logoStyle} resizeMode="contain" />
                </View>
                <HTML
                  html={data}
                  imagesMaxWidth={Dimensions.get('window').width}
                  ignoredStyles={['line-height']}
                />
                <View>
                  <Text style={{fontWeight: 'bold'}}>
                    I hearby agree to the agreement by clicking on the signature
                    button
                  </Text>
                </View>
                <View style={{alignItems: 'center', marginVertical: hp('4%')}}>
                  <TouchableOpacity
                    style={{
                      width: '50%',
                      borderRadius: 30,
                      alignItems: 'center',
                      backgroundColor: commonColor.color,
                      paddingVertical: '3%',
                    }}
                    onPress={() =>
                      props.navigation.navigate('Declaration', {
                        userToken: userToken,
                      })
                    }>
                    <Text style={{color: 'white', textTransform: 'uppercase'}}>
                      signature
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </View>
  );
};

export default Agreement;
