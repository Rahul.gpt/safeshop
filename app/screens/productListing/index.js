import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
  StyleSheet,
} from 'react-native';
import Header from '../../components/header';
import ProductCard from '../productCard.js/index';
import {commonColor} from '../../components/commonStyle';
import SearchProduct from '../searchProduct/index';
import * as productListingAction from '../../actions/productListingAction';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';

const ProdcutListing = props => {
  const [subCategoryData, setSubCategory] = useState([]);
  const [productData, setProductData] = useState([]);
  const [currentSelectedSubCategory, setCurrentSubCategory] = useState('');
  const [searchBool, setSearchBool] = useState(false);

  const dispatch = useDispatch();
  const productDataList = useSelector(
    state => state.ProductListingReducer.productDataList,
  );
  const isLoading = useSelector(state => state.CommonLoaderReducer.isLoading);

  useEffect(() => {
    if (Object.keys(productDataList).length > 0) {
      if (productDataList.response.status == 200) {
        setProductData(productDataList.data.products);
      }
    }
  }, [productDataList]);

  useEffect(() => {
    if (
      props.route &&
      props.route.params &&
      props.route.params.subCategory &&
      props.route.params.subCategory.length
    ) {
      setProductData([]);
      dispatch(
        productListingAction.getProducts(props.route.params.subCategory[0]),
      );
      dispatch(loadingAction.commaonLoader(true));
      setSubCategory(props.route.params.subCategory);
      setCurrentSubCategory(props.route.params.subCategory[0]);
    }
  }, []);

  const _renderList = (item, index) => {
    return (
      <TouchableOpacity
        style={styles.listItem}
        onPress={() =>
          props.navigation.navigate('ProductDetail', {
            packgeName: item.package,
            name: item.detl,
          })
        }>
        <ProductCard data={item} {...props} />
      </TouchableOpacity>
    );
  };

  const _renderSubCategory = item => {
    return (
      <TouchableOpacity
        style={{height: 50}}
        onPress={() => {
          setCurrentSubCategory(item);
          setProductData([]);
          dispatch(productListingAction.getProducts(item));
          dispatch(loadingAction.commaonLoader(true));
        }}>
        <View style={styles.subCategoryInnerContainer}>
          <Text
            style={
              currentSelectedSubCategory == item
                ? {color: commonColor.darkColor, fontWeight: 'bold'}
                : {color: commonColor.lightColor}
            }>
            {item}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const headerTextData = () => {
    setSearchBool(true);
  };

  return searchBool ? (
    <SearchProduct {...props} />
  ) : (
    <View style={{flex: 1,}}>
      <View>
        <Header
          {...props}
          back={true}
          showCart={true}
          showNotification={false}
          title=""
          headeHeight={60}
          headerTextData={headerTextData}
        />
      </View>
      <View style={{flex: 1, paddingBottom: '10%'}}>
        {/* <View style={{width: '100%'}}> */}
        <View style={styles.subCategoryContainer} elevation={5}>
          <FlatList
            data={subCategoryData}
            horizontal={true}
            renderItem={({item}) => _renderSubCategory(item)}
            showsHorizontalScrollIndicator={false}
          />
        </View>
        {/* </View> */}
        <View>
          <FlatList
            data={productData}
            renderItem={({item, index}) => _renderList(item, index)}
            numColumns={2}
            style={{marginHorizontal: 10}}
          />
        </View>
      </View>
      {isLoading ? <Loader /> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  subCategoryContainer: {
    width: '100%',
    backgroundColor: 'white',
    height: 50,
  },
  subCategoryInnerContainer: {
    height: 50,
    width: 90,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: '5%',
  },
  listItem: {
    width: '45%',
    marginVertical: '4%',
    marginHorizontal: 10,
    borderRadius: 20,
    // backgroundColor: 'white',
  },
});

export default ProdcutListing;
