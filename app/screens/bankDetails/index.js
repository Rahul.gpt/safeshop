import React, {useState, useEffect, Component} from 'react';
import Header from '../../components/header';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Image,
  ActivityIndicator,
} from 'react-native';
import BannerBox from '../../assets/images/box.png';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import DSData from '../DSdata';
import CustomTextInput from '../../components/commonTextInput';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';
import AsyncStorage from '@react-native-community/async-storage';
import * as allDSActions from '../../actions/allDSActions';

const BankDetails = props => {
  const [bankData, setBankData] = useState({});
  const [loading, setLoading] = useState(true);

  const dispatch = useDispatch();
  const getBankData = useSelector(state => state.AllDSReducer.getBankData);

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        dispatch(allDSActions.getBankDetails(token));
      }
    });
  }, []);

  useEffect(() => {
    console.warn('getBankData', getBankData);
    if (Object.keys(getBankData).length) {
      if (getBankData.response && getBankData.response.status == 200) {
        setBankData(getBankData.data.query[0]);
        setLoading(false);
      }
    }
  }, [getBankData]);

  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          title="Bank Details"
          showCart={false}
          showNotification={true}
          headeHeight={60}
          back={true}
        />
      </View>
      <View style={{flex: 1}}>
        <KeyboardAwareScrollView>
          <View style={mainContainer}>
            <View style={commonScreenWidth}>
              <DSData />
              <View style={{paddingTop: hp('5%'), paddingBottom: hp('3%')}}>
                <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
                  Please Confirm your Bank Account Number so that your
                  Commission reaches you directly in your account
                </Text>
              </View>
              {loading ? (
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <ActivityIndicator />
                </View>
              ) : (
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: '#e2e2e2',
                    paddingHorizontal: '4%',
                    paddingVertical: hp('3%'),
                  }}>
                  <View>
                    <View>
                      <Text style={styles.inputLabel}>Name as per Bank</Text>
                    </View>
                    <View style={styles.commonMargin}>
                      <CustomTextInput
                        customStyle={true}
                        customStyleData={styles.textInputStyle}
                        placeholder="Name"
                        editable={false}
                        value = {bankData.name}
                      />
                    </View>
                  </View>
                  <View>
                    <View>
                      <Text style={styles.inputLabel}>IFSC Code</Text>
                    </View>
                    <View style={styles.commonMargin}>
                      <CustomTextInput
                        customStyle={true}
                        customStyleData={styles.textInputStyle}
                        placeholder="IFSC Code"
                        editable={false}
                        value = {bankData.issccode}
                      />
                    </View>
                  </View>
                  <View>
                    <View>
                      <Text style={styles.inputLabel}>Account Number</Text>
                    </View>
                    <View style={styles.commonMargin}>
                      <CustomTextInput
                        customStyle={true}
                        customStyleData={styles.textInputStyle}
                        placeholder="Account Number"
                        editable={false}
                        value = {bankData.acntno}
                      />
                    </View>
                  </View>
                </View>
              )}
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    </View>
  );
};

export default BankDetails;

const styles = StyleSheet.create({
  textInputStyle: {
    borderWidth: 1,
    borderColor: '#e2e2e2',
    borderRadius: 6,
    paddingLeft: '5%',
    height: 50,
  },
  commonMargin: {
    marginVertical: '5%',
  },
  inputLabel: {
    fontWeight: 'bold',
  },
});
