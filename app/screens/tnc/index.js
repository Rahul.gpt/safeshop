import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
  Image,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  InputTextLabel,
  commonColor,
  commonButton,
  commonButtonText,
  logoContainer,
  commonScreenWidth,
  mainContainer,
  logoStyle,
} from '../../components/commonStyle';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {showMessage} from 'react-native-flash-message';
import HTML from 'react-native-render-html';
import {IGNORED_TAGS} from 'react-native-render-html/src/HTMLUtils';
import * as forgotPasswordAction from '../../actions/forgotPasswordAction';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import Logo from '../../assets/images/logo.png';

const TnC = props => {
  const [bool, setBool] = useState(true);
  const [data, setData] = useState('');
  const [userToken, setToken] = useState('');

  const dispatch = useDispatch();
  const tncData = useSelector(state => state.ForgotPasswordReducer.tncData);
  const submitUser = useSelector(
    state => state.ForgotPasswordReducer.submitUser,
  );
  const isLoading = useSelector(state => state.CommonLoaderReducer.isLoading);

  useEffect(() => {
    if (props.route && props.route.params && props.route.params.userToken) {
      dispatch(forgotPasswordAction.getTnC(props.route.params.userToken));
      setToken(props.route.params.userToken);
    }
  }, []);

  useEffect(() => {
    console.warn('getAgreeData', tncData);
    if (Object.keys(tncData).length) {
      setData(tncData);
      setBool(false);
    }
  }, [tncData]);

  useEffect(() => {
    if (Object.keys(submitUser).length) {
      showMessage({
        message: 'Now you can login with new password',
        type: 'success',
      });
      AsyncStorage.removeItem('token');
      AsyncStorage.removeItem('userCredentials');
      props.navigation.pop();
      props.navigation.pop();
      props.navigation.pop();
      props.navigation.pop();
      props.navigation.pop();
      // props.navigation.pop();
      // props.navigation.pop();
      // props.navigation.navigate('Login');
      dispatch(forgotPasswordAction.emptyFinal());
    }
  }, [submitUser]);

  const finalSubmit = () => {
    dispatch(loadingAction.commaonLoader(true));
    dispatch(forgotPasswordAction.submitFinalUser(userToken));
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <SafeAreaView style={{flex: 0, backgroundColor: 'white'}} />
      <SafeAreaView style={{flex: 1}}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity style={{width: '20%', alignItems: 'center'}}>
            <Icon
              name="ios-arrow-round-back"
              size={50}
              color="#000"
              onPress={() => props.navigation.goBack()}
            />
          </TouchableOpacity>
          <View style={{width: '80%'}}>
            <Text style={{fontSize: hp('2.2%')}} numberOfLines={1}>
              Legal Terms And Conditions
            </Text>
          </View>
        </View>
        <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
          {bool ? (
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <ActivityIndicator color={commonColor.color} />
            </View>
          ) : (
            <View style={mainContainer}>
              <View style={commonScreenWidth}>
                <View style={logoContainer}>
                  <Image source={Logo} style={logoStyle} resizeMode="contain" />
                </View>
                <HTML
                  html={data}
                  imagesMaxWidth={Dimensions.get('window').width}
                  ignoredStyles={['line-height']}
                />
                <View>
                  <Text style={{fontWeight: 'bold'}}>
                    I hearby agree to the agreement by clicking on the signature
                    button
                  </Text>
                </View>
                <View style={{alignItems: 'center', marginVertical: hp('4%')}}>
                  <TouchableOpacity
                    style={{
                      width: '50%',
                      borderRadius: 30,
                      alignItems: 'center',
                      backgroundColor: commonColor.color,
                      paddingVertical: '3%',
                    }}
                    onPress={finalSubmit}>
                    <Text style={{color: 'white', textTransform: 'uppercase'}}>
                      Confirm
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}
          {isLoading ? <Loader /> : null}
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </View>
  );
};

export default TnC;
