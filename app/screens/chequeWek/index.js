import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import Header from '../../components/header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import DSData from '../DSdata';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import * as allDSActions from '../../actions/allDSActions';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';

const ChequeWeek = props => {
  const [saleData, setSaleData] = useState([]);
  const [loading, setLoading] = useState(true);

  const dispatch = useDispatch();
  const getWeeklyData = useSelector(state => state.AllDSReducer.getWeeklyData);

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        dispatch(allDSActions.getWeeklySale(token));
      }
    });
  }, []);

  useEffect(() => {
    console.warn('getWeeklyData', getWeeklyData)
    if (Object.keys(getWeeklyData).length) {
      if (getWeeklyData.response && getWeeklyData.response.status == 200) {
        setSaleData(getWeeklyData.data.query);
        setLoading(false);
      } else {
        setLoading(false);
      }
    }
  }, [getWeeklyData]);
  return (
    <View style={{flex: 1}}>
      <View>
        <Header
          {...props}
          back={true}
          showCart={true}
          showNotification={false}
          title=" This Week Letter"
          showNotification={true}
          showCart={false}
        />
      </View>
      <View style={mainContainer}>
        <View style={commonScreenWidth}>
          <DSData allDetails={false} />
        </View>
        <ScrollView contentContainerStyle={{paddingBottom: hp('25%')}}>
          {loading ? (
            <View style={{height: 200, justifyContent: 'center'}}>
              <ActivityIndicator size="large" />
            </View>
          ) : saleData.length ? (
            <View style={styles.boxContainer}>
              {/* <View style={styles.boxContainer}> */}
              <View style={{alignItems: 'center', paddingVertical: '5%'}}>
                <Text style={{color: 'red', fontWeight: 'bold'}}>
                  Congratulations !!
                </Text>
              </View>
              <View>
                <Text style={styles.commFontSize}>Dear Direct Seller,</Text>
              </View>
              <View>
                <Text style={styles.commFontSize}>
                  Summary of business volume for product sales this week.
                </Text>
              </View>
              <View
                style={{
                  borderWidth: 1,
                  borderColor: '#e2e2e2',
                  marginVertical: '5%',
                }}>
                <View style={{flexDirection: 'row', paddingVertical: '5%'}}>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      borderRightWidth: 1,
                      borderRightColor: '#e2e2e2',
                    }}>
                    <View style={{paddingTop: hp('8%')}}>
                      <Text style={styles.commFontSize}>
                        Total Business Volume of Products Sales (1st Purchase)
                      </Text>
                    </View>
                  </View>
                  <View style={{width: '25%', alignItems: 'center'}}>
                    <View style={styles.columnHeadingContainer}>
                      <Text style={[styles.commFontSize, {color: '#2a74bc'}]}>
                        Blue Group
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Left</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].cntmonpvl) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Right</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].cntmonpvr) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      borderLeftWidth: 1,
                      borderRightWidth: 1,
                      borderColor: '#e2e2e2',
                    }}>
                    <View style={styles.columnHeadingContainer}>
                      <Text style={[styles.commFontSize, {color: '#df7728'}]}>
                        Amber Group
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Left</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].cntmonpvl2) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Right</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].cntmonpvr2) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                  <View style={{width: '25%', alignItems: 'center'}}>
                    <View style={styles.columnHeadingContainer}>
                      <Text style={[styles.commFontSize, {color: '#a732ce'}]}>
                        Purple Group
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Left</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].cntmonpvl3) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Right</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].cntmonpvr3) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    borderTopWidth: 1,
                    borderTopColor: '#e2e2e2',
                    paddingVertical: '5%',
                  }}>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      borderRightWidth: 1,
                      borderRightColor: '#e2e2e2',
                    }}>
                    <Text style={styles.commFontSize}>
                      Total Business Volume of Products Sales(Re-Purchase)
                    </Text>
                  </View>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {parseInt(Number(saleData[0].cntmonrpvl) * 200)}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].cntmonrpvr) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      borderLeftWidth: 1,
                      borderRightWidth: 1,
                      borderColor: '#e2e2e2',
                      justifyContent: 'center',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].cntmonrpvl2) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].cntmonrpvr2) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].cntmonrpvl3) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].cntmonrpvr3) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              {/* </View> */}

              <View
                style={{
                  borderWidth: 1,
                  borderColor: '#e2e2e2',
                  marginVertical: '5%',
                }}>
                <View style={{flexDirection: 'row', paddingVertical: '5%'}}>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      borderRightWidth: 1,
                      borderRightColor: '#e2e2e2',
                    }}>
                    <View style={{paddingTop: hp('8%')}}>
                      <Text style={styles.commFontSize}>
                        Business Volume Points Balance (1st Purchase)
                      </Text>
                    </View>
                  </View>
                  <View style={{width: '25%', alignItems: 'center'}}>
                    <View style={styles.columnHeadingContainer}>
                      <Text style={[styles.commFontSize, {color: '#2a74bc'}]}>
                        Blue Group
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Left</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].ball) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Right</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].balr) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      borderLeftWidth: 1,
                      borderRightWidth: 1,
                      borderColor: '#e2e2e2',
                    }}>
                    <View style={styles.columnHeadingContainer}>
                      <Text style={[styles.commFontSize, {color: '#df7728'}]}>
                        Amber Group
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Left</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].bal2l) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Right</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].bal2r) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                  <View style={{width: '25%', alignItems: 'center'}}>
                    <View style={styles.columnHeadingContainer}>
                      <Text style={[styles.commFontSize, {color: '#a732ce'}]}>
                        Purple Group
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Left</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].bal3l) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Right</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].bal3r) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    borderTopWidth: 1,
                    borderTopColor: '#e2e2e2',
                    paddingVertical: '5%',
                  }}>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      borderRightWidth: 1,
                      borderRightColor: '#e2e2e2',
                    }}>
                    <Text style={styles.commFontSize}>
                      Business Volume Points Balance (Re-Purchase)
                    </Text>
                  </View>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].balrl) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].balrr) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      borderLeftWidth: 1,
                      borderRightWidth: 1,
                      borderColor: '#e2e2e2',
                      justifyContent: 'center',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].balr2l) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].balr2r) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].balr3l) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {Number(saleData[0].balr3r) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              <View style={{alignItems: 'flex-start'}}>
                <View>
                  <Text style={{fontSize: 12}}>Keep up the good work.</Text>
                </View>

                <View style={{alignItems: 'flex-start'}}>
                  <Text style={{fontSize: 12}}>
                    * This is computer generated document and may contain
                    printing errors. If you notice such an error please contact
                    SSOM (P) Ltd within 7 days of the date mentioned. The
                    Company bears no responsibility whatsoever for errors not
                    reported within the stipulated time.
                  </Text>
                </View>
                <View style={{alignItems: 'flex-start'}}>
                  <Text style={{fontSize: 12}}>
                    ** As per the current union budget, TDS rates are revised to
                    5%, If PAN number is not mentioned then TDS will be deducted
                    @ 20% . Please enter your PAN No in the TDS details in
                    website to get your TDS certificate.
                  </Text>
                </View>
                <View style={{alignItems: 'flex-start'}}>
                  <Text style={{fontSize: 12}}>
                    *** Please Update Your PAN Card Details on Website if not
                    done, as it is a Mandatory Requirement from Govt Of India.
                  </Text>
                </View>
              </View>
            </View>
          ) : (
            <View style={{height: 200, justifyContent: 'center'}}>
              <Text>No Data</Text>
            </View>
          )}
        </ScrollView>
      </View>
    </View>
  );
};

export default ChequeWeek;

const styles = StyleSheet.create({
  boxContainer: {
    backgroundColor: 'white',
    elevation: 2,
    width: wp('95%'),
    paddingHorizontal: '2%',
    paddingVertical: '5%',
  },
  commFontSize: {
    fontSize: 12,
    textAlign: 'center',
  },
  columnHeadingContainer: {
    paddingTop: '5%',
    paddingBottom: hp('2%'),
  },
});
