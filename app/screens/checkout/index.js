import React, {useEffect, useState, Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
  StyleSheet,
  Image,
  ActivityIndicator,
  Alert,
} from 'react-native';
import Header from '../../components/header';
import {
  InputTextLabel,
  commonColor,
  commonButton,
  commonButtonText,
  logoContainer,
  commonScreenWidth,
  mainContainer,
  logoStyle,
} from '../../components/commonStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import * as cartActions from '../../actions/cartActions';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {showMessage, hideMessage} from 'react-native-flash-message';
import AsyncStorage from '@react-native-community/async-storage';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ImageLoad from 'react-native-image-placeholder';
import {imageUrl, baseUrl} from '../../services';
import CheckBox from 'react-native-check-box';
import * as profileActions from '../../actions/profileAction';
import {connect} from 'react-redux';
import Modal from 'react-native-modal';
import Icon1 from 'react-native-vector-icons/Entypo';
import {TextInput} from 'react-native-gesture-handler';
import CustomTextInput from '../../components/commonTextInput';
import RazorpayCheckout from 'react-native-razorpay';
import * as retailsActions from '../../actions/retailOrderActions';
import Invoice from '../../components/invoice';

class Checkout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentHeadingTab: 'shipping',
      isChecked: true,
      userProfileData: {},
      visibleModal: false,
      updateAddress: {},
      userToken: '',
      vnum: '',
      visiblePaymentModal: false,
      showInvoice: false,
    };
  }

  async componentDidMount() {
    console.warn('CDM', this.props);
    let token = await AsyncStorage.getItem('token');
    if (token !== null) {
      this.setState({userToken: token});
      this.props.getProfile(token);
    }
  }

  componentDidUpdate(prevProps) {
    console.warn('CDP', this.props);

    if (prevProps.route !== this.props.route) {
      if (
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.data
      ) {
        let userProfileData = {...this.state.userProfileData};
        userProfileData.sh_addr1 = this.props.route.params.data.address;
        userProfileData.sh_nearby = this.props.route.params.data.nearBy;
        userProfileData.sh_name = this.props.route.params.data.name;
        userProfileData.sh_city = this.props.route.params.data.city;
        userProfileData.sh_pincode = this.props.route.params.data.pincode;
        userProfileData.sh_state = this.props.route.params.data.states;
        userProfileData.cellno = this.props.route.params.data.mobile;
        userProfileData.sh_district = this.props.route.params.data.district;

        this.setState({userProfileData});
      }
    }
    if (prevProps.getProfileData !== this.props.getProfileData) {
      if (this.props.getProfileData.response.status == 200) {
        this.setState({
          userProfileData: this.props.getProfileData.data.query[0],
        });
      }
    }
    if (prevProps.checkoutData !== this.props.checkoutData) {
      if (
        this.props.checkoutData &&
        this.props.checkoutData.response &&
        this.props.checkoutData.response.status == 200
      ) {
        this.setState({
          currentHeadingTab: 'payment',
          vnum: this.props.checkoutData.data.query2[0].VNO,
          amount: this.props.checkoutData.data.query2[0].AMTDR,
          // showInvoice: true,
        });
        this.props.navigation.navigate('Invoice', {
          id: this.props.checkoutData.data.query2[0].VNO,
        });
        this.props.viewCart(this.state.userToken);
      } else if (
        this.props.checkoutData &&
        this.props.checkoutData.response &&
        this.props.checkoutData.response.status == 206
      ) {
        showMessage({
          type: 'danger',
          message: this.props.checkoutData.response.message,
        });
      }
    }
  }

  modalData = data => {
    let userProfileData = {...this.state.userProfileData};
    userProfileData.sh_addr1 = data.address;
    userProfileData.sh_nearby = data.nearBy;
    userProfileData.sh_name = data.name;
    userProfileData.sh_city = data.city;
    userProfileData.sh_pincode = data.pincode;
    userProfileData.sh_state = data.states;
    userProfileData.cellno = data.mobile;
    userProfileData.sh_district = data.district;

    this.setState({visibleModal: false, userProfileData});
  };

  placeOrder = () => {
    let userProfileData = {...this.state.userProfileData};
    AsyncStorage.getItem('token').then(token => {
      let Addressdata = '';
      if (userProfileData.sh_addr1 !== null) {
        console.warn('if');
        Addressdata = {
          addr1: userProfileData.sh_addr1,
          nearby: userProfileData.sh_nearby,
          city: userProfileData.sh_city,
          astate: userProfileData.sh_state,
          name: userProfileData.sh_name,
          cellno: userProfileData.cellno,
          pincode: userProfileData.sh_pincode,
        };
      } else {
        console.warn('else');
        Addressdata = {
          addr1: userProfileData.addr1,
          nearby: userProfileData.nearby,
          city: userProfileData.city,
          astate: userProfileData.state,
          name: userProfileData.name,
          cellno: userProfileData.cellno,
          pincode: userProfileData.pincode,
        };
      }

      if (token !== null) {
        console.warn('Addressdata', Addressdata);
        this.props.checkoutCart(Addressdata, token);
        this.props.commaonLoader(true);
      }
    });
  };
  getPaymentOrderId = () => {
    let data = {
      amount: parseInt(this.state.amount),
      vnum: this.state.vnum,
    };
    console.warn('data', data);
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        this.setState({visiblePaymentModal: true});
        this.props.emptyPaymentId();
        this.props.getPaymentId(data, token);
      }
    });
  };

  confirmPayment = () => {
    this.setState({visiblePaymentModal: false}, () => {
      setTimeout(() => {
        console.warn('data', this.props.paymentId.data.response_array);
        let {id, amount, receipt} = this.props.paymentId.data.response_array;
        // console.warn('hit', id, amount_due);
        var options = {
          description: 'Credits towards consultation',
          image: 'https://www.safeshopindia.com/dm/assets/images/logo.png',
          currency: 'INR',
          key: this.props.paymentId.data.msg.key_id,
          amount: this.props.paymentId.data.msg.amount,
          name: 'SafeShop India',
          order_id: id, //Replace this with an order_id created using Orders API. Learn more at https://razorpay.com/docs/api/orders.
          prefill: {
            email: '',
            contact: '',
            name: '',
          },
          theme: {color: commonColor.color},
        };
        console.warn(options);
        RazorpayCheckout.open(options)
          .then(data => {
            Alert.alert(
              'SafeShop India',
              'Payment Received ' +
                this.props.paymentId.data.msg.amount +
                ' against Invoice No. ' +
                receipt +
                ' Please note the Transaction No. ' +
                data.razorpay_payment_id,
              [
                {
                  text: 'OK',
                  onPress: () => this.props.navigation.navigate('Category'),
                },
              ],
              {cancelable: false},
            );
            this.props.emptyPaymentId();
            // handle success
            // showMessage({
            //   type: 'success',
            //   message: `${data.razorpay_payment_id}`,
            // });
          })
          .catch(error => {
            // // handle failure
            // showMessage({
            //   type: 'danger',
            //   message: `${error.code} | ${error.description}`,
            // });
            alert(`Error: ${error.code} | ${error.description}`);
            this.props.emptyPaymentId();
          });
      }, 1500);
    });
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <View>
          <Header
            {...this.props}
            back={true}
            showCart={true}
            showNotification={false}
            title="Checkout"
            showNotification={true}
            showCart={false}
          />
        </View>
        <View style={styles.headingContainer}>
          <View style={styles.headingTextContainer}>
            <Text
              style={{
                fontWeight:
                  this.state.currentHeadingTab === 'shipping'
                    ? 'bold'
                    : 'normal',
              }}>
              Shipping Address
            </Text>
          </View>
          <View style={styles.headingTextContainer}>
            <Text
              style={{
                fontWeight:
                  this.state.currentHeadingTab === 'payment'
                    ? 'bold'
                    : 'normal',
              }}>
              Payment
            </Text>
          </View>
        </View>
        <KeyboardAwareScrollView>
          <View style={{alignItems: 'center'}}>
            {this.state.currentHeadingTab === 'shipping' ? (
              Object.keys(this.state.userProfileData).length ? (
                <View style={{width: '90%'}}>
                  <View style={styles.container}>
                    <View style={{width: '50%'}}>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          textTransform: 'uppercase',
                        }}>
                        Delievery to :{' '}
                      </Text>
                    </View>
                    <View style={{width: '50%', alignItems: 'flex-end'}}>
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation.navigate('EditAddress', {
                            data: this.state.userProfileData,
                          })
                        }>
                        <Text
                          style={{
                            textDecorationLine: 'underline',
                            color: commonColor.color,
                          }}>
                          Edit shipping address
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View>
                    <Text style={{color: commonColor.lightColor}}>Address</Text>
                  </View>
                  {this.state.userProfileData.sh_addr1 !== null ? (
                    <View
                      style={{
                        paddingVertical: '5%',
                        borderBottomWidth: 1,
                        borderBottomColor: '#e2e2e2',
                      }}>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          textTransform: 'capitalize',
                        }}>
                        {this.state.userProfileData.sh_addr1}
                      </Text>
                      <Text style={{fontWeight: 'bold'}}>
                        {this.state.userProfileData.sh_nearby +
                          ' ' +
                          this.state.userProfileData.sh_district +
                          ' ' +
                          this.state.userProfileData.sh_city}
                      </Text>
                      <Text style={{fontWeight: 'bold'}}>
                        {this.state.userProfileData.sh_state +
                          ' ' +
                          this.state.userProfileData.sh_pincode}
                      </Text>
                    </View>
                  ) : (
                    <View
                      style={{
                        paddingVertical: '5%',
                        borderBottomWidth: 1,
                        borderBottomColor: '#e2e2e2',
                      }}>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          textTransform: 'capitalize',
                        }}>
                        {'' + this.state.userProfileData.addr1}
                      </Text>
                      <Text style={{fontWeight: 'bold'}}>
                        {this.state.userProfileData.nearby
                          ? this.state.userProfileData.nearby
                          : '' +
                            ' ' +
                            this.state.userProfileData.district +
                            ' ' +
                            this.state.userProfileData.city}
                      </Text>
                      <Text style={{fontWeight: 'bold'}}>
                        {this.state.userProfileData.state +
                          ' ' +
                          this.state.userProfileData.pincode}
                      </Text>
                    </View>
                  )}
                  <View
                    style={{
                      paddingVertical: '5%',
                      borderBottomWidth: 1,
                      borderBottomColor: '#e2e2e2',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <View style={{width: '50%', alignItems: 'flex-start'}}>
                      <Text style={{color: commonColor.lightColor}}>
                        Receiver's Name
                      </Text>
                    </View>
                    <View style={{width: '50%', alignItems: 'flex-end'}}>
                      {this.state.userProfileData &&
                      this.state.userProfileData.sh_name ? (
                        <Text style={{fontWeight: 'bold'}}>
                          {this.state.userProfileData.sh_name}
                        </Text>
                      ) : (
                        <Text style={{fontWeight: 'bold'}}>
                          {this.state.userProfileData.name}
                        </Text>
                      )}
                    </View>
                  </View>
                  <View
                    style={{
                      paddingVertical: '5%',
                      borderBottomWidth: 1,
                      borderBottomColor: '#e2e2e2',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <View style={{width: '50%', alignItems: 'flex-start'}}>
                      <Text style={{color: commonColor.lightColor}}>
                        Phone Number
                      </Text>
                    </View>
                    <View style={{width: '50%', alignItems: 'flex-end'}}>
                      <Text style={{fontWeight: 'bold'}}>
                        {this.state.userProfileData.cellno}
                      </Text>
                    </View>
                  </View>
                  {/* <View style={{paddingVertical: '5%'}}>
                    <CheckBox
                      style={{flex: 1, padding: 10}}
                      onClick={() => {
                        this.setState({
                          isChecked: !this.state.isChecked,
                        });
                      }}
                      isChecked={this.state.isChecked}
                      rightText={'Billing address is same as above'}
                      checkBoxColor={commonColor.color}
                      uncheckedCheckBoxColor={'black'}
                    />
                  </View> */}

                  <View style={{marginVertical: '8%'}}>
                    <View>
                      <TouchableOpacity
                        style={[commonButton, {borderRadius: 90}]}
                        onPress={this.placeOrder}>
                        <Text style={commonButtonText}>Place Order</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              ) : (
                <View style={{height: 300, justifyContent: 'center'}}>
                  <ActivityIndicator size="large" />
                </View>
              )
            ) : (
              <View
                style={{
                  width: '100%',
                }}>
                <View
                  style={{
                    backgroundColor: '#e2e2e2',
                    width: '100%',
                    paddingVertical: '3%',
                    marginVertical: '5%',
                  }}>
                  <Text
                    style={{
                      textTransform: 'uppercase',
                      fontWeight: 'bold',
                      marginHorizontal: '5%',
                    }}>
                    Select payment method
                  </Text>
                </View>
                <View style={{alignItems: 'center'}}>
                  <View style={{width: '90%'}}>
                    <View style={{marginVertical: '8%'}}>
                      <View>
                        <TouchableOpacity
                          style={[commonButton, {borderRadius: 90}]}
                          onPress={() => this.getPaymentOrderId()}>
                          <Text style={commonButtonText}>Pay Now</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View>
                      <View>
                        <TouchableOpacity
                          style={[commonButton, {borderRadius: 90}]}
                          onPress={() =>
                            this.props.navigation.navigate('Category')
                          }>
                          <Text style={commonButtonText}>Pay Later</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            )}
          </View>
        </KeyboardAwareScrollView>
        <View>
          <Modal
            deviceWidth={wp('100%')}
            deviceHeight={hp('100%')}
            style={{
              justifyContent: 'flex-end',
              alignItems: 'center',
              marginBottom: hp('0%'),
            }}
            isVisible={this.state.visiblePaymentModal}>
            {this.props.paymentId &&
            this.props.paymentId.response &&
            this.props.paymentId.response.status == 200 &&
            this.props.paymentId.data ? (
              <View
                style={{
                  height: hp('80%'),
                  width: wp('100%'),
                  paddingHorizontal: wp('3%'),
                  paddingTop: hp('3%'),
                  backgroundColor: '#ffffff',
                  borderTopLeftRadius: hp('5%'),
                  borderTopRightRadius: hp('5%'),
                  alignItems: 'center',
                }}>
                <View style={{width: '100%', alignItems: 'flex-end'}}>
                  <TouchableOpacity
                    style={{padding: 10}}
                    onPress={() => this.setState({visiblePaymentModal: false})}>
                    <Icon1 name="cross" color="black" size={30} />
                  </TouchableOpacity>
                </View>
                <View>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: hp('3%'),
                      textAlign: 'center',
                    }}>
                    Confirm Payment Amount and Retail Order Id
                  </Text>
                </View>
                <View
                  style={{
                    backgroundColor: 'white',
                    width: '90%',
                    marginVertical: '5%',
                    paddingVertical: '5%',
                    paddingHorizontal: '4%',
                    alignItems: 'center',
                  }}
                  elevation={5}>
                  <View>
                    <View
                      style={{
                        flexDirection: 'row',
                        paddingVertical: '4%',
                        borderBottomWidth: 1,
                        borderBottomColor: '#e2e2e250',
                      }}>
                      <View style={{width: '45%', alignItems: 'flex-start'}}>
                        <Text style={styles.modalTextLeft}>
                          Amount To Pay :
                        </Text>
                      </View>
                      <View style={{width: '55%', alignItems: 'flex-start'}}>
                        <Text style={styles.modalTextRight}>
                          {this.props.paymentId.data.msg.amount}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        paddingVertical: '4%',
                        borderBottomWidth: 1,
                        borderBottomColor: '#e2e2e250',
                      }}>
                      <View style={{width: '45%', alignItems: 'flex-start'}}>
                        <Text style={styles.modalTextLeft}>
                          Retail Order Id :
                        </Text>
                      </View>
                      <View style={{width: '55%', alignItems: 'flex-start'}}>
                        <Text style={styles.modalTextRight}>
                          {this.props.paymentId.data.msg.receipt}
                        </Text>
                      </View>
                    </View>
                    <View style={{alignItems: 'center'}}>
                      <TouchableOpacity
                        style={styles.confirmButtonContainer}
                        onPress={() => this.confirmPayment()}>
                        <View>
                          <Text style={{color: 'white'}}>Confirm</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            ) : (
              <Loader />
            )}
          </Modal>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    getProfileData: state.ProfileReducer.getProfileData,
    checkoutData: state.CartReducer.checkoutData,
    paymentId: state.RetailOrderReducer.paymentId,
  }),
  {...profileActions, ...cartActions, ...loadingAction, ...retailsActions},
)(Checkout);

// const ViewInvoiceMOdal = props => {
//   return (
//     <Modal
//       deviceWidth={wp('100%')}
//       deviceHeight={hp('100%')}
//       style={{
//         justifyContent: 'center',
//         alignItems: 'center',
//         marginBottom: hp('0%'),
//       }}
//       isVisible={props.visibleModal}>
//       <View
//         style={{
//           width: wp('90'),
//           height: hp('90%'),
//           justifyContent: 'center',
//           alignItems: 'center',
//         }}
//       />
//       <View
//         style={{
//           width: '100%',
//           justifyContent: 'center',
//           backgroundColor: '#fff',
//           padding: 10,
//           height: '100%',
//         }}>
//         <View style={{width: '100%', alignItems: 'flex-end'}}>
//           <TouchableOpacity style={{padding: 10}} onPress={props.crossModal}>
//             <Icon1 name="cross" color="black" size={30} />
//           </TouchableOpacity>
//         </View>
//         <View>
//           <Invoice />
//         </View>
//       </View>
//     </Modal>
//   );
// };

// const ViewAddressModal = props => {
//   const [state, setState] = useState({
//     name: props.userData.sh_name,
//     address: props.userData.sh_addr1,
//     nearBy: props.userData.sh_nearby,
//     city: props.userData.sh_city,
//     states: props.userData.sh_state,
//     pincode: props.userData.sh_pincode,
//     mobile: props.userData.cellno,
//     district: props.userData.sh_district,
//   });
//   onChangeText = (value, type) => {
//     let name = '';
//     let address = '';
//     let nearBy = '';
//     let city = '';
//     let states = '';
//     let pincode = '';
//     let mobile = '';
//     let district = '';
//     if (type === 'name') {
//       name = value.replace(/[^A-Za-z_ ]/g, '');
//       setState({...state, name});
//     } else if (type === 'address') {
//       address = value;
//       setState({...state, address});
//     } else if (type === 'nearBy') {
//       nearBy = value;
//       setState({...state, nearBy});
//     } else if (type === 'city') {
//       city = value.replace(/[^A-Za-z]/g, '');
//       setState({...state, city});
//     } else if (type === 'state') {
//       states = value.replace(/[^A-Za-z]/g, '');
//       setState({...state, states});
//     } else if (type === 'pincode') {
//       pincode = value.replace(/[^0-9]/g, '');
//       setState({...state, pincode});
//     } else if (type === 'mobile') {
//       mobile = value.replace(/[^0-9]/g, '');
//       setState({...state, mobile});
//     } else if (type === 'district') {
//       district = value.replace(/[^A-Za-z]/g, '');
//       setState({...state, district});
//     }
//   };

//   return (
//     <Modal
//       deviceWidth={wp('100%')}
//       deviceHeight={hp('100%')}
//       style={{
//         justifyContent: 'center',
//         alignItems: 'center',
//         marginBottom: hp('0%'),
//       }}
//       isVisible={props.visibleModal}>
//       <View
//         style={{
//           width: wp('90'),
//           height: hp('90%'),
//           justifyContent: 'center',
//           alignItems: 'center',
//         }}>
//         <KeyboardAwareScrollView>
//           <View style={styles.modalContainer}>
//             <View style={{width: '100%', alignItems: 'flex-end'}}>
//               <TouchableOpacity
//                 style={{padding: 10}}
//                 onPress={props.crossModal}>
//                 <Icon1 name="cross" color="black" size={30} />
//               </TouchableOpacity>
//             </View>
//             <View style={{marginBottom: hp('2%'), alignItems: 'center'}}>
//               <Text style={{fontWeight: 'bold', fontSize: hp('2.5%')}}>
//                 Add New Shipping Address
//               </Text>
//             </View>

//             <View style={{marginBottom: hp('2%')}}>
//               <Text style={{fontWeight: 'bold'}}>Receiver's Name</Text>
//             </View>
//             <View>
//               <CustomTextInput
//                 customStyle={true}
//                 customStyleData={styles.textInputStyle}
//                 placeholder="Name"
//                 value={state.name}
//                 changeText={e => this.onChangeText(e, 'name')}
//               />
//             </View>
//             <View style={{marginVertical: hp('2%')}}>
//               <Text style={{fontWeight: 'bold'}}>Address</Text>
//             </View>
//             <View>
//               <CustomTextInput
//                 customStyle={true}
//                 customStyleData={styles.textInputStyle}
//                 placeholder="Address"
//                 value={state.address}
//                 changeText={e => this.onChangeText(e, 'address')}
//               />
//               <View style={{marginVertical: '2%'}}>
//                 <CustomTextInput
//                   customStyle={true}
//                   customStyleData={styles.textInputStyle}
//                   placeholder="Near By"
//                   value={state.nearBy}
//                   changeText={e => this.onChangeText(e, 'nearBy')}
//                 />
//               </View>
//               <CustomTextInput
//                 customStyle={true}
//                 customStyleData={styles.textInputStyle}
//                 placeholder="City"
//                 value={state.city}
//                 changeText={e => this.onChangeText(e, 'city')}
//               />
//               <View style={{marginVertical: '2%'}}>
//                 <CustomTextInput
//                   customStyle={true}
//                   customStyleData={styles.textInputStyle}
//                   placeholder="District"
//                   value={state.district}
//                   changeText={e => this.onChangeText(e, 'district')}
//                 />
//               </View>
//             </View>
//             <View
//               style={{
//                 flexDirection: 'row',
//                 justifyContent: 'space-around',
//                 marginVertical: hp('2%'),
//               }}>
//               <View style={{width: '48%'}}>
//                 <View style={{marginBottom: hp('2%')}}>
//                   <Text style={{fontWeight: 'bold'}}>State</Text>
//                 </View>
//                 <View>
//                   <CustomTextInput
//                     customStyle={true}
//                     customStyleData={styles.textInputStyle}
//                     placeholder="state"
//                     value={state.states}
//                     changeText={e => this.onChangeText(e, 'state')}
//                   />
//                 </View>
//               </View>
//               <View style={{width: '48%'}}>
//                 <View style={{marginBottom: hp('2%')}}>
//                   <Text style={{fontWeight: 'bold'}}>Pin Code</Text>
//                 </View>
//                 <View>
//                   <CustomTextInput
//                     customStyle={true}
//                     customStyleData={styles.textInputStyle}
//                     placeholder="Pin Code"
//                     value={state.pincode}
//                     keyboardType="numeric"
//                     changeText={e => this.onChangeText(e, 'pincode')}
//                   />
//                 </View>
//               </View>
//             </View>
//             <View style={{marginBottom: hp('2%')}}>
//               <Text style={{fontWeight: 'bold'}}>Reciever's Phone Number</Text>
//             </View>
//             <View>
//               <CustomTextInput
//                 customStyle={true}
//                 customStyleData={styles.textInputStyle}
//                 placeholder="Contact Number"
//                 keyboardType="numeric"
//                 value={state.mobile}
//                 changeText={e => this.onChangeText(e, 'mobile')}
//                 maxLength={10}
//               />
//             </View>
//             <View style={styles.buttonContainer}>
//               <TouchableOpacity
//                 style={styles.button}
//                 onPress={props.crossModal}>
//                 <Text style={{color: 'white'}}>Back</Text>
//               </TouchableOpacity>
//               <TouchableOpacity
//                 style={styles.button}
//                 onPress={() => props.modalData(state)}>
//                 <Text style={{color: 'white'}}>Submit</Text>
//               </TouchableOpacity>
//             </View>
//           </View>
//         </KeyboardAwareScrollView>
//       </View>
//     </Modal>
//   );
// };

const styles = StyleSheet.create({
  headingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: '4%',
    backgroundColor: 'white',
    elevation: 3,
  },
  headingTextContainer: {
    width: '50%',
    alignItems: 'center',
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: '8%',
  },
  modalContainer: {
    width: '100%',
    justifyContent: 'center',
    backgroundColor: '#fff',
    padding: 10,
    height: '100%',
  },
  textInputStyle: {
    borderWidth: 1,
    borderColor: '#e2e2e2',
    borderRadius: 6,
    paddingLeft: '5%',
    height: 50,
  },
  button: {
    width: '40%',
    alignItems: 'center',
    paddingVertical: '3%',
    borderRadius: 30,
    backgroundColor: commonColor.color,
  },
  buttonContainer: {
    flexDirection: 'row',
    paddingVertical: '5%',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  confirmButtonContainer: {
    width: '60%',
    backgroundColor: commonColor.color,
    paddingVertical: '4%',
    borderRadius: 30,
    alignItems: 'center',
    marginVertical: '5%',
  },
  modalTextRight: {
    fontWeight: 'bold',
  },
});
