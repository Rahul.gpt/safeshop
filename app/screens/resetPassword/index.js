import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  TextInput,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import Logo from '../../assets/images/logo.png';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from '../../components/commonTextInput';
import {
  InputTextLabel,
  commonColor,
  commonButton,
  commonButtonText,
  logoContainer,
  commonScreenWidth,
  mainContainer,
  logoStyle,
} from '../../components/commonStyle';
import Icon from 'react-native-vector-icons/Ionicons';
import * as forgotPasswordAction from '../../actions/forgotPasswordAction';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';
import AsyncStorage from '@react-native-community/async-storage';
import * as cartActions from '../../actions/cartActions';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import * as otpActions from '../../actions/otpActions';

function ResetPassword(props) {
  const [state, setState] = useState({
    otp: '',
    newPassword: '',
    confirmPassword: '',
    showOldPass: false,
    oldPassword: '',
    previousComesScreen: '',
    userToken: '',
    loginId: '',
    mobile: '',
  });

  const dispatch = useDispatch();
  const resetPassResponse = useSelector(
    state => state.ForgotPasswordReducer.resetPassResponse,
  );

  const resetPassResponseNew = useSelector(
    state => state.ForgotPasswordReducer.resetPassResponseNew,
  );
  const isLoading = useSelector(state => state.CommonLoaderReducer.isLoading);
  const otpFromApi = useSelector(state => state.OtpReducer.otpData);

  useEffect(() => {
    console.warn(props);
    if (
      (props.route &&
        props.route.params &&
        props.route.params.oldPasswordField) ||
      props.route.params.previousScreen ||
      props.route.params.cellno
    ) {
      setState({
        ...state,
        showOldPass: props.route.params.oldPasswordField,
        previousComesScreen: props.route.params.previousScreen,
        userToken: props.route.params.userToken,
        loginId: props.route.params.loginId,
        mobile: props.route.params.cellno,
      });
    }
  }, []);

  useEffect(() => {
    if (Object.keys(resetPassResponse).length) {
      if (resetPassResponse.response.status == 200) {
        dispatch(cartActions.emptyCartData());
        dispatch(forgotPasswordAction.emptyresetPassword());
        AsyncStorage.removeItem('token');
        AsyncStorage.removeItem('userCredentials').then(() => {
          if (state.previousComesScreen == 'ForgotPassword') {
            console.warn('if resetpass');
            props.navigation.pop();
            props.navigation.pop();
            props.navigation.pop();
            props.navigation.navigate('Login');
            showMessage({
              type: 'success',
              message: 'Your pssword reset successfully',
            });
          } else {
            props.navigation.pop();
            showMessage({
              type: 'success',
              message: 'Your pssword reset successfully',
            });
            // props.navigation.pop();
          }
        });
      } else {
        showMessage({
          message: resetPassResponse.data,
          type: 'danger',
        });
        dispatch(forgotPasswordAction.emptyresetPassword());
      }
    }
  }, [resetPassResponse]);

  useEffect(() => {
    console.warn('resetPassResponseNew', resetPassResponseNew);
    if (Object.keys(resetPassResponseNew).length) {
      if (resetPassResponseNew.response.status == 200) {
        dispatch(forgotPasswordAction.emptyresetPasswordNew());
        props.navigation.navigate('FirstLogin', {
          userToken: state.userToken,
        });
      } else {
        showMessage({
          message: resetPassResponseNew.data.message,
          type: 'danger',
        });
        dispatch(forgotPasswordAction.emptyresetPasswordNew());
      }
    }
  }, [resetPassResponseNew]);

  useEffect(() => {
    if (Object.keys(otpFromApi).length) {
      if (otpFromApi.response.status == 200) {
        showMessage({
          type: 'success',
          message: 'Otp is successfully send to your registered mobile numer',
        });
        dispatch(otpActions.emptyOtpData());
      } else {
        console.warn('else');
        // dispatch(forgotPasswordAction.emptyForgot());
        showMessage({
          message: 'Something went wrong',
          type: 'danger',
        });
      }
    }
  }, [otpFromApi]);

  function submit() {
    if (state.previousComesScreen == 'ForgotPassword') {
      if (!state.newPassword || !state.confirmPassword) {
        showMessage({
          type: 'danger',
          message: 'All fields are mandatory',
        });
      } else {
        let data = {
          newPassword: state.newPassword,
          confirmPassword: state.confirmPassword,
          logid: state.loginId,
        };
        dispatch(forgotPasswordAction.resetPassword(data));
        dispatch(loadingAction.commaonLoader(true));
      }
    } else if (state.previousComesScreen == 'profilePassword') {
      if (!state.newPassword || !state.confirmPassword || !state.oldPassword) {
        showMessage({
          type: 'danger',
          message: 'All fields are mandatory',
        });
      } else {
        let data = {
          newPassword: state.newPassword,
          confirmPassword: state.confirmPassword,
          logid: state.loginId,
          oldPassword: state.oldPassword,
        };
        dispatch(forgotPasswordAction.resetPassword(data));
        dispatch(loadingAction.commaonLoader(true));
      }
    } else {
      if (
        !state.newPassword ||
        !state.confirmPassword ||
        !state.otp ||
        !state.oldPassword
      ) {
        showMessage({
          type: 'danger',
          message: 'All fields are mandatory',
        });
      } else {
        let data = {
          newPassword: state.newPassword,
          oldPassword: state.oldPassword,
          confirmPassword: state.confirmPassword,
          otp: state.otp,
        };
        console.warn('data', data);
        dispatch(forgotPasswordAction.resetPasswordNew(data, state.userToken));
        dispatch(loadingAction.commaonLoader(true));
      }
    }
  }

  const resendOtp = () => {
    let data = {
      loginId: state.loginId,
    };
    dispatch(otpActions.sendOtp(data));
    dispatch(loadingAction.commaonLoader(true));
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <SafeAreaView style={{flex: 0, backgroundColor: 'white'}} />
      <SafeAreaView style={{flex: 1}}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity style={{width: '20%', alignItems: 'center'}}>
            <Icon
              name="ios-arrow-round-back"
              size={50}
              color="#000"
              onPress={() => props.navigation.goBack()}
            />
          </TouchableOpacity>
          <View style={{width: '80%'}}>
            <Text style={{fontSize: hp('2.2%')}} numberOfLines={1}>
              Reset Password
            </Text>
          </View>
        </View>
        <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
          <View style={mainContainer}>
            <View style={commonScreenWidth}>
              <View style={logoContainer}>
                <Image source={Logo} style={logoStyle} resizeMode="contain" />
              </View>
              {state.showOldPass ? (
                <View style={{marginVertical: '4%'}}>
                  <View>
                    <Text style={InputTextLabel}>Old Password</Text>
                  </View>
                  <View>
                    <CustomTextInput
                      value={state.oldPassword}
                      changeText={e => setState({...state, oldPassword: e})}
                      placeholder="old Password"
                      secureText={true}
                    />
                  </View>
                </View>
              ) : null}
              <View style={{marginVertical: '4%'}}>
                <View>
                  <Text style={InputTextLabel}>New Password</Text>
                </View>
                <View>
                  <CustomTextInput
                    value={state.newPassword}
                    changeText={e => setState({...state, newPassword: e})}
                    placeholder="New Password"
                    secureText={true}
                  />
                </View>
              </View>
              <View>
                <View>
                  <Text style={InputTextLabel}>Confirm Password</Text>
                </View>
                <View>
                  <CustomTextInput
                    value={state.confirmPassword}
                    changeText={e => setState({...state, confirmPassword: e})}
                    placeholder="Confirm Password"
                    secureText={true}
                  />
                </View>
              </View>
              {state.previousComesScreen == 'login' ? (
                <View style={{marginVertical: '4%'}}>
                  <View>
                    <Text style={InputTextLabel}>OTP</Text>
                  </View>
                  <View>
                    <CustomTextInput
                      value={state.otp}
                      changeText={e => setState({...state, otp: e})}
                      placeholder="Enter otp"
                      keyboardType="numeric"
                      maxLength={6}
                    />
                  </View>
                  {/* <View
                    style={{marginVertical: hp('2%'), alignItems: 'flex-end'}}>
                    <TouchableOpacity onPress={resendOtp}>
                      <Text
                        style={{
                          color: commonColor.color,
                          textDecorationLine: 'underline',
                        }}>
                        Click here to Resend OTP on {state.mobile}
                      </Text>
                    </TouchableOpacity>
                  </View> */}
                </View>
              ) : null}

              <View style={{marginVertical: '6%'}}>
                <View>
                  <TouchableOpacity style={commonButton} onPress={submit}>
                    <Text style={commonButtonText}>submit</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
        {/* {isLoading ? <Loader /> : null} */}
      </SafeAreaView>
    </View>
  );
}

export default ResetPassword;
