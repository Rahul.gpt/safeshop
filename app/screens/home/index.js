import React, {useState, useEffect, Component} from 'react';
import Header from '../../components/header';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Image,
  BackHandler,
  Alert,
} from 'react-native';
import BannerBox from '../../assets/images/box.png';
import {commonScreenWidth, mainContainer} from '../../components/commonStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import SquareBox from '../../assets/images/square_box.png';
import NewDSSignup from '../../assets/images/new_direct_seller.png';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import DSData from '../DSdata';
import OrderProductIcon from '../../assets/images/order_product.png';
import ViewRetailIcon from '../../assets/images/view_retail.png';
import ViewDsIcon from '../../assets/images/view_ds_tax.png';
import ViewRetailBussinessIcon from '../../assets/images/view_retail_business.png';
import PayIcon from '../../assets/images/pay.png';
import CourierIcon from '../../assets/images/courier_detail.png';
import ChequeIconfrom from '../../assets/images/cheque.png';
import NewDSAIcon from '../../assets/images/new_dsa.png';
import WeeklyIcon from '../../assets/images/weekly.png';
import GSTIcong from '../../assets/images/gst.png';
import NextLevelIcon from '../../assets/images/new_level.png';
import * as loginActions from '../../actions/loginAction';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';

class DSHome extends Component {
  // let route = props.navigation;
  constructor(props) {
    console.warn('constructor');
    super(props);
    this.state = {
      logid: '',
      DSmenu: [
        {imageIcon: NewDSSignup, title: 'New Direct Seller Signup'},
        {imageIcon: OrderProductIcon, title: 'Order Your Product'},
        {imageIcon: ViewRetailIcon, title: 'View Retail Orders'},
        {imageIcon: ViewDsIcon, title: 'View DS Tax Invoice'},
        {
          imageIcon: ViewRetailBussinessIcon,
          title: 'View Retail Business Volume Details',
        },
        {imageIcon: PayIcon, title: 'Pay For Product By Credit/Debit Card'},
        {imageIcon: CourierIcon, title: 'Courier Details'},
        {imageIcon: ChequeIconfrom, title: 'Cheque This Week'},
        {imageIcon: NewDSAIcon, title: 'New DSA Approval List'},
        {imageIcon: WeeklyIcon, title: 'This Week Letter'},
        {imageIcon: GSTIcong, title: 'GST Invoice'},
        {imageIcon: NextLevelIcon, title: 'Promotion Status'},
        {imageIcon: ViewDsIcon, title: 'Declaration'},
        {imageIcon: WeeklyIcon, title: 'Next Level Status'},
      ],
    };
    this._unsubscribeSiFocus = this.props.navigation.addListener('focus', e => {
      console.warn('focus');
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    });
    this._unsubscribeSiBlur = this.props.navigation.addListener('blur', e => {
      console.warn('blurr');
      BackHandler.removeEventListener(
        'hardwareBackPress',
        this.handleBackButton,
      );
    });
  }

  handleBackButton = () => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        BackHandler.exitApp();
      }
    });

    return true;
  };

  async componentDidMount() {
    this._unsubscribeSiFocus = this.props.navigation.addListener('focus', e => {
      console.warn('focus');
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    });
    this._unsubscribeSiBlur = this.props.navigation.addListener('blur', e => {
      console.warn('blurr');
      BackHandler.removeEventListener(
        'hardwareBackPress',
        this.handleBackButton,
      );
    });
    console.warn('cdm');
    let screen = await AsyncStorage.getItem('NotificationScreen');
    if (JSON.parse(screen) !== null) {
      this.props.navigation.navigate('Notifications');
      AsyncStorage.removeItem('NotificationScreen');
    } else {
      AsyncStorage.removeItem('NotificationScreen');
    }
    let data = await AsyncStorage.getItem('userCredentials');
    let userData = JSON.parse(data);
    if (userData !== null) {
      this.setState({logid: userData.logid});
      this.props.userData(userData);
    }
  }

  componentWillUnmount() {
    this._unsubscribeSiFocus();
    this._unsubscribeSiBlur();
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handlenavigation = async index => {
    let route = this.props.navigation;
    if (index == 1) {
      route.navigate('Products');
    } else if (index == 0) {
      let data = await AsyncStorage.getItem('userCredentials');
      let userData = JSON.parse(data);
      if (userData !== null) {
        route.navigate('NewDSSignup', {
          logid: userData.logid,
          previousScreen: 'home',
          prelogId: '',
        });
      }
    } else if (index == 2 || index == 5) {
      route.navigate('RetailOrderList');
    } else if (index == 3) {
      route.navigate('RetailProductBought');
    } else if (index == 4) {
      route.navigate('RetailBusinessVolume');
    } else if (index == 7 || index == 10) {
      route.navigate('WeeklyPayment');
    } else if (index == 6) {
      route.navigate('CourierDetail');
    } else if (index == 9) {
      route.navigate('ChequeWeek');
    } else if (index == 8) {
      route.navigate('NewDSAList');
    } else if (index == 11) {
      route.navigate('PromotionStatus');
    } else if (index == 12) {
      route.navigate('Declaration');
    } else if (index == 13) {
      route.navigate('NextLevelStatus');
    }
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <View>
          <Header
            {...this.props}
            title="Home"
            showCart={false}
            showNotification={true}
            headeHeight={60}
          />
        </View>
        <View style={{flex: 1}}>
          <KeyboardAwareScrollView>
            <View style={mainContainer}>
              <View style={commonScreenWidth}>
                <DSData allDetails={true} />
                <View style={styles.menuMainContainer}>
                  {this.state.DSmenu.map((item, index) => {
                    return (
                      <TouchableOpacity
                        style={{width: '50%', height: hp('25%')}}
                        onPress={() => this.handlenavigation(index)}>
                        <ImageBackground
                          source={SquareBox}
                          style={styles.menuContainer}
                          resizeMode="contain">
                          <View style={styles.menuIconContainer} elevation={5}>
                            <Image
                              source={item.imageIcon}
                              resizeMode="contain"
                              style={styles.menuIcon}
                            />
                          </View>
                          <View style={styles.menuTextContainer}>
                            <Text
                              style={{
                                textAlign: 'center',
                                color: 'gray',
                                fontSize: hp('2%'),
                              }}
                              numberOfLines={2}>
                              {item.title}
                            </Text>
                          </View>
                        </ImageBackground>
                      </TouchableOpacity>
                    );
                  })}
                </View>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  rowContainer: {
    flexDirection: 'row',
  },
  rowLeftContainer: {
    width: '50%',
    alignItems: 'flex-start',
  },
  rowRightContainer: {
    width: '50%',
    alignItems: 'flex-end',
  },
  bannerStyle: {
    width: '100%',
    marginVertical: '4%',
  },
  rowLeftText: {
    fontSize: hp('1.7%'),
  },
  rowRightText: {
    color: 'white',
    textTransform: 'uppercase',
    fontSize: hp('1.7%'),
  },
  menuMainContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '100%',
    justifyContent: 'space-around',
  },
  menuContainer: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  menuIconContainer: {
    backgroundColor: 'white',
    width: 60,
    height: 60,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 0.2,
  },
  menuIcon: {
    width: '60%',
    height: '60%',
  },
  menuTextContainer: {
    width: '70%',
    paddingTop: '5%',
  },
});

export default connect(
  state => ({}),
  {...loginActions},
)(DSHome);
