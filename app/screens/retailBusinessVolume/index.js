import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Header from '../../components/header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import DSData from '../DSdata';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import * as allDSActions from '../../actions/allDSActions';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';
import {set} from 'react-native-reanimated';

const RetailBusinessVolume = props => {
  const [activeTab, setActiveTab] = useState('first');
  const [bussinessData, setBussinessData] = useState([]);
  const [loading, setLoading] = useState(true);

  const dispatch = useDispatch();
  const getBussinessVolumeData = useSelector(
    state => state.AllDSReducer.getBussinessVolumeData,
  );

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        dispatch(allDSActions.getBussinessVolume(token));
      }
    });
  }, []);

  useEffect(() => {
    if (Object.keys(getBussinessVolumeData).length) {
      if (
        getBussinessVolumeData.response &&
        getBussinessVolumeData.response.status == 200
      ) {
        setBussinessData(getBussinessVolumeData.data.query);
        setLoading(false);
      }
    }
  }, [getBussinessVolumeData]);

  const setBussineesType = type => {
    if (type == 'first') {
      setActiveTab('first');
    } else {
      setActiveTab('re');
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <View>
        <Header
          {...props}
          back={true}
          showCart={true}
          showNotification={false}
          title="Retail Business Volume Details"
          showNotification={true}
          showCart={false}
        />
      </View>
      <View style={mainContainer}>
        <View style={commonScreenWidth}>
          <DSData allDetails={false} />
        </View>
        <View style={{flexDirection: 'row', marginHorizontal: wp('2%')}}>
          <TouchableOpacity
            style={{
              width: '50%',
              alignItems: 'center',
              paddingVertical: '3%',
              backgroundColor:
                activeTab == 'first' ? commonColor.color : '#e2e2e2',
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
            }}
            onPress={() => setBussineesType('first')}>
            <Text
              style={{
                color: activeTab == 'first' ? 'white' : 'black',
                fontWeight: 'bold',
              }}>
              First Purchase
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '50%',
              alignItems: 'center',
              paddingVertical: '3%',
              backgroundColor:
                activeTab == 're' ? commonColor.color : '#e3e3e3',
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
            }}
            onPress={() => setBussineesType('re')}>
            <Text
              style={{
                color: activeTab == 're' ? 'white' : 'black',
                fontWeight: 'bold',
              }}>
              Re-Purchase
            </Text>
          </TouchableOpacity>
        </View>
        <ScrollView contentContainerStyle={{paddingBottom: hp('25%')}}>
          {loading ? (
            <View style={{height: 200, justifyContent: 'center'}}>
              <ActivityIndicator size="large" />
            </View>
          ) : (
            <View
              style={{
                backgroundColor: 'white',
                elevation: 2,
                marginHorizontal: wp('2%'),
              }}>
              <View
                style={{
                  borderWidth: 1,
                  borderColor: '#e2e2e2',
                  marginVertical: '5%',
                  marginHorizontal: wp('2%'),
                }}>
                <View style={{flexDirection: 'row', paddingVertical: '5%'}}>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      borderRightWidth: 1,
                      borderRightColor: '#e2e2e2',
                    }}>
                    <View style={styles.columnHeadingContainer}>
                      <Text
                        style={[
                          styles.commFontSize,
                          {fontWeight: 'bold', paddingBottom: hp('4%')},
                        ]}>
                        SALES STATUS
                      </Text>
                    </View>
                    <View style={{paddingHorizontal: '1%'}}>
                      <Text style={styles.commFontSize}>
                        This Week Business Volume
                      </Text>
                    </View>
                  </View>
                  <View style={{width: '25%', alignItems: 'center'}}>
                    <View style={styles.columnHeadingContainer}>
                      <Text style={[styles.commFontSize, {color: '#2a74bc'}]}>
                        Blue Group
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Left</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {activeTab == 'first'
                              ? Number(bussinessData[0].cntmonpvl) * 200
                              : Number(bussinessData[0].cntmonrpvl) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Right</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {activeTab == 'first'
                              ? Number(bussinessData[0].cntmonpvr) * 200
                              : Number(bussinessData[0].cntmonrpvr) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      borderLeftWidth: 1,
                      borderRightWidth: 1,
                      borderColor: '#e2e2e2',
                    }}>
                    <View style={styles.columnHeadingContainer}>
                      <Text style={[styles.commFontSize, {color: '#df7728'}]}>
                        Amber Group
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Left</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {activeTab == 'first'
                              ? Number(bussinessData[0].cntmonpvl2) * 200
                              : Number(bussinessData[0].cntmonrpvl2) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Right</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {activeTab == 'first'
                              ? Number(bussinessData[0].cntmonpvr2) * 200
                              : Number(bussinessData[0].cntmonrpvr2) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                  <View style={{width: '25%', alignItems: 'center'}}>
                    <View style={styles.columnHeadingContainer}>
                      <Text style={[styles.commFontSize, {color: '#a732ce'}]}>
                        Purple Group
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Left</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {activeTab == 'first'
                              ? Number(bussinessData[0].cntmonpvl3) * 200
                              : Number(bussinessData[0].cntmonpvr3) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View style={styles.columnHeadingContainer}>
                          <Text style={styles.commFontSize}>Right</Text>
                        </View>
                        <View>
                          <Text style={styles.commFontSize}>
                            {activeTab == 'first'
                              ? Number(bussinessData[0].cntmonpvr3) * 200
                              : Number(bussinessData[0].cntmonrpvl3) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    borderTopWidth: 1,
                    borderTopColor: '#e2e2e2',
                    paddingVertical: '5%',
                  }}>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      borderRightWidth: 1,
                      borderRightColor: '#e2e2e2',
                      paddingHorizontal: '1%',
                    }}>
                    <Text style={styles.commFontSize}>
                      Balance Business Volume Before Current Week
                    </Text>
                  </View>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {activeTab == 'first'
                              ? Number(bussinessData[0].ball) * 200
                              : Number(bussinessData[0].balrl) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {activeTab == 'first'
                              ? Number(bussinessData[0].balr) * 200
                              : Number(bussinessData[0].balrr) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      borderLeftWidth: 1,
                      borderRightWidth: 1,
                      borderColor: '#e2e2e2',
                      justifyContent: 'center',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {activeTab == 'first'
                              ? Number(bussinessData[0].bal2l) * 200
                              : Number(bussinessData[0].balr2l) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {activeTab == 'first'
                              ? Number(bussinessData[0].bal2r) * 200
                              : Number(bussinessData[0].balr2r) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      width: '25%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {activeTab == 'first'
                              ? Number(bussinessData[0].bal3l) * 200
                              : Number(bussinessData[0].balr3l) * 200}
                          </Text>
                        </View>
                      </View>
                      <View style={{width: '50%', alignItems: 'center'}}>
                        <View>
                          <Text style={styles.commFontSize}>
                            {activeTab == 'first'
                              ? Number(bussinessData[0].bal3r) * 200
                              : Number(bussinessData[0].balr3r) * 200}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          )}
          <View style={{marginVertical: hp('3%'), marginHorizontal: wp('2%')}}>
            <Text style={{fontSize: hp('1.7%')}}>
              Safe and Secure Online Marketing Pvt. Ltd. has no offices, Branch
              Offices or representative offices, anywhere in India apart from
              the address mentioned. Please send your payments by Demand Draft
              Payable to Safe and Secure Online Marketing Pvt. Ltd. (Payable in
              New Delhi) only to this address. you are requested not to handover
              payments or drafts to anyone but to send them directly by courier
              to the Company. The Company does not take any responsibility for
              payments handed over to anybody in person.
            </Text>
          </View>
          <View style={{marginVertical: hp('3%'), marginHorizontal: wp('2%')}}>
            <Text style={styles.addressStyle}>
              Safe and Secure Online Marketing Pvt. Ltd.
            </Text>
            <Text style={styles.addressStyle}>
              A-3/24 Janakpuri, New Delhi - 110 058
            </Text>
            <Text style={styles.addressStyle}>
              Ph. No.: +91-11-4157 4142 / 4143 / 4145 / 4146
            </Text>
            <Text style={styles.addressStyle}>
              email : support@safeshopindia.com web : www.safeshopindia.com
            </Text>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default RetailBusinessVolume;

const styles = StyleSheet.create({
  boxContainer: {
    backgroundColor: 'white',
    elevation: 2,
    width: wp('95%'),
    paddingHorizontal: '2%',
    paddingVertical: '5%',
  },
  commFontSize: {
    fontSize: 12,
    textAlign: 'center',
  },
  columnHeadingContainer: {
    paddingTop: '5%',
    paddingBottom: hp('2%'),
  },
  addressStyle: {
    fontSize: hp('1.7%'),
    color: commonColor.color,
    fontWeight: 'bold',
  },
});
