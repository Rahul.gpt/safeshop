import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  InputTextLabel,
  commonColor,
  commonButton,
  commonButtonText,
  logoContainer,
  commonScreenWidth,
  mainContainer,
  logoStyle,
} from '../../components/commonStyle';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {showMessage} from 'react-native-flash-message';
import HTML from 'react-native-render-html';
import {IGNORED_TAGS} from 'react-native-render-html/src/HTMLUtils';
import * as forgotPasswordAction from '../../actions/forgotPasswordAction';

const Declaration = props => {
  const [bool, setBool] = useState(true);
  const [data, setData] = useState('');
  const [userToken, setToken] = useState('');

  const dispatch = useDispatch();
  const declData = useSelector(state => state.ForgotPasswordReducer.declData);

  useEffect(() => {
    if (props.route && props.route.params && props.route.params.userToken) {
      dispatch(
        forgotPasswordAction.getDeclaration(props.route.params.userToken),
      );
      console.warn(props.route.params.userToken);
      setToken(props.route.params.userToken);
    }
  }, []);

  useEffect(() => {
    console.warn('getAgreeData', declData);
    if (Object.keys(declData).length) {
      setData(declData);
      setBool(false);
    }
  }, [declData]);

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <SafeAreaView style={{flex: 0, backgroundColor: 'white'}} />
      <SafeAreaView style={{flex: 1}}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity style={{width: '20%', alignItems: 'center'}}>
            <Icon
              name="ios-arrow-round-back"
              size={50}
              color="#000"
              onPress={() => props.navigation.goBack()}
            />
          </TouchableOpacity>
          <View style={{width: '80%'}}>
            <Text style={{fontSize: hp('2.2%')}} numberOfLines={1}>
              Declaration
            </Text>
          </View>
        </View>
        <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
          {bool ? (
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <ActivityIndicator color={commonColor.color} />
            </View>
          ) : (
            <View style={mainContainer}>
              <View style={commonScreenWidth}>
                <HTML
                  html={data}
                  imagesMaxWidth={Dimensions.get('window').width}
                  ignoredStyles={['line-height']}
                  // onLinkPress={url => console.warn('clicked link: ', url)}
                />
                <View>
                  <Text style={{fontWeight: 'bold'}}>
                    I hearby agree to the agreement by clicking on the signature
                    button
                  </Text>
                </View>
                <View style={{alignItems: 'center', marginVertical: hp('4%')}}>
                  <TouchableOpacity
                    style={{
                      width: '50%',
                      borderRadius: 30,
                      alignItems: 'center',
                      backgroundColor: commonColor.color,
                      paddingVertical: '3%',
                    }}
                    onPress={() =>
                      props.navigation.navigate('TnC', {
                        userToken: userToken,
                      })
                    }>
                    <Text style={{color: 'white', textTransform: 'uppercase'}}>
                      signature
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </View>
  );
};

export default Declaration;
