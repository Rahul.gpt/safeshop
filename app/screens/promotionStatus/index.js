import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Header from '../../components/header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import DSData from '../DSdata';
import {
  commonScreenWidth,
  mainContainer,
  commonColor,
} from '../../components/commonStyle';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import * as allDSActions from '../../actions/allDSActions';
import * as loadingAction from '../../actions/commonLoaderAction';
import Loader from '../../components/loader';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';

const PromotionStatus = props => {
  const [promotionData, setPromotionData] = useState([]);
  const [loading, setLoading] = useState(true);

  const dispatch = useDispatch();
  const getpromotion = useSelector(state => state.AllDSReducer.getpromotion);

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token !== null) {
        dispatch(allDSActions.getPromotionData(token));
      }
    });
  }, []);

  useEffect(() => {
    if (Object.keys(getpromotion).length) {
      if (getpromotion.response && getpromotion.response.status == 200) {
        setPromotionData(getpromotion.data.query);
        setLoading(false);
      }
    }
  }, [getpromotion]);
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <View>
        <Header
          {...props}
          back={true}
          showNotification={false}
          title="Promotion Status"
          showNotification={true}
          showCart={false}
        />
      </View>
      <View style={mainContainer}>
        <View style={commonScreenWidth}>
          <DSData allDetails={false} />
        </View>
        <View>
          <Text style={[styles.commFontSize, {fontWeight: 'bold'}]}>
            Current Promotion BV's in each group
          </Text>
        </View>
        <View>
          <Text style={[styles.commFontSize, {fontWeight: 'bold'}]}>
            Your Current DS Level for Promotion is Crown Ambassador
          </Text>
        </View>
        {loading ? (
          <View style={{height: 200, justifyContent: 'center'}}>
            <ActivityIndicator size="large" />
          </View>
        ) : promotionData.length ? (
          <View
            style={{
              backgroundColor: 'white',
              elevation: 2,
              marginHorizontal: wp('2%'),
            }}>
            <View
              style={{
                borderWidth: 1,
                borderColor: '#e2e2e2',
                marginVertical: '5%',
                marginHorizontal: wp('2%'),
              }}>
              <View style={{flexDirection: 'row', paddingVertical: '5%'}}>
                <View
                  style={{
                    width: '25%',
                    alignItems: 'center',
                    borderRightWidth: 1,
                    borderRightColor: '#e2e2e2',
                  }}>
                  <View style={styles.columnHeadingContainer}>
                    <Text
                      style={[
                        styles.commFontSize,
                        {fontWeight: 'bold', paddingBottom: hp('4%')},
                      ]}>
                      SALES STATUS
                    </Text>
                  </View>
                  <View style={{paddingHorizontal: '1%'}}>
                    <Text style={styles.commFontSize}>Current Promo Sales</Text>
                  </View>
                </View>
                <View style={{width: '25%', alignItems: 'center'}}>
                  <View style={styles.columnHeadingContainer}>
                    <Text style={[styles.commFontSize, {color: '#2a74bc'}]}>
                      Blue Group
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View
                      style={{
                        width: '50%',
                        alignItems: 'center',
                        borderRightColor: '#e2e2e2',
                        borderRightWidth: 1,
                      }}>
                      <View style={styles.columnHeadingContainer}>
                        <Text style={styles.commFontSize}>Left</Text>
                      </View>
                      <View>
                        <Text style={styles.commFontSize}>
                          {Number(promotionData[0].cntschl) * 200}
                        </Text>
                      </View>
                    </View>
                    <View style={{width: '50%', alignItems: 'center'}}>
                      <View style={styles.columnHeadingContainer}>
                        <Text style={styles.commFontSize}>Right</Text>
                      </View>
                      <View>
                        <Text style={styles.commFontSize}>
                          {Number(promotionData[0].cntschr) * 200}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    width: '25%',
                    alignItems: 'center',
                    borderLeftWidth: 1,
                    borderRightWidth: 1,
                    borderColor: '#e2e2e2',
                  }}>
                  <View style={styles.columnHeadingContainer}>
                    <Text style={[styles.commFontSize, {color: '#df7728'}]}>
                      Amber Group
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View
                      style={{
                        width: '50%',
                        alignItems: 'center',
                        borderRightColor: '#e2e2e2',
                        borderRightWidth: 1,
                      }}>
                      <View style={styles.columnHeadingContainer}>
                        <Text style={styles.commFontSize}>Left</Text>
                      </View>
                      <View>
                        <Text style={styles.commFontSize}>
                          {Number(promotionData[0].cntsch2l) * 200}
                        </Text>
                      </View>
                    </View>
                    <View style={{width: '50%', alignItems: 'center'}}>
                      <View style={styles.columnHeadingContainer}>
                        <Text style={styles.commFontSize}>Right</Text>
                      </View>
                      <View>
                        <Text style={styles.commFontSize}>
                          {Number(promotionData[0].cntsch2r) * 200}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
                <View style={{width: '25%', alignItems: 'center'}}>
                  <View style={styles.columnHeadingContainer}>
                    <Text style={[styles.commFontSize, {color: '#a732ce'}]}>
                      Purple Group
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View
                      style={{
                        width: '50%',
                        alignItems: 'center',
                        borderRightColor: '#e2e2e2',
                        borderRightWidth: 1,
                      }}>
                      <View style={styles.columnHeadingContainer}>
                        <Text style={styles.commFontSize}>Left</Text>
                      </View>
                      <View>
                        <Text style={styles.commFontSize}>
                          {Number(promotionData[0].cntsch3l * 200)}
                        </Text>
                      </View>
                    </View>
                    <View style={{width: '50%', alignItems: 'center'}}>
                      <View style={styles.columnHeadingContainer}>
                        <Text style={styles.commFontSize}>Right</Text>
                      </View>
                      <View>
                        <Text style={styles.commFontSize}>
                          {Number(promotionData[0].cntsch3r) * 200}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  borderTopWidth: 1,
                  borderTopColor: '#e2e2e2',
                  paddingVertical: '5%',
                }}>
                <View
                  style={{
                    width: '25%',
                    alignItems: 'center',
                    borderRightWidth: 1,
                    borderRightColor: '#e2e2e2',
                    paddingHorizontal: '1%',
                  }}>
                  <Text style={styles.commFontSize}>
                    Total BV matches in 3 groups
                  </Text>
                </View>
                <View
                  style={{
                    width: '37%',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <View style={styles.columnHeadingContainer}>
                    <Text style={styles.commFontSize}>
                      Left ( Blue + Amber + Purple )
                    </Text>
                  </View>
                  <View style={{alignItems: 'center'}}>
                    <View>
                      <Text style={styles.commFontSize}>
                        {Number(promotionData[0].cntschl) * 200 +
                          Number(promotionData[0].cntsch2l) * 200 +
                          Number(promotionData[0].cntsch3l) * 200}
                      </Text>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    width: '37%',
                    alignItems: 'center',
                    borderLeftWidth: 1,
                    borderRightWidth: 1,
                    borderColor: '#e2e2e2',
                    justifyContent: 'center',
                  }}>
                  <View style={styles.columnHeadingContainer}>
                    <Text style={styles.commFontSize}>
                      Right ( Blue + Amber + Purple )
                    </Text>
                  </View>
                  <View style={{alignItems: 'center'}}>
                    <View>
                      <Text style={styles.commFontSize}>
                        {Number(promotionData[0].cntschr) * 200 +
                          Number(promotionData[0].cntsch2r) * 200 +
                          Number(promotionData[0].cntsch3r) * 200}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        ) : (
          <View>
            <Text>No Data Found</Text>
          </View>
        )}
      </View>
    </View>
  );
};

export default PromotionStatus;

const styles = StyleSheet.create({
  boxContainer: {
    backgroundColor: 'white',
    elevation: 2,
    width: wp('95%'),
    paddingHorizontal: '2%',
    paddingVertical: '5%',
  },
  commFontSize: {
    fontSize: 12,
    textAlign: 'center',
  },
  columnHeadingContainer: {
    paddingTop: '5%',
    paddingBottom: hp('2%'),
  },
});
