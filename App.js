import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {NavigationContainer} from '@react-navigation/native';
import MainNav from './app/navigations';
import store from './app/store/index';
import {Provider} from 'react-redux';
import FlashMessage, {showMessage} from 'react-native-flash-message';
import NetInfo from '@react-native-community/netinfo';
import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-community/async-storage';

const App = props => {
  useEffect(() => {
    SplashScreen.hide();
    // messaging()
    //   .getInitialNotification()
    //   .then(async remoteMessage => {
    //     if (remoteMessage) {
    //       await AsyncStorage.setItem(
    //         'NotificationScreen',
    //         JSON.stringify(remoteMessage.data),
    //       );
    //       console.warn(
    //         'Notification caused app to open from quit state:',
    //         remoteMessage.notification,
    //       );
    //     }
    //   });
    // messaging().setBackgroundMessageHandler(async remoteMessage => {
    //   console.warn('Message handled in the background!', remoteMessage.data);
    //   alert('ergerg');
    //   // setScreenData(remoteMessage.data);
    //   await AsyncStorage.setItem(
    //     'NotificationScreen',
    //     JSON.stringify(remoteMessage.data),
    //   );
    // });

    // messaging().onMessage(async remoteMessage => {
    //   alert('ferferf');
    // });

    // messaging().setBackgroundMessageHandler(async remoteMessage => {
    //   console.log('Message handled in the background!', remoteMessage);
    // });
    requestUserPermission();
    NetInfo.fetch().then(response => {
      if (!response.isConnected) {
        showMessage({
          message: 'Internet connection is lost, Please check',
          type: 'success',
        });
      }
    });
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      console.log('FCM Message Data:', remoteMessage.data);
    });

    return unsubscribe;
  }, []);

  messaging().setBackgroundMessageHandler(async remoteMessage => {
    console.warn('Message handled in the background!', remoteMessage.data);
    alert('ergerg');
    // setScreenData(remoteMessage.data);
    await AsyncStorage.setItem(
      'NotificationScreen',
      JSON.stringify(remoteMessage.data),
    );
  });

  async function requestUserPermission() {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      getFcmToken();
    }
  }

  const getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      console.log(fcmToken);
      await AsyncStorage.setItem('fcmtoken', fcmToken);
      console.log('Your Firebase Token is:', fcmToken);
    } else {
      console.log('Failed', 'No token received');
    }
  };

  return (
    <NavigationContainer>
      <Provider store={store}>
        <MainNav />
        <FlashMessage position="bottom" />
      </Provider>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({});

export default App;
